package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCategoryDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseReportDBManager;

import java.util.Calendar;
import java.util.Date;

public class AddBusinessCategoryActivity extends Activity {

    public BusinessCategoryDBManager businessCategoryDBManager;
    private BusinessCategory.FormType formType;
    private CommandBusinessCategory commandBusinessCategory; // Interface d'action des commandes pour cette activity

    private void init() throws Exception {
        businessCategoryDBManager = BusinessCategoryDBManager.getInstance(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_business_category);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_business_category_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_save_businessCategory:
                try{
                    onClickButtonSave();
                }
                catch(Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickButtonSave() throws Exception {
        EditText txtDescriptionCategorie = (EditText) findViewById(R.id.txtDescriptionCategorie);
        String description = txtDescriptionCategorie.getText().toString();

        /* TEst de champ vide (ici description) */
        if(description.equals("")){
//            Log.d("AddExpenseCat","onClickButtonSave -> Description must be filled");
            Toast.makeText(getApplicationContext(), "Le champ Description est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }

        String strId = "";
        BusinessCategory businessCategory = new BusinessCategory(0,strId, "codeTest", description, formType.toString());
        BusinessCategoryAdapter.getInstance(this).add(businessCategory,false,true);
        this.finish();
    }

    public void onRadioButtonTypeClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.rdCategoryMontant:
                if (checked)
                    formType = BusinessCategory.FormType.amount;
                    break;
            case R.id.rdCategoryFrais:
                if (checked)
                    formType = BusinessCategory.FormType.mileageExpense;
                    break;
        }
    }

}

