package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCustomerAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCustomerDBManager;

public class AddBusinessCustomerActivity extends Activity implements View.OnClickListener {

    public BusinessCustomerDBManager BusinessCustomerDBManager;
    private CommandBusinessCustomer commandBusinessCustomer; // Interface d'action des commandes pour cette activity

    private void init() throws Exception {
        BusinessCustomerDBManager = BusinessCustomerDBManager.getInstance(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_business_customer);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_business_customer_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_save_businessCustomer:
                try{
                    onClickButtonSave();
                }
                catch(Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickButtonSave() throws Exception {
        EditText txtDescriptionCategorie = (EditText) findViewById(R.id.txtDescriptionClient);
        EditText txtCodeClient = (EditText) findViewById(R.id.txtCodeClient);

        String description = txtDescriptionCategorie.getText().toString();
        String codeClient = txtCodeClient.getText().toString();


        /* TEst de champ vide (ici description) */
        if(description.equals("")){
//            Log.d("AddExpenseCust", "onClickButtonSave -> Description must be filled");
            Toast.makeText(getApplicationContext(), "Le champ Description est obligatoire", Toast.LENGTH_LONG).show();

        }

        String strId = "";
        BusinessCustomer BusinessCustomer = new BusinessCustomer(0,strId, codeClient, description);
        BusinessCustomerAdapter.getInstance(this).add(BusinessCustomer, false, true);

        this.finish();
    }

}

