package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.ActivityWithCustomerSelection;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessProjectDBManager;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomCheckBox;

public class AddBusinessProjectActivity extends ActivityWithCustomerSelection implements View.OnClickListener {

    public com.esgi.breakingdev.notesdefraisandroid.sql.BusinessProjectDBManager businessProjectDBManager;
    private CommandBusinessProject commandBusinessProject; // Interface d'action des commandes pour cette activity

    private BusinessCustomer customerSelected = null;
    private boolean rebilling;
    private boolean internalProject;

    @Override
    public void setSelectedCustomer(BusinessCustomer customer) {
        customerSelected = customer;
    }

    @Override
    public void setInternalItem(boolean internal) {
        internalProject = internal;
    }

    private void init() throws Exception {
        businessProjectDBManager = BusinessProjectDBManager.getInstance(this);

        // Chargement de la liste des clients
        int[] itemsToHide = new int[] { R.id.textViewCustomer};
        CustomCheckBox.setCheckBoxWithCustomerSpinner(this,this,R.id.lstClients,R.id.checkBoxInternal,itemsToHide);
        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBoxRebilling);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rebilling = isChecked;
            }
        });
        checkBox.setChecked(false);
        checkBox.setChecked(true);
     }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_business_project);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_business_project_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_save_businessProject:
                try{
                    onClickButtonSave();
                }
                catch(Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickButtonSave() throws Exception {
        EditText txtDescriptionProjet = (EditText) findViewById(R.id.txtDescriptionProjet);

        String description = txtDescriptionProjet.getText().toString();
        long customerId = 0;
        String customerStringId = "";
        if(!internalProject){
            customerId = customerSelected.getBusinessCustomerId();
            customerStringId = customerSelected.getStringId();
        }

        /* Test de champ incorrect (ici description) */
        if(description.equals("")){
//            Log.d("AddExpenseProj", "onClickButtonSave -> Description must be filled");
            Toast.makeText(getApplicationContext(), "Le champ Description est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }

        String strId = "";
        BusinessProject businessProject = new BusinessProject(0,strId, "codeTest", description, rebilling, customerId,customerStringId);
        BusinessProjectAdapter.getInstance(this).add(businessProject,false,true);
        this.finish();
    }


}

