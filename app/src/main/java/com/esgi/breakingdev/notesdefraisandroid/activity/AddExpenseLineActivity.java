package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.ActivityWithCategorySelection;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.IActivityWithCustomerSelection;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.IActivityWithProjectSelection;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.AbstractAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.EventInfo;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.SimpleAmount;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseLineServices;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseReportServices;
import com.esgi.breakingdev.notesdefraisandroid.sql.EventInfoDBManager;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomCheckBox;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomSpinner;
import com.esgi.breakingdev.notesdefraisandroid.tools.PhotoHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddExpenseLineActivity extends ActivityWithCategorySelection implements IActivityWithCustomerSelection,IActivityWithProjectSelection {

    private static final int CAMERA_REQUEST = 1888;
    private ExpenseReport expenseReport;                // Note de frais concernée
    private BusinessCategory categorySelected = null;  // Catégorie sélectionnée
    private List<Object> listProjectsOfCustomer;
    private BusinessCustomer customerSelected = null;
    private boolean internalLine,isImageFitToScreen;
    private Bitmap photo = null;
    private Uri photoUri;
    private int lineNumber = 0;

    @Override
    public void setSelectedCustomer(BusinessCustomer customer) {
        customerSelected = customer;
        Log.d("AddExpenseLineActivity", "setSelectedCustomer : " + customer.getDescription());

    }

    @Override
    public void setInternalItem(boolean internal) {
        internalLine = internal;
    }

    @Override
    public void setProjectsOfCustomer(List<Object> listProjects) {
        listProjectsOfCustomer = listProjects;
    }

    @Override
    public List<Object> getCustomerProjects() {
        return listProjectsOfCustomer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense_line);
        init();
    }

    private void init() {
        Intent i = getIntent();
        expenseReport = (ExpenseReport) i.getSerializableExtra("expenseReport");
        lineNumber = i.getIntExtra("lineNumber", 0);

        Log.d("AddExpenseListAct", "ActivityAdd for " + expenseReport.getDescription());

        String descriptionNDF = expenseReport.getDescription();
        EditText txtNoteDeFrais = (EditText) findViewById(R.id.txtNoteDeFrais);
        txtNoteDeFrais.setText(descriptionNDF);
        txtNoteDeFrais.setKeyListener(null);

        Date dateDebutNDF = expenseReport.getBeginDate();
        Date dateFinNDF = expenseReport.getEndDate();
        DatePicker dpickerDateLigneFrais = (DatePicker) findViewById(R.id.dpickerDateLigneFrais);

        Calendar cal = Calendar.getInstance();

        cal.setTime(dateDebutNDF);
        long timeDebutNDF = cal.getTimeInMillis();
        dpickerDateLigneFrais.setMinDate(timeDebutNDF);
        cal.setTime(dateFinNDF);
        long timeFinNDF = cal.getTimeInMillis();
        dpickerDateLigneFrais.setMaxDate(timeFinNDF);

        // LISTE DES CATEGORIES DE DEPENSES
        int[] gridTextCellsId = new int[] { R.id.gridTextViewElement1,R.id.gridTextViewElement2 };
        int[] gridEditTextCellsId = new int[] { R.id.gridEditTextElement1,R.id.gridEditTextElement2 };
        CustomSpinner.setSpinnerForBusinessCategories(R.id.lstCategoriesDepenses, this, gridTextCellsId, gridEditTextCellsId);

        // LISTE DES CLIENTS
        int[] itemsToHide = { R.id.txtViewLineCustomer,R.id.textViewp1,R.id.lstProjects, R.id.switchRefacturable};
        CustomCheckBox.setCheckBoxWithCustomerSpinner(this, this, R.id.lstLineCustomers, R.id.checkBoxInternalExpense, itemsToHide);
        // LISTE DES PROJETS
        CustomSpinner.setProjectSpinnerAccordingToCustomerSelected(this, this,this, R.id.lstLineCustomers, R.id.lstProjects);
        CustomSpinner.setRebillingSwitchAccorgindToProjectSelected(this, this, R.id.lstProjects, R.id.switchRefacturable);


        // JUSTIFICATIF - PHOTOS
        Button photoButton = (Button) this.findViewById(R.id.btnSelectPhoto);

        final Context context = this;
        final Activity activity = this;
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                File imagesFolder = new File(getExternalCacheDir(), "MyImages");
//                File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyImages");
                imagesFolder.mkdirs();
                File mypath = new File(imagesFolder,"profile.jpg");
                photoUri = Uri.fromFile(mypath);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); // set the image file name
                activity.startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });


        // Full Screen when clicking on photo
        final ImageView imgJustificatif = (ImageView) findViewById(R.id.imgJustificatifLigne);
        imgJustificatif.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(activity, DisplayFullScreenImageActivity.class);
                Uri bitmapUri = PhotoHandler.writeBitmapToFile(context, photo, "fullscreenImg.jpg");
                if (bitmapUri == null) return false;
                intent.putExtra("imageUri", bitmapUri);
                activity.startActivity(intent);
                return false;
            }
        });

        final Button removeButton = (Button) this.findViewById(R.id.btnRemovePhoto);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photo = null;
                imgJustificatif.setImageBitmap(null);
                imgJustificatif.setVisibility(View.GONE);
                removeButton.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void setCategorySelected(BusinessCategory businessCategory) {
        categorySelected = businessCategory;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
                photo = PhotoHandler.getBitmapFromUri(photoUri);
                PhotoHandler.deleteFileFromUri(photoUri);
                ImageView imgJustificatifLigne = (ImageView) findViewById(R.id.imgJustificatifLigne);
                Button buttonRemove = (Button)findViewById(R.id.btnRemovePhoto);
                imgJustificatifLigne.setVisibility(View.VISIBLE);
                buttonRemove.setVisibility(View.VISIBLE);
                photo = PhotoHandler.compressBitmap(photo,50);
                imgJustificatifLigne.setImageBitmap(photo);

            // Encodage
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
//            byte[] byteArray = stream.toByteArray();
//            String strBase64= Base64.encodeToString(byteArray, 0);

            // Decodage
//            byte[] decodedString = Base64.decode(strBase64, Base64.DEFAULT);
//            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//            imgJustificatifLigne.setImageBitmap(decodedByte);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_expense_line_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_save_expenseLine:
                try {
                    onClickButtonSave();
                } catch (Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickButtonSave() throws Exception {
        Log.d("AddExpenseListAct", "onClickButtonSave : OK");

        DatePicker datePicker = (DatePicker) findViewById(R.id.dpickerDateLigneFrais);
        Date date = BusinessExpensesApplication.getDateFromDatePicker(datePicker);


        long expenseReportId = expenseReport.getExpenseReportId();
        String expenseReportStrId = expenseReport.getStringId();
        long businessCategoryId = categorySelected.getBusinessCategoryId();
        String businessCategoryStringId = categorySelected.getStringId();

        EditText commentTxtView = (EditText) findViewById(R.id.txtNomLigneFrais);
        String comment = commentTxtView.getText().toString();

        if(comment.isEmpty()){
//            Log.d("AddExpenseLineActiv", "onClickButtonSave: comment field is empty !");
            Toast.makeText(getApplicationContext(), "Le champ Motif est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }

        AbstractAmount amount;

        EditText textTradingName = (EditText) findViewById(R.id.textTradingName);
        String tradingName = textTradingName.getText().toString();
        EditText gridEditTextElement2,gridEditTextElement1;

        Log.d("AddExpenseLineActiv", "onClickButtonSave: 1 !");
        // GET CUSTOMER ID
        long customerId = internalLine ? 0 : customerSelected.getBusinessCustomerId();
        String customerStringId = internalLine ? "" : customerSelected.getStringId();
        Log.d("AddExpenseLineActiv", "onClickButtonSave: 2 !");

        // GET PROJECT ID
        long projectId = 0;
        String projectStrId = "";
        Spinner lstProjects = (Spinner) findViewById(R.id.lstProjects);
        if(!internalLine && lstProjects.getSelectedItemPosition() > 0) {
            BusinessProject projectSelected = (BusinessProject) (listProjectsOfCustomer.get(lstProjects.getSelectedItemPosition() - 1));
            projectId = projectSelected.getBusinessProjectId();
            projectStrId = projectSelected.getStringId();
        }
        Log.d("AddExpenseLineActiv", "onClickButtonSave: 3 !");


        Switch switchRebilling = (Switch) findViewById(R.id.switchRefacturable);
        boolean rebilling = switchRebilling.isChecked();
        Log.d("AddExpenseLineActiv", "onClickButtonSave: 4 !");

        String proofRef = "";
        boolean canHaveJustificatif = true;
        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();

        switch(categorySelected.getFormType())
        {
            case amount:
                gridEditTextElement1 = (EditText) findViewById(R.id.gridEditTextElement1);
                gridEditTextElement2 = (EditText) findViewById(R.id.gridEditTextElement2);

                float ttc = 0, tva = 0;
                String ttcTxt = gridEditTextElement2.getText().toString();
                if(ttcTxt.isEmpty()) {
//                    Log.d("AddExpenseLineActiv", "onClickButtonSave: ttc field is empty !");
                    Toast.makeText(getApplicationContext(), "Le champ TTC est obligatoire", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    ttc = Float.parseFloat(ttcTxt);
                } catch (NumberFormatException e) {
//                    Log.d("AddExpenseLineActiv", "onClickButtonSave: ttc is not a Number !");
                    Toast.makeText(getApplicationContext(), "Le champ TTC doit être numérique", Toast.LENGTH_LONG).show();
                    return;
                }

                String tvaTxt = gridEditTextElement1.getText().toString();
                if(!tvaTxt.isEmpty()) {
                    try {
                        tva = Float.parseFloat(tvaTxt);
                    } catch (NumberFormatException e) {
//                        Log.d("AddExpenseLineActiv","onClickButtonSave: tva is not a Number !");
                        Toast.makeText(getApplicationContext(), "Le champ TVA doit être numérique", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                amount = new SimpleAmount(0,0,expenseReportId,ttc,tva);
                /* Récupération du justificatif */
                EditText editTextRefProof = (EditText) findViewById(R.id.editTextProofRef);
                proofRef = editTextRefProof.getText().toString();
                if(!app.isStandAlone() && proofRef.isEmpty()){
                    Toast.makeText(getApplicationContext(), "La référence est obligatoire !", Toast.LENGTH_LONG).show();
                    return;
                }
                break;
            case mileageExpense:
                canHaveJustificatif = false;
                gridEditTextElement1 = (EditText) findViewById(R.id.gridEditTextElement1);
                gridEditTextElement2 = (EditText) findViewById(R.id.gridEditTextElement2);
                int distance = 0, hp = 0;

                String distanceTxt = gridEditTextElement1.getText().toString();
                if(distanceTxt.isEmpty()){
//                    Log.d("AddExpenseLineActiv", "onClickButtonSave: distance field is empty !");
                    Toast.makeText(getApplicationContext(), "Le champ Distance est obligatoire", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    distance = Integer.parseInt(distanceTxt);
                }catch(NumberFormatException e){
                    Log.d("AddExpenseLineActiv", "onClickButtonSave: distance is not a Number !");
                    return;
                }

                String hpTxt = gridEditTextElement2.getText().toString();
                if(hpTxt.isEmpty()){
//                    Log.d("AddExpenseLineActiv", "onClickButtonSave: horse Power field is empty !");
                    Toast.makeText(getApplicationContext(), "Le champ Chevaux Fiscaux est obligatoire", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    hp = Integer.parseInt(hpTxt);
                }catch(NumberFormatException e){
                    Log.d("AddExpenseLineActiv", "onClickButtonSave: horsePower is not a Number !");
                    return;
                }

                amount = new MileageExpenseAmount(0,0,expenseReportId,datePicker.getYear(),distance, MileageAllowanceScale.Vehicle.car.toString(),hp);
                if(!MileageExpenseAmount.doesScaleExistFor((MileageExpenseAmount)amount)){
                    Toast.makeText(getApplicationContext(), "Il n'y a pas de barème kilométrique associé ", Toast.LENGTH_LONG).show();
                    return;
                }
                break;
            default:
            case flateRate:
                Log.d("AddExpenseListAct", "onclickButtonSave: Category not recognized");
                return;
        }

        byte[] imageByteArray = null;
        if(canHaveJustificatif && photo != null){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            imageByteArray = stream.toByteArray();
        }

        String strId = "";
        ExpenseLine expenseLine = new ExpenseLine(0,strId,expenseReportId,expenseReportStrId,lineNumber,date,businessCategoryId,businessCategoryStringId,rebilling,comment,tradingName,customerId,customerStringId,projectId,projectStrId,amount,proofRef,imageByteArray);

        if(!app.isStandAlone())
        {
            if(app.isServerConnectionActive()) {
                String newJsonER = expenseLine.getJsonObject().toString();
                ExpenseLineServices expenseLineServices = new ExpenseLineServices();
                strId = expenseLineServices.postObject(newJsonER);
                if (strId.contains("businessExpenses_")) {
                    Log.i("addEL", "idLigneDeFrais = " + strId);
                    expenseLine.setStringId(strId);
                    ExpenseLineAdapter.getInstance(this, expenseReportId).add(expenseLine, false, true);
                    this.finish();
                } else {
                    Log.i("addER", "Ligne de frais non crée - Considérée comme non conforme par le serveur");
                    Toast.makeText(getApplicationContext(), "Cette ligne de frais est considérée comme non conforme par le serveur", Toast.LENGTH_LONG).show();
                }
            }else{ // Hors connexion
                ExpenseLineAdapter.getInstance(this, expenseReportId).add(expenseLine, false, true);
                EventInfoDBManager.getInstance(this).createItem(new EventInfo(EventInfo.Action.ADD.toString(), EventInfo.ObjectType.ExpenseLine.toString(), expenseLine.getExpenseLineId()));
                Toast.makeText(getApplicationContext(), "Echec de l'envoi de la ligne de frais sur le serveur. Veuillez synchroniser", Toast.LENGTH_LONG).show();
                this.finish();
            }
        } else { // StandAlone

            ExpenseLineAdapter.getInstance(this, expenseReportId).add(expenseLine, false, true);
            this.finish();
        }


    }

}
