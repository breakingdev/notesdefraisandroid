package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.EventInfo;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseReportServices;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.ActivityWithServiceSelection;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.sql.EventInfoDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseReportDBManager;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomSpinner;

import java.util.Date;

public class AddExpenseReportActivity extends ActivityWithServiceSelection {

    public ExpenseReportDBManager expenseReportDBManager;   // Manager des données de la table Notes de frais
    private BusinessService serviceSelected;

    @Override
    public void setServiceSelected(BusinessService service) {
        serviceSelected = service;
    }

    /* Initialisation des données membres de l'Activity */
    private void init() throws Exception {
        expenseReportDBManager = ExpenseReportDBManager.getInstance(this);

        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();

        if(!app.isStandAlone())
        {
            setFields();
        }
        else
        {
            LinearLayout layoutServices = (LinearLayout) findViewById(R.id.layoutServices);
            layoutServices.setVisibility(View.GONE);
        }
    }

    private void setFields()
    {
        CustomSpinner.setSpinnerForServices(this,R.id.lstServices);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expensereport);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_expense_report_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_save_expense:
                try{
                    onClickButtonSave();
                }
                catch(Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickButtonSave() throws Exception {
        EditText txtDescriptionNote = (EditText) findViewById(R.id.txtDescriptionNote);
        DatePicker dpickerDateDebut = (DatePicker) findViewById(R.id.dpickerDateDebut);
        DatePicker dpickerDateFin = (DatePicker) findViewById(R.id.dpickerDateFin);


        String descriptionNote = txtDescriptionNote.getText().toString();

        Date dateDebut = BusinessExpensesApplication.getDateFromDatePicker(dpickerDateDebut);
        Date dateFin = BusinessExpensesApplication.getDateFromDatePicker(dpickerDateFin);

        /* Comparaison entre date debut et date de fin */
        if(dateDebut.compareTo(dateFin) > 0 ){
//            Log.d("AddExpenseActiv","onClickButtonSave -> Date debut must be less or equals to Date Fin");
            Toast.makeText(getApplicationContext(), "La Date de début doit être inférieure ou égale à la Date de fin", Toast.LENGTH_LONG).show();
            return;
        }

        /* Test de champ vide (ici description)*/
        if(descriptionNote.equals("")){
//            Log.d("AddExpenseActiv","onClickButtonSave -> Description must be filled");
            Toast.makeText(getApplicationContext(), "Le champ Description est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }
        ExpenseReportAdapter adapter = ExpenseReportAdapter.getInstance(this);
        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();

        /* Vérifier si la période n'empiète pas sur celle d'une autre note de frais */
        long serviceId =  app.isStandAlone() ? 0 : serviceSelected.getServiceId();
        if(ExpenseReport.isConflicted(dateDebut,dateFin,adapter.getDbListObjects(),0,serviceId)){
//            Log.d("AddExpenseActiv","onClickButtonSave -> Expense Report period is conflicted with another !");
            Toast.makeText(getApplicationContext(), "Cette période correspond déjà à une autre note de frais", Toast.LENGTH_LONG).show();
            return;
        }


        String idNoteDeFrais = "";
        String serviceStrId = "";

        if(!app.isStandAlone())
        {
            serviceStrId = serviceSelected.getStringId();
            BusinessService service = (BusinessService) BusinessServiceAdapter.getInstance(this).getItemWithStringId(serviceStrId);
            serviceId = service.getServiceId();
        }
        ExpenseReport expenseReport = new ExpenseReport(descriptionNote, idNoteDeFrais, ExpenseReport.Status.inEdition.toString(), dateDebut, dateFin,serviceId,serviceStrId);


        if(!app.isStandAlone())
        {
            if(app.isServerConnectionActive()) {
                String newJsonER = expenseReport.getJsonObject().toString();

                ExpenseReportServices expenseReportServices = new ExpenseReportServices();
                idNoteDeFrais = expenseReportServices.postObject(newJsonER);

                if (idNoteDeFrais.contains("businessExpenses_")) {
                    Log.i("addER", "idNoteDeFrais = " + idNoteDeFrais);
                    expenseReport.setStringId(idNoteDeFrais);
                    adapter.add(expenseReport, false, true);
                    this.finish();
                } else {
                    Log.i("addER", "Note non crée : période correspond déjà à une autre note de frais");
                    Toast.makeText(getApplicationContext(), "Cette période correspond déjà à une autre note de frais", Toast.LENGTH_LONG).show();
                }
            }else{
                adapter.add(expenseReport,false,true);
                try {
                    EventInfoDBManager.getInstance(this).createItem(new EventInfo(EventInfo.Action.ADD.toString(), EventInfo.ObjectType.ExpenseReport.toString(),expenseReport.getExpenseReportId()));
                    Toast.makeText(getApplicationContext(), "Echec de l'envoi de la note sur le serveur. Veuillez synchroniser", Toast.LENGTH_LONG).show();
                    this.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            adapter.add(expenseReport,false,true);
            this.finish();
        }
    }
}
