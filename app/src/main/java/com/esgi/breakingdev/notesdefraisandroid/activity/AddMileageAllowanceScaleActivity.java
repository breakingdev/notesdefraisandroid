package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.content.Context;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandAddMileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;

import java.util.ArrayList;
import java.util.List;

public class AddMileageAllowanceScaleActivity extends Activity {

    private CommandAddMileageAllowanceScale command;
    public MileageAllowanceScale.Vehicle vehicleSelected;

    private void init()
    {
        command = new CommandAddMileageAllowanceScale(this);
        setFields();
    }

    private void setFields()
    {
        // SPINNER
        Spinner spinnerVehicles = (Spinner) findViewById(R.id.spinnerVehicles);
        final Context context = this;
        List<String> listVehicles = MileageAllowanceScale.getListVehicles(this);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listVehicles);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerVehicles.setAdapter(dataAdapter);
        spinnerVehicles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                vehicleSelected = MileageAllowanceScale.listVehicles.get(position);
                Log.i("vehicleSelected", "vehicleSelected = " + vehicleSelected.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        spinnerVehicles.setSelection(0);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mileage_allowance_scale);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_mileage_allowance_scale_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_mileage_allowance_scale: command.saveMileageAllowanceScale(); break;
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }
}
