package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import com.esgi.breakingdev.notesdefraisandroid.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by Tibo Mathieu on 05/07/2016.
 */
public class DisplayFullScreenImageActivity extends Activity {
    private Bitmap photo;
    private File photoFile;

    private void init()
    {
        getActionBar().setTitle("");
        getActionBar().hide();
        Uri photoUri = (Uri)getIntent().getExtras().get("imageUri");
        photoFile = new File(photoUri.getPath());
        try {
            photo =  BitmapFactory.decodeStream(new FileInputStream(photoFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        setFields();
    }

    private void setFields()
    {
        ImageView imageFullScreen = (ImageView) findViewById(R.id.imgFullScreen);
        imageFullScreen.setImageBitmap(photo);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_fullscreen_image);
        init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        photoFile.delete();
    }
}
