package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCategoryDBManager;

public class EditBusinessCategoryActivity extends Activity {

    private BusinessCategory.FormType formType;
    private BusinessCategory businessCategoryEdit;

    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnCancelEditBusinessCategory:
                try {
                    onClickButtonCancelEdit();
                }
                catch (Exception e){

                }
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_business_category);

        init();
    }

    private void init() {
        Intent i = getIntent();
        businessCategoryEdit = (BusinessCategory)i.getSerializableExtra("businessCategoryEdit");

        Log.d("EditBusinessCategoryAct", "ActivityEdit");
        Log.d("-->", "item.getCode = " + businessCategoryEdit.getCode());
        Log.d("-->", "item.getBusinessCategoryId = " + businessCategoryEdit.getBusinessCategoryId());
        Log.d("-->", "item.getDescription = " + businessCategoryEdit.getDescription());
        Log.d("-->", "item.getFormType = " + businessCategoryEdit.getFormType());

        String description = businessCategoryEdit.getDescription();
        EditText txtDescriptionCategorie = (EditText) findViewById(R.id.txtEditDescriptionCategorie);

        txtDescriptionCategorie.setText(description);

        RadioButton rdCategoryMontant = (RadioButton) findViewById(R.id.rdEditCategoryMontant);
        RadioButton rdCategoryForfait = (RadioButton) findViewById(R.id.rdEditCategoryForfait);
        RadioButton rdCategoryFrais = (RadioButton) findViewById(R.id.rdEditCategoryFrais);

        if(businessCategoryEdit.getFormType() == BusinessCategory.FormType.amount) {
            rdCategoryMontant.setChecked(true);
            rdCategoryForfait.setChecked(false);
            rdCategoryFrais.setChecked(false);
            formType = BusinessCategory.FormType.amount;
        }
        else if(businessCategoryEdit.getFormType() == BusinessCategory.FormType.flateRate) {
            rdCategoryMontant.setChecked(false);
            rdCategoryForfait.setChecked(true);
            rdCategoryFrais.setChecked(false);
            formType = BusinessCategory.FormType.flateRate;
        }
        else if(businessCategoryEdit.getFormType() == BusinessCategory.FormType.mileageExpense) {
            rdCategoryMontant.setChecked(false);
            rdCategoryForfait.setChecked(false);
            rdCategoryFrais.setChecked(true);
            formType = BusinessCategory.FormType.mileageExpense;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_business_category_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_edit_businessCategory:
                try{
                    onClickButtonSave();
                }
                catch(Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onRadioButtonTypeClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.rdEditCategoryMontant:
                if (checked)
                    formType = BusinessCategory.FormType.amount;
                break;
            case R.id.rdEditCategoryForfait:
                if (checked)
                    formType = BusinessCategory.FormType.flateRate;
                break;
            case R.id.rdEditCategoryFrais:
                if (checked)
                    formType = BusinessCategory.FormType.mileageExpense;
                break;
        }
    }


    public void onClickButtonSave() throws Exception {
        Log.i("AddExpenseCat", "ADD Debut");
        EditText txtDescriptionCategorie = (EditText) findViewById(R.id.txtEditDescriptionCategorie);

        String description = txtDescriptionCategorie.getText().toString();

        /* TEst de champ vide (ici description) */
        if(description.equals("")){
//            Log.d("AddExpenseCat","onClickButtonSave -> Description must be filled");
            Toast.makeText(getApplicationContext(), "Le champ Description est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }
        businessCategoryEdit.setDescription(description);
        businessCategoryEdit.setFormType(formType.toString());
        Log.i("AddExpenseCat", "getDescription = " + businessCategoryEdit.getDescription());
        Log.i("AddExpenseCat", "getFormType = " + businessCategoryEdit.getFormType().toString());

        BusinessCategoryDBManager.getInstance(this).updateItem(businessCategoryEdit);
        Log.i("AddExpenseCat", "ADD Fin");
        this.finish();
    }

    public void onClickButtonCancelEdit() throws Exception {
        this.finish();
    }
}
