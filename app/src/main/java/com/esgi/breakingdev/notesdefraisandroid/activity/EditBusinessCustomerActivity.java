package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCustomerDBManager;

public class EditBusinessCustomerActivity extends Activity {

    
    private BusinessCustomer businessCustomerEdit;

    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnCancelEditBusinessCustomer:
                try {
                    onClickButtonCancelEdit();
                }
                catch (Exception e){

                }
                break;
        }
    }

    private void init() {
        Log.d("EditBusinessCustomerAct", "ActivityEdit");
        Intent i = getIntent();
        businessCustomerEdit = (BusinessCustomer)i.getSerializableExtra("businessCustomerEdit");

        Log.d("-->", "item.getCode = " + businessCustomerEdit.getCode());
        Log.d("-->", "item.getBusinessCustomerId = " + businessCustomerEdit.getBusinessCustomerId());
        Log.d("-->", "item.getDescription = " + businessCustomerEdit.getDescription());

        String description = businessCustomerEdit.getDescription();
        String codeClient = businessCustomerEdit.getCode();
        EditText txtEditDescriptionClient = (EditText) findViewById(R.id.txtEditDescriptionClient);
        EditText txtEditCodeClient = (EditText) findViewById(R.id.txtEditCodeClient);

        txtEditDescriptionClient.setText(description);
        txtEditCodeClient.setText(codeClient);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_business_customer);

        init();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_business_customer_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_edit_businessCustomer:
                try{
                    onClickButtonSave();
                }
                catch(Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickButtonSave() throws Exception {
        EditText txtEditDescriptionClient = (EditText) findViewById(R.id.txtEditDescriptionClient);
        EditText txtEditCodeClient = (EditText) findViewById(R.id.txtEditCodeClient);

        String description = txtEditDescriptionClient.getText().toString();
        String codeClient = txtEditCodeClient.getText().toString();

        businessCustomerEdit.setDescription(description);
        businessCustomerEdit.setCode(codeClient);

        /* TEst de champ vide (ici description) */
        if(description.equals("")){
            Log.d("AddExpenseCat","onClickButtonSave -> Description must be filled");
            // Afficher un toast ici
            Toast.makeText(getApplicationContext(), "Le champ Description est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }
        BusinessCustomerDBManager.getInstance(this).updateItem(businessCustomerEdit);
        this.finish();
    }

    public void onClickButtonCancelEdit() throws Exception {
        this.finish();
    }
}
