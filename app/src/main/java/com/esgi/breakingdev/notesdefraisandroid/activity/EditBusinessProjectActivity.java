package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.ActivityWithCustomerSelection;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessProjectDBManager;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomCheckBox;

import java.util.List;

public class EditBusinessProjectActivity extends ActivityWithCustomerSelection {

    private BusinessCustomer customerSelected = null;
    private BusinessProject businessProjectEdit;
    private boolean rebilling;
    private boolean internalProject;

    @Override
    public void setSelectedCustomer(BusinessCustomer customer) {
        customerSelected = customer;
    }

    @Override
    public void setInternalItem(boolean internal) {
        internalProject = internal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_business_project);
        
        init();
    }


    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnCancelEditBusinessProject:
                try {
                    onClickButtonCancelEdit();
                }
                catch (Exception e){

                }
                break;
        }
    }

    private void init() {
        Intent i = getIntent();
        businessProjectEdit = (BusinessProject)i.getSerializableExtra("businessProjectEdit");

        Log.d("EditBusinessProjectAct", "ActivityEdit");
        String description = businessProjectEdit.getDescription();
        EditText txtEditDescriptionProjet = (EditText) findViewById(R.id.txtEditDescriptionProjet);
        txtEditDescriptionProjet.setText(description);
        CheckBox checkBoxRebilling = (CheckBox) findViewById(R.id.checkBoxRebilling_Edit);
        rebilling = businessProjectEdit.getRefacturation();
        if(rebilling)
            checkBoxRebilling.setChecked(true);
        else
            checkBoxRebilling.setChecked(false);

        checkBoxRebilling.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rebilling = isChecked;
            }
        });

        // LISTE DES CLIENTS
        // Chargement de la liste des clients
        int[] itemsToHide = new int[] { R.id.textView9};
        CustomCheckBox.setCheckBoxWithCustomerSpinner(this,this,R.id.lstEditClients, R.id.checkBoxInternal_Edit,itemsToHide );
        CheckBox checkBoxInternal = (CheckBox) findViewById(R.id.checkBoxInternal_Edit);
        if(businessProjectEdit.isInternal())
            checkBoxInternal.setChecked(true);
        else
            checkBoxInternal.setChecked(false);
        Spinner lstDropDownClients = (Spinner) findViewById(R.id.lstEditClients);
        final List<Object> listClients = ExpenseLine.businessCustomerList;

        int cptPosition = 0;
        int positionClient = 0;

        for (Object o : listClients) {
            BusinessCustomer tmpBC = (BusinessCustomer) o;

            if(businessProjectEdit.getIdClient() ==  tmpBC.getBusinessCustomerId())
            {
                positionClient = cptPosition;
                break;
            }
            cptPosition++;
        }
        lstDropDownClients.setSelection(positionClient);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_business_project_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_edit_businessProject:
                try{
                    onClickButtonSave();
                }
                catch(Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void onClickButtonSave() throws Exception {
        EditText txtEditDescriptionProjet = (EditText) findViewById(R.id.txtEditDescriptionProjet);

        String description = txtEditDescriptionProjet.getText().toString();

        businessProjectEdit.setDescription(description);
        businessProjectEdit.setRefacturation(rebilling);
        if(internalProject) {
            businessProjectEdit.setIdClient(0);
            businessProjectEdit.setStringIdClient("");
        }
        else {
            businessProjectEdit.setIdClient(customerSelected.getBusinessCustomerId());
            businessProjectEdit.setStringIdClient(customerSelected.getStringId());
        }


        /* TEst de champ vide (ici description) */
        if(description.equals("")){
//            Log.d("AddExpenseCat","onClickButtonSave -> Description must be filled");
            Toast.makeText(getApplicationContext(), "Le champ Description est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }

        BusinessProjectDBManager.getInstance(this).updateItem(businessProjectEdit);
        this.finish();
    }

    public void onClickButtonCancelEdit() throws Exception {
        this.finish();
    }
}
