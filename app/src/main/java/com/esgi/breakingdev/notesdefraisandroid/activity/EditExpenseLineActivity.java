package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandEditExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.ActivityWithCategorySelection;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.IActivityWithCustomerSelection;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.IActivityWithProjectSelection;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.SimpleAmount;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomCheckBox;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomSpinner;
import com.esgi.breakingdev.notesdefraisandroid.tools.PhotoHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.List;

public class EditExpenseLineActivity extends ActivityWithCategorySelection implements IActivityWithCustomerSelection,IActivityWithProjectSelection {

    public ExpenseLine expenseLine;
    public BusinessCategory categorySelected = null;  // Catégorie sélectionnée
    private CommandEditExpenseLine command;
    public List<Object> listProjectsOfCustomer;
    public BusinessCustomer customerSelected;
    public boolean internalLine;
    public Bitmap photo = null;
    private Uri photoUri;
    private static final int CAMERA_REQUEST = 1888;


    @Override
    public void setSelectedCustomer(BusinessCustomer customer) {
        customerSelected = customer;
        Log.d("EditExpenseLineActivity", "setSelectedCustomer : " + customer.getDescription());
    }

    @Override
    public void setInternalItem(boolean internal) {
        internalLine = internal;
    }

    @Override
    public void setProjectsOfCustomer(List<Object> listProjects) {
        listProjectsOfCustomer = listProjects;
    }

    @Override
    public List<Object> getCustomerProjects() {
        return listProjectsOfCustomer;
    }

    private void init() throws Exception
    {
        command = new CommandEditExpenseLine(this);
        Intent i = getIntent();
        long uid = i.getLongExtra("expenseLineEditId",0);
        expenseLine = (ExpenseLine) ExpenseLineAdapter.getInstance(this,0).getItemWithId(uid);
        setFields();

    }

    private void setFields() throws Exception
    {
        ExpenseReport expenseReport = (ExpenseReport) ExpenseReportAdapter.getInstance(this).getItemWithId(expenseLine.getExpenseReportId());
        DatePicker datePicker = (DatePicker) findViewById(R.id.dpickerDateLigneFrais_Edit);
        datePicker.setMinDate(expenseReport.getBeginDate().getTime());
        datePicker.setMaxDate(expenseReport.getEndDate().getTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(expenseLine.getDate());
        datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        TextView textRelatedExpenseReport = (TextView) findViewById(R.id.txtRelatedExpenseReport);
        textRelatedExpenseReport.setText(expenseReport.getDescription());

        EditText textDescription = (EditText) findViewById(R.id.txtDescriptionLigne);
        textDescription.setText(expenseLine.getComment());

        EditText textTradingName = (EditText) findViewById(R.id.textTradingName_Edit);
        textTradingName.setText(expenseLine.getTradingName());

        // LISTE DES CLIENTS
        long customerIdEdited = expenseLine.getCustomerId();
        long projectIdEdited = expenseLine.getProjectId();
        int[] itemsToHide = { R.id.txtViewLineCustomers_Edit, R.id.textViewp2,R.id.lstProjects_Edit,R.id.switchEditRebilling};
        CustomCheckBox.setCheckBoxWithCustomerSpinner(this, this, R.id.lstLineCustomers_Edit,R.id.checkBoxInternalExpense_Edit,  itemsToHide, customerIdEdited);
        // LISTE DES PROJETS
        CustomSpinner.setProjectSpinnerAccordingToCustomerSelected(this, this,this, R.id.lstLineCustomers_Edit, R.id.lstProjects_Edit, customerIdEdited, projectIdEdited);
        CustomSpinner.setRebillingSwitchAccorgindToProjectSelected(this, this, R.id.lstProjects_Edit, R.id.switchEditRebilling);

        /* Position du switch par défault sur celui de la ligne de frais éditée */
        Switch rebillingSwitch = (Switch) findViewById(R.id.switchEditRebilling);
        if(expenseLine.getProjectId() > 0) {
            rebillingSwitch.setEnabled(true);
            rebillingSwitch.setClickable(true);
            rebillingSwitch.setChecked(expenseLine.isRebilling());
        }else{
            rebillingSwitch.setEnabled(false);
            rebillingSwitch.setClickable(false);
            rebillingSwitch.setChecked(false);
        }

        // LISTE DES CATEGORIES DE DEPENSES
        int[] gridTextCellsId = new int[] { R.id.gridTextViewElement1_Edit,R.id.gridTextViewElement2_Edit };
        int[] gridEditTextCellsId = new int[] { R.id.gridEditTextElement1_Edit,R.id.gridEditTextElement2_Edit };
        CustomSpinner.setSpinnerForBusinessCategories(R.id.lstCategoriesDepenses_EditLine, this, gridTextCellsId, gridEditTextCellsId);


        // récupérer l'index de la cat de dépense dans la liste
        List<Object> listCategories = ExpenseLine.businessCategoryList;
//        Log.d("EditExpenseLineActiv","setFields() -> expenseLine.getCategory() = " + expenseLine.getCategory().toString());
        int i = 0, spinnerSelection = 0;
        for(Object o : listCategories){
            BusinessCategory item = (BusinessCategory) o;

            if(item.getBusinessCategoryId() == expenseLine.getBusinessCategoryId()) {
                spinnerSelection = i;
//                Log.d("EditExpenseLineActiv","setFields() -> spinner Selection found: " + spinnerSelection);
                break;
            }
            ++i;
        }
        Spinner spinnerCategories  = (Spinner) findViewById(R.id.lstCategoriesDepenses_EditLine);
        spinnerCategories.setSelection(spinnerSelection);
        spinnerCategories.setEnabled(false);


        EditText gridEditTextElement1 = (EditText) findViewById(R.id.gridEditTextElement1_Edit); //distance OU tva
        EditText gridEditTextElement2 = (EditText) findViewById(R.id.gridEditTextElement2_Edit); //horsePower  OU ttc
        boolean canHaveJustificatif = true;
        if(expenseLine.getCategory().equals(BusinessCategory.FormType.amount))
        {
            SimpleAmount amount = (SimpleAmount) expenseLine.getAmount();

            String amountTVA = String.format("%.2f", amount.getAmountTVA());
            String amountTTC = String.format("%.2f", amount.getAmountTTC());

            // On remplace les virgules par des points
            amountTVA = amountTVA.replace(",", ".");
            amountTTC = amountTTC.replace(",", ".");

            gridEditTextElement1.setText(amountTVA);
            gridEditTextElement2.setText(amountTTC);
        }
        else if(expenseLine.getCategory().equals(BusinessCategory.FormType.mileageExpense))
        {
            canHaveJustificatif = false;
            MileageExpenseAmount amount = (MileageExpenseAmount) expenseLine.getAmount();

            String amountKm = String.format("%.0f", amount.getKilometers());
            String amountHorsePower = String.format("%d", amount.getHorsePower());

            amountKm = amountKm.replace(",", ".");
            amountHorsePower = amountHorsePower.replace(",", ".");

            gridEditTextElement1.setText(amountKm);
            gridEditTextElement2.setText(amountHorsePower);
        }

        EditText textProofRef = (EditText) findViewById(R.id.editTextProofRef);
        textProofRef.setText(expenseLine.getProofReference());

        // JUSTIFICATIF - PHOTOS
        final ImageView imgJustificatif = (ImageView) findViewById(R.id.imgJustificatifLigne);
        Button buttonRemove = (Button)findViewById(R.id.btnRemovePhoto);
        if(canHaveJustificatif && expenseLine.getProofImage() != null) {
            imgJustificatif.setVisibility(View.VISIBLE);
            buttonRemove.setVisibility(View.VISIBLE);
            photo = BitmapFactory.decodeByteArray(expenseLine.getProofImage(), 0, expenseLine.getProofImage().length);
            imgJustificatif.setImageBitmap(photo);
        }
        final Activity activity = this;
        imgJustificatif.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(activity, DisplayFullScreenImageActivity.class);
                Uri bitmapUri = PhotoHandler.writeBitmapToFile(activity, photo, "fullscreenImg.jpg");
                if (bitmapUri == null) return false;
                intent.putExtra("imageUri", bitmapUri);
                activity.startActivity(intent);
                return false;
            }
        });


        Button photoButton = (Button) this.findViewById(R.id.btnSelectPhoto);
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                File imagesFolder = new File(getExternalCacheDir(), "MyImages");
//                File imagesFolder = new File(Environment.getExternalStorageDirectory(), "MyImages");
                imagesFolder.mkdirs();
                File mypath = new File(imagesFolder,"profile.jpg");
                photoUri = Uri.fromFile(mypath);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri); // set the image file name
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        final Button removeButton = (Button) this.findViewById(R.id.btnRemovePhoto);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photo = null;
                imgJustificatif.setImageBitmap(null);
                imgJustificatif.setVisibility(View.GONE);
                removeButton.setVisibility(View.GONE);
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            photo = PhotoHandler.getBitmapFromUri(photoUri);
            PhotoHandler.deleteFileFromUri(photoUri);
            ImageView imgJustificatifLigne = (ImageView) findViewById(R.id.imgJustificatifLigne);
            Button buttonRemove = (Button)findViewById(R.id.btnRemovePhoto);
            imgJustificatifLigne.setVisibility(View.VISIBLE);
            buttonRemove.setVisibility(View.VISIBLE);
            photo = PhotoHandler.compressBitmap(photo,50);
            imgJustificatifLigne.setImageBitmap(photo);
        }
    }

    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnCancelEditExpenseLine:
                try {
                    onClickButtonCancelEdit();
                }
                catch (Exception e){

                }
                break;
        }
    }


    public void onClickButtonCancelEdit() throws Exception {
        this.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_expense_line);
        try {
            init();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_expense_line_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_edit_expenseLine:command.saveExpenseLine(); return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        try {
            ExpenseLineAdapter.getInstance(this,expenseLine.getExpenseReportId()).setExpenseReportId(expenseLine.getExpenseReportId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onBackPressed();
    }

    /* IMPLEMENTED FROM  ActivityWithCategorySelection */

    @Override
    public void setCategorySelected(BusinessCategory businessCategory) {
        categorySelected = businessCategory;
    }
}
