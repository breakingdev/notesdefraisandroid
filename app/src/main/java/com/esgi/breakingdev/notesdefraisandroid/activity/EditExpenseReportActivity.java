package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.model.EventInfo;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseReportServices;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.ActivityWithServiceSelection;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.sql.EventInfoDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseReportDBManager;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class EditExpenseReportActivity extends ActivityWithServiceSelection {

    private ExpenseReport expenseReportEdit;

    @Override
    public void setServiceSelected(BusinessService service) {
        expenseReportEdit.setServiceId(service.getServiceId());
        expenseReportEdit.setServiceStringId(service.getStringId());
    }

    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnCancelEditExpenseReport:
                try {
                    onClickButtonCancelEdit();
                }
                catch (Exception e){

                }
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_expense_report);
        init();
    }

    private void init() {
        Intent i = getIntent();
        expenseReportEdit = (ExpenseReport)i.getSerializableExtra("expenseReportEdit");

        Log.d("EditExpenseReportAct", "ActivityEdit");
        String description = expenseReportEdit.getDescription();
        EditText txtDescriptionNote = (EditText) findViewById(R.id.txtEditDescriptionNote);
        txtDescriptionNote.setText(description);

        // CALENDRIERS
        Date beginDate = expenseReportEdit.getBeginDate();
        Date endDate = expenseReportEdit.getEndDate();
        DatePicker dpickerEditDateDebut = (DatePicker) findViewById(R.id.dpickerEditDateDebut);
        DatePicker dpickerEditDateFin = (DatePicker) findViewById(R.id.dpickerEditDateFin);

        Calendar cal = Calendar.getInstance();
        //DATE DE DEBUT
        cal.setTime(beginDate);
        dpickerEditDateDebut.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        //DATE DE FIN
        cal.setTime(endDate);
        dpickerEditDateFin.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));

        // SERVICES
        BusinessExpensesApplication app = (BusinessExpensesApplication) getApplicationContext();
        if(!app.isStandAlone())
        {
            CustomSpinner.setSpinnerForServices(this,R.id.lstServices,expenseReportEdit.getServiceId());
        }
        else
        {
            LinearLayout layoutServices = (LinearLayout) findViewById(R.id.layoutEditServices);
            layoutServices.setVisibility(View.GONE);
        }

    }

    public void onClickButtonSave() throws Exception {
        Log.d("EditExpenseReportAct", "onClickButtonSave : OK");

        EditText txtDescriptionNote = (EditText) findViewById(R.id.txtEditDescriptionNote);

        DatePicker dpickerEditDateDebut = (DatePicker) findViewById(R.id.dpickerEditDateDebut);
        DatePicker dpickerEditDateFin = (DatePicker) findViewById(R.id.dpickerEditDateFin);

        String description = txtDescriptionNote.getText().toString();
        Log.d("EditBusinessCategoryAct", "description = " + description);


        /* TEst de champ vide (ici description) */
        if(description.equals("")){
//            Log.d("AddExpenseCat","onClickButtonSave -> Description must be filled");
            Toast.makeText(getApplicationContext(), "Le champ Description est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }

        expenseReportEdit.setDescription(description);

        // DATE DE DEBUT
        Date dateDebut = BusinessExpensesApplication.getDateFromDatePicker(dpickerEditDateDebut);
        expenseReportEdit.setBeginDate(dateDebut);

        // DATE DE FIN
        Date dateFin = BusinessExpensesApplication.getDateFromDatePicker(dpickerEditDateFin);
        expenseReportEdit.setEndDate(dateFin);

        if(dateDebut.compareTo(dateFin) > 0) {
            Toast.makeText(getApplicationContext(), "La Date de début doit être inférieure ou égale à la Date de fin", Toast.LENGTH_LONG).show();
            return;
        }
        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();

        /* Vérifier si la période n'empiète pas sur celle d'une autre note de frais */
        long serviceId =  app.isStandAlone() ? 0 : expenseReportEdit.getServiceId();

        if(ExpenseReport.isConflicted(dateDebut, dateFin, ExpenseReportAdapter.getInstance(this).getDbListObjects(), expenseReportEdit.getExpenseReportId(),serviceId)){
//            Log.d("EditExpenseActiv","onClickButtonSave -> Expense Report period is conflicted with another !");
            Toast.makeText(getApplicationContext(), "Cette période correspond déjà à une autre note de frais", Toast.LENGTH_LONG).show();
            return;
        }


        expenseReportEdit.setBeginDate(dateDebut);
        expenseReportEdit.setEndDate(dateFin);

        if(!app.isStandAlone())
        {
            if(app.isServerConnectionActive()) {
                String newJsonER = expenseReportEdit.getJsonObject().toString();

                ExpenseReportServices expenseReportServices = new ExpenseReportServices();
                String idNoteDeFrais = expenseReportServices.editObject(newJsonER).replaceAll("\"", "").replaceAll("\n", "");

                Log.i("EditER", "idNoteDeFrais = " + idNoteDeFrais);
                Log.i("EditER", "expensereport str id = " + expenseReportEdit.getStringId());

                if (idNoteDeFrais.equals(expenseReportEdit.getStringId())) {
                    ExpenseReportDBManager.getInstance(this).updateItem(expenseReportEdit);
                    this.finish();
                } else {
                    Log.i("EditER", "Note non crée : période correspond déjà à une autre note de frais");
                    Toast.makeText(getApplicationContext(), "Cette période correspond déjà à une autre note de frais", Toast.LENGTH_LONG).show();
                }
            } else{
                try {
                    EventInfoDBManager.getInstance(this).createItem(new EventInfo(EventInfo.Action.EDIT.toString(), EventInfo.ObjectType.ExpenseReport.toString(),expenseReportEdit.getExpenseReportId()));
                    ExpenseReportDBManager.getInstance(this).updateItem(expenseReportEdit);
                    Toast.makeText(getApplicationContext(), "Echec de l'édition de la note sur le serveur. Veuillez synchroniser", Toast.LENGTH_LONG).show();
                    this.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        else
        {
            ExpenseReportDBManager.getInstance(this).updateItem(expenseReportEdit);
            this.finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_expense_report_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_edit_expenseReport:
                try{
                    onClickButtonSave();
                }
                catch(Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public void onClickButtonCancelEdit() throws Exception {
        this.finish();
    }
}
