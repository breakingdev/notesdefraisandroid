package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.adapter.MileageAllowanceScalesAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.sql.MileageAllowanceScaleDBManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EditMileageAllowanceScaleActivity extends Activity {

    private MileageAllowanceScale mileageAllowanceScaleEdit;
    public MileageAllowanceScale.Vehicle vehicleSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_mileage_allowance_scale);
        init();
    }

    private void init() {
        Intent i = getIntent();
        mileageAllowanceScaleEdit = (MileageAllowanceScale) i.getSerializableExtra("mileageAllowanceScaleEdit");

        Log.d("EditBusinessProjectAct", "ActivityEdit");
        Log.d("-->", "item.getHorsePower = " + mileageAllowanceScaleEdit.getHorsePower());
        Log.d("-->", "item.getMaxDistance = " + mileageAllowanceScaleEdit.getMaxDistance());
        Log.d("-->", "item.getMinDistance = " + mileageAllowanceScaleEdit.getMinDistance());
        Log.d("-->", "item.getVehicle = " + mileageAllowanceScaleEdit.getVehicle());
        Log.d("-->", "item.getRate = " + mileageAllowanceScaleEdit.getRate());

        EditText txtEditYear = (EditText) findViewById(R.id.txtEditYear);
        EditText txtEditMinDistance = (EditText) findViewById(R.id.txtEditMinDistance);
        EditText txtEditMaxDistance = (EditText) findViewById(R.id.txtEditMaxDistance);
        EditText txtEditRate = (EditText) findViewById(R.id.txtEditRate);
        EditText txtEditOffset = (EditText) findViewById(R.id.txtEditOffset);
        EditText txtEditHorsePower = (EditText) findViewById(R.id.txtEditHorsePower);

        txtEditYear.setText("" + mileageAllowanceScaleEdit.getMileageAllowanceYear());
        txtEditMinDistance.setText("" + mileageAllowanceScaleEdit.getMinDistance());
        txtEditMaxDistance.setText("" + mileageAllowanceScaleEdit.getMaxDistance());
        txtEditRate.setText(String.format("%.03f", mileageAllowanceScaleEdit.getRate()).replace(",","."));
        txtEditOffset.setText(String.format("%.0f", mileageAllowanceScaleEdit.getOffset()));
        txtEditHorsePower.setText("" + mileageAllowanceScaleEdit.getHorsePower());

        // LISTE DES VEHICULES
        Spinner spinnerEditVehicles = (Spinner) findViewById(R.id.spinnerEditVehicles);

        spinnerEditVehicles.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                vehicleSelected = MileageAllowanceScale.listVehicles.get(position);
                Log.i("vehicleSelected", "vehicleSelected = " + vehicleSelected.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

        List<String> list = MileageAllowanceScale.getListVehicles(this);

        final List<MileageAllowanceScale.Vehicle> listVehicules = MileageAllowanceScale.listVehicles;

        int cptPosition = 0;
        int positionVehicule = 0;

        for (Object o : listVehicules) {
            MileageAllowanceScale.Vehicle tmpVehicule = (MileageAllowanceScale.Vehicle) o;

            Log.d("EditMASAct", "cptPosition = " + cptPosition);

            if(mileageAllowanceScaleEdit.getVehicle() == tmpVehicule)
            {
                Log.d("EditMASAct", "IF OK = " + cptPosition);
                positionVehicule = cptPosition;
                break;
            }
            cptPosition++;
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEditVehicles.setAdapter(dataAdapter);
        spinnerEditVehicles.setSelection(positionVehicule);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.edit_mileage_allowance_scale_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_edit_mileage_allowance_scale:
                try{
                    onClickButtonSave();
                }
                catch(Exception e) {

                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void onClick(View view)
    {
        switch(view.getId())
        {
            case R.id.btnCancelEditMileageAllowanceScale:
                try {
                    onClickButtonCancelEdit();
                }
                catch (Exception e){

                }
                break;
        }
    }

    public void onClickButtonSave() throws Exception {

        saveMileageAllowanceScale();
    }

    public void onClickButtonCancelEdit() throws Exception {
        this.finish();
    }

    public void saveMileageAllowanceScale()
    {

        // YEAR
        EditText txtYear = (EditText) findViewById(R.id.txtEditYear);
        int year = 0;
        int yearNow = Calendar.getInstance().get(Calendar.YEAR);
        try {
            year = Integer.parseInt(txtYear.getText().toString());
        }catch(NumberFormatException e){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: year is not a Number !");
            return;
        }

        if(year == 0){
//            Log.d("CommandAddMileage","onClickButtonSave -> Year must be filled");
            Toast.makeText(getApplicationContext(), "Le champ Année est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }

        if(year < 2000 || year > yearNow){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: Year must be between 2000 and " + yearNow);
            return;
        }

        // VEHICLE
        MileageAllowanceScale.Vehicle vehicle = vehicleSelected;


        // HORSE POWER
        EditText txtHorsePower = (EditText) findViewById(R.id.txtEditHorsePower);

        int horsePowers = 0;
        try {
            horsePowers = Integer.parseInt(txtHorsePower.getText().toString());
        }catch(NumberFormatException e){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: horsePower is not a Number !");
            return;
        }
        if(horsePowers <= 0){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: horsePower must be positive !");
            return;
        }

        // MIN DISTANCE
        EditText txtMinDistance = (EditText) findViewById(R.id.txtEditMinDistance);

        int minDistance = 0;
        try {
            minDistance = Integer.parseInt(txtMinDistance.getText().toString());
        }catch(NumberFormatException e){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: MinDistance is not a Number !");
            return;
        }
        if(minDistance <= 0){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: MinDistance must be positive !");
            return;
        }


        // MAX DISTANCE
        EditText txtMaxDistance = (EditText) findViewById(R.id.txtEditMaxDistance);
        int maxDistance = 0;

        try {
            maxDistance = Integer.parseInt(txtMaxDistance.getText().toString());
        }catch(NumberFormatException e){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: MaxDistance is not a Number !");
            return;
        }
        if(maxDistance <= minDistance ){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: MaxDistance must > MinDistance value !");
            return;
        }


        // RATE
        EditText txtRate = (EditText) findViewById(R.id.txtEditRate);
        float rate = 0;

        try {
            rate = Float.parseFloat(txtRate.getText().toString());
            rate = (float)(Math.round(rate*1000)/1000.0d);

        }catch(NumberFormatException e){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: Rate is not a Number !");
            return;
        }
        if(rate <= 0 ){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: rate must > positive !");
            return;
        }

        // OFFSET
        EditText txtOffset = (EditText) findViewById(R.id.txtEditOffset);
        float offset = 0;

        try {
            offset = Float.parseFloat(txtOffset.getText().toString());
            offset = (float)(Math.round(offset*100)/100.0d);
        }catch(NumberFormatException e){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: Offset is not a Number !");
            return;
        }
        if(offset < 0 ){
            Log.d("EditMileage..Scale", "saveMileageAllowanceScale: Offset must > positive !");
            return;
        }
        if(vehicle.equals(MileageAllowanceScale.Vehicle.moto)){
            Toast.makeText(this, "La catégorie Moto n'est pas encore supportée", Toast.LENGTH_LONG).show();
            return;
        }

        String strId = mileageAllowanceScaleEdit.getStringId();
        MileageAllowanceScale scale = new MileageAllowanceScale(mileageAllowanceScaleEdit.getMileageAllowanceScaleId(),strId,year,vehicle.toString(),horsePowers,minDistance,maxDistance,rate,offset);

        try {
            MileageAllowanceScaleDBManager.getInstance(this).updateItem(scale);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.finish();
    }

}
