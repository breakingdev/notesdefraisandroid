package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandManageCategories;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.OnSwipeTouchListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.synchro.SynchroServer;

public class ListBusinessCategoryActivity extends Activity {

    private ListView listViewBusinessCategory; // list View des Categories de dépenses (affichage graphique)
    public BusinessCategoryAdapter listAdapter; // Adapteur
    private CommandBusinessCategory commandBusinessCategory; // Interface d'action des commandes pour cette activity


    /* Initialisation des membres de classe de l'Activity */
    private void init() throws Exception {
        listViewBusinessCategory = (ListView) findViewById(R.id.listCategoriesDepenses);
        listAdapter = BusinessCategoryAdapter.getInstance(this);
        listAdapter.refresh();
//        listAdapter.addListener(this);
        listViewBusinessCategory.setAdapter(listAdapter);
        commandBusinessCategory = new CommandBusinessCategory(this);
        commandBusinessCategory.categories.getIntentForTransition(this);

        final Context context = this;

        RelativeLayout layoutAll = (RelativeLayout) findViewById(R.id.layoutAll);
        layoutAll.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                commandBusinessCategory.categories.manageMileageAllowanceScales(true, CommandManageCategories.Direction.Right);
            }

            @Override
            public void onSwipeRight() {
                commandBusinessCategory.categories.manageExpenseReport(true, CommandManageCategories.Direction.Left);
            }

            @Override
            public void onSwipeTop() {}

            @Override
            public void onSwipeBottom() {
                SynchroServer.getInstance(context).sendNonTransmittedEventsToServer();
                SynchroServer.getInstance(context).checkNewServerObjects();
                Toast.makeText(getApplicationContext(), R.string.synchronization_ok, Toast.LENGTH_LONG).show();}
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_business_category);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
        BusinessCategory category = new BusinessCategory(0, "CodeTestJPA", "TestJPA 3", BusinessCategory.FormType.amount);
        businessCategoryDBManager.createItem(category);
        */
    }

//    public void searchCategorieDepenses(View view)
//    {
//        EditText txtSearchCategorieDepenses = (EditText) findViewById(R.id.txtSearchCategorieDepenses);
//
//        String nomCategorieDepenses = txtSearchCategorieDepenses.getText().toString();

        // Récupération de toutes les notes de la BDD correspondant à la recherche
//        listBusinessCategory = businessCategoryDBManager.getBusinessCategorySearchByDescription(nomCategorieDepenses);
//        adapter = new BusinessCategoryAdapter(this,listBusinessCategory);
//        adapter.addListener(this);
//        listViewBusinessCategory.setAdapter(adapter);
//        businessCategoryDBManager.getInfoBusinessCategory();

//        adapter.getFilter().filter(nomCategorieDepenses);
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_business_category_menu, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView search = (SearchView) menu.findItem(R.id.action_searchBusinessCategory).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                search.clearFocus(); // Evite le rechargement de l'Activity
//               createNoteTest();
//               listAdapter.notifyDataSetChanged();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                commandBusinessCategory.searchCategorieDeDepenses(query);
                return true;
            }

        });


        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        if(!app.isStandAlone())
        {
            MenuItem editButton = menu.findItem(R.id.action_add_businessCategory);
            editButton.setVisible(false);
        }
        else
        {
            MenuItem services = menu.findItem(R.id.action_view_business_services);
            services.setVisible(false);
            MenuItem config = menu.findItem(R.id.action_view_applicationParameters);
            config.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_add_businessCategory: commandBusinessCategory.addCategorieDeDepenses(); return true;
//            case R.id.btnFilterNote: commandListExpense.filterExpenseReports(); return true;
            case R.id.action_view_expenseReport: commandBusinessCategory.categories.manageExpenseReport(); return true;
            case R.id.action_view_mileage_allowance_scales: commandBusinessCategory.categories.manageMileageAllowanceScales(); return true;
            case R.id.action_view_businessProject: commandBusinessCategory.categories.manageBusinessProject(); return true;
            case R.id.action_view_businessCustomer: commandBusinessCategory.categories.manageBusinessCustomer(); return true;
            case R.id.action_view_applicationParameters: commandBusinessCategory.categories.manageApplicationParameters(); return true;
            case R.id.action_view_business_services:
                commandBusinessCategory.categories.viewBusinessServices();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void manageNotesDeFrais(View view)
    {
        Intent intent = new Intent(this, ListExpenseReportActivity.class);
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        listAdapter.removeListener(commandBusinessCategory);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        listAdapter.addListener(commandBusinessCategory);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,ListExpenseReportActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
