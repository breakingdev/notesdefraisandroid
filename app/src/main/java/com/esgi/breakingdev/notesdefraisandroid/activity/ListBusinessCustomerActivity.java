package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandManageCategories;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessCustomerListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.OnSwipeTouchListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCustomerAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.synchro.SynchroServer;

public class ListBusinessCustomerActivity extends Activity implements BusinessCustomerListener {

    private ListView listViewBusinessCustomer; // list View des clients (affichage graphique)
    public BusinessCustomerAdapter listAdapter; // Adapteur
    private CommandBusinessCustomer commandBusinessCustomer; // Interface d'action des commandes pour cette activity


    /* Initialisation des membres de classe de l'Activity */
    private void init() throws Exception
    {
        final BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();

        listViewBusinessCustomer = (ListView) findViewById(R.id.listClients);
        listAdapter = BusinessCustomerAdapter.getInstance(this);
//        listAdapter.addListener(this);
        listViewBusinessCustomer.setAdapter(listAdapter);
        commandBusinessCustomer = new CommandBusinessCustomer(this);
        commandBusinessCustomer.categories.getIntentForTransition(this);

        final Context context = this;

        RelativeLayout layoutAll = (RelativeLayout) findViewById(R.id.layoutAll);
        layoutAll.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                if(!app.isStandAlone())
                {
                    commandBusinessCustomer.categories.manageApplicationParameters(true, CommandManageCategories.Direction.Right);
                }
                else
                {
                    commandBusinessCustomer.categories.manageExpenseReport(true, CommandManageCategories.Direction.Right);
                }
            }

            @Override
            public void onSwipeRight() {
                commandBusinessCustomer.categories.manageBusinessProject(true, CommandManageCategories.Direction.Left);
            }

            @Override
            public void onSwipeTop() {}

            @Override
            public void onSwipeBottom() {
                SynchroServer.getInstance(context).sendNonTransmittedEventsToServer();
                SynchroServer.getInstance(context).checkNewServerObjects();
                Toast.makeText(getApplicationContext(), R.string.synchronization_ok, Toast.LENGTH_LONG).show();}
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_business_customer);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_business_customer_menu, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView search = (SearchView) menu.findItem(R.id.action_searchBusinessCustomer).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                search.clearFocus(); // Evite le rechargement de l'Activity
//               createNoteTest();
//               listAdapter.notifyDataSetChanged();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                commandBusinessCustomer.searchClient(query);
                return true;
            }

        });

        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        if(!app.isStandAlone())
        {
            MenuItem editButton = menu.findItem(R.id.action_add_BusinessCustomer);
            editButton.setVisible(false);
        }
        else
        {
            MenuItem services = menu.findItem(R.id.action_view_business_services);
            services.setVisible(false);
            MenuItem config = menu.findItem(R.id.action_view_applicationParameters);
            config.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_add_BusinessCustomer: commandBusinessCustomer.addClient(); return true;
//            case R.id.btnFilterNote: commandListExpense.filterExpenseReports(); return true;
            case R.id.action_view_expenseReport: commandBusinessCustomer.categories.manageExpenseReport(); return true;
            case R.id.action_view_businessCategory: commandBusinessCustomer.categories.manageBusinessCategories(); return true;
            case R.id.action_view_mileage_allowance_scales: commandBusinessCustomer.categories.manageMileageAllowanceScales(); return true;
            case R.id.action_view_businessProject: commandBusinessCustomer.categories.manageBusinessProject(); return true;
            case R.id.action_view_applicationParameters: commandBusinessCustomer.categories.manageApplicationParameters(); return true;
            case R.id.action_view_business_services:
                commandBusinessCustomer.categories.viewBusinessServices();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClickCustomer(BusinessCustomer item) {
        // Lancement d'une nouvelle activity
        Log.d("ListBusinessCustomerAct", "onClickCategory");
        commandBusinessCustomer.viewClient(item);
    }

    @Override
    public void onClickButtonDelete(BusinessCustomer item) {
        confirmDialogDeleteCustomer(item);
    }

    private void confirmDialogDeleteCustomer(BusinessCustomer item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final BusinessCustomer itemDelete = item;

        builder
                .setMessage("Êtes-vous sûr de vouloir supprimer ce client ?")
                .setPositiveButton("Oui",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        listAdapter.remove(itemDelete, true);
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    @Override
    public void onClickButtonEdit(BusinessCustomer item) {
        Intent intent = new Intent(this, EditBusinessCustomerActivity.class);
        intent.putExtra("businessCustomerEdit", item);
        startActivity(intent);
    }


    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        listAdapter.removeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        listAdapter.addListener(this);
        try {
            listAdapter.refresh();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,ListExpenseReportActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }

}
