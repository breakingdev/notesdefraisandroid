package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandManageCategories;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessProjectListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.OnSwipeTouchListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.synchro.SynchroServer;

public class ListBusinessProjectActivity extends Activity implements BusinessProjectListener {

    private ListView listViewBusinessProject; // list View des projets (affichage graphique)
    public BusinessProjectAdapter listAdapter; // Adapteur
    private CommandBusinessProject commandBusinessProject; // Interface d'action des commandes pour cette activity


    /* Initialisation des membres de classe de l'Activity */
    private void init() throws Exception {

        listViewBusinessProject = (ListView) findViewById(R.id.listProjets);

        listAdapter = BusinessProjectAdapter.getInstance(this);
//        listAdapter.addListener(this);
        listViewBusinessProject.setAdapter(listAdapter);
        commandBusinessProject = new CommandBusinessProject(this);
        commandBusinessProject.categories.getIntentForTransition(this);

        final Context context = this;

        RelativeLayout layoutAll = (RelativeLayout) findViewById(R.id.layoutAll);
        layoutAll.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                commandBusinessProject.categories.manageBusinessCustomer(true, CommandManageCategories.Direction.Right);
            }

            @Override
            public void onSwipeRight() {
                commandBusinessProject.categories.manageMileageAllowanceScales(true, CommandManageCategories.Direction.Left);
            }

            @Override
            public void onSwipeTop() {}

            @Override
            public void onSwipeBottom() {
                SynchroServer.getInstance(context).sendNonTransmittedEventsToServer();
                SynchroServer.getInstance(context).checkNewServerObjects();
                Toast.makeText(getApplicationContext(), R.string.synchronization_ok, Toast.LENGTH_LONG).show();}
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_business_project);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_business_project_menu, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView search = (SearchView) menu.findItem(R.id.action_searchBusinessProject).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                search.clearFocus(); // Evite le rechargement de l'Activity
//               createNoteTest();
//               listAdapter.notifyDataSetChanged();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                commandBusinessProject.searchProjet(query);
                return true;
            }

        });

        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        if(!app.isStandAlone())
        {
            MenuItem editButton = menu.findItem(R.id.action_add_BusinessProject);
            editButton.setVisible(false);
        }
        else
        {
            MenuItem services = menu.findItem(R.id.action_view_business_services);
            services.setVisible(false);
            MenuItem config = menu.findItem(R.id.action_view_applicationParameters);
            config.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_add_BusinessProject: commandBusinessProject.addProjet(); return true;
//            case R.id.btnFilterNote: commandListExpense.filterExpenseReports(); return true;
            case R.id.action_view_expenseReport: commandBusinessProject.categories.manageExpenseReport(); return true;
            case R.id.action_view_businessCategory: commandBusinessProject.categories.manageBusinessCategories(); return true;
            case R.id.action_view_mileage_allowance_scales: commandBusinessProject.categories.manageMileageAllowanceScales(); return true;
            case R.id.action_view_businessCustomer: commandBusinessProject.categories.manageBusinessCustomer(); return true;
            case R.id.action_view_applicationParameters: commandBusinessProject.categories.manageApplicationParameters(); return true;
            case R.id.action_view_business_services:
                commandBusinessProject.categories.viewBusinessServices();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    public void manageNotesDeFrais(View view)
    {
        Intent intent = new Intent(this, ListExpenseReportActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClickProject(BusinessProject item) {
        // Lancement d'une nouvelle activity
        Log.d("ListBusinessProjectAct", "onClickProject");
        commandBusinessProject.viewProjet(item);
    }

    @Override
    public void onClickButtonDelete(BusinessProject item) {
        Log.d("ListBusinessProjectAct", "onClickButtonDelete");
        Log.d("-->", "item.getCode = " + item.getCode());
        Log.d("-->", "item.getBusinessProjectId = " + item.getBusinessProjectId());
        Log.d("-->", "item.getDescription = " + item.getDescription());

        confirmDialogDeleteProject(item);

    }

    private void confirmDialogDeleteProject(BusinessProject item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final BusinessProject itemDelete = item;

        builder
                .setMessage("Êtes-vous sûr de vouloir supprimer ce projet ?")
                .setPositiveButton("Oui",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        listAdapter.remove(itemDelete, true);
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }


    @Override
    public void onClickButtonEdit(BusinessProject item) {
        Log.d("ListBusinessProjectAct", "onClickButtonEdit");
        Log.d("-->", "item.getCode = " + item.getCode());
        Log.d("-->", "item.getBusinessProjectId = " + item.getBusinessProjectId());
        Log.d("-->", "item.getDescription = " + item.getDescription());
        Intent intent = new Intent(this, EditBusinessProjectActivity.class);
        intent.putExtra("businessProjectEdit", item);
        startActivity(intent);
    }


    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        listAdapter.removeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        try {
            listAdapter.refresh();
        }catch(Exception e){
            e.printStackTrace();
        }
        listAdapter.addListener(this);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,ListExpenseReportActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }

}
