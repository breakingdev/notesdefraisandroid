package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandListBusinessService;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandManageCategories;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessServiceListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.OnSwipeTouchListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.synchro.SynchroServer;

public class ListBusinessServiceActivity extends Activity  {

    private ListView listViewBusinessServices; // list View des projets (affichage graphique)
    public BusinessServiceAdapter listAdapter; // list View des Services (IHM)
    private CommandListBusinessService command;

    private void init()
    {
        command = new CommandListBusinessService(this);
        command.categories.getIntentForTransition(this);
        listViewBusinessServices = (ListView) findViewById(R.id.listServices);

        try {
            listAdapter = BusinessServiceAdapter.getInstance(this);
            listViewBusinessServices.setAdapter(listAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final Context context = this;

        RelativeLayout layoutAll = (RelativeLayout) findViewById(R.id.layoutAll);
        layoutAll.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                command.categories.manageExpenseReport(true, CommandManageCategories.Direction.Right);
            }

            @Override
            public void onSwipeRight() {
                command.categories.manageApplicationParameters(true, CommandManageCategories.Direction.Left);
            }

            @Override
            public void onSwipeTop() {
            }

            @Override
            public void onSwipeBottom() {
                SynchroServer.getInstance(context).sendNonTransmittedEventsToServer();
                SynchroServer.getInstance(context).checkNewServerObjects();
                Toast.makeText(getApplicationContext(), R.string.synchronization_ok, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_business_service);
        init();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_business_services_menu, menu);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView search = (SearchView) menu.findItem(R.id.action_searchBusinessServices).getActionView();
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                search.clearFocus(); // Evite le rechargement de l'Activity
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                command.searchBusinessService(query);
                return true;
            }

        });

//        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
//        if(!app.isStandAlone())
//        {
//            MenuItem editButton = menu.findItem(R.id.action_add_BusinessProject);
//            editButton.setVisible(false);
//        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
//            case R.id.btnFilterNote: commandListExpense.filterExpenseReports(); return true;
            case R.id.action_view_expenseReport: command.categories.manageExpenseReport(); return true;
            case R.id.action_view_businessCategory: command.categories.manageBusinessCategories(); return true;
            case R.id.action_view_mileage_allowance_scales: command.categories.manageMileageAllowanceScales(); return true;
            case R.id.action_view_businessProject: command.categories.manageBusinessProject(); return true;
            case R.id.action_view_businessCustomer: command.categories.manageBusinessCustomer(); return true;
            case R.id.action_view_applicationParameters: command.categories.manageApplicationParameters(); return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        listAdapter.removeListener(command);
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        try {
            listAdapter.refresh();
        }catch(Exception e){
            e.printStackTrace();
        }
        listAdapter.addListener(command);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,ListExpenseReportActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
