package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandListExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandManageCategories;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.OnSwipeTouchListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCustomerAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.MileageAllowanceScalesAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.services.BusinessCategoryServices;
import com.esgi.breakingdev.notesdefraisandroid.services.BusinessCustomerServices;
import com.esgi.breakingdev.notesdefraisandroid.services.BusinessProjectServices;
import com.esgi.breakingdev.notesdefraisandroid.services.BusinessServiceServices;
import com.esgi.breakingdev.notesdefraisandroid.services.MileageAllowanceScaleServices;
import com.esgi.breakingdev.notesdefraisandroid.synchro.SynchroServer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListExpenseReportActivity extends Activity {

    private static boolean receiveListCategorys;
    private static boolean receiveListCustomers;
    private static boolean receiveListProjects;
    private static boolean receiveListMAS;
    private static boolean receiveListServices;
    private ListView listViewExpenseReport; // list View des Notes de frais (affichage graphique)
    public ExpenseReportAdapter listAdapter; // Adapteur
    private CommandListExpenseReport commandListExpenseReport; // Interface d'action des commandes pour cette activity

    /* Initialisation des membres de classe de l'Activity */
    private void init() throws Exception {

        final BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();

        commandListExpenseReport = new CommandListExpenseReport(this);
        commandListExpenseReport.categories.getIntentForTransition(this);
        listViewExpenseReport = (ListView) findViewById(R.id.listNotes);
        ExpenseLineAdapter.getInstance(this,0);
        listAdapter = ExpenseReportAdapter.getInstance(this);
//        listAdapter.refresh();
        listViewExpenseReport.setAdapter(listAdapter);

        final Context context = this;

        RelativeLayout layoutAll = (RelativeLayout) findViewById(R.id.layoutAll);
        layoutAll.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                commandListExpenseReport.categories.manageBusinessCategories(true, CommandManageCategories.Direction.Right);
            }

            @Override
            public void onSwipeRight() {
                if(!app.isStandAlone())
                {
                    commandListExpenseReport.categories.viewBusinessServices(true, CommandManageCategories.Direction.Left);
                }
                else
                {
                    commandListExpenseReport.categories.manageBusinessCustomer(true, CommandManageCategories.Direction.Left);
                }
            }

            @Override
            public void onSwipeTop() {

            }

            @Override
            public void onSwipeBottom() {
                SynchroServer.getInstance(context).sendNonTransmittedEventsToServer();
                SynchroServer.getInstance(context).checkNewServerObjects();
                Toast.makeText(getApplicationContext(), R.string.synchronization_ok, Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_expensereport);
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }

//        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
//        Log.i("onCreate", "isServerConnectionActive ? = " + app.isServerConnectionActive());


//        Intent intent = new Intent(this, ViewParametersQRCodeActivity.class);
//        this.startActivity(intent);
    }

   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_expense_report_menu, menu);
       SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
       final SearchView search = (SearchView) menu.findItem(R.id.action_searchExpenseReport).getActionView();
       search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
       search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

           @Override
           public boolean onQueryTextSubmit(String query) {
               search.clearFocus(); // Evite le rechargement de l'Activity
               return true;
           }

           @Override
           public boolean onQueryTextChange(String query) {
               commandListExpenseReport.searchNotesDeFrais(query);
               return true;
           }

       });

       BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
       if(app.isStandAlone())
       {
           MenuItem synchro = menu.findItem(R.id.action_synchro);
           synchro.setVisible(false);
           MenuItem services = menu.findItem(R.id.action_view_business_services);
           services.setVisible(false);
           MenuItem config = menu.findItem(R.id.action_view_applicationParameters);
           config.setVisible(false);
       }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_add_expense: commandListExpenseReport.addNotesDeFrais(); return true;
//            case R.id.btnFilterNote: commandListExpenseReport.filterExpenseReports(); return true;
            case R.id.action_view_businessCategory: commandListExpenseReport.categories.manageBusinessCategories(); return true;
            case R.id.action_view_mileage_allowance_scales: commandListExpenseReport.categories.manageMileageAllowanceScales(); return true;
            case R.id.action_view_businessProject: commandListExpenseReport.categories.manageBusinessProject(); return true;
            case R.id.action_view_businessCustomer: commandListExpenseReport.categories.manageBusinessCustomer(); return true;
            case R.id.action_view_applicationParameters: commandListExpenseReport.categories.manageApplicationParameters(); return true;
            case R.id.action_view_business_services:
                commandListExpenseReport.categories.viewBusinessServices();
                return true;
            case R.id.action_synchro:
                SynchroServer.getInstance(this).sendNonTransmittedEventsToServer();
                SynchroServer.getInstance(this).checkNewServerObjects();
                Toast.makeText(getApplicationContext(), R.string.synchronization_ok, Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        listAdapter.removeListener(commandListExpenseReport);

    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

        try {
            listAdapter.addListener(commandListExpenseReport);
//            ExpenseLineAdapter.getInstance(this,0).
            listAdapter.refresh();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
