package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandListMileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandManageCategories;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.OnSwipeTouchListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.AbstractArrayAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.MileageAllowanceScalesAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.synchro.SynchroServer;

public class ListMileageAllowanceScaleActivity extends Activity {

    private CommandListMileageAllowanceScale command;
    private MileageAllowanceScalesAdapter listAdapter;
    private void init()
    {
        command = new CommandListMileageAllowanceScale(this);
        command.categories.getIntentForTransition(this);
        try {
            listAdapter =  MileageAllowanceScalesAdapter.getInstance(this);
        }catch(Exception e){
            e.printStackTrace();
        }

        final Context context = this;

        RelativeLayout layoutAll = (RelativeLayout) findViewById(R.id.layoutAll);
        layoutAll.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                command.categories.manageBusinessProject(true, CommandManageCategories.Direction.Right);
            }

            @Override
            public void onSwipeRight() {
                command.categories.manageBusinessCategories(true, CommandManageCategories.Direction.Left);
            }

            @Override
            public void onSwipeTop() {}

            @Override
            public void onSwipeBottom() {
                SynchroServer.getInstance(context).sendNonTransmittedEventsToServer();
                SynchroServer.getInstance(context).checkNewServerObjects();
                Toast.makeText(getApplicationContext(), R.string.synchronization_ok, Toast.LENGTH_LONG).show();}
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_mileage_allowance_scale);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.list_mileage_allowance_scale_menu, menu);

        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        if(!app.isStandAlone())
        {
            MenuItem editButton = menu.findItem(R.id.action_add_mileage_allowance_scale);
            editButton.setVisible(false);
        }
        else
        {
            MenuItem services = menu.findItem(R.id.action_view_business_services);
            services.setVisible(false);
            MenuItem config = menu.findItem(R.id.action_view_applicationParameters);
            config.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_mileage_allowance_scale: command.addMileageAllowanceScale(); break;

            case R.id.action_view_businessCategory: command.categories.manageBusinessCategories(); break;
            case R.id.action_view_expenseReport: command.categories.manageExpenseReport(); break;
            case R.id.action_view_businessProject: command.categories.manageBusinessProject(); return true;
            case R.id.action_view_businessCustomer: command.categories.manageBusinessCustomer(); return true;
            case R.id.action_view_applicationParameters: command.categories.manageApplicationParameters(); return true;
            case R.id.action_view_business_services:
                command.categories.viewBusinessServices();
                return true;
            default: break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onResume() {
        super.onResume();
        ListView listScales = (ListView) findViewById(R.id.list_scales);
        listScales.setAdapter(listAdapter);
        try {
            listAdapter.refresh();
            listAdapter.addListener(command);

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        listAdapter.removeListener(command);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,ListExpenseReportActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
