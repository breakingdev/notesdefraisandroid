package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.model.EventInfo;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.UserInfo;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseReportServices;
import com.esgi.breakingdev.notesdefraisandroid.services.HttpServices;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCategoryDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCustomerDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessProjectDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessServiceDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.EventInfoDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseLineDBHelper;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseLineDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseReportDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.FlateRateAmountDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.MileageAllowanceScaleDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.MileageExpenseAmountDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.SimpleAmountDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.UserInfoDBManager;
import com.esgi.breakingdev.notesdefraisandroid.synchro.SynchroServer;

import java.text.SimpleDateFormat;

public class MainActivity extends Activity {

    private void init() throws Exception {
        /* Parémétrage de l'utilitaire de formatage de date à l'initialisation du contexte de l'application */
        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        app.setDateFormat(new SimpleDateFormat(this.getResources().getString(R.string.date_format_model)));
        app.setDateFormatForGUI(new SimpleDateFormat(this.getResources().getString(R.string.date_format_model_gui)));
        app.setDateFormatForServer(new SimpleDateFormat(this.getResources().getString(R.string.date_format_model_server)));


        MileageExpenseAmount.mileageAllowanceScaleList =  MileageAllowanceScaleDBManager.getInstance(this).getDBItems();

        ExpenseLine.businessCategoryList =  BusinessCategoryDBManager.getInstance(this).getDBItems();
        ExpenseLine.simpleAmountList =  SimpleAmountDBManager.getInstance(this).getDBItems();
        ExpenseLine.mileageExpenseList =  MileageExpenseAmountDBManager.getInstance(this).getDBItems();
        ExpenseLine.flateRateList =  FlateRateAmountDBManager.getInstance(this).getDBItems();
        ExpenseLine.businessProjectList = BusinessProjectDBManager.getInstance(this).getDBItems();
        ExpenseLine.businessCustomerList = BusinessCustomerDBManager.getInstance(this).getDBItems();
        BusinessService.businessServicesList = BusinessServiceDBManager.getInstance(this).getDBItems();

        UserInfo.listData = UserInfoDBManager.getInstance(this).getDBItems();
        EventInfo.listEvent = EventInfoDBManager.getInstance(this).getDBItems();
        HttpServices.getInstance().notifyUserInfos();
        ExpenseReportDBManager.getInstance(this);
        ExpenseLineDBManager.getInstance(this);

//        SynchroServer.getInstance(this).startThread();
        // MODE DE L'APPLICATION
        String mode = UserInfo.searchValueOf("mode");
        String[] modes = getResources().getStringArray(R.array.application_modes_arrays);

        if((mode.equals(modes[0])) || (mode.equals(modes[1]))) // Client-Server ou Standalone
        {
            Log.i("MainAct", "MODE RENSEIGNE : ---> mode = " + mode);
            Intent intent = null;
            if(mode.equals(modes[0]) && UserInfo.searchValueOf("userToken").isEmpty())
                intent = new Intent(this,ViewApplicationParametersActivity.class);
            else
                intent = new Intent(this, ListExpenseReportActivity.class);

            startActivity(intent);
        }
        else // L'utilisateur n'a jamais renseigné de mode
        {
            Log.i("MainAct", "MODE NON-RENSEIGNE");
            Intent intent = new Intent(this, ViewChoiceModeActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(this, "android.permission.CAMERA") != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.CAMERA"}, 0);
        }
        
        setContentView(R.layout.activity_main);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
