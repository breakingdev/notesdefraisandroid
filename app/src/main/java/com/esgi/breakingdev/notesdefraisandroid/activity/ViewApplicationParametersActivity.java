package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

// on importe les classes IntentIntegrator et IntentResult de la librairie zxing

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandManageCategories;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandViewApplicationParameters;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.OnSwipeTouchListener;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseReportServices;
import com.esgi.breakingdev.notesdefraisandroid.model.UserInfo;
import com.esgi.breakingdev.notesdefraisandroid.services.HttpServices;
import com.esgi.breakingdev.notesdefraisandroid.services.UserServices;
import com.esgi.breakingdev.notesdefraisandroid.sql.UserInfoDBManager;
import com.esgi.breakingdev.notesdefraisandroid.tools.JsonTool;
import com.esgi.breakingdev.notesdefraisandroid.tools.QRCodeFileReader;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONObject;
import org.w3c.dom.Text;

public class ViewApplicationParametersActivity extends Activity implements View.OnClickListener {

    private CommandViewApplicationParameters commandViewApplicationParameters; // Interface d'action des commandes pour cette activity

    private void init()
    {
        final BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();

        commandViewApplicationParameters = new CommandViewApplicationParameters(this);
        commandViewApplicationParameters.categories.getIntentForTransition(this);


        UserInfo.searchValueOf("userId");
        UserInfo.searchValueOf("userDisplayName");
        UserInfo.searchValueOf("userPositions");

        RelativeLayout layoutAll = (RelativeLayout) findViewById(R.id.layoutAll);
        layoutAll.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                if(!app.isStandAlone())
                {
                    if (!displayToastErrorIfConfigNotOk()) return;
                    commandViewApplicationParameters.categories.viewBusinessServices(true, CommandManageCategories.Direction.Right);
                }
                else
                {
                    commandViewApplicationParameters.categories.manageExpenseReport(true, CommandManageCategories.Direction.Right);
                }
            }

            @Override
            public void onSwipeRight() {
                if (!displayToastErrorIfConfigNotOk()) return;
                commandViewApplicationParameters.categories.manageBusinessCustomer(true, CommandManageCategories.Direction.Left);
            }

            @Override
            public void onSwipeTop() {
            }

            @Override
            public void onSwipeBottom() {
            }
        });
        setFields();
    }

    private void setFields()
    {
        Spinner lstModesApplication = (Spinner) findViewById(R.id.lstModesApplication);

        String[] modes = getResources().getStringArray(R.array.application_modes_arrays);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, modes);
        int modePosition = spinnerArrayAdapter.getPosition(UserInfo.searchValueOf("mode"));

        lstModesApplication.setSelection(modePosition);

        lstModesApplication.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String modeSelectionne = (String) parentView.getItemAtPosition(position);
                String[] modes = getResources().getStringArray(R.array.application_modes_arrays);

                LinearLayout linearLayoutModeApplication = (LinearLayout) findViewById(R.id.linearLayoutModeApplication);

                if (modeSelectionne.equals(modes[0])) // Client-Server
                {
                    linearLayoutModeApplication.setVisibility(View.VISIBLE);
                } else if (modeSelectionne.equals(modes[1])) // Standalone
                {
                    linearLayoutModeApplication.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        lstModesApplication.setEnabled(false);

        Button btnScanQRCode = (Button) findViewById(R.id.qrCode_scanButton);
        btnScanQRCode.setOnClickListener(this);

        TextView textConfig = (TextView)findViewById(R.id.txtConfigOk);
        BusinessExpensesApplication app = (BusinessExpensesApplication) getApplicationContext();
        if(app.isServerConfigOk()) {
            textConfig.setText(getString(R.string.text_config_ok));
            textConfig.setTextColor(Color.GREEN);
        }
        else
            textConfig.setText(getString(R.string.text_config_not_ok));

        if(!(UserInfo.searchValueOf("userId").equals("")) && !(UserInfo.searchValueOf("userDisplayName").equals("")) && !(UserInfo.searchValueOf("userPositions").equals("")))
        {
            TextView txtUserPosition = (TextView)findViewById(R.id.txtUserPosition);
            TextView txtUserDisplayName = (TextView)findViewById(R.id.txtUserDisplayName);

            txtUserPosition.setText(UserInfo.searchValueOf("userPositions"));
            txtUserDisplayName.setText(UserInfo.searchValueOf("userDisplayName"));
            textConfig.setVisibility(View.GONE);
        }

    }

    private boolean displayToastErrorIfConfigNotOk()
    {
        BusinessExpensesApplication app = (BusinessExpensesApplication) getApplicationContext();
        if(!app.isServerConfigOk()){
            Toast.makeText(getApplicationContext(), "Vous devez d'abord scanner un QRCode de Maarch !", Toast.LENGTH_LONG).show();

            return false;
        }
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_application_parameters);
        init();
    }

    private void scanQRCodeFromFile()
    {
//                         on lance le scanner au clic sur notre bouton
                boolean read = QRCodeFileReader.getInstance(this).readInfo();
                String token = QRCodeFileReader.getInstance(this).getToken();
                String urlServer = QRCodeFileReader.getInstance(this).getUrlServer();
                if(read && !token.isEmpty() && !urlServer.isEmpty()){
                    UserInfo infoUrl = new UserInfo("serverURL", urlServer);
                    UserInfo infoToken = new UserInfo("userToken", token);
                    try {
                        if (UserInfo.searchValueOf("serverURL").equals(""))
                            UserInfoDBManager.getInstance(this).createItem(infoUrl, true);
                        else
                            UserInfoDBManager.getInstance(this).updateItem(infoUrl);

                        if (UserInfo.searchValueOf("userToken").equals(""))
                            UserInfoDBManager.getInstance(this).createItem(infoToken, true);
                        else
                            UserInfoDBManager.getInstance(this).updateItem(infoToken);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    HttpServices.getInstance().notifyUserInfos();
                    BusinessExpensesApplication app = (BusinessExpensesApplication) getApplicationContext();

                    Toast toast = null;

                    if(app.isServerConfigOk()) {

                        toast = Toast.makeText(this, getString(R.string.text_config_ok),  Toast.LENGTH_LONG);
                        toast.show();

                        Log.i("ViewApplicationParam", "QRCode scan OK");
                        Log.i("ViewApplicationParam", "url = " + UserInfo.searchValueOf("serverURL"));
                        Log.i("ViewApplicationParam", "token = " + UserInfo.searchValueOf("userToken"));
                    }
                    else
                    {
                        toast = Toast.makeText(this, getString(R.string.text_config_not_ok),  Toast.LENGTH_LONG);
                        Log.i("ViewApplicationParam", "QRCode scan Failed");
                    }

                    UserServices userServices = new UserServices();
                    String userId = userServices.getUserId();
                    String userPositions = userServices.getUserPosition();
                    String userDisplayName = userServices.getUserDisplayName(userId);

                    UserInfo infoUserId = new UserInfo("userId", userId);
                    UserInfo infoUserDisplayName = new UserInfo("userDisplayName", userDisplayName);
                    UserInfo infoUserPositions = new UserInfo("userPositions", userPositions);

                    try {
                        // UserID
                        if (UserInfo.searchValueOf("userId").equals(""))
                            UserInfoDBManager.getInstance(this).createItem(infoUserId, true);
                        else
                            UserInfoDBManager.getInstance(this).updateItem(infoUserId);

                        // UserDisplayName
                        if (UserInfo.searchValueOf("userDisplayName").equals(""))
                            UserInfoDBManager.getInstance(this).createItem(infoUserDisplayName, true);
                        else
                            UserInfoDBManager.getInstance(this).updateItem(infoUserDisplayName);

                        // UserPositions
                        if (UserInfo.searchValueOf("userPositions").equals(""))
                            UserInfoDBManager.getInstance(this).createItem(infoUserPositions, true);
                        else
                            UserInfoDBManager.getInstance(this).updateItem(infoUserPositions);
                    } catch(Exception e){
                        e.printStackTrace();
                        return;
                    }
                    UserInfo.searchValueOf("userId");
                    UserInfo.searchValueOf("userDisplayName");
                    UserInfo.searchValueOf("userPositions");
                    TextView txtUserPosition = (TextView)findViewById(R.id.txtUserPosition);
                    TextView txtUserDisplayName = (TextView)findViewById(R.id.txtUserDisplayName);
                    txtUserPosition.setText(userPositions);
                    txtUserDisplayName.setText(userDisplayName);

                }
                else {
                    if(!read)
                        Log.d("ViewApplicationParam", "QRCodeFileReader Failed (readInfo failed)");
                    else
                        Log.d("ViewApplicationParam", "QRCodeFileReader Failed (token or url empty)");
                }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.qrCode_scanButton) {

            BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
            if(app.isOnline()) {
                new IntentIntegrator(this).initiateScan();
//                scanQRCodeFromFile();
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Vous n'êtes pas connectés à internet", Toast.LENGTH_LONG).show();
            }

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        String url = "";
        String token = "";

        // nous utilisons la classe IntentIntegrator et sa fonction parseActivityResult pour parser le résultat du scan
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            // nous récupérons le contenu du code barre
            String scanContent = scanningResult.getContents();
            Log.d("ViewApplicationParam","QRCode content = " + scanContent);

            if(!JsonTool.isJSONValid(scanContent)){
                Toast.makeText(this, getString(R.string.text_QRCode_not_valid),  Toast.LENGTH_LONG).show();
                TextView txtConf = (TextView) findViewById(R.id.txtConfigOk);
                if(scanContent != null) txtConf.setText(R.string.text_config_not_ok);

                Log.d("ViewApplicationParam","QRCode content does not match or has not been scanned correctly");
                return;
            }

            // nous récupérons le format du code barre
            String scanFormat = scanningResult.getFormatName();



            // nous affichons le résultat dans nos TextView
            //            scan_format.setText("FORMAT: " + scanFormat);
            //            scan_content.setText("CONTENT: " + scanContent);

            try
            {
                String donneesJSON = scanContent;
                Log.d("ViewApplicationParam","QRCode Scan content = " + donneesJSON);
                JSONObject reader = new JSONObject(donneesJSON);

                url = reader.getString("url");
                token = reader.getString("token");
                UserInfo infoUrl = new UserInfo("serverURL", url);
                UserInfo infoToken = new UserInfo("userToken", token);

                if(UserInfo.searchValueOf("serverURL").equals(""))
                    UserInfoDBManager.getInstance(this).createItem(infoUrl, true);
                else
                    UserInfoDBManager.getInstance(this).updateItem(infoUrl);

                if(UserInfo.searchValueOf("userToken").equals(""))
                    UserInfoDBManager.getInstance(this).createItem(infoToken, true);
                else
                    UserInfoDBManager.getInstance(this).updateItem(infoToken);

//                HttpServices.notifyUserInfos();
                HttpServices.getInstance().notifyUserInfos();
                TextView textConfig = (TextView)findViewById(R.id.txtConfigOk);
                TextView txtUserPosition = (TextView)findViewById(R.id.txtUserPosition);
                TextView txtUserDisplayName = (TextView)findViewById(R.id.txtUserDisplayName);

                BusinessExpensesApplication app = (BusinessExpensesApplication) getApplicationContext();

                Toast toast = null;

                if(app.isServerConfigOk()) {

                    toast = Toast.makeText(this, getString(R.string.text_config_ok),  Toast.LENGTH_LONG);
                    toast.show();

                    Log.i("ViewApplicationParam", "QRCode scan OK");
                }
                else
                {
                    toast = Toast.makeText(this, getString(R.string.text_config_not_ok),  Toast.LENGTH_LONG);
                    Log.i("ViewApplicationParam", "QRCode scan Failed");
                }

                textConfig.setVisibility(View.GONE);

                UserServices userServices = new UserServices();
                String userId = userServices.getUserId();
                String userPositions = userServices.getUserPosition();
                String userDisplayName = userServices.getUserDisplayName(userId);

                UserInfo infoUserId = new UserInfo("userId", userId);
                UserInfo infoUserDisplayName = new UserInfo("userDisplayName", userDisplayName);
                UserInfo infoUserPositions = new UserInfo("userPositions", userPositions);

                // UserID
                if(UserInfo.searchValueOf("userId").equals(""))
                    UserInfoDBManager.getInstance(this).createItem(infoUserId, true);
                else
                    UserInfoDBManager.getInstance(this).updateItem(infoUserId);

                // UserDisplayName
                if(UserInfo.searchValueOf("userDisplayName").equals(""))
                    UserInfoDBManager.getInstance(this).createItem(infoUserDisplayName, true);
                else
                    UserInfoDBManager.getInstance(this).updateItem(infoUserDisplayName);

                // UserPositions
                if(UserInfo.searchValueOf("userPositions").equals(""))
                    UserInfoDBManager.getInstance(this).createItem(infoUserPositions, true);
                else
                    UserInfoDBManager.getInstance(this).updateItem(infoUserPositions);

                UserInfo.searchValueOf("userId");
                UserInfo.searchValueOf("userDisplayName");
                UserInfo.searchValueOf("userPositions");

                txtUserPosition.setText(userPositions);
                txtUserDisplayName.setText(userDisplayName);


            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Echec rencontré lors du scan du QRCode", Toast.LENGTH_LONG).show();

//                Log.d("ViewApplicationParam", "QRCode scan Failed");
            }



        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Aucune donnée reçu!", Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_application_parameters_menu, menu);

        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        if(app.isStandAlone())
        {
            MenuItem services = menu.findItem(R.id.action_view_business_services);
            services.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(!displayToastErrorIfConfigNotOk()) return false;
        // Handle item selection
        switch (item.getItemId()) {
//            case R.id.btnFilterNote: commandListExpenseReport.filterExpenseReports(); return true;
            case R.id.action_view_businessCategory:
                commandViewApplicationParameters.categories.manageBusinessCategories();
                return true;
            case R.id.action_view_mileage_allowance_scales:
                commandViewApplicationParameters.categories.manageMileageAllowanceScales();
                return true;
            case R.id.action_view_businessProject:
                commandViewApplicationParameters.categories.manageBusinessProject();
                return true;
            case R.id.action_view_expenseReport:
                commandViewApplicationParameters.categories.manageExpenseReport();
                return true;
            case R.id.action_view_businessCustomer:
                commandViewApplicationParameters.categories.manageBusinessCustomer();
                return true;
            case R.id.action_view_business_services:
                commandViewApplicationParameters.categories.viewBusinessServices();
                return true;
            case R.id.action_save_applicationParameters:
                onClickButtonSave();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClickButtonSave() {
        Log.i("AppParametersAct", "onClickButtonSave()");

        Intent intent = new Intent(this, ListExpenseReportActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if(displayToastErrorIfConfigNotOk()) {
            Intent intent = new Intent(this, ListExpenseReportActivity.class);
            startActivity(intent);
        }
        super.onBackPressed();
    }
}