package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;

public class ViewBusinessCategoryActivity extends Activity {

    private BusinessCategory businessCategoryEdit;
    private CommandBusinessCategory commandBusinessCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_businesscategory);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_business_category_menu, menu);


        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        if(!app.isStandAlone())
        {
            MenuItem editButton = menu.findItem(R.id.action_edit_businessCategory);
            editButton.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.action_edit_businessCategory : commandBusinessCategory.onClickButtonEdit(businessCategoryEdit); return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void init() {
        commandBusinessCategory = new CommandBusinessCategory(this);
        Intent i = getIntent();
        businessCategoryEdit = (BusinessCategory)i.getSerializableExtra("businessCategoryEdit");

        Log.d("EditBusinessCategoryAct", "ActivityEdit");
        Log.d("-->", "item.getCode = " + businessCategoryEdit.getCode());
        Log.d("-->", "item.getBusinessCategoryId = " + businessCategoryEdit.getBusinessCategoryId());
        Log.d("-->", "item.getDescription = " + businessCategoryEdit.getDescription());
        Log.d("-->", "item.getFormType = " + businessCategoryEdit.getFormType());

        String description = businessCategoryEdit.getDescription();
        EditText txtDescriptionCategorie = (EditText) findViewById(R.id.txtViewDescriptionCategorie);

        txtDescriptionCategorie.setText(description);
        txtDescriptionCategorie.setKeyListener(null);

        String type = BusinessCategory.getFormTypeString(businessCategoryEdit.getFormType(), this);
        EditText txtTypeCategorie = (EditText) findViewById(R.id.txtViewTypeDescriptionCategorie);

        txtTypeCategorie.setText(type);
        txtTypeCategorie.setKeyListener(null);

//        RadioButton rdCategoryMontant = (RadioButton) findViewById(R.id.rdViewCategoryMontant);
//        RadioButton rdCategoryForfait = (RadioButton) findViewById(R.id.rdViewCategoryForfait);
//        RadioButton rdCategoryFrais = (RadioButton) findViewById(R.id.rdViewCategoryFrais);
//
//        if(businessCategoryEdit.getFormType() == BusinessCategory.FormType.amount) {
//            rdCategoryMontant.setChecked(true);
//            rdCategoryForfait.setChecked(false);
//            rdCategoryFrais.setChecked(false);
//        }
//        else if(businessCategoryEdit.getFormType() == BusinessCategory.FormType.flateRate) {
//            rdCategoryMontant.setChecked(false);
//            rdCategoryForfait.setChecked(true);
//            rdCategoryFrais.setChecked(false);
//        }
//        else if(businessCategoryEdit.getFormType() == BusinessCategory.FormType.mileageExpense) {
//            rdCategoryMontant.setChecked(false);
//            rdCategoryForfait.setChecked(false);
//            rdCategoryFrais.setChecked(true);
//        }
    }


}
