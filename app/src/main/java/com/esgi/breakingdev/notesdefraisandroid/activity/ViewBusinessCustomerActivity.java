package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCustomerAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;

public class ViewBusinessCustomerActivity extends Activity {

    private long businessCustomerId;
    public BusinessCustomer businessCustomer;    // Client à consulter
    private CommandBusinessCustomer commandBusinessCustomer;  // Interface de commandes de cette activity
    private BusinessCustomerAdapter listAdapter; // Adapteur pour la liste des lignes de frais

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_business_customer);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{
            init();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void init() throws Exception
    {
        commandBusinessCustomer = new CommandBusinessCustomer(this);
        businessCustomerId =  getIntent().getLongExtra("businessCustomerId",0);
        businessCustomer = (BusinessCustomer) BusinessCustomerAdapter.getInstance(this).getItemWithId(businessCustomerId);
        setTextFields();
    }


    /* Remplissage des informations sur la note de frais */
    private void setTextFields() {
        TextView txtViewDescriptionClient = (TextView)findViewById(R.id.txtViewDescriptionClient2);
        TextView txtViewCodeClient = (TextView)findViewById(R.id.txtViewCodeClient);

        txtViewDescriptionClient.setText(businessCustomer.getDescription());
        txtViewCodeClient.setText(businessCustomer.getCode());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_business_customer, menu);

        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        if(!app.isStandAlone())
        {
            MenuItem editButton = menu.findItem(R.id.action_edit_businessCustomer_View);
            editButton.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_businessCustomer_View: commandBusinessCustomer.editClient(); return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
