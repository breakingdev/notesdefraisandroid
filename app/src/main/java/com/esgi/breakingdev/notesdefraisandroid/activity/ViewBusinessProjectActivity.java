package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandBusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomCheckBox;

import java.util.ArrayList;
import java.util.List;

public class ViewBusinessProjectActivity extends Activity {

    private long businessProjectId;
    public BusinessProject businessProject;    // Note de frais à consulter
    private CommandBusinessProject commandBusinessProject;  // Interface de commandes de cette activity
    private BusinessProjectAdapter listAdapter; // Adapteur pour la liste des lignes de frais

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_business_project);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{
            init();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void init() throws Exception
    {
        commandBusinessProject = new CommandBusinessProject(this);
        businessProjectId =  getIntent().getLongExtra("businessProjectId",0);
        businessProject = (BusinessProject) BusinessProjectAdapter.getInstance(this).getItemWithId(businessProjectId);
        setTextFields();
    }


    /* Remplissage des informations sur la note de frais */
    private void setTextFields() {
        CheckBox checkBoxInternal = (CheckBox) findViewById(R.id.checkBoxInternal_View);
        TextView txtViewDescriptionProjet = (TextView)findViewById(R.id.txtViewDescriptionProjet);
        txtViewDescriptionProjet.setText(businessProject.getDescription());
        TextView fieldClient = (TextView) findViewById(R.id.textView2);
        TextView txtViewClientProjet = (TextView)findViewById(R.id.txtViewClientProjet);
        CheckBox checkBoxRebilling = (CheckBox)findViewById(R.id.checkBoxRebilling_View);

        // Chargement de la liste des clients
        if(businessProject.isInternal()) {
            checkBoxInternal.setChecked(true);
            txtViewClientProjet.setVisibility(View.GONE);
            fieldClient.setVisibility(View.GONE);
//            fieldClient.setText("Internal project");
        }
        else {
            checkBoxInternal.setChecked(false);
            txtViewClientProjet.setVisibility(View.VISIBLE);
            fieldClient.setVisibility(View.VISIBLE);
            String descriptionClient = "";
            BusinessExpensesApplication app = (BusinessExpensesApplication) getApplicationContext();
            if(app.isStandAlone())
                descriptionClient = BusinessCustomer.getCustomerDescription(businessProject.getIdClient());
            else
                descriptionClient = BusinessCustomer.getCustomerDescription(businessProject.getStringIdClient());
            txtViewClientProjet.setText(descriptionClient);
        }
        if(businessProject.getRefacturation())
            checkBoxRebilling.setChecked(true);
        else
            checkBoxRebilling.setChecked(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_business_project, menu);

        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        if(!app.isStandAlone())
        {
            MenuItem editButton = menu.findItem(R.id.action_edit_businessProject);
            editButton.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_businessProject: commandBusinessProject.editProject(); return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
