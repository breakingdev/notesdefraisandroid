package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.os.Bundle;
import android.app.Activity;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;

public class ViewBusinessServiceActivity extends Activity {

    private long businessServiceId;
    private BusinessService service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_business_service);
        init();
    }

    private void init()
    {
        long businessServiceId = getIntent().getLongExtra("businessServiceId",0);
        try {
            service = (BusinessService)BusinessServiceAdapter.getInstance(this).getItemWithId(businessServiceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setFields();
    }

    private void setFields()
    {
        TextView textDescription = (TextView) findViewById(R.id.txtViewDescriptionService);
        textDescription.setText(service.getDescription());

        TextView textInfos = (TextView) findViewById(R.id.txtViewInfoService);
        textInfos.setText(service.getInfos());
    }



}
