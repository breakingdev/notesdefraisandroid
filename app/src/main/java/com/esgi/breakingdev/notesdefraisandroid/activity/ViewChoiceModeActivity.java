package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.application.StandaloneDataSet;
import com.esgi.breakingdev.notesdefraisandroid.model.UserInfo;
import com.esgi.breakingdev.notesdefraisandroid.sql.UserInfoDBManager;

public class ViewChoiceModeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_choice_mode);


    }

    /* Affichage des notes de frais en mode Stand Alone ou en mode Serveur Web (synchro)*/
    public void launchNotesDeFrais(View view)
    {

        Intent intent = null;

        BusinessExpensesApplication app = ((BusinessExpensesApplication)getApplicationContext());

        String[] modes = getResources().getStringArray(R.array.application_modes_arrays);
        UserInfo userInfo = null;

        switch (view.getId()) {
            case R.id.btnWebServer:
                // ClientServer button
                try
                {
                    userInfo = new UserInfo("mode", modes[0]);
                    Log.i("ViewModeAct", "BEFORE ADD : modes[0] = " + modes[0]);
//                    UserInfo.listData.add(userInfo);
                    UserInfoDBManager.getInstance(this).createItem(userInfo, false);


                    Log.i("ViewModeAct", "AFTER ADD : mode = " + UserInfo.searchValueOf("mode"));

                    // On lance l'utilisateur sur l'écran de paramétrage, pour qu'il puisse scanner le QR Code
                    intent = new Intent(this, ViewApplicationParametersActivity.class);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btnStandAlone:
                // StandAlone button
                try
                {
                    userInfo = new UserInfo("mode", modes[1]);
                    Log.i("ViewModeAct", "BEFORE ADD : modes[1] = " + modes[1]);
                    UserInfoDBManager.getInstance(this).createItem(userInfo, false);


                    Log.i("ViewModeAct", "AFTER ADD : mode = " + UserInfo.searchValueOf("mode"));

                    // On créé un jeu de test par défaut
                    StandaloneDataSet standaloneDataSet = new StandaloneDataSet(this);
                    standaloneDataSet.generateBusinessCategory();
                    standaloneDataSet.generateMileageAllowanceScale();

                    // On emmène l'utilisateur sur l'écran de la liste des notes de frais
                    intent = new Intent(this, ListExpenseReportActivity.class);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

        startActivity(intent);
        finish();
    }
}
