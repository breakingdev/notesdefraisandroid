package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandViewExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.SimpleAmount;
import com.esgi.breakingdev.notesdefraisandroid.tools.CustomSpinner;
import com.esgi.breakingdev.notesdefraisandroid.tools.PhotoHandler;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.List;

public class ViewExpenseLineActivity extends Activity {

    private long expenseLineViewId;
    public ExpenseLine expenseLine;    // Note de frais à consulter
    private CommandViewExpenseLine commandViewExpenseLine;  // Interface de commandes de cette activity
    private ExpenseReport expenseReport = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_expense_line);
        commandViewExpenseLine = new CommandViewExpenseLine(this);
        expenseLineViewId = getIntent().getLongExtra("expenseLineViewId",0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init()
    {
        if(expenseLineViewId > 0){
            try {
                expenseLine = (ExpenseLine) ExpenseLineAdapter.getInstance(this, 0).getItemWithId(expenseLineViewId);
                expenseReport = (ExpenseReport) ExpenseReportAdapter.getInstance(this).getItemWithId(expenseLine.getExpenseReportId());
                ExpenseLineAdapter.getInstance(this, expenseReport.getExpenseReportId()).refresh();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }

        setTextFields();
    }


    /* Remplissage des informations sur la note de frais */
    private void setTextFields() {
        TextView txtViewNoteDeFrais = (TextView)findViewById(R.id.txtViewNoteDeFrais);
        TextView txtViewDescription = (TextView)findViewById(R.id.txtViewDescription);
        TextView txtViewRefacturable = (TextView)findViewById(R.id.txtViewRefacturable);
        TextView txtViewDateLigne = (TextView)findViewById(R.id.txtViewDateLigne);
        TextView txtViewCategorie = (TextView)findViewById(R.id.txtViewCategorie);

        EditText txtViewContenuElement1 = (EditText)findViewById(R.id.txtViewContenuElement1);
        TextView txtViewTitreElement1 = (TextView)findViewById(R.id.txtViewTitreElement1);

        EditText txtViewContenuElement2 = (EditText)findViewById(R.id.txtViewContenuElement2);
        TextView txtViewTitreElement2 = (TextView)findViewById(R.id.txtViewTitreElement2);

        txtViewNoteDeFrais.setText("" + expenseReport.getDescription());
        txtViewDescription.setText(expenseLine.getComment());

        BusinessExpensesApplication app = (BusinessExpensesApplication) getApplicationContext();
        SimpleDateFormat sdf = app.getDateFormatForGUI();
        txtViewDateLigne.setText(sdf.format(expenseLine.getDate()));

        TextView textTradingName = (TextView) findViewById(R.id.textTradingName_View);
        textTradingName.setText(expenseLine.getTradingName());

        TextView textCustomer = (TextView) findViewById(R.id.textLineCustomer);
        TextView textProject = (TextView) findViewById(R.id.textLineProject);

        if(expenseLine.isInternal()){
            textCustomer.setText(getString(R.string.internal));
            textProject.setText(getString(R.string.select_no_projet));
            txtViewRefacturable.setText(getString(R.string.no));
        }
        else {
            String descriptionClient = BusinessCustomer.getCustomerDescription(expenseLine.getCustomerId());
            textCustomer.setText(descriptionClient);
            if(expenseLine.getProjectId() == 0)
                textProject.setText(getString(R.string.select_no_projet));
            else
            {
                try {
                    BusinessProject businessProject = (BusinessProject) BusinessProjectAdapter.getInstance(this).getItemWithId(expenseLine.getProjectId());
                    textProject.setText(businessProject.getDescription());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(expenseLine.isRebilling())
                txtViewRefacturable.setText(getString(R.string.yes));
            else
                txtViewRefacturable.setText(getString(R.string.no));
        }

        // LISTE CAT DEPENSES
        List<Object> listCategories = ExpenseLine.businessCategoryList;

        String category = "";
        for(Object o : listCategories){
            BusinessCategory item = (BusinessCategory) o;

            if(item.getBusinessCategoryId() == expenseLine.getBusinessCategoryId()) {
                category = item.getDescription();
                break;
            }
        }
        txtViewCategorie.setText(category);


        int[] gridTextCellId = { R.id.txtViewTitreElement1,R.id.txtViewTitreElement2   };
        int[] gridEditCellId = { R.id.txtViewContenuElement1,R.id.txtViewContenuElement2   };


        CustomSpinner.changeAmountFieldsAndBusinessProof(this,expenseLine.getCategory(),gridTextCellId,gridEditCellId);

        boolean canHaveJustificatif = true;
        if(expenseLine.getCategory().equals(BusinessCategory.FormType.amount))
        {
            SimpleAmount amount = (SimpleAmount) expenseLine.getAmount();

            String amountTVA = String.format("%.2f", amount.getAmountTVA());
            String amountTTC = String.format("%.2f", amount.getAmountTTC());

            // On remplace les virgules par des points
            amountTVA = amountTVA.replace(",", ".");
            amountTTC = amountTTC.replace(",", ".");

            txtViewContenuElement1.setText(amountTVA);
            txtViewContenuElement2.setText(amountTTC);
        }
        else if(expenseLine.getCategory().equals(BusinessCategory.FormType.mileageExpense))
        {
            canHaveJustificatif = false;
            MileageExpenseAmount amount = (MileageExpenseAmount) expenseLine.getAmount();

            String amountKm = String.format("%.0f", amount.getKilometers());
            String amountHorsePower = String.format("%d", amount.getHorsePower());

            amountKm = amountKm.replace(",", ".");
            amountHorsePower = amountHorsePower.replace(",", ".");

            txtViewContenuElement1.setText(amountKm);
            txtViewContenuElement2.setText(amountHorsePower);
        }

//        expenseLine.getBusinessCategoryId()

        TextView textProofRef = (TextView) findViewById(R.id.txtViewProofRef);
        if(expenseLine.getProofReference().isEmpty())
            textProofRef.setText(getString(R.string.not_filled_in));
        else
            textProofRef.setText(expenseLine.getProofReference());

        ImageView imgJustificatif = (ImageView) findViewById(R.id.imgJustificatifLigne);
        if(canHaveJustificatif && expenseLine.getProofImage() != null) {
            imgJustificatif.setVisibility(View.VISIBLE);
            final Bitmap photo = BitmapFactory.decodeByteArray(expenseLine.getProofImage(), 0, expenseLine.getProofImage().length);
            imgJustificatif.setImageBitmap(photo);
            final Activity activity = this;
            imgJustificatif.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Intent intent = new Intent(activity, DisplayFullScreenImageActivity.class);
                    Uri bitmapUri = PhotoHandler.writeBitmapToFile(activity, photo, "fullscreenImg.jpg");
                    if (bitmapUri == null) return false;
                    intent.putExtra("imageUri", bitmapUri);
                    activity.startActivity(intent);
                    return false;
                }
            });
        }
        else
            imgJustificatif.setVisibility(View.GONE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_business_project, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.action_edit_businessProject: commandViewExpenseLine.editExpenseLine(expenseLine); return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        try {
            ExpenseLineAdapter.getInstance(this, expenseReport.getExpenseReportId());
        }catch(Exception e){
            e.printStackTrace();
        }
        super.onBackPressed();
    }
}
