package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandViewExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseLineServices;

import java.text.SimpleDateFormat;

public class ViewExpenseReportActivity extends Activity {

    private long expenseReportId;
    public ExpenseReport expenseReport;    // Note de frais à consulter
    private CommandViewExpenseReport commandViewExpenseReport;  // Interface de commandes de cette activity
    public ExpenseLineAdapter listAdapter; // Adapteur pour la liste des lignes de frais

    private void init()
    {
//        Log.d("ViewExpenseReportAct","expenseReportId = " + )
        if(expenseReportId > 0){
            try {
                expenseReport = (ExpenseReport) ExpenseReportAdapter.getInstance(this).getItemWithId(expenseReportId);
                listAdapter = ExpenseLineAdapter.getInstance(this,expenseReport.getExpenseReportId());

            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_expensereport);
        expenseReportId = getIntent().getLongExtra("ExpenseReportId",0);
        commandViewExpenseReport = new CommandViewExpenseReport(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();

        ListView listViewExpenseLines = (ListView) findViewById(R.id.listview_expense_lines);
        listViewExpenseLines.setAdapter(listAdapter);
        try {
            if(listAdapter == null) {
                Log.d("ViewExpenseReport", "list Adapter is null !");
                        return;
            }
            listAdapter.addListener(commandViewExpenseReport);
            listAdapter.refresh();
            setTextFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String listJsonLines =  new ExpenseLineServices().getListObject(expenseReport.getStringId());
//        Log.d("ViewExpenseReport", "onResume -> liste lignes de frais pour " + expenseReport.getDescription() + " = " + listJsonLines);
    }


    /* Remplissage des informations sur la note de frais */
    private void setTextFields() throws Exception {
        LinearLayout layoutStatus = (LinearLayout) findViewById(R.id.layoutStatus);
        RadioButton validatedState = (RadioButton) findViewById(R.id.radioButton_validated);
        RadioButton accountedState = (RadioButton) findViewById(R.id.radioButton_accounted);
        BusinessExpensesApplication app = (BusinessExpensesApplication) getApplicationContext();
        if(app.isStandAlone())
            layoutStatus.setVisibility(View.GONE);
        else {
            if (!expenseReport.getStatus().equals(ExpenseReport.Status.inEdition)) {
                validatedState.setChecked(true);
                if (expenseReport.getStatus().equals(ExpenseReport.Status.reject)) {
                    validatedState.setText("Rejected");
                    validatedState.setTextColor(Color.RED);
                }
                if (expenseReport.getStatus().equals(ExpenseReport.Status.inAccounting))
                    accountedState.setChecked(true);
            }
        }

        // Commentaire en cas d'état rejeté
        LinearLayout layoutRejected = (LinearLayout) findViewById(R.id.layoutRejected);
        if(!expenseReport.getValidatorComment().equals("") && expenseReport.getStatus().toString().equals(ExpenseReport.Status.reject.toString()))
        {
            layoutRejected.setVisibility(View.VISIBLE);
            TextView textView_expense_report_description = (TextView) findViewById(R.id.textView_expense_report_comment_rejected);
            textView_expense_report_description.setText(expenseReport.getValidatorComment());
        }
        else
        {
            layoutRejected.setVisibility(View.GONE);
        }

        TextView descriptionTxtView = (TextView)findViewById(R.id.textView_expense_report_description);
        descriptionTxtView.setText(expenseReport.getDescription());

        SimpleDateFormat sdf = ((BusinessExpensesApplication)getApplicationContext()).getDateFormatForGUI();
        TextView beginDateTxtView = (TextView)findViewById(R.id.textView_date_begin);
        beginDateTxtView.setText(sdf.format(expenseReport.getBeginDate()));

        TextView endDateTxtView = (TextView)findViewById(R.id.textView_date_end);
        endDateTxtView.setText(sdf.format(expenseReport.getEndDate()));

        TextView nbExpenseLinesTxtView = (TextView)findViewById(R.id.textView_total_expense_lines);
        nbExpenseLinesTxtView.setText(Integer.toString(listAdapter.totalDisplayedElements));

        TextView totalAmountTxtView = (TextView)findViewById(R.id.textview_total_amount);
        totalAmountTxtView.setText(String.format("%.2f", listAdapter.getTotalAmount()));

        if(!app.isStandAlone()) {
            TextView txtService = (TextView) findViewById(R.id.textViewService);
            BusinessService service = (BusinessService) BusinessServiceAdapter.getInstance(this).getItemWithId(expenseReport.getServiceId());
            if (service != null)
                txtService.setText(service.getDescription());
        }

        // Etat de la note
        Log.i("STATUSER", "Status = " + expenseReport.getStatus());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_expense_report, menu);


        if(expenseReport.getStatus().equals(ExpenseReport.Status.inValidation) || expenseReport.getStatus().equals(ExpenseReport.Status.inAccounting))
        {
            MenuItem editButton = menu.findItem(R.id.action_edit_expense_report);
            editButton.setVisible(false);
            MenuItem addExpenseLineButton = menu.findItem(R.id.action_add_expense_line);
            addExpenseLineButton.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_expense_report: commandViewExpenseReport.editExpenseReport(); return false;
            case R.id.action_add_expense_line: commandViewExpenseReport.addExpenseLineFor(expenseReport); return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        listAdapter.removeListener(commandViewExpenseReport);
//        ListView listViewExpenseLines = (ListView) findViewById(R.id.listview_expense_lines);
//        listViewExpenseLines.setAdapter(null);

    }


}
