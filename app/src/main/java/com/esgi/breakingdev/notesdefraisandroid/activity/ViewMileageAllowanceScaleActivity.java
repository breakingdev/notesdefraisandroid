package com.esgi.breakingdev.notesdefraisandroid.activity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.action.CommandListMileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;

public class ViewMileageAllowanceScaleActivity extends Activity {

    public MileageAllowanceScale mileageAllowanceScale;
    private CommandListMileageAllowanceScale commandListMileageAllowanceScale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_mileage_allowance_scale);

        init();
    }

    public void init()
    {
        commandListMileageAllowanceScale = new CommandListMileageAllowanceScale(this);
        mileageAllowanceScale = (MileageAllowanceScale) getIntent().getSerializableExtra("mileageAllowanceScaleView");

        setTextFields();
    }

    private void setTextFields() {
        TextView txtViewYear = (TextView)findViewById(R.id.txtViewYear);
        TextView txtViewVehicle = (TextView)findViewById(R.id.txtViewVehicle);
        TextView txtViewHorsePower = (TextView)findViewById(R.id.txtViewHorsePower);
        TextView txtViewMinDistance = (TextView)findViewById(R.id.txtViewMinDistance);
        TextView txtViewMaxDistance = (TextView)findViewById(R.id.txtViewMaxDistance);
        TextView txtViewRate = (TextView)findViewById(R.id.txtViewRate);
        TextView txtViewOffset = (TextView)findViewById(R.id.txtViewOffset);

        txtViewYear.setText("" + mileageAllowanceScale.getMileageAllowanceYear());
        txtViewVehicle.setText("" + MileageAllowanceScale.getVehicleString(mileageAllowanceScale.getVehicle(), this));
        txtViewHorsePower.setText("" + mileageAllowanceScale.getHorsePower());
        txtViewMinDistance.setText("" + mileageAllowanceScale.getMinDistance()+ " km");
        txtViewMaxDistance.setText("" + mileageAllowanceScale.getMaxDistance() + " km");
        txtViewRate.setText(String.format("%.03f", mileageAllowanceScale.getRate()));
        txtViewOffset.setText(String.format("%.0f",mileageAllowanceScale.getOffset()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_mileage_allowance_scale, menu);
        BusinessExpensesApplication app = (BusinessExpensesApplication) this.getApplicationContext();
        if(!app.isStandAlone())
        {
            MenuItem editButton = menu.findItem(R.id.action_edit_mileage_allowance_scale);
            editButton.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_mileage_allowance_scale: commandListMileageAllowanceScale.editBareme(); return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
