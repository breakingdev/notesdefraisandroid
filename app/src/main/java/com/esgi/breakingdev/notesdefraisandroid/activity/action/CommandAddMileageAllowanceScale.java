package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.content.Context;
import android.support.annotation.FloatRange;
import android.util.Log;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.AddMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.MileageAllowanceScaleListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.MileageAllowanceScalesAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.sql.MileageAllowanceScaleDBManager;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Tibo Mathieu on 27/05/2016.
 */
public class CommandAddMileageAllowanceScale{

    private Context context;

    public CommandAddMileageAllowanceScale(Context context)
    {
        this.context = context;
    }

    public void saveMileageAllowanceScale()
    {
        Log.d("CommandAddMileag..Scale","saveMileageAllowanceScale");
        AddMileageAllowanceScaleActivity activity = (AddMileageAllowanceScaleActivity) context;

        // YEAR

        EditText txtYear = (EditText) activity.findViewById(R.id.txtYear);
        int year = 0;
        int yearNow = Calendar.getInstance().get(Calendar.YEAR);
        try {
            year = Integer.parseInt(txtYear.getText().toString());
        }catch(NumberFormatException e)
        {
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: year is not a Number !");
        /* Comparaison entre date debut et date de fin */

            Toast.makeText(context, "L'année doit être numérique", Toast.LENGTH_LONG).show();

            return;
        }


        /* Comparaison entre date debut et date de fin */
        if(year == 0){
//            Log.d("CommandAddMileage","onClickButtonSave -> Year must be filled");
            Toast.makeText(context, "Le champ Année est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }

        if(year < 2000 || year > yearNow){
//            Log.d("CommandAddMileage", "saveMileageAllowanceScale: Year must be between 2000 and " + yearNow);
            Toast.makeText(context, "L'année doit être supérieure à 2000 et inférieure ou égale à l'année actuelle", Toast.LENGTH_LONG).show();
            return;
        }

        // VEHICLE
        MileageAllowanceScale.Vehicle vehicle = activity.vehicleSelected;


        // HORSE POWER
        EditText txtHorsePower = (EditText) activity.findViewById(R.id.txtHorsePower);

        int horsePowers = 0;
        try {
            horsePowers = Integer.parseInt(txtHorsePower.getText().toString());
        }catch(NumberFormatException e){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: horsePower is not a Number !");
            Toast.makeText(context, "La valeur des Chevaux doit être numérique", Toast.LENGTH_LONG).show();
            return;
        }
        if(horsePowers <= 0){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: horsePower must be positive !");
            Toast.makeText(context, "La valeur des Chevaux doit être supérieure à zéro", Toast.LENGTH_LONG).show();
            return;
        }

        // MIN DISTANCE
        EditText txtMinDistance = (EditText) activity.findViewById(R.id.txtMinDistance);

        int minDistance = 0;
        try {
            minDistance = Integer.parseInt(txtMinDistance.getText().toString());
        }catch(NumberFormatException e){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: MinDistance is not a Number !");

            Toast.makeText(context, "La distance minimale doit être numérique", Toast.LENGTH_LONG).show();
            return;
        }
        if(minDistance <= 0){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: MinDistance must be positive !");
            Toast.makeText(context, "La valeur de la distance minimale doit être supérieure à zéro", Toast.LENGTH_LONG).show();
            return;
        }


        // MAX DISTANCE
        EditText txtMaxDistance = (EditText) activity.findViewById(R.id.txtMaxDistance);
        int maxDistance = 0;

        try {
            maxDistance = Integer.parseInt(txtMaxDistance.getText().toString());
        }catch(NumberFormatException e){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: MaxDistance is not a Number !");
            Toast.makeText(context, "La distance maximale doit être numérique", Toast.LENGTH_LONG).show();
            return;
        }
        if(maxDistance <= minDistance ){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: MaxDistance must > MinDistance value !");
            Toast.makeText(context, "La valeur de la distance maximale doit être supérieure à zéro", Toast.LENGTH_LONG).show();
            return;
        }


        // RATE
        EditText txtRate = (EditText) activity.findViewById(R.id.txtRate);
        float rate = 0;

        try {
            rate = Float.parseFloat(txtRate.getText().toString());
            rate = (float)(Math.round(rate*1000)/1000.0d);
        }catch(NumberFormatException e){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: Rate is not a Number !");
            Toast.makeText(context, "Le Ratio doit être numérique", Toast.LENGTH_LONG).show();
            return;
        }
        if(rate <= 0 ){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: rate must > positive !");
            Toast.makeText(context, "La valeur du Ratio doit être supérieure à zéro", Toast.LENGTH_LONG).show();
            return;
        }

        // OFFSET
        EditText txtOffset = (EditText) activity.findViewById(R.id.txtOffset);
        float offset = 0;

        try {
            offset = Float.parseFloat(txtOffset.getText().toString());
            offset = (float)(Math.round(offset*100)/100.0d);

        }catch(NumberFormatException e){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: Offset is not a Number !");
            Toast.makeText(context, "L'Ajustement doit être numérique", Toast.LENGTH_LONG).show();
            return;
        }
        if(offset < 0 ){
            Log.d("CommandAddMileage", "saveMileageAllowanceScale: Offset must > positive !");
            Toast.makeText(context, "La valeur de l'Ajustement doit être supérieure ou égale à zéro", Toast.LENGTH_LONG).show();
            return;
        }

        if(vehicle.equals(MileageAllowanceScale.Vehicle.moto)){
            Toast.makeText(context, "La catégorie Moto n'est pas encore supportée", Toast.LENGTH_LONG).show();
            return;
        }
        Log.i("CommandAddMileage", "rate = " + rate + " & offset = " + offset);

        String strId = ""; //
        MileageAllowanceScale scale = new MileageAllowanceScale(0,strId,year,vehicle.toString(),horsePowers,minDistance,maxDistance,rate,offset);
        for(Object o : MileageExpenseAmount.mileageAllowanceScaleList)
        {
            MileageAllowanceScale item = (MileageAllowanceScale) o;
            if(item.equals(scale)){
                Log.d("CommandAddMileageActiv","This mileage allowance scale already exists !");
                Toast.makeText(context, "Ce barème kilométrique existe déjà", Toast.LENGTH_LONG).show();
                return;
            }
        }


        Log.i("CommandAddMileage", "scale.getRate( = " + scale.getRate() + " & scale.getOffset() = " + scale.getOffset());


        try {
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        activity.finish();
    }

}
