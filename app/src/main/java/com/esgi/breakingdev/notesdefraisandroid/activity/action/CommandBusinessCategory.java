package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.activity.AddBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.AddExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessCategoryListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;

/**
 * Created by Tibo Mathieu on 16/05/2016.
 */
public class CommandBusinessCategory  implements BusinessCategoryListener {

    private Context context;
    public CommandManageCategories categories;

    public CommandBusinessCategory(Context context) {
        this.context = context;
        this.categories = new CommandManageCategories(context);
    }

    public void addCategorieDeDepenses()
    {
        Intent intent = new Intent(context, AddBusinessCategoryActivity.class);
        context.startActivity(intent);
    }

    public void searchCategorieDeDepenses(String text)
    {
        ListBusinessCategoryActivity listBusinessCategoryActivity = (ListBusinessCategoryActivity) context;
        listBusinessCategoryActivity.listAdapter.getFilter().filter(text);
    }

    public void viewCategorieDeDepenses(BusinessCategory item)
    {
        Intent intent = new Intent(context, ViewBusinessCategoryActivity.class);
        intent.putExtra("businessCategoryEdit", item);
        context.startActivity(intent);
    }

    /* IMPLEMENTED FROM BusinessCategoryListener */
    @Override
    public void onClickCategory(BusinessCategory item) {
        // Lancement d'une nouvelle activity
        Log.d("ListBusinessCategoryAct", "onClickCategory");
        viewCategorieDeDepenses(item);
    }

    @Override
    public void onClickButtonDelete(BusinessCategory item) {
        Log.d("ListBusinessCategoryAct", "onClickButtonDelete");
        confirmDialogDeleteCategory(item);
    }

    private void confirmDialogDeleteCategory(BusinessCategory item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final BusinessCategory itemDelete = item;

        builder
                .setMessage("Êtes-vous sûr de vouloir supprimer cette catégorie ?")
                .setPositiveButton("Oui",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        try {
                            BusinessCategoryAdapter.getInstance(context).remove(itemDelete, true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    @Override
    public void onClickButtonEdit(BusinessCategory item) {
        Log.d("ListBusinessCategoryAct", "onClickButtonEdit");
        Intent intent = new Intent(context, EditBusinessCategoryActivity.class);
        Activity activity = (Activity) context;
        intent.putExtra("businessCategoryEdit", item);
        activity.startActivity(intent);
    }
}
