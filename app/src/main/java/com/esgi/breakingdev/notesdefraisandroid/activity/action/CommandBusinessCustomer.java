package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.activity.AddBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;

/**
 * Created by Jeremy on 31/05/2016.
 */
public class CommandBusinessCustomer {

    private Context context;
    public CommandManageCategories categories;

    public CommandBusinessCustomer(Context context) {
        this.context = context;
        this.categories = new CommandManageCategories(context);
    }

    public void addClient()
    {
        Intent intent = new Intent(context, AddBusinessCustomerActivity.class);
        context.startActivity(intent);
    }

    public void searchClient(String text)
    {
        ListBusinessCustomerActivity listBusinessCustomerActivity = (ListBusinessCustomerActivity) context;
        listBusinessCustomerActivity.listAdapter.getFilter().filter(text);
    }

    public void editClient(){
        // Bascule vers une nouvelle activity
        Log.d("CommandViewExpenseRe", "editExpenseReport");
        ViewBusinessCustomerActivity viewBusinessCustomerActivity = (ViewBusinessCustomerActivity) context;
        Intent intent = new Intent(context,EditBusinessCustomerActivity.class);
        intent.putExtra("businessCustomerEdit", viewBusinessCustomerActivity.businessCustomer);
        context.startActivity(intent);
    }

    public void viewClient(BusinessCustomer item)
    {
        Intent intent = new Intent(context, ViewBusinessCustomerActivity.class);
        intent.putExtra("businessCustomerId", item.getBusinessCustomerId());
        context.startActivity(intent);
    }

    public void filterExpenseReports() {
        Log.d("CommandListExpenseRep", "filterExpenseReports");
    }

}
