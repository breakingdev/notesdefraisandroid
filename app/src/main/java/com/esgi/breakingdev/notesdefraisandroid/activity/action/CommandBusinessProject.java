package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.activity.AddBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;

/**
 * Created by Jeremy on 31/05/2016.
 */
public class CommandBusinessProject {

    private Context context;
    public CommandManageCategories categories;

    public CommandBusinessProject(Context context) {
        this.context = context;
        this.categories = new CommandManageCategories(context);
    }

    public void addProjet()
    {
        if(ExpenseLine.businessCustomerList.isEmpty()){
//            Log.d("CommandBusinessProject","addProjet -> you must add a client first !");
            Toast toast = Toast.makeText(context, "L'ajout d'un client est nécessaire pour cette action", Toast.LENGTH_LONG);
            toast.show();
            return;
        }
        Intent intent = new Intent(context, AddBusinessProjectActivity.class);
        context.startActivity(intent);
    }


    public void editProject(){
        // Bascule vers une nouvelle activity
        Log.d("CommandBusinessProject", "editProject");
        ViewBusinessProjectActivity viewbusinessProject = (ViewBusinessProjectActivity) context;
        Intent intent = new Intent(context,EditBusinessProjectActivity.class);
        intent.putExtra("businessProjectEdit", viewbusinessProject.businessProject);
        context.startActivity(intent);
    }

    public void searchProjet(String text)
    {
        ListBusinessProjectActivity listBusinessProjectActivity = (ListBusinessProjectActivity) context;
        listBusinessProjectActivity.listAdapter.getFilter().filter(text);
    }

    public void viewProjet(BusinessProject item)
    {
        Intent intent = new Intent(context, ViewBusinessProjectActivity.class);
        intent.putExtra("businessProjectId", item.getBusinessProjectId());
        context.startActivity(intent);
    }

    public void filterExpenseReports() {
        Log.d("CommandListExpenseRepo", "filterExpenseReports");
    }



}
