package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditExpenseLineActivity;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.AbstractAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.EventInfo;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.SimpleAmount;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseLineServices;
import com.esgi.breakingdev.notesdefraisandroid.sql.EventInfoDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseLineDBManager;

import java.io.ByteArrayOutputStream;
import java.util.Date;

/**
 * Created by Tibo Mathieu on 01/06/2016.
 */
public class CommandEditExpenseLine {
    private Context context;

    public CommandEditExpenseLine(Context context){
        this.context = context;
    }

    public void saveExpenseLine()
    {
        Log.d("CommandEditExpenseLine", "saveExpenseLine");
        EditExpenseLineActivity activity = (EditExpenseLineActivity) context;

        DatePicker datePicker = (DatePicker) activity.findViewById(R.id.dpickerDateLigneFrais_Edit);
        Date date = BusinessExpensesApplication.getDateFromDatePicker(datePicker);

        ExpenseLine expenseLine = activity.expenseLine;
        Bitmap photo = activity.photo;
        ExpenseReport expenseReport;
        try {
            expenseReport = (ExpenseReport) ExpenseReportAdapter.getInstance(activity).getItemWithId(expenseLine.getExpenseReportId());
        }catch(Exception e){
            e.printStackTrace();
            return;
        }

        long expenseReportId = expenseReport.getExpenseReportId();
        String expenseReportStrId = expenseReport.getStringId();
        long businessCategoryId = activity.categorySelected.getBusinessCategoryId();
        String businessCategoryStringId = activity.categorySelected.getStringId();

        EditText commentTxtView = (EditText) activity.findViewById(R.id.txtDescriptionLigne);
        String comment = commentTxtView.getText().toString();

        if(comment.isEmpty()){
//            Log.d("AddExpenseLineActiv", "onClickButtonSave: comment field is empty !");
            Toast.makeText(context, "Le champ Motif est obligatoire", Toast.LENGTH_LONG).show();
            return;
        }


        AbstractAmount amount;
        EditText textTradingName  = (EditText) activity.findViewById(R.id.textTradingName_Edit);
        String tradingName = textTradingName.getText().toString();

        // GET CLIENT ID
        CheckBox checkBoxInternal = (CheckBox) activity.findViewById(R.id.checkBoxInternalExpense_Edit);
        Spinner lstCustomers = (Spinner)activity.findViewById(R.id.lstLineCustomers_Edit);

        long customerId = 0;
        String customerStrId = "";
        if(!activity.internalLine) { // ligne de frais non interne
            customerId = activity.customerSelected.getBusinessCustomerId();
            customerStrId = activity.customerSelected.getStringId();
        }

        // GET PROJECT ID
        long projectId = 0;
        String projectStrId = "";
        if(customerId > 0) {
            Spinner lstProjects = (Spinner) activity.findViewById(R.id.lstProjects_Edit);
            if(lstProjects.getSelectedItemPosition() > 0) {
                BusinessProject selectedProject = (BusinessProject) activity.listProjectsOfCustomer.get(lstProjects.getSelectedItemPosition() - 1);
                projectId = selectedProject.getBusinessProjectId();
                projectStrId = selectedProject.getStringId();
            }
        }

        boolean rebilling = false;
        if(projectId > 0) {
            Switch switchRebilling = (Switch) activity.findViewById(R.id.switchEditRebilling);
            rebilling = switchRebilling.isChecked();
        }

        EditText gridEditTextElement2,gridEditTextElement1;
        String proofRef = "";
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();

        boolean canHaveJustificatif = true;
        switch(activity.categorySelected.getFormType())
        {
            case amount:
                gridEditTextElement1 = (EditText) activity.findViewById(R.id.gridEditTextElement1_Edit);
                gridEditTextElement2 = (EditText) activity.findViewById(R.id.gridEditTextElement2_Edit);

                float ttc = 0, tva = 0;
                String ttcTxt = gridEditTextElement2.getText().toString();
                if(ttcTxt.isEmpty()) {
//                    Log.d("CommandEditExpenseLine", "saveExpenseLine: ttc field is empty !");
                    Toast.makeText(activity.getApplicationContext(), "Le champ ttc est vide !", Toast.LENGTH_LONG).show();

                    return;
                }
                try {
                    ttc = Float.parseFloat(ttcTxt);
                } catch (NumberFormatException e) {
                    Log.d("CommandEditExpenseLine", "saveExpenseLine: ttc is not a Number !");
                    // Afficher un Toast !
                    return;
                }

                String tvaTxt = gridEditTextElement1.getText().toString();
                if(!tvaTxt.isEmpty()) {
                    try {
                        tva = Float.parseFloat(tvaTxt);
                    } catch (NumberFormatException e) {
//                        Log.d("CommandEditExpenseLine","saveExpenseLine: tva is not a Number !");
                        Toast.makeText(activity.getApplicationContext(), "Le champ tva n'est pas un nombre !", Toast.LENGTH_LONG).show();

                        return;
                    }
                }
                amount = new SimpleAmount(expenseLine.getAmount().getAmountID(),expenseLine.getExpenseLineId(),expenseReportId,ttc,tva);
                /* Récupération du justificatif */
                EditText editTextRefProof = (EditText) activity.findViewById(R.id.editTextProofRef);
                proofRef = editTextRefProof.getText().toString();
                if(!app.isStandAlone() && proofRef.isEmpty()){
                    Toast.makeText(activity.getApplicationContext(), "La référence est obligatoire !", Toast.LENGTH_LONG).show();
                    return;
                }
                break;
            case mileageExpense:
                canHaveJustificatif = false;
                gridEditTextElement1 = (EditText) activity.findViewById(R.id.gridEditTextElement1_Edit);
                gridEditTextElement2 = (EditText) activity.findViewById(R.id.gridEditTextElement2_Edit);
                int distance = 0, hp = 0;

                String distanceTxt = gridEditTextElement1.getText().toString();
                if(distanceTxt.isEmpty()){
//                    Log.d("CommandEditExpenseLine", "saveExpenseLine: distance field is empty !");
                    Toast.makeText(activity.getApplicationContext(), "Le champ distance n'est pas rempli !", Toast.LENGTH_LONG).show();

                    return;
                }
                try {
                    distance = Integer.parseInt(distanceTxt);
                }catch(NumberFormatException e){
                    Log.d("CommandEditExpenseLine", "saveExpenseLine: distance is not a Number !");
                    return;
                }

                String hpTxt = gridEditTextElement2.getText().toString();
                if(hpTxt.isEmpty()){
                    Log.d("CommandEditExpenseLine", "saveExpenseLine: horse Power field is empty !");
                    // Afficher un Toast !
                    return;
                }
                try {
                    hp = Integer.parseInt(hpTxt);
                }catch(NumberFormatException e){
                    Log.d("CommandEditExpenseLine", "saveExpenseLine: horsePower is not a Number !");
                    return;
                }

                amount = new MileageExpenseAmount(expenseLine.getAmount().getAmountID(),expenseLine.getExpenseLineId(),expenseReportId,datePicker.getYear(),distance, MileageAllowanceScale.Vehicle.car.toString(),hp);
                if(!MileageExpenseAmount.doesScaleExistFor((MileageExpenseAmount)amount)){
                    Toast.makeText(activity.getApplicationContext(), "Il n'y a pas de barème kilométrique associé ", Toast.LENGTH_LONG).show();
                    return;
                }
                break;
            default:
            case flateRate:
                Log.d("CommandEditExpenseLine", "saveExpenseLine: Category not recognized");
                return;
        }

        byte[] imageByteArray = null;
        if(canHaveJustificatif && photo != null){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            imageByteArray = stream.toByteArray();
        }

        int lineNumber = expenseLine.getLineNumber();


        ExpenseLine newExpenseLine = new ExpenseLine(expenseLine.getExpenseLineId(),expenseLine.getStringId(),expenseReportId,expenseReportStrId,lineNumber,date,businessCategoryId,businessCategoryStringId,rebilling,comment,tradingName,customerId,customerStrId,projectId,projectStrId,amount,proofRef,imageByteArray);

        if(!app.isStandAlone()) {
            if(app.isServerConnectionActive()) {

                String newJsonER = newExpenseLine.getJsonObject().toString();
                ExpenseLineServices expenseLineServices = new ExpenseLineServices();
                String idExpenseLine = expenseLineServices.editObject(newJsonER);
                if (idExpenseLine.replaceAll("\n", "").replaceAll("\"", "").equals(newExpenseLine.getStringId())) {
                    try {
                        ExpenseLineDBManager.getInstance(context).updateItem(newExpenseLine);
                        ExpenseLineAdapter.getInstance(context, expenseReportId); // reset le expenseReportID des lignes
                        activity.finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                } else { // Refus du serveur
                    Log.i("CommandeEditExpenseRepo", "Ligne de frais non éditée - Considérée comme non conforme par le serveur");
                    Toast.makeText(activity.getApplicationContext(), "Cette ligne de frais est Considérée comme non conforme par le serveur", Toast.LENGTH_LONG).show();

                }
            }else{ // Hors connexion
                try {
                    ExpenseLineDBManager.getInstance(context).updateItem(newExpenseLine);
                    EventInfoDBManager.getInstance(context).createItem(new EventInfo(EventInfo.Action.EDIT.toString(), EventInfo.ObjectType.ExpenseLine.toString(),newExpenseLine.getExpenseLineId()));
                    ExpenseLineAdapter.getInstance(context, expenseReportId); // reset le expenseReportID des lignes
                    activity.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }


            }
        else{ // StandAlone
            try {
                ExpenseLineDBManager.getInstance(context).updateItem(newExpenseLine);
                ExpenseLineAdapter.getInstance(context, expenseReportId); // reset le expenseReportID des lignes
                Toast.makeText(activity.getApplicationContext(), "Echec de l'édition de la ligne de frais sur le serveur. Veuillez synchroniser", Toast.LENGTH_LONG).show();
                activity.finish();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }


    }

}
