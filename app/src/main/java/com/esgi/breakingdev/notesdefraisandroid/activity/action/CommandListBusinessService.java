package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessServiceActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewBusinessServiceActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessServiceListener;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;

/**
 * Created by Tibo Mathieu on 28/06/2016.
 */
public class CommandListBusinessService implements BusinessServiceListener {

    private Context context;
    public CommandManageCategories categories;

    public CommandListBusinessService(Context context) {
        this.context = context;
        this.categories = new CommandManageCategories(context);
    }

    public void searchBusinessService(String text)
    {
        ListBusinessServiceActivity listBusinessServiceActivity = (ListBusinessServiceActivity) context;
        listBusinessServiceActivity.listAdapter.getFilter().filter(text);
    }

    /* IMPLEMENTED FROM BusinessServiceListener */

    @Override
    public void onClickService(BusinessService item) {
        Log.i("ListBusinessServiceAct", "onClickService -> item = " + item.getDescription());
        Intent intent = new Intent(context, ViewBusinessServiceActivity.class);
        intent.putExtra("businessServiceId",item.getServiceId());
        context.startActivity(intent);
    }
}
