package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.activity.AddExpenseLineActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.AddExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.ExpenseReportListener;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseReportServices;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseReportDBManager;

/**
 * Created by Tibo Mathieu on 16/05/2016.
 */
public class CommandListExpenseReport implements ExpenseReportListener {

    private Context context;
    public CommandManageCategories categories;

    public CommandListExpenseReport(Context context) {
        this.context = context;
        this.categories = new CommandManageCategories(context);
    }

    public void addNotesDeFrais()
    {
        BusinessExpensesApplication app = (BusinessExpensesApplication)context.getApplicationContext();
        if(!app.isStandAlone() && BusinessService.businessServicesList.isEmpty()){
            Log.d("CommandListExpenseRepo","addNotesDeFrais -> You must have services before adding a note");
            return;
        }
        Intent intent = new Intent(context, AddExpenseReportActivity.class);
        context.startActivity(intent);
    }

    public void searchNotesDeFrais(String text)
    {
        ListExpenseReportActivity listExpenseReportActivity = (ListExpenseReportActivity) context;
        listExpenseReportActivity.listAdapter.getFilter().filter(text);
    }

    public void viewNotesDeFrais(ExpenseReport item)
    {
        Intent intent = new Intent(context, ViewExpenseReportActivity.class);
        intent.putExtra("ExpenseReportId",item.getExpenseReportId());
        context.startActivity(intent);
    }

    public void filterExpenseReports() {
        Log.d("CommandListExpenseRepor", "filterExpenseReports");
    }


    @Override
    public void onClickNote(ExpenseReport item) {
        // Lancement d'une nouvelle activity
        viewNotesDeFrais(item);

    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof CommandListExpenseReport) return true;
        return false;
    }

    @Override
    public void onClickButtonDelete(ExpenseReport item) {
        Log.d("ListExpRepAct", "onClickButtonDelete");
        Log.d("-->", "item.getExpenseReportId = " + item.getExpenseReportId());
        Log.d("-->", "item.getDescription = " + item.getDescription());

        confirmDialogDeleteExpenseReport(item);
    }

    private void confirmDialogDeleteExpenseReport(ExpenseReport item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final ExpenseReport itemDelete = item;

        builder
                .setMessage("Êtes-vous sûr de vouloir supprimer cette note de frais ?")
                .setPositiveButton("Oui",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        String idExpenseReport = itemDelete.getStringId();

                        Log.i("commandList", "idExpenseReport = " + idExpenseReport);

                        // Suppression sur le serveur
                        ExpenseReportServices expenseReportServices = new ExpenseReportServices();
                        expenseReportServices.deleteObject(idExpenseReport);

                        // Suppression sur l'appli
                        ListExpenseReportActivity listExpenseReportActivity = (ListExpenseReportActivity) context;
                        listExpenseReportActivity.listAdapter.remove(itemDelete, true);
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    @Override
    public void onClickButtonEdit(ExpenseReport item) {
        Log.d("ListExpRepAct", "onClickButtonEdit");
        Log.d("-->", "item.getExpenseReportId = " + item.getExpenseReportId());
        Log.d("-->", "item.getDescription = " + item.getDescription());

        Intent intent = new Intent(context, EditExpenseReportActivity.class);
        intent.putExtra("expenseReportEdit", item);
        context.startActivity(intent);
    }

    @Override
    public void onClickButtonSendInValidation(ExpenseReport expenseReport) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final ExpenseReport item = expenseReport;

        builder
                .setMessage("Êtes-vous sûr de vouloir envoyer cette ligne de frais en validation ?")
                .setPositiveButton("Oui",  new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        try
                        {
                            ExpenseReportServices expenseReportServices = new ExpenseReportServices();
                            expenseReportServices.editObject("/Sendinvalidation", "/" + item.getStringId());

                            item.setStatus(ExpenseReport.Status.inValidation.toString());

                            ExpenseReportDBManager.getInstance(context).updateItem(item);

                        } catch(Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }
    final Activity activity = (Activity) context;
    @Override
    public void onClickButtonAddLine(ExpenseReport item, int lineNumber) {
        Log.d("ListExpRepAct", "onClickButtonAddLine");
        Log.d("-->", "item.getExpenseReportId = " + item.getExpenseReportId());
        Log.d("-->", "item.getDescription = " + item.getDescription());

        /* S'il n'y a pas de catégories de dépense avertir l'utilisateur */
        if(ExpenseLine.businessCategoryList.isEmpty()){
//            Log.d("CommandListExpenseRep","onClickButtonAddLine -> Business category list is empty !");
            Toast.makeText(activity.getApplicationContext(), "La liste des catégories est vide !", Toast.LENGTH_LONG).show();

            return;
        }
        Intent intent = new Intent(context, AddExpenseLineActivity.class);
        intent.putExtra("expenseReport", item);
        intent.putExtra("lineNumber", lineNumber);
        context.startActivity(intent);
    }

}
