package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.activity.AddMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.MileageAllowanceScaleListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.MileageAllowanceScalesAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;

/**
 * Created by Tibo Mathieu on 26/05/2016.
 */
public class CommandListMileageAllowanceScale implements MileageAllowanceScaleListener {

    private Context context;
    public CommandManageCategories categories;

    public CommandListMileageAllowanceScale(Context context) {
        this.context = context;
        this.categories = new CommandManageCategories(context);
    }

    public void addMileageAllowanceScale()
    {
        Intent intent = new Intent(context, AddMileageAllowanceScaleActivity.class);
        context.startActivity(intent);
    }


    public void editBareme(){
        // Bascule vers une nouvelle activity
        Log.d("CommandMAS", "editMileageAllowanceScale");
        ViewMileageAllowanceScaleActivity viewMileageAllowanceScaleActivity = (ViewMileageAllowanceScaleActivity) context;
        Intent intent = new Intent(context,EditMileageAllowanceScaleActivity.class);
        intent.putExtra("mileageAllowanceScaleEdit", viewMileageAllowanceScaleActivity.mileageAllowanceScale);
        context.startActivity(intent);
    }

    /* IMPLEMENTED FROM  MileageAllowanceScaleListener */

    @Override
    public void onClickItem(MileageAllowanceScale item) {
        Log.d("CommandListMileageAllow", "onClickItem, id = " + item.getMileageAllowanceScaleId());
        Intent intent = new Intent(context, ViewMileageAllowanceScaleActivity.class);
        intent.putExtra("mileageAllowanceScaleView",item);
        context.startActivity(intent);
    }

    @Override
    public void onClickButtonEdit(MileageAllowanceScale item) {
        Log.d("CommandListMileageAllow", "onClickButtonEdit, id = " + item.getMileageAllowanceScaleId());
        Intent intent = new Intent(context, EditMileageAllowanceScaleActivity.class);
        intent.putExtra("mileageAllowanceScaleEdit",item);
        context.startActivity(intent);
    }

    @Override
    public void onClickButtonDelete(MileageAllowanceScale item) {
        Log.d("CommandListMileageAllow", "onClickButtonDelete, id = " + item.getMileageAllowanceScaleId());
        confirmDialogDeleteMileageAllowanceScale(item);
    }


    private void confirmDialogDeleteMileageAllowanceScale(MileageAllowanceScale item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final MileageAllowanceScale itemDelete = item;

        builder
                .setMessage("Êtes-vous sûr de vouloir supprimer ce barème de frais kilométriques ?")
                .setPositiveButton("Oui",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        try {
                            MileageAllowanceScalesAdapter.getInstance(context).remove(itemDelete, true);
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }
}
