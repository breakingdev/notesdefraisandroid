package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessServiceActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewApplicationParametersActivity;

/**
 * Created by Tibo Mathieu on 28/06/2016.
 */
public class CommandManageCategories {
    public enum Direction{ Left,Right};
    private Context context;
    static String INTENT_TRANSITION_NAME = "transition";

    /* Private Methods */
    private void setIntentForTransition(Intent intent, Direction direction,Activity activity)
    {
        if(direction.equals(Direction.Right)) {
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            intent.putExtra(INTENT_TRANSITION_NAME, Direction.Right.toString());
        }
        else
            intent.putExtra(INTENT_TRANSITION_NAME,Direction.Left.toString());
    }

    public void getIntentForTransition(Activity activity)
    {
        String direction = activity.getIntent().getStringExtra(INTENT_TRANSITION_NAME);
        if(direction != null && !direction.isEmpty() ) {
            if(direction.equals(CommandManageCategories.Direction.Left.toString()))
                activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            else
                activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

        }
    }

    /* CONSTRUCTOR */
    public CommandManageCategories(Context context)
    {
        this.context = context;
    }

    /* METHODS */
    public void manageExpenseReport()   {       manageExpenseReport(false, null);    }
    public void manageExpenseReport(boolean withTransition, Direction direction)
    {
        Activity activity = (Activity) context;
        Intent intent = new Intent(context, ListExpenseReportActivity.class);
        if(withTransition)
            setIntentForTransition(intent,direction,activity);
        context.startActivity(intent);
        activity.finish();
    }

    public void manageBusinessCategories(){       manageBusinessCategories(false, null);    }
    public void manageBusinessCategories(boolean withTransition, Direction direction)
    {
        Activity activity = (Activity)context;

        Intent intent = new Intent(context, ListBusinessCategoryActivity.class);
        if(withTransition)
            setIntentForTransition(intent,direction,activity);

        context.startActivity(intent);
        activity.finish();
    }


    public void manageMileageAllowanceScales(){
        manageMileageAllowanceScales(false, null);
    }
    public void manageMileageAllowanceScales(boolean withTransition, Direction direction)
    {
        Activity activity = (Activity) context;
        Intent intent = new Intent(context, ListMileageAllowanceScaleActivity.class);
        if(withTransition)
            setIntentForTransition(intent,direction,activity);
        context.startActivity(intent);
        activity.finish();
    }

    public void manageBusinessCustomer(){
        manageBusinessCustomer(false, null);
    }
    public void manageBusinessCustomer(boolean withTransition, Direction direction)
    {
        Activity activity = (Activity) context;
        Intent intent = new Intent(context, ListBusinessCustomerActivity.class);
        if(withTransition)
            setIntentForTransition(intent,direction,activity);
        context.startActivity(intent);
        activity.finish();
    }

    public void manageBusinessProject(){
        manageBusinessProject(false, null);
    }
    public void manageBusinessProject(boolean withTransition, Direction direction)
    {
        Activity activity = (Activity) context;
        Intent intent = new Intent(context, ListBusinessProjectActivity.class);
        if(withTransition)
            setIntentForTransition(intent,direction,activity);
        context.startActivity(intent);
        activity.finish();
    }

    public void manageApplicationParameters(){
        manageApplicationParameters(false, null);
    }
    public void manageApplicationParameters(boolean withTransition, Direction direction)
    {
        Activity activity = (Activity) context;
        Intent intent = new Intent(context, ViewApplicationParametersActivity.class);
        if(withTransition)
            setIntentForTransition(intent,direction,activity);
        context.startActivity(intent);
        activity.finish();
    }

    public void viewBusinessServices(){
        viewBusinessServices(false, null);
    }
    public void viewBusinessServices(boolean withTransition, Direction direction)
    {
        Activity activity = (Activity) context;
        Intent intent = new Intent(context, ListBusinessServiceActivity.class);
        if(withTransition)
            setIntentForTransition(intent,direction,activity);
        context.startActivity(intent);
        activity.finish();
    }



}
