package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.activity.AddExpenseLineActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.AddExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCategoryActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessCustomerActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListBusinessProjectActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListMileageAllowanceScaleActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewApplicationParametersActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.ExpenseReportListener;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;

/**
 * Created by Tibo Mathieu on 16/05/2016.
 */
public class CommandViewApplicationParameters {

    private Context context;
    public CommandManageCategories categories;

    public CommandViewApplicationParameters(Context context) {
        this.context = context;
        this.categories = new CommandManageCategories(context);
    }

    public void viewNotesDeFrais(ExpenseReport item)
    {
        Intent intent = new Intent(context, ViewExpenseReportActivity.class);
        intent.putExtra("ExpenseReportId",item.getExpenseReportId());
        context.startActivity(intent);
    }

}
