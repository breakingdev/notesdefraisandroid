package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.activity.EditExpenseLineActivity;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;

/**
 * Created by Tibo Mathieu on 02/06/2016.
 */
public class CommandViewExpenseLine {

    private Context context;

    public CommandViewExpenseLine(Context context){
        this.context = context;
    }

    public void editExpenseLine(ExpenseLine expenseLine)
    {
        Log.d("CommandViewExpenseLine", "editExpenseLine expenseLine = " + expenseLine.getExpenseLineId());
        Intent intent = new Intent(context, EditExpenseLineActivity.class);
        intent.putExtra("expenseLineEditId",expenseLine.getExpenseLineId() );
        context.startActivity(intent);
    }
}
