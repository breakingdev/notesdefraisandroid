package com.esgi.breakingdev.notesdefraisandroid.activity.action;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.AddExpenseLineActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditExpenseLineActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.EditExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ListExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewExpenseLineActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.ExpenseLineListener;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;

import java.util.List;

/**
 * Created by Tibo Mathieu on 18/05/2016.
 */
public class CommandViewExpenseReport implements ExpenseLineListener{
    private Context context;

    public CommandViewExpenseReport(Context context) {
        this.context = context;
    }

    public void editExpenseReport(){
        // Bascule vers une nouvelle activity
        Log.d("CommandViewExpenseRe", "editExpenseReport");
        ViewExpenseReportActivity viewExpenseReportActivity = (ViewExpenseReportActivity) context;
        Intent intent = new Intent(context,EditExpenseReportActivity.class);
        intent.putExtra("expenseReportEdit", viewExpenseReportActivity.expenseReport);
        context.startActivity(intent);
    }

    public void addExpenseLineFor(ExpenseReport expenseReport){
        /* S'il n'y a pas de catégories de dépense avertir l'utilisateur */
        if(ExpenseLine.businessCategoryList.isEmpty()){
            Log.d("CommandListExpenseRe","addExpenseLine -> Business category list is empty !");
            // afficher un
            return;
        }

        // Bascule vers une nouvelle activity
        ViewExpenseReportActivity viewExpenseReportActivity = (ViewExpenseReportActivity) context;

        Intent intent = new Intent(context, AddExpenseLineActivity.class);
        intent.putExtra("expenseReport",viewExpenseReportActivity.expenseReport );
        intent.putExtra("lineNumber",viewExpenseReportActivity.listAdapter.totalDisplayedElements+1 );
        context.startActivity(intent);
    }

    @Override
    public void onClickExpenseLine(ExpenseLine item) {
        Log.d("CommandViewExpenseRe", "onClickExpenseLine amount = "+ item.getAmountValue());

        Intent intent = new Intent(context, ViewExpenseLineActivity.class);
        intent.putExtra("expenseLineViewId",item.getExpenseLineId());
        context.startActivity(intent);
    }

    @Override
    public void onClickButtonEdit(ExpenseLine item) {
        Log.d("CommandViewExpenseRe", "onClickButtonEdit amount = "+ item.getAmountValue());
        Intent intent = new Intent(context, EditExpenseLineActivity.class);
        intent.putExtra("expenseLineEditId",item.getExpenseLineId() );
        context.startActivity(intent);
    }

    @Override
    public void onClickButtonDelete(ExpenseLine item) {
        Log.d("CommandViewExpenseRe", "onClickButtonDelete amount = " + item.getAmountValue());
        confirmDialogDeleteExpenseLine(item);
    }




    private void confirmDialogDeleteExpenseLine(ExpenseLine item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final ExpenseLine itemDelete = item;

        builder
                .setMessage("Êtes-vous sûr de vouloir supprimer cette ligne de frais ?")
                .setPositiveButton("Oui",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code

                        // Utiliser le String ID
                        long idExpenseReport = itemDelete.getExpenseReportId();

                        Log.i("commandList", "idExpenseReport = " + idExpenseReport);
//
                       // Suppression sur le serveur
//                        HttpServices httpServices = new HttpServices();
//                        httpServices.deleteObject(idExpenseReport);

                        // Suppression sur l'appli

                        ExpenseLineAdapter adapter;
                        try {
                            adapter = ExpenseLineAdapter.getInstance(context, itemDelete.getExpenseReportId());
                            adapter.remove(itemDelete, true);
                            adapter.refresh();

                        }catch(Exception e){
                            e.printStackTrace();
                            return;
                        }
                        ViewExpenseReportActivity activity = (ViewExpenseReportActivity) context;

                        /* Mise à jour du montant total et du nombre de lignes de frais sur la page  */
                        TextView nbExpenseLinesTxtView = (TextView)activity.findViewById(R.id.textView_total_expense_lines);
                        nbExpenseLinesTxtView.setText(Integer.toString(adapter.totalDisplayedElements));

                        TextView totalAmountTxtView = (TextView)activity.findViewById(R.id.textview_total_amount);
                        totalAmountTxtView.setText(String.format("%.2f", adapter.getTotalAmount()));
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof CommandViewExpenseReport) return true;
        return false;
    }
}
