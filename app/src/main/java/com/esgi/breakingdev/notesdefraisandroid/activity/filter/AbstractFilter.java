package com.esgi.breakingdev.notesdefraisandroid.activity.filter;

import android.widget.Filter;

import com.esgi.breakingdev.notesdefraisandroid.adapter.AbstractArrayAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 17/05/2016.
 */
abstract public class AbstractFilter extends Filter {

    protected AbstractArrayAdapter adapter;

    public AbstractFilter(AbstractArrayAdapter adapter) {
        this.adapter = adapter;
    }

    abstract boolean objectFillConditions(CharSequence constraint,Object object);
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults filterResults = new FilterResults();
        ArrayList<Object> tempList=new ArrayList<Object>();
        List<Object> dbListObjects  = adapter.getDbListObjects();
        if(constraint != null && constraint.length() != 0 && dbListObjects != null) {

//                    Log.d("getFilter", "performFiltering " + constraint.toString());
            for (int i=0; i<dbListObjects.size();++i) {

                Object item = dbListObjects.get(i);
                if(objectFillConditions(constraint,item))
                    tempList.add(item);
            }
        }
        filterResults.values = tempList;
        filterResults.count = tempList.size();
        return filterResults;
    }

    @Override
    protected void publishResults(CharSequence contraint, FilterResults results) {
        adapter.clear(false);
        if (results.count > 0) {
            ArrayList<Object> excluded = (ArrayList<Object>) results.values;
            for(Object o : excluded)
                try {
                    adapter.add(o, false, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
        else {          // Exécute un refresh depuis la base si le champ de recherche est vide
            if(contraint.length() == 0){
                try {
                    adapter.refresh();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            adapter.notifyDataSetInvalidated();
        }
    }
}
