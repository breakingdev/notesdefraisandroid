package com.esgi.breakingdev.notesdefraisandroid.activity.filter;

import com.esgi.breakingdev.notesdefraisandroid.adapter.AbstractArrayAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;

/**
 * Created by Tibo Mathieu on 17/05/2016.
 */
public class BusinessCategoryFilter extends AbstractFilter {
    public BusinessCategoryFilter(AbstractArrayAdapter adapter) {
        super(adapter);
    }

    @Override
    boolean objectFillConditions(CharSequence constraint, Object object) {
        BusinessCategory item = (BusinessCategory) object;

        String code = item.getDescription();
        String constraintStr = constraint.toString();
        if (code.toUpperCase().contains(constraintStr.toUpperCase())) {
            return true;
//                            Log.d("getFilter","added item " + ((ExpenseReport) dbListObjects.get(i)).getDescription());
        }
        return false;
    }
}
