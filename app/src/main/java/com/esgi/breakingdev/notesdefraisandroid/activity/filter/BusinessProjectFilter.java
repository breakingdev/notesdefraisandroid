package com.esgi.breakingdev.notesdefraisandroid.activity.filter;

import com.esgi.breakingdev.notesdefraisandroid.adapter.AbstractArrayAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;

/**
 * Created by Jeremy on 19/05/2016.
 *
 * IMPORTANT : Les Models des Projets n'est pas encore implémenté
 * Le code suivant est une copie de BusinessCategory : Penser à mettre à jour les références
 *
 */

public class BusinessProjectFilter extends AbstractFilter {
    public BusinessProjectFilter(AbstractArrayAdapter adapter) {
        super(adapter);
    }

    @Override
    boolean objectFillConditions(CharSequence constraint, Object object) {
        BusinessProject item = (BusinessProject) object;

        String code = item.getDescription();
        String constraintStr = constraint.toString();
        if (code.toUpperCase().contains(constraintStr.toUpperCase())) {
            return true;
//                            Log.d("getFilter","added item " + ((ExpenseReport) dbListObjects.get(i)).getDescription());
        }
        return false;
    }
}
