package com.esgi.breakingdev.notesdefraisandroid.activity.filter;

import com.esgi.breakingdev.notesdefraisandroid.adapter.AbstractArrayAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;

/**
 * Created by Tibo Mathieu on 28/06/2016.
 */
public class BusinessServiceFilter extends AbstractFilter {
    public BusinessServiceFilter(AbstractArrayAdapter adapter) {
        super(adapter);
    }

    @Override
    boolean objectFillConditions(CharSequence constraint, Object object) {
        BusinessService item = (BusinessService) object;

        String code = item.getDescription();
        String constraintStr = constraint.toString();
        if (code.toUpperCase().contains(constraintStr.toUpperCase())) {
            return true;
//                            Log.d("getFilter","added item " + ((ExpenseReport) dbListObjects.get(i)).getDescription());
        }
        return false;
    }
}
