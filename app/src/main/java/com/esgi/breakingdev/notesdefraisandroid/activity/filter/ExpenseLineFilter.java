package com.esgi.breakingdev.notesdefraisandroid.activity.filter;

import android.widget.Filter;

import com.esgi.breakingdev.notesdefraisandroid.adapter.AbstractArrayAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;

import java.util.List;

/**
 * Created by Tibo Mathieu on 18/05/2016.
 */
public class ExpenseLineFilter extends AbstractFilter {
    public ExpenseLineFilter(AbstractArrayAdapter adapter) {
        super(adapter);
    }

    @Override
    boolean objectFillConditions(CharSequence constraint, Object object)
    {
        ExpenseLine item = (ExpenseLine) object;

        long expenseReportId = Long.parseLong(constraint.toString());
        if (expenseReportId == item.getExpenseReportId()) {
            if(adapter instanceof ExpenseLineAdapter) {
                ((ExpenseLineAdapter) adapter).expenseLinesIdListMap.put(item.getExpenseLineId(), item.getLineNumber());
            }
            return true;
        }
        return false;
    }

}
