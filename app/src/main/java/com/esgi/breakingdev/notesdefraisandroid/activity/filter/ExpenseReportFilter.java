package com.esgi.breakingdev.notesdefraisandroid.activity.filter;

import com.esgi.breakingdev.notesdefraisandroid.adapter.AbstractArrayAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;

/**
 * Created by Tibo Mathieu on 17/05/2016.
 */
public class ExpenseReportFilter extends AbstractFilter{
    public ExpenseReportFilter(AbstractArrayAdapter adapter) {
        super(adapter);
    }

    @Override
    boolean objectFillConditions(CharSequence constraint,Object object) {
        ExpenseReport item = (ExpenseReport) object;

        String description = item.getDescription();
        String constraintStr = constraint.toString();
        if (description.toUpperCase().contains(constraintStr.toUpperCase())) {
            return true;
//                            Log.d("getFilter","added item " + ((ExpenseReport) dbListObjects.get(i)).getDescription());
        }
        return false;
    }
}
