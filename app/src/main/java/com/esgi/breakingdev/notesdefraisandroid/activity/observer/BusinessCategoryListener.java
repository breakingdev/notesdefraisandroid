package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;

/**
 * Created by Tibo Mathieu on 17/05/2016.
 */
public interface BusinessCategoryListener {
    public void onClickCategory(BusinessCategory item);
    public void onClickButtonDelete(BusinessCategory item);
    public void onClickButtonEdit(BusinessCategory item);
}
