package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import android.view.View;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 17/05/2016.
 */
public interface BusinessCategorySubject {
    List<BusinessCategoryListener> observers = new ArrayList<BusinessCategoryListener>();

    public void addListener(BusinessCategoryListener observer);
    public void removeListener(BusinessCategoryListener observer);
    public void clearListeners();

    public void setBusinessCategoryListener(BusinessCategory item);
    public void sendButtonDeleteListener(BusinessCategory item);
    public void sendButtonEditListener(BusinessCategory item);


    public View.OnClickListener itemListOnClickListener(final BusinessCategory item);
}
