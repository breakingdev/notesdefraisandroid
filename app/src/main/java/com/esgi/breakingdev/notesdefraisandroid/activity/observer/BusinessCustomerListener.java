package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;

/**
 * Created by Jeremy on 19/05/2016.
 *
 * IMPORTANT : Les Models des Clients n'est pas encore implémenté
 * Le code suivant est une copie de BusinessCategory : Penser à mettre à jour les références
 *
 */
public interface BusinessCustomerListener {
    public void onClickCustomer(BusinessCustomer item);
    public void onClickButtonDelete(BusinessCustomer item);
    public void onClickButtonEdit(BusinessCustomer item);
}
