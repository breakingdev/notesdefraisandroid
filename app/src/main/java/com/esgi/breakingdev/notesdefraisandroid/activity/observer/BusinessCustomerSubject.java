package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import android.view.View;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jeremy on 19/05/2016.
 *
 * IMPORTANT : Les Models des Clients n'est pas encore implémenté
 * Le code suivant est une copie de BusinessCategory : Penser à mettre à jour les références
 *
 */
public interface BusinessCustomerSubject {
    List<BusinessCustomerListener> observers = new ArrayList<BusinessCustomerListener>();

    public void addListener(BusinessCustomerListener observer);
    public void removeListener(BusinessCustomerListener observer);
    public void clearListeners();

    public void setBusinessCustomerListener(BusinessCustomer item);
    public void sendButtonDeleteListener(BusinessCustomer item);
    public void sendButtonEditListener(BusinessCustomer item);


    public View.OnClickListener itemListOnClickListener(final BusinessCustomer item);
}
