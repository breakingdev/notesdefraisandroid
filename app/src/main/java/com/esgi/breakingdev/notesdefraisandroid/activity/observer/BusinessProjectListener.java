package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;

/**
 * Created by Jeremy on 19/05/2016.
 *
 * IMPORTANT : Les Models des Projets n'est pas encore implémenté
 * Le code suivant est une copie de BusinessCategory : Penser à mettre à jour les références
 *
 */
public interface BusinessProjectListener {
    public void onClickProject(BusinessProject item);
    public void onClickButtonDelete(BusinessProject item);
    public void onClickButtonEdit(BusinessProject item);
}
