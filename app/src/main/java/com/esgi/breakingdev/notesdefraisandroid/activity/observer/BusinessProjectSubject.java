package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import android.view.View;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jeremy on 19/05/2016.
 *
 * IMPORTANT : Les Models des Clients n'est pas encore implémenté
 * Le code suivant est une copie de BusinessCategory : Penser à mettre à jour les références
 *
 */
public interface BusinessProjectSubject {
    List<BusinessProjectListener> observers = new ArrayList<BusinessProjectListener>();

    public void addListener(BusinessProjectListener observer);
    public void removeListener(BusinessProjectListener observer);
    public void clearListeners();

    public void setBusinessProjectListener(BusinessProject item);
    public void sendButtonDeleteListener(BusinessProject item);
    public void sendButtonEditListener(BusinessProject item);


    public View.OnClickListener itemListOnClickListener(final BusinessProject item);
}
