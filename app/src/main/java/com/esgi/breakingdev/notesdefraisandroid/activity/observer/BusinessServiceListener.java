package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;

/**
 * Created by Tibo Mathieu on 28/06/2016.
 */
public interface BusinessServiceListener {
    public void onClickService(BusinessService item);
}
