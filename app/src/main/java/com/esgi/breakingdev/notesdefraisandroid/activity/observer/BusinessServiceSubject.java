package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import android.view.View;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 28/06/2016.
 */
public interface BusinessServiceSubject  {
    List<BusinessServiceListener> observers = new ArrayList<>();

    public void addListener(BusinessServiceListener observer);
    public void removeListener(BusinessServiceListener observer);
    public void clearListeners();

    public void setBusinessServiceListener(BusinessService item);

    public View.OnClickListener itemListOnClickListener(final BusinessService item);
}
