package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;

/**
 * Created by Tibo Mathieu on 18/05/2016.
 */
public interface ExpenseLineListener {
    public void onClickExpenseLine(ExpenseLine item);
    public void onClickButtonEdit(ExpenseLine item);
    public void onClickButtonDelete(ExpenseLine item);
}
