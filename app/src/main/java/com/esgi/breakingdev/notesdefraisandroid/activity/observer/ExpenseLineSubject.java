package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import android.view.View;

import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;

import java.util.ArrayList;

/**
 * Created by Tibo Mathieu on 18/05/2016.
 */
public interface ExpenseLineSubject  {
    ArrayList<ExpenseLineListener> observers = new ArrayList<ExpenseLineListener>();

    public void addListener(ExpenseLineListener observer);
    public void removeListener(ExpenseLineListener observer);
    public void clearListeners();
    public void setExpenseLineListener(ExpenseLine item);
    public View.OnClickListener itemListOnClickListener(ExpenseLine item);
    public void sendButtonEditExpenseLine(ExpenseLine item);
    public void sendButtonDeleteExpenseLine(ExpenseLine item);
}
