package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;

/**
 * Created by Tibo Mathieu on 12/05/2016.
 *
 * Observer pour les items de la liste des notes de frais
 */
public interface ExpenseReportListener {
    public void onClickNote(ExpenseReport item);
    public void onClickButtonDelete(ExpenseReport item);
    public void onClickButtonEdit(ExpenseReport item);
    public void onClickButtonSendInValidation(ExpenseReport item);
    public void onClickButtonAddLine(ExpenseReport item, int lineNumber);

}
