package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import android.view.View;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;

import java.util.ArrayList;

/**
 * Created by Tibo Mathieu on 12/05/2016.
 */
 public interface ExpenseReportSubject {

    ArrayList<ExpenseReportListener> observers = new ArrayList<ExpenseReportListener>();

    public void addListener(ExpenseReportListener observer);
    public void removeListener(ExpenseReportListener observer);
    public void clearListeners();
    public void setExpenseReportListener(ExpenseReport item);
    public View.OnClickListener itemListOnClickListener(ExpenseReport item);
    public void sendButtonDeleteListener(ExpenseReport item);
    public void sendButtonEditListener(ExpenseReport item);
    public void sendButtonAddLineListener(ExpenseReport item,int lineNumber);

 }


