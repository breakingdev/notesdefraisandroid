package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;

/**
 * Created by Tibo Mathieu on 30/05/2016.
 */
public interface MileageAllowanceScaleListener {
    public void onClickItem(MileageAllowanceScale item);
    public void onClickButtonEdit(MileageAllowanceScale item);
    public void onClickButtonDelete(MileageAllowanceScale item);
}
