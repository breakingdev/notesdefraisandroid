package com.esgi.breakingdev.notesdefraisandroid.activity.observer;

import android.view.View;

import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;

import java.util.ArrayList;

/**
 * Created by Tibo Mathieu on 30/05/2016.
 */
public interface MileageAllowanceScaleSubject {
    ArrayList<MileageAllowanceScaleListener> observers = new ArrayList<MileageAllowanceScaleListener>();

    public void addListener(MileageAllowanceScaleListener observer);
    public void removeListener(MileageAllowanceScaleListener observer);
    public void clearListeners();
    public void setMileageAllowanceScaleListeners(MileageAllowanceScale item);
    public View.OnClickListener itemListOnClickListener(MileageAllowanceScale item);
    public void sendButtonEditScale(MileageAllowanceScale item);
    public void sendButtonDeleteScale(MileageAllowanceScale item);
}
