package com.esgi.breakingdev.notesdefraisandroid.activity.special;

import android.app.Activity;

import com.esgi.breakingdev.notesdefraisandroid.activity.special.IActivityWithCategorySelection;

/**
 * Created by Tibo Mathieu on 01/06/2016.
 */
public abstract class ActivityWithCategorySelection extends Activity implements IActivityWithCategorySelection {

}
