package com.esgi.breakingdev.notesdefraisandroid.activity.special;

import android.app.Activity;

import com.esgi.breakingdev.notesdefraisandroid.activity.special.IActivityWithCustomerSelection;

/**
 * Created by Tibo Mathieu on 14/06/2016.
 */
public abstract class ActivityWithCustomerSelection extends Activity implements IActivityWithCustomerSelection {
}
