package com.esgi.breakingdev.notesdefraisandroid.activity.special;

import android.app.Activity;

/**
 * Created by Tibo Mathieu on 30/06/2016.
 */
public abstract class ActivityWithServiceSelection extends Activity implements IActivityWithServiceSelection {
}
