package com.esgi.breakingdev.notesdefraisandroid.activity.special;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;

/**
 * Created by Tibo Mathieu on 01/06/2016.
 */
public interface IActivityWithCategorySelection {
    public void setCategorySelected(BusinessCategory businessCategory);
}
