package com.esgi.breakingdev.notesdefraisandroid.activity.special;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;

/**
 * Created by Tibo Mathieu on 14/06/2016.
 */
public interface IActivityWithCustomerSelection {
    public void setSelectedCustomer(BusinessCustomer customer);
    public void setInternalItem(boolean internal);
}
