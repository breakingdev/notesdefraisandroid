package com.esgi.breakingdev.notesdefraisandroid.activity.special;

import java.util.List;

/**
 * Created by Tibo Mathieu on 19/06/2016.
 */
public interface IActivityWithProjectSelection {
    public void setProjectsOfCustomer(List<Object> listProjects);
    public List<Object> getCustomerProjects();

}
