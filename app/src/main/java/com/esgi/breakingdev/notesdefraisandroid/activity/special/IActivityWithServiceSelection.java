package com.esgi.breakingdev.notesdefraisandroid.activity.special;

import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;

/**
 * Created by Tibo Mathieu on 30/06/2016.
 */
public interface IActivityWithServiceSelection {
    void setServiceSelected(BusinessService service);
}
