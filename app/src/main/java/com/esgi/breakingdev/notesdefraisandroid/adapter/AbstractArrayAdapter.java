package com.esgi.breakingdev.notesdefraisandroid.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.sql.AbstractDBManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Tibo Mathieu on 12/05/2016.
 */
abstract public class AbstractArrayAdapter extends ArrayAdapter<Object> {

    /* CLASS MEMBERS */
    int itemLayoutID;
    protected ArrayList<Object> dbListObjects;    // Liste des objets de la base de données
    protected ArrayList<Object> listObjects = new ArrayList<Object>();         // Liste des objets à adapter
    private LayoutInflater inflaterAdapter;     //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
    protected AbstractDBManager dbManager;        // Manager de la base de données associée
    protected Context context;                              // Contexte de l'application
    //    private AssetManager assetManager;

    public ArrayList<Object> getDbListObjects() {
        return dbListObjects;
    }

    /* CONSTRUCTOR */
    protected AbstractArrayAdapter(Context context, int resource, AbstractDBManager dbManager) {
        super(context, resource);
        this.context = context;
        this.itemLayoutID = resource;
        inflaterAdapter = LayoutInflater.from(context);
//        assetManager = context.getAssets();
        this.dbManager = dbManager;
        try {
            refresh();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** SPECIFIC METHODS **/
    /* Récupération minimale de la vue d'un item de la liste (à utiliser dans getView) */
    protected View inflateItemView(View convertView, ViewGroup parent)
    {
        LinearLayout layoutItem;
        //(1) : Réutilisation des layouts
        if (convertView == null) {
            //Initialisation de notre item à partir du  layout XML "item_note_list.xml"
            layoutItem = (LinearLayout) inflaterAdapter.inflate(itemLayoutID, parent, false);
        } else {
            layoutItem = (LinearLayout) convertView;
        }
        return layoutItem;
    }

    /* Récupération d'un item dont l'id est donné en paramètre */
    public Object getItemWithId(long id){
        Object object = null;
        for(int i=0; i<getCount();++i){
            if(getItemId(i) == id) return getItem(i);
        }
        return object;
    }

    /* Récupération d'un item dont l'id (Serveur)est donné en paramètre */
    public Object getItemWithStringId(String strId){
        Object object = null;
        for(int i=0; i<getCount();++i){
            if(getItemStringId(i).equals(strId)) return getItem(i);
        }
        return object;
    }



    /** METHODS TO IMPLEMENT **/
    /* Récupération de la vue d'un item de la liste */
    public abstract View getView(int position, View convertView, ViewGroup parent);

    /* Association de la vue de l'item avec ses données (à utiliser dans getView) */
    protected abstract void setItemView(View itemView,Object itemObject);

    /* Récupération de l'ID de l'item à la position spécifiée */
    public abstract long getItemId(int position);

      /* Récupération de l'ID (Serveur) de l'item à la position spécifiée */
    public abstract String getItemStringId(int position);

    /* Récupération de la position d'un objet de la liste  */
    public abstract int getPosition(Object item);







    /** OVERRIDED METHODS **/
    @Override
    public void add(Object object)
    {
        try {
            add(object,false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /* Ajout d'un item avec un id spécifique dans la base */
    public void add(Object object, boolean with_id) throws Exception {
        add(object, with_id, true);
    }

    public void add(Object object,boolean with_id, boolean inDB) throws Exception {
        if(inDB){
            dbManager.createItem(object,with_id);
            dbListObjects.add(object);
        }
        listObjects.add(object);
        super.add(object);
    }

    @Override
    public void insert(Object object, int index) {
        add(object);
    }

    /* Suppression d'un item de la liste */
    public void remove(Object object)
    {
        remove(object, true);
    }

    public void remove(Object object, boolean fromDB)
    {
        removeItem(getPosition(object), fromDB);
    }

    public Object removeItem(int position)
    {
        return removeItem(position,true);
    }

    public Object removeItem(int position,boolean fromDB){
        if(position < 0 || position >= getCount())
            return null;

        Object object = getItem(position);
        long id = getItemId(position);
        listObjects.remove(position);
        super.remove(object);
        if(fromDB) {
            dbManager.deleteItem(id);
            dbListObjects.remove(object);
        }
        return object;
    }

    @Override
    public void clear() {
        clear(true);
    }

    public void clear(boolean fromDB)
    {
        listObjects.clear();
        super.clear();
        if(fromDB) {
            dbManager.deleteTable();
            dbListObjects.clear();
        }
    }

    public void refresh() throws Exception {
        clear(false);
        dbListObjects = dbManager.getDBItems();
        for(Object o : dbListObjects)  add(o,false,false);
    }


}
