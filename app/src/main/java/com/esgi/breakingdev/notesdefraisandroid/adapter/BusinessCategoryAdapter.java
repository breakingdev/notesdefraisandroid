package com.esgi.breakingdev.notesdefraisandroid.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.filter.BusinessCategoryFilter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessCategoryListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessCategorySubject;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.sql.AbstractDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCategoryDBManager;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Jeremy on 12/05/2016.
 */
public class BusinessCategoryAdapter extends AbstractArrayAdapter implements BusinessCategorySubject {
    /* CLASS MEMBERS */
    private static BusinessCategoryAdapter instance;       // Instance unique de l'adapter pour les notes de frais
    private LayoutInflater inflaterAdapter;             //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
//    private AssetManager assetManager;

    /* SINGLETON */
    public static BusinessCategoryAdapter getInstance(Context context) throws Exception {

        if(instance == null)
            instance = new BusinessCategoryAdapter(context.getApplicationContext(), BusinessCategoryDBManager.getInstance(context));
//        instance.refresh();
        return instance;
    }

    /* CONSTRUCTOR */
    protected BusinessCategoryAdapter(Context context,AbstractDBManager manager) {
        super(context, R.layout.item_businesscategory_list, manager);
        inflaterAdapter = LayoutInflater.from(context);
//        assetManager = context.getAssets();
    }

    /* IMPLEMENTED METHODS FROM AbstractArrayAdapter */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem = (LinearLayout) inflateItemView(convertView, parent);
        final BusinessCategory category = (BusinessCategory) getItem(position);
        //On ajoute un listener
        layoutItem.setOnClickListener(itemListOnClickListener(category));
        setItemView(layoutItem, category);
        return layoutItem;
    }

    @Override
    protected void setItemView(View itemView, Object itemObject) {
        LinearLayout layoutItem = (LinearLayout) itemView;
        final BusinessCategory category = (BusinessCategory) itemObject;

        //(2) : Récupération des TextView de notre layout
        TextView txtNomCategorie = (TextView) layoutItem.findViewById(R.id.txtDescriptionCategorie);
        TextView txtType = (TextView) layoutItem.findViewById(R.id.txtFormType);

        //(3) : Renseignement des valeurs
        txtNomCategorie.setText(category.getDescription());
        txtType.setText(BusinessCategory.getFormTypeString(category.getFormType(), context));

        //(4) : Récupération des ImageButton de la liste
        ImageButton btnDeleteCategorie = (ImageButton) layoutItem.findViewById(R.id.btnDeleteCategorie);
        ImageButton btnEditCategorie = (ImageButton) layoutItem.findViewById(R.id.btnEditCategorie);

        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(!app.isStandAlone())
        {
            btnDeleteCategorie.setVisibility(View.GONE);
            btnEditCategorie.setVisibility(View.GONE);
        }
        else
        {
            btnDeleteCategorie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendButtonDeleteListener(category);
                }
            });
            btnEditCategorie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendButtonEditListener(category);
                }
            });
        }

    }

    @Override
    public long getItemId(int position) {
        BusinessCategory item = (BusinessCategory) getItem(position);
        return item.getBusinessCategoryId();
    }

    @Override
    public String getItemStringId(int position) {
        BusinessCategory item = (BusinessCategory) getItem(position);
        return item.getStringId();
    }

    @Override
    public int getPosition(Object item) {
        BusinessCategory castedItem = (BusinessCategory) item;
        for (int i = 0; i < getCount(); ++i) {
            BusinessCategory listItem = (BusinessCategory) getItem(i);
            if (listItem.getBusinessCategoryId() == castedItem.getBusinessCategoryId()) return i;
            if (!listItem.getStringId().isEmpty() && listItem.getStringId().equals(castedItem.getStringId())) return i;
        }
        return -1;
    }

    /* OVERRIDED METHODS */
    @Override
    public Filter getFilter() {
        return new BusinessCategoryFilter(this);
    }

    /* IMPLEMENTED METHODS FROM BusinessCategorySubject */
    @Override
    public void addListener(BusinessCategoryListener observer) {
        observers.add(observer);
    }

    @Override
    public void removeListener(BusinessCategoryListener observer) {
        observers.remove(observer);
    }

    @Override
    public void clearListeners() {
        observers.clear();
    }


    @Override
    public void setBusinessCategoryListener(BusinessCategory item) {
        for(BusinessCategoryListener o : observers){
            o.onClickCategory(item);
        }
    }

    @Override
    public void sendButtonDeleteListener(BusinessCategory item) {
        for(BusinessCategoryListener o : observers){
            o.onClickButtonDelete(item);
        }
    }

    @Override
    public void sendButtonEditListener(BusinessCategory item) {
        for(BusinessCategoryListener o : observers){
            o.onClickButtonEdit(item);
        }
    }

    @Override
    public View.OnClickListener itemListOnClickListener(final BusinessCategory item) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //On prévient les listeners qu'il y a eu un clic sur le layoutItem
                setBusinessCategoryListener(item);
            }
        };
    }

    @Override
    public void refresh() throws Exception {
        super.refresh();
        Log.d("BusinessCategoryAdapter", "refreshed");
        ExpenseLine.businessCategoryList.clear();
        for(Object o : dbListObjects)
            ExpenseLine.businessCategoryList.add(o);
    }
}
