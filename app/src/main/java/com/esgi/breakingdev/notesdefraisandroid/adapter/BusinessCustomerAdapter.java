package com.esgi.breakingdev.notesdefraisandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.filter.BusinessCustomerFilter;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessCustomerListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessCustomerSubject;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessCustomerSubject;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.sql.AbstractDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCustomerDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCustomerDBManager;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

/**
 * Created by Jeremy on 19/05/2016.
 *
 * IMPORTANT : Les Models des Clients n'est pas encore implémenté
 * Le code suivant est une copie de BusinessCustomer : Penser à mettre à jour les références
 *
 */
public class BusinessCustomerAdapter  extends AbstractArrayAdapter implements BusinessCustomerSubject {

    /* CLASS MEMBERS */
    private static BusinessCustomerAdapter instance;       // Instance unique de l'adapter pour les notes de frais
    private LayoutInflater inflaterAdapter;             //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
//    private AssetManager assetManager;

    /* SINGLETON */
    public static BusinessCustomerAdapter getInstance(Context context) throws Exception {

        if(instance == null)
            instance = new BusinessCustomerAdapter(context.getApplicationContext(), BusinessCustomerDBManager.getInstance(context));
        instance.refresh();
        return instance;
    }

    /* CONSTRUCTOR */
    protected BusinessCustomerAdapter(Context context,AbstractDBManager manager) {
        super(context, R.layout.item_businesscustomer_list, manager);
        inflaterAdapter = LayoutInflater.from(context);
//        assetManager = context.getAssets();
    }

    /* IMPLEMENTED METHODS FROM AbstractArrayAdapter */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem = (LinearLayout) inflateItemView(convertView, parent);
        final BusinessCustomer customer = (BusinessCustomer) getItem(position);
        //On ajoute un listener
        layoutItem.setOnClickListener(itemListOnClickListener(customer));
        setItemView(layoutItem, customer);
        return layoutItem;
    }

    @Override
    protected void setItemView(View itemView, Object itemObject) {
        LinearLayout layoutItem = (LinearLayout) itemView;
        final BusinessCustomer customer = (BusinessCustomer) itemObject;

        //(2) : Récupération des TextView de notre layout
        TextView txtNomClient = (TextView) layoutItem.findViewById(R.id.txtDescriptionClient);

        //(3) : Renseignement des valeurs
        txtNomClient.setText(customer.getDescription());

        //(4) : Récupération des ImageButton de la liste
        ImageButton btnDeleteClient = (ImageButton) layoutItem.findViewById(R.id.btnDeleteClient);
        ImageButton btnEditClient = (ImageButton) layoutItem.findViewById(R.id.btnEditClient);

        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(!app.isStandAlone())
        {
            btnDeleteClient.setVisibility(View.GONE);
            btnEditClient.setVisibility(View.GONE);
        }
        else
        {
            btnDeleteClient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendButtonDeleteListener(customer);
                }
            });
            btnEditClient.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendButtonEditListener(customer);
                }
            });

        }

    }

    @Override
    public long getItemId(int position) {
        BusinessCustomer item = (BusinessCustomer) getItem(position);
        return item.getBusinessCustomerId();
    }

    @Override
    public String getItemStringId(int position) {
        BusinessCustomer item = (BusinessCustomer) getItem(position);
        return item.getStringId();
    }

    @Override
    public int getPosition(Object item) {
        BusinessCustomer castedItem = (BusinessCustomer) item;
        for (int i = 0; i < getCount(); ++i) {
            BusinessCustomer listItem = (BusinessCustomer) getItem(i);
            if (listItem.getBusinessCustomerId() == castedItem.getBusinessCustomerId()) return i;
            if (!listItem.getStringId().isEmpty() && listItem.getStringId().equals(castedItem.getStringId())) return i;
        }
        return -1;
    }

    /* OVERRIDED METHODS */
    @Override
    public Filter getFilter() {
        return new BusinessCustomerFilter(this);
    }

    /* IMPLEMENTED METHODS FROM BusinessCustomerSubject */
    @Override
    public void addListener(BusinessCustomerListener observer) {
        observers.add(observer);
    }

    @Override
    public void removeListener(BusinessCustomerListener observer) {
        observers.remove(observer);
    }

    @Override
    public void clearListeners() {
        observers.clear();
    }


    @Override
    public void setBusinessCustomerListener(BusinessCustomer item) {
        for(BusinessCustomerListener o : observers){
            o.onClickCustomer(item);
        }
    }


    @Override
    public void sendButtonDeleteListener(BusinessCustomer item) {
        for(BusinessCustomerListener o : observers){
            o.onClickButtonDelete(item);
        }
    }

    @Override
    public void sendButtonEditListener(BusinessCustomer item) {
        for(BusinessCustomerListener o : observers){
            o.onClickButtonEdit(item);
        }
    }

    @Override
    public View.OnClickListener itemListOnClickListener(final BusinessCustomer item) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //On prévient les listeners qu'il y a eu un clic sur le layoutItem
                setBusinessCustomerListener(item);
            }
        };
    }

    @Override
    public void refresh() throws Exception {
        super.refresh();
        ExpenseLine.businessCustomerList.clear();
        for(Object o : dbListObjects)
            ExpenseLine.businessCustomerList.add(o);
    }
}
