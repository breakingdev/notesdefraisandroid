package com.esgi.breakingdev.notesdefraisandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.filter.BusinessProjectFilter;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessProjectListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessProjectSubject;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.sql.AbstractDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessProjectDBManager;

/**
 * Created by Jeremy on 19/05/2016.
 *
 * IMPORTANT : Les Models des Clients n'est pas encore implémenté
 * Le code suivant est une copie de BusinessProject : Penser à mettre à jour les références
 *
 *
 */
public class BusinessProjectAdapter extends AbstractArrayAdapter implements BusinessProjectSubject {
    /* CLASS MEMBERS */
    private static BusinessProjectAdapter instance;       // Instance unique de l'adapter pour les notes de frais
    private LayoutInflater inflaterAdapter;             //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
//    private AssetManager assetManager;

    /* SINGLETON */
    public static BusinessProjectAdapter getInstance(Context context) throws Exception {

        if(instance == null)
            instance = new BusinessProjectAdapter(context.getApplicationContext(), BusinessProjectDBManager.getInstance(context));
        instance.refresh();
        return instance;
    }

    /* CONSTRUCTOR */
    protected BusinessProjectAdapter(Context context,AbstractDBManager manager) {
        super(context, R.layout.item_businessproject_list, manager);
        inflaterAdapter = LayoutInflater.from(context);
//        assetManager = context.getAssets();
    }

    /* IMPLEMENTED METHODS FROM AbstractArrayAdapter */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem = (LinearLayout) inflateItemView(convertView, parent);
        final BusinessProject project = (BusinessProject) getItem(position);
        //On ajoute un listener
        layoutItem.setOnClickListener(itemListOnClickListener(project));
        setItemView(layoutItem, project);
        return layoutItem;
    }

    @Override
    protected void setItemView(View itemView, Object itemObject) {
        LinearLayout layoutItem = (LinearLayout) itemView;
        final BusinessProject project = (BusinessProject) itemObject;

        //(2) : Récupération des TextView de notre layout
        TextView txtNomProjet = (TextView) layoutItem.findViewById(R.id.txtDescriptionProjet);
        TextView txtProjetRefacturable = (TextView) layoutItem.findViewById(R.id.txtProjetRefacturable);

        //(3) : Renseignement des valeurs
        txtNomProjet.setText(project.getDescription());
        if(project.getRefacturation())
            txtProjetRefacturable.setText(context.getString(R.string.yes));
        else
            txtProjetRefacturable.setText(context.getString(R.string.no));

        //(4) : Récupération des ImageButton de la liste
        ImageButton btnDeleteProjet = (ImageButton) layoutItem.findViewById(R.id.btnDeleteProjet);
        ImageButton btnEditProjet = (ImageButton) layoutItem.findViewById(R.id.btnEditProjet);

        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(!app.isStandAlone())
        {
            btnDeleteProjet.setVisibility(View.GONE);
            btnEditProjet.setVisibility(View.GONE);
        }
        else
        {
            btnDeleteProjet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendButtonDeleteListener(project);
                }
            });
            btnEditProjet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendButtonEditListener(project);
                }
            });
        }
    }

    @Override
    public long getItemId(int position) {
        BusinessProject item = (BusinessProject) getItem(position);
        return item.getBusinessProjectId();
    }

    @Override
    public String getItemStringId(int position) {
        BusinessProject item = (BusinessProject) getItem(position);
        return item.getStringId();
    }

    @Override
    public int getPosition(Object item) {
        BusinessProject castedItem = (BusinessProject) item;
        for (int i = 0; i < getCount(); ++i) {
            BusinessProject listItem = (BusinessProject) getItem(i);
            if (listItem.getBusinessProjectId() == castedItem.getBusinessProjectId()) return i;
            if (!listItem.getStringId().isEmpty() && listItem.getStringId().equals(castedItem.getStringId())) return i;
        }
        return -1;
    }

    /* OVERRIDED METHODS */
    @Override
    public Filter getFilter() {
        return new BusinessProjectFilter(this);
    }

    @Override
    public void refresh() throws Exception {
        super.refresh();
        ExpenseLine.businessProjectList.clear();
        for(Object o : dbListObjects)
            ExpenseLine.businessProjectList.add(o);
    }

    /* IMPLEMENTED METHODS FROM BusinessProjectSubject */
    @Override
    public void addListener(BusinessProjectListener observer) {
        observers.add(observer);
    }

    @Override
    public void removeListener(BusinessProjectListener observer) {
        observers.remove(observer);
    }

    @Override
    public void clearListeners() {
        observers.clear();
    }


    @Override
    public void setBusinessProjectListener(BusinessProject item) {
        for(BusinessProjectListener o : observers){
            o.onClickProject(item);
        }
    }

    @Override
    public void sendButtonDeleteListener(BusinessProject item) {
        for(BusinessProjectListener o : observers){
            o.onClickButtonDelete(item);
        }
    }

    @Override
    public void sendButtonEditListener(BusinessProject item) {
        for(BusinessProjectListener o : observers){
            o.onClickButtonEdit(item);
        }
    }

    @Override
    public View.OnClickListener itemListOnClickListener(final BusinessProject item) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //On prévient les listeners qu'il y a eu un clic sur le layoutItem
                setBusinessProjectListener(item);
            }
        };
    }


}
