package com.esgi.breakingdev.notesdefraisandroid.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.filter.BusinessServiceFilter;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessServiceListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessServiceSubject;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.sql.AbstractDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessServiceDBManager;

/**
 * Created by Tibo Mathieu on 28/06/2016.
 */
public class BusinessServiceAdapter extends AbstractArrayAdapter implements BusinessServiceSubject
{
    /* CLASS MEMBERS */
    private static BusinessServiceAdapter instance;       // Instance unique de l'adapter pour les notes de frais
    private LayoutInflater inflaterAdapter;             //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
//    private AssetManager assetManager;

    /* SINGLETON */
    public static BusinessServiceAdapter getInstance(Context context) throws Exception {

        if(instance == null)
            instance = new BusinessServiceAdapter(context.getApplicationContext(), BusinessServiceDBManager.getInstance(context));
        instance.refresh();
        return instance;
    }

    /* CONSTRUCTOR */
    protected BusinessServiceAdapter(Context context,AbstractDBManager manager) {
        super(context, R.layout.item_business_service_list, manager);
        inflaterAdapter = LayoutInflater.from(context);
//        assetManager = context.getAssets();
    }

    /* IMPLEMENTED METHODS FROM AbstractArrayAdapter */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem = (LinearLayout) inflateItemView(convertView, parent);
        final BusinessService service = (BusinessService) getItem(position);
        //On ajoute un listener
        layoutItem.setOnClickListener(itemListOnClickListener(service));
        setItemView(layoutItem, service);
        return layoutItem;
    }

    @Override
    protected void setItemView(View itemView, Object itemObject) {
        LinearLayout layoutItem = (LinearLayout) itemView;
        final BusinessService service = (BusinessService) itemObject;

        //(2) : Récupération des TextView de notre layout
        TextView txtDescriptionService = (TextView) layoutItem.findViewById(R.id.txtDescriptionService);

        //(3) : Renseignement des valeurs
        txtDescriptionService.setText(service.getDescription());
    }

    @Override
    public long getItemId(int position) {
        BusinessService item = (BusinessService) getItem(position);
        return item.getServiceId();    }

    @Override
    public String getItemStringId(int position) {
        BusinessService item = (BusinessService) getItem(position);
        return item.getStringId();
    }

    @Override
    public int getPosition(Object item) {
        BusinessService castedItem = (BusinessService) item;
        for (int i = 0; i < getCount(); ++i) {
            BusinessService listItem = (BusinessService) getItem(i);
            if (listItem.getServiceId() == castedItem.getServiceId()) return i;
            if (!listItem.getStringId().isEmpty() && listItem.getStringId().equals(castedItem.getStringId())) return i;
        }
        return -1;    }


    /* OVERRIDED METHODS */
    @Override
    public Filter getFilter() {
        return new BusinessServiceFilter(this);
    }

    @Override
    public void refresh() throws Exception {
        super.refresh();
        BusinessService.businessServicesList.clear();
        for(Object o : dbListObjects)
            BusinessService.businessServicesList.add(o);
    }



    /* IMPLEMENTED METHODS FROM BusinessServiceSubject */

    @Override
    public void addListener(BusinessServiceListener observer) {
        observers.add(observer);
    }

    @Override
    public void removeListener(BusinessServiceListener observer) {
        observers.remove(observer);
    }

    @Override
    public void clearListeners() {
        observers.clear();
    }

    @Override
    public void setBusinessServiceListener(BusinessService item) {
        for(BusinessServiceListener o : observers){
            o.onClickService(item);
        }
    }

    @Override
    public View.OnClickListener itemListOnClickListener(final BusinessService item) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //On prévient les listeners qu'il y a eu un clic sur le layoutItem
                setBusinessServiceListener(item);
            }
        };    }
}
