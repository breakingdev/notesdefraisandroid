package com.esgi.breakingdev.notesdefraisandroid.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.filter.ExpenseLineFilter;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.ExpenseLineListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.ExpenseLineSubject;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseLineServices;
import com.esgi.breakingdev.notesdefraisandroid.sql.AbstractDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseLineDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.FlateRateAmountDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.MileageExpenseAmountDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.SimpleAmountDBManager;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Tibo Mathieu on 18/05/2016.
 */
public class ExpenseLineAdapter extends AbstractArrayAdapter implements ExpenseLineSubject {

    /* CLASS MEMBERS */
    private static ExpenseLineAdapter instance;       // Instance unique de l'adapter pour les notes de frais
    private LayoutInflater inflaterAdapter;             //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
//    private AssetManager assetManager;
    public long expenseReportId;               // Id de la note de frais
    private float totalAmount;      // Montant total des lignes de frais affichées
    public int totalDisplayedElements; // Nombre d'éléments affichés
    public List<Long> expenseLinesIdList = new ArrayList<>();
    public Map<Long,Integer> expenseLinesIdListMap; // à utiliser sur le mode serveur

    /* SINGLETON */
    public static ExpenseLineAdapter getInstance(Context context,long expenseReportId) throws Exception {

        if(instance == null)
            instance = new ExpenseLineAdapter(context.getApplicationContext(), ExpenseLineDBManager.getInstance(context),expenseReportId);
        else
            instance.expenseReportId = expenseReportId;
//        instance.refresh();
        return instance;
    }

    /* CONSTRUCTOR */
    protected ExpenseLineAdapter(Context context,AbstractDBManager manager,long expenseReportId) {
        super(context, R.layout.item_expense_line, manager);
        inflaterAdapter = LayoutInflater.from(context);
        this.expenseReportId = expenseReportId;
        expenseLinesIdListMap = new HashMap<Long,Integer>();
    }

    /* SPECIFIC METHODS */
    public void calculateTotalAmount()
    {
        float somme = 0;
        int count = 0;
        for(int i = 0; i<dbListObjects.size(); ++i){
            ExpenseLine expenseLine = (ExpenseLine)dbListObjects.get(i);
            if(expenseLine.getExpenseReportId() == expenseReportId) {
                somme += expenseLine.getAmountValue();
                count++;
            }

        }
        totalAmount = somme;
        totalDisplayedElements = count;

    }

    /* GETTERS AND SETTERS */
    public void setExpenseReportId(long expenseReportId) {
        this.expenseReportId = expenseReportId;
        try {
            refresh();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public float getTotalAmount() {
        return totalAmount;
    }

    public float getTotalAmount(long expenseReportId) {
        this.expenseReportId = expenseReportId;
        getFilter().filter(Long.toString(expenseReportId));
        calculateTotalAmount();

        return totalAmount;
    }

    /* IMPLEMENTED METHODS FROM AbstractArrayAdapter */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem = (LinearLayout) inflateItemView(convertView, parent);
        final ExpenseLine expenseLine = (ExpenseLine) getItem(position);

        //On ajoute un listener
        layoutItem.setOnClickListener(itemListOnClickListener(expenseLine));
        setItemView(layoutItem, expenseLine);
        return layoutItem;
    }

    @Override
    public Filter getFilter() {
        return new ExpenseLineFilter(this);
    }

    @Override
    protected void setItemView(View itemView, Object itemObject) {
        LinearLayout layoutItem = (LinearLayout) itemView;
        final ExpenseLine expenseLine = (ExpenseLine) itemObject;
        if(expenseLine == null)
            Log.d("ExpenseLineAdapter","expenseLine is null !");

        // Récupération de l'utilitaire de formatage de date pour la base de données
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        SimpleDateFormat sdf = app.getDateFormatForGUI();

        //(2) : Récupération des TextView de notre layout
        TextView descTextView = (TextView)layoutItem.findViewById(R.id.textView_line_comment);
        TextView dateTxtView = (TextView)layoutItem.findViewById(R.id.textView_date);
        TextView amountTxtView = (TextView)layoutItem.findViewById(R.id.textView_expense_line_amount);
        if(descTextView == null)
            Log.d("ExpenseLineAdapter","descTextView is null !");

        if(expenseLine.getCategory() == null)
            Log.d("ExpenseLineAdapter","expenseLine.getCategory() is null !");
        //(3) : Renseignement des valeurs
        descTextView.setText(expenseLine.getComment());
        dateTxtView.setText(sdf.format(expenseLine.getDate()));
//        Log.d(/)
        if(expenseLine.getAmountValue() == 0)
            amountTxtView.setText("null"); // Précision 2 digits apres la virgule
        else
            amountTxtView.setText(String.format("%.2f", expenseLine.getAmountValue()) ); // Précision 2 digits apres la virgule
//        amountTxtView.setText(new DecimalFormat("###.##").format(-5.05f)); // Précision 2 digits apres la virgule

        //(4) : Récupération des ImageButton de la liste
        ImageButton btnDeleteLine = (ImageButton) layoutItem.findViewById(R.id.btnDeleteExpenseLine);
        ImageButton btnEditLine = (ImageButton) layoutItem.findViewById(R.id.btnEditExpenseLine);

        try
        {
            ExpenseReport expenseReport = (ExpenseReport) ExpenseReportAdapter.getInstance(context).getItemWithId(expenseLine.getExpenseReportId());

            if(!expenseReport.getStatus().equals(ExpenseReport.Status.inEdition) && !expenseReport.getStatus().equals(ExpenseReport.Status.reject) )
            {
                btnDeleteLine.setVisibility(View.GONE);
                btnEditLine.setVisibility(View.GONE);
            }
            else{
                btnDeleteLine.setVisibility(View.VISIBLE);
                btnEditLine.setVisibility(View.VISIBLE);
            }

        } catch(Exception e)
        {
            e.printStackTrace();
        }

        btnDeleteLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendButtonDeleteExpenseLine(expenseLine);

                ExpenseLineServices expenseLineServices = new ExpenseLineServices();
                expenseLineServices.deleteObject(expenseLine.getStringId());
            }
        });
        btnEditLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendButtonEditExpenseLine(expenseLine);
            }
        });

        // Affichage des numéros de ligne

        TextView textLine = (TextView) layoutItem.findViewById(R.id.textLineNumber);
        if(!app.isStandAlone())
            textLine.setText("" + expenseLine.getLineNumber());
        else {
            int cpt = 0;
            for(long id : expenseLinesIdList){
                if(id == expenseLine.getExpenseLineId()) break;
                ++cpt;
            }
            textLine.setText("" + (cpt + 1));
        }
    }

    @Override
    public long getItemId(int position) {
        ExpenseLine item = (ExpenseLine) getItem(position);
        return item.getExpenseLineId();
    }

    @Override
    public String getItemStringId(int position) {
        ExpenseLine item = (ExpenseLine) getItem(position);
        return item.getStringId();
    }

    @Override
    public int getPosition(Object item) {
        ExpenseLine castedItem = (ExpenseLine) item;
        for (int i = 0; i < getCount(); ++i) {
            ExpenseLine listItem = (ExpenseLine) getItem(i);
            if (listItem.getExpenseLineId() == castedItem.getExpenseLineId()) return i;
            if (!listItem.getStringId().isEmpty() && listItem.getStringId().equals(castedItem.getStringId())) return i;
        }
        return -1;
    }

    /* IMPLEMENTED METHODS FROM ExpenseReportSubject */
    @Override
    public void addListener(ExpenseLineListener observer) {
        observers.add(observer);
    }

    @Override
    public void setExpenseLineListener(ExpenseLine item) {
        for(int i=0; i<observers.size();++i)
            observers.get(i).onClickExpenseLine(item);
    }

    @Override
    public void removeListener(ExpenseLineListener observer) {
        observers.remove(observer);
    }

    @Override
    public void clearListeners() {
        observers.clear();
    }

    @Override
    public View.OnClickListener itemListOnClickListener(final ExpenseLine expenseLine) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //On prévient les listeners qu'il y a eu un clic sur le layoutItem
                setExpenseLineListener(expenseLine);
            }
        };
    }

    @Override
    public void sendButtonEditExpenseLine(ExpenseLine item) {
        for(int i=0; i<observers.size();++i)
            observers.get(i).onClickButtonEdit(item);
    }

    @Override
    public void sendButtonDeleteExpenseLine(ExpenseLine item) {
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();

        if(app.isOnline() || app.isStandAlone())
        {
            for(int i=0; i<observers.size();++i)
                observers.get(i).onClickButtonDelete(item);
        }
        else
        {
            Toast toast = Toast.makeText(context, "Une connexion a internet est nécessaire pour cette action", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    /* OVERRIDED METHODS */
    @Override
    public void refresh() throws Exception {
        super.refresh();
        Log.d("ExpenseLineAdapter", "refreshed");
        if(expenseLinesIdListMap == null) return;
        expenseLinesIdListMap.clear();
        getFilter().filter(Long.toString(expenseReportId));
        sortExpenseLinesIdByLineNumber();
        calculateTotalAmount();
    }

    public void sortExpenseLinesIdByLineNumber()
    {
        expenseLinesIdList.clear();
        int min_Number = Integer.MAX_VALUE;
        long id = 0;
        int size = expenseLinesIdListMap.size();
        while(expenseLinesIdList.size() < size){
            Iterator iter = expenseLinesIdListMap.entrySet().iterator();
                while(iter.hasNext()){
                    Map.Entry<Long,Integer> entry = (Map.Entry) iter.next();
                    if(entry.getValue() < min_Number) {
                        min_Number = entry.getValue();
                        id = entry.getKey();
                    }
                }
            expenseLinesIdListMap.remove(id);
            expenseLinesIdList.add(id);
        }
    }

    @Override
    public void add(Object object, boolean with_id, boolean inDB) throws Exception {
        super.add(object, with_id, inDB);
        ExpenseLine expenseLine = (ExpenseLine) object;
        if(inDB) {
            Log.d("ExpenseLineAdapter", "adding expense line in db" + expenseLine.getExpenseLineId());

        }
//        else
//            Log.d("ExpenseLineAdapter","adding expense line (not in db)" + expenseLine.getExpenseLineId());

        switch(expenseLine.getCategory())
        {
            case amount:
                if(inDB)
                    SimpleAmountDBManager.getInstance(context).createItem(expenseLine.getAmount(),false);
                break;
            case mileageExpense:
                if(inDB)
                    MileageExpenseAmountDBManager.getInstance(context).createItem(expenseLine.getAmount(),false);
                break;
            case flateRate:
                if(inDB)
                    FlateRateAmountDBManager.getInstance(context).createItem(expenseLine.getAmount(),false);
                break;
            default: throw new Exception("ExpenseLineAdapter - add: expenseLine category was not recognized");
        }
    }

    @Override
    public void remove(Object object, boolean fromDB)   {
        super.remove(object, fromDB);
        ExpenseLine expenseLine = (ExpenseLine) object;
        AbstractDBManager dbmanager;

        switch(expenseLine.getCategory())
        {
            case amount:
                    try {
                        if(fromDB) {
                            dbmanager = SimpleAmountDBManager.getInstance(context);
                            dbmanager.deleteItem(expenseLine.getAmount().getAmountID());
                        }
                        else
                            ListDeleter.removeAmountOfList(expenseLine.getAmount().getAmountID(),ExpenseLine.simpleAmountList);

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                break;
            case mileageExpense:
                if(fromDB) {
                    try {
                        if(fromDB) {
                            dbmanager = MileageExpenseAmountDBManager.getInstance(context);
                            dbmanager.deleteItem(expenseLine.getAmount().getAmountID());
                        }
                        else
                            ListDeleter.removeAmountOfList(expenseLine.getAmount().getAmountID(), ExpenseLine.mileageExpenseList);

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
                break;
            case flateRate:
                if(fromDB) {
                    try {
                        if(fromDB) {
                            dbmanager = FlateRateAmountDBManager.getInstance(context);
                            dbmanager.deleteItem(expenseLine.getAmount().getAmountID());
                        }
                        else
                            ListDeleter.removeAmountOfList(expenseLine.getAmount().getAmountID(), ExpenseLine.flateRateList);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;

        }
    }
}
