package com.esgi.breakingdev.notesdefraisandroid.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.R;

import com.esgi.breakingdev.notesdefraisandroid.activity.ViewExpenseReportActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.filter.ExpenseReportFilter;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.BusinessCategoryListener;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.ExpenseReportListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.ExpenseReportSubject;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseReportServices;
import com.esgi.breakingdev.notesdefraisandroid.sql.AbstractDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseReportDBManager;

import java.text.SimpleDateFormat;

/**
 * Created by Jeremy on 02/04/2016.
 */

public class ExpenseReportAdapter extends AbstractArrayAdapter implements ExpenseReportSubject {
    /* CLASS MEMBERS */
    private static ExpenseReportAdapter instance;       // Instance unique de l'adapter pour les notes de frais
    private LayoutInflater inflaterAdapter;             //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
//    private AssetManager assetManager;

    /* SINGLETON */
    public static ExpenseReportAdapter getInstance(Context context) throws Exception {

        if(instance == null)
            instance = new ExpenseReportAdapter(context.getApplicationContext(),ExpenseReportDBManager.getInstance(context));
//        instance.refresh();
        return instance;
    }

    /* CONSTRUCTOR */
    protected ExpenseReportAdapter(Context context,AbstractDBManager manager) {
        super(context, R.layout.item_expensereport_list, manager);
//                ExpenseReportDBManager.getInstance(context), ExpenseReportDBManager.getInstance(context).getDBItems() );
        inflaterAdapter = LayoutInflater.from(context);
//        assetManager = context.getAssets();
    }



    /* IMPLEMENTED METHODS FROM AbstractArrayAdapter */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem = (LinearLayout) inflateItemView(convertView, parent);
        final ExpenseReport expenseReport = (ExpenseReport) getItem(position);

        //On ajoute un listener
        layoutItem.setOnClickListener(itemListOnClickListener(expenseReport));
        setItemView(layoutItem, expenseReport);
        return layoutItem;
    }

    @Override
    public Filter getFilter() {
        return new ExpenseReportFilter(this);
    }

    @Override
    /* Association de la vue de l'item avec ses données (à utiliser dans getView) */
    protected void setItemView(View itemView,Object itemObject) {
        LinearLayout layoutItem = (LinearLayout) itemView;
        final ExpenseReport expenseReport = (ExpenseReport) itemObject;

        //(2) : Récupération des TextView de notre layout
        TextView txtNomNote = (TextView)layoutItem.findViewById(R.id.txtDescriptionNote);
        TextView txtPrixNote = (TextView)layoutItem.findViewById(R.id.txtPrixNote);
        TextView txtDateDebut = (TextView)layoutItem.findViewById(R.id.txtDateDebut);
        TextView txtDateFin = (TextView)layoutItem.findViewById(R.id.txtDateFin);

        //(3) : Renseignement des valeurs
        txtNomNote.setText(expenseReport.getDescription());
        try {
            float prix = ExpenseLineAdapter.getInstance(context,expenseReport.getExpenseReportId()).getTotalAmount(expenseReport.getExpenseReportId());
            txtPrixNote.setText(String.format("%.2f",prix) + " €");

        } catch (Exception e) {
            txtPrixNote.setText("null");
            e.printStackTrace();
        }

        // Récupération de l'utilitaire de formatage de date pour la base de données
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        SimpleDateFormat sdf = app.getDateFormatForGUI();

        txtDateDebut.setText(sdf.format(expenseReport.getBeginDate()));
        txtDateFin.setText(sdf.format(expenseReport.getEndDate()));


        //(4) : Récupération des ImageButton de la liste
        ImageButton btnDeleteNote = (ImageButton) layoutItem.findViewById(R.id.btnDeleteNote);
        ImageButton btnEditNote = (ImageButton) layoutItem.findViewById(R.id.btnEditNote);
        ImageButton btnAddLine = (ImageButton) layoutItem.findViewById(R.id.btnAddLine);
        ImageButton btnSendValidation = (ImageButton) layoutItem.findViewById(R.id.btnSendValidation);

        if(expenseReport.getStatus().equals(ExpenseReport.Status.inValidation))
        {
            btnDeleteNote.setVisibility(View.GONE);
            btnEditNote.setVisibility(View.GONE);
            btnAddLine.setVisibility(View.GONE);
        }
        else
        {
            btnDeleteNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer position = (Integer) v.getTag();
                    sendButtonDeleteListener(expenseReport);
                }
            });
            btnEditNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer position = (Integer) v.getTag();
                    sendButtonEditListener(expenseReport);
                }
            });
        }

        final int nbLines;
        ExpenseLineAdapter expenseLineAdapter = null;
        try {
             expenseLineAdapter = ExpenseLineAdapter.getInstance(context, expenseReport.getExpenseReportId());
             expenseLineAdapter.getFilter().filter(Long.toString(expenseReport.getExpenseReportId()));
             expenseLineAdapter.sortExpenseLinesIdByLineNumber();
             expenseLineAdapter.calculateTotalAmount();
             nbLines = expenseLineAdapter.totalDisplayedElements;

             btnAddLine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer position = (Integer) v.getTag();
                    sendButtonAddLineListener(expenseReport,nbLines+1);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            btnAddLine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer position = (Integer) v.getTag();
                    sendButtonAddLineListener(expenseReport, 0);
                }
            });
        }

        app = (BusinessExpensesApplication) context.getApplicationContext();
        if(app.isStandAlone() || expenseReport.getStatus().equals(ExpenseReport.Status.inValidation) ||
                expenseReport.getStatus().equals(ExpenseReport.Status.inAccounting))
        {
            btnSendValidation.setVisibility(View.GONE);
        }

        else
        {
            if (expenseLineAdapter != null && expenseLineAdapter.totalDisplayedElements == 0)
                btnSendValidation.setVisibility(View.INVISIBLE);
            else
                btnSendValidation.setVisibility(View.VISIBLE);
        }

        if(expenseReport.getStatus().equals(ExpenseReport.Status.inValidation)
                || expenseReport.getStatus().equals(ExpenseReport.Status.inAccounting))
        {
            btnDeleteNote.setVisibility(View.GONE);
            btnEditNote.setVisibility(View.GONE);
            btnAddLine.setVisibility(View.GONE);
            btnSendValidation.setVisibility(View.GONE);
        }
        else{
            btnDeleteNote.setVisibility(View.VISIBLE);
            btnEditNote.setVisibility(View.VISIBLE);
            btnAddLine.setVisibility(View.VISIBLE);
        }

        btnSendValidation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ExpenseReportAdapter", "Envoi en Validation de " + expenseReport.getDescription());
                sendNoteInValidation(expenseReport);
            }
        });

    }

    @Override
    /* Récupération de l'ID de l'item à une position donnée */
    public long getItemId(int position) {
        ExpenseReport item = (ExpenseReport) getItem(position);
        return item.getExpenseReportId();
    }

    @Override
    public String getItemStringId(int position) {
        ExpenseReport item = (ExpenseReport) getItem(position);
        return item.getStringId();
    }

    @Override
    /* Récupération de la position d'un objet donné */
    public int getPosition(Object item) {
        ExpenseReport castedItem = (ExpenseReport) item;
        for (int i = 0; i < getCount(); ++i) {
            ExpenseReport listItem = (ExpenseReport) getItem(i);
            if (listItem.getExpenseReportId() == castedItem.getExpenseReportId()) return i;
            if (!listItem.getStringId().isEmpty() && listItem.getStringId().equals(castedItem.getStringId())) return i;
        }
        return -1;
    }


    /* IMPLEMENTED METHODS FROM ExpenseReportSubject */
    @Override
    public void addListener(ExpenseReportListener observer) {
        observers.add(observer);
    }

    @Override
    public void setExpenseReportListener(ExpenseReport item) {
        for(int i=0; i<observers.size();++i)
            observers.get(i).onClickNote(item);
    }

    @Override
    public void removeListener(ExpenseReportListener observer) {
        observers.remove(observer);
    }

    @Override
    public void clearListeners() {
        observers.clear();
    }

    @Override
    public View.OnClickListener itemListOnClickListener(final ExpenseReport expenseReport) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //On prévient les listeners qu'il y a eu un clic sur le layoutItem
                setExpenseReportListener(expenseReport);
            }
        };
    }

    @Override
    public void sendButtonDeleteListener(ExpenseReport item) {
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();

        if(app.isOnline() || app.isStandAlone())
        {
            for(ExpenseReportListener o : observers){
                o.onClickButtonDelete(item);
            }
        }
        else
        {
            Toast toast = Toast.makeText(context, "Une connexion a internet est nécessaire pour cette action", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public void sendButtonAddLineListener(ExpenseReport item,int lineNumber) {
        for(ExpenseReportListener o : observers){
            o.onClickButtonAddLine(item,lineNumber);
        }
    }

    @Override
    public void sendButtonEditListener(ExpenseReport item) {
        for(ExpenseReportListener o : observers){
            o.onClickButtonEdit(item);
        }
    }


    public void sendNoteInValidation(ExpenseReport expenseReport)
    {
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();

        if(app.isOnline() || app.isStandAlone())
        {
            for(ExpenseReportListener o : observers){
                o.onClickButtonSendInValidation(expenseReport);
            }
        }
        else
        {
            Toast toast = Toast.makeText(context, "Une connexion a internet est nécessaire pour cette action", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    @Override
    public void refresh() throws Exception {
        super.refresh();
        Log.d("ExpenseReportAdapter", "refreshed");

    }
}
