package com.esgi.breakingdev.notesdefraisandroid.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.MileageAllowanceScaleListener;
import com.esgi.breakingdev.notesdefraisandroid.activity.observer.MileageAllowanceScaleSubject;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.sql.AbstractDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.MileageAllowanceScaleDBManager;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

/**
 * Created by Tibo Mathieu on 27/05/2016.
 */
public class MileageAllowanceScalesAdapter extends AbstractArrayAdapter implements MileageAllowanceScaleSubject {
    /* CLASS MEMBERS */
    private static MileageAllowanceScalesAdapter instance;       // Instance unique de l'adapter pour les notes de frais
    private LayoutInflater inflaterAdapter;             //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
//    private AssetManager assetManager;

    /* SINGLETON */
    public static MileageAllowanceScalesAdapter getInstance(Context context) throws Exception {

        if(instance == null)
            instance = new MileageAllowanceScalesAdapter(context.getApplicationContext(), MileageAllowanceScaleDBManager.getInstance(context));
//        instance.refresh();
        return instance;
    }

    /* CONSTRUCTOR */
    protected MileageAllowanceScalesAdapter(Context context,AbstractDBManager manager) {
        super(context, R.layout.item_mileage_allowance_scale_list, manager);
//                ExpenseReportDBManager.getInstance(context), ExpenseReportDBManager.getInstance(context).getDBItems() );
        inflaterAdapter = LayoutInflater.from(context);
//        assetManager = context.getAssets();
    }

    /* IMPLEMENTED METHODS FROM AbstractArrayAdapter */

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layoutItem = (LinearLayout) inflateItemView(convertView, parent);
        final MileageAllowanceScale mileageAllowanceScale = (MileageAllowanceScale) getItem(position);

        //On ajoute un listener
        layoutItem.setOnClickListener(itemListOnClickListener(mileageAllowanceScale));
        setItemView(layoutItem, mileageAllowanceScale);
        return layoutItem;
    }

    @Override
    protected void setItemView(View itemView, Object itemObject) {
        LinearLayout layoutItem = (LinearLayout) itemView;
        final MileageAllowanceScale mileageAllowanceScale = (MileageAllowanceScale) itemObject;

        //(2) : Récupération des TextView de notre layout
        TextView txtYear = (TextView)layoutItem.findViewById(R.id.txtYear);
        TextView txtVehicle = (TextView)layoutItem.findViewById(R.id.txtViewVehicle);
        TextView txtScaleValue = (TextView)layoutItem.findViewById(R.id.txtScaleOperation);
        TextView txtMinDistance = (TextView)layoutItem.findViewById(R.id.txtMinDistance_item);
        TextView txtMaxDistance = (TextView)layoutItem.findViewById(R.id.txtMaxDistance_item);
        TextView txtHorsePower = (TextView)layoutItem.findViewById(R.id.textViewHorsePower);

        //(3) : Renseignement des valeurs
        txtYear.setText(Integer.toString(mileageAllowanceScale.getMileageAllowanceYear()));
        txtVehicle.setText(MileageAllowanceScale.getVehicleString(mileageAllowanceScale.getVehicle(),context));
        Log.i("setItemView", "mileageAllowanceScale.getVehicle = " + mileageAllowanceScale.getVehicle().toString());
        String scaleText = String.format("%.3f",mileageAllowanceScale.getRate()) + " x d + " + String.format("%.0f", mileageAllowanceScale.getOffset());
        txtScaleValue.setText(scaleText);
        txtMinDistance.setText(Integer.toString(mileageAllowanceScale.getMinDistance()) + " km");
        txtMaxDistance.setText(Integer.toString(mileageAllowanceScale.getMaxDistance()) + " km");
        txtHorsePower.setText(Integer.toString(mileageAllowanceScale.getHorsePower()));

        //(4) : Récupération des ImageButton de la liste
        ImageButton btnDeleteScale = (ImageButton) layoutItem.findViewById(R.id.btnDeleteScale);
        ImageButton btnEditScale = (ImageButton) layoutItem.findViewById(R.id.btnEditScale);

        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(!app.isStandAlone())
        {
            btnDeleteScale.setVisibility(View.GONE);
            btnEditScale.setVisibility(View.GONE);
        }
        else
        {
            btnDeleteScale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendButtonDeleteScale(mileageAllowanceScale);
                }
            });
            btnEditScale.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendButtonEditScale(mileageAllowanceScale);
                }
            });
        }
    }

    @Override
    public void refresh() throws Exception {
        super.refresh();
        Log.d("MileageAllowanceAdapter", "refreshed");
    }

    @Override
    public long getItemId(int position) {
        MileageAllowanceScale item = (MileageAllowanceScale) getItem(position);
        return item.getMileageAllowanceScaleId();
   }

    @Override
    public String getItemStringId(int position) {
        MileageAllowanceScale item = (MileageAllowanceScale) getItem(position);
        return item.getStringId();
    }

    @Override
    public int getPosition(Object item) {
        MileageAllowanceScale castedItem = (MileageAllowanceScale) item;
        for (int i = 0; i < getCount(); ++i) {
            MileageAllowanceScale listItem = (MileageAllowanceScale) getItem(i);
            if (listItem.getMileageAllowanceScaleId() == castedItem.getMileageAllowanceScaleId()) return i;
            if (!listItem.getStringId().isEmpty() && listItem.getStringId().equals(castedItem.getStringId())) return i;

        }
        return -1;
    }


    /* IMPLEMENTED METHODS FROM MileageAllowanceSubject */
    @Override
    public void addListener(MileageAllowanceScaleListener observer) {
        observers.add(observer);
    }

    @Override
    public void removeListener(MileageAllowanceScaleListener observer) {
        observers.remove(observer);
    }

    @Override
    public void clearListeners() {
        observers.clear();
    }

    @Override
    public void setMileageAllowanceScaleListeners(MileageAllowanceScale item) {
        for(int i=0; i<observers.size();++i)
            observers.get(i).onClickItem(item);
    }

    @Override
    public View.OnClickListener itemListOnClickListener(final MileageAllowanceScale item) {
        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //On prévient les listeners qu'il y a eu un clic sur le layoutItem
                setMileageAllowanceScaleListeners(item);
            }
        };
    }

    @Override
    public void sendButtonEditScale(MileageAllowanceScale item) {
        for(int i=0; i<observers.size();++i)
            observers.get(i).onClickButtonEdit(item);
    }

    @Override
    public void sendButtonDeleteScale(MileageAllowanceScale item) {
        for(int i=0; i<observers.size();++i)
            observers.get(i).onClickButtonDelete(item);
    }

    /* OVERRIDED METHODS */
    @Override
    public void add(Object object, boolean with_id, boolean inDB) throws Exception {
        super.add(object, with_id, inDB);
        /* Ajout du barème kilométrique à la liste des barèmes */
        MileageAllowanceScale mileageAllowanceScale = (MileageAllowanceScale) object;

        Log.i("MASAdapter", "mileageAllowanceScale.getRate( = " + mileageAllowanceScale.getRate() + " & mileageAllowanceScale.getOffset() = " + mileageAllowanceScale.getOffset());

        MileageExpenseAmount.mileageAllowanceScaleList.add(mileageAllowanceScale);
    }

    @Override
    public void remove(Object object, boolean fromDB) {
        super.remove(object, fromDB);
        /* Suppression du barème kilométrique de la liste des barèmes */
        MileageAllowanceScale mileageAllowanceScale = (MileageAllowanceScale) object;
        try {
            ListDeleter.removeMileageAllowanceScaleOfList(mileageAllowanceScale.getMileageAllowanceScaleId(), MileageExpenseAmount.mileageAllowanceScaleList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
