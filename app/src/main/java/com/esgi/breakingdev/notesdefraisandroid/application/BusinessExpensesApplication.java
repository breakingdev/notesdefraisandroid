package com.esgi.breakingdev.notesdefraisandroid.application;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.DatePicker;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.model.UserInfo;
import com.esgi.breakingdev.notesdefraisandroid.services.HttpServices;
import com.esgi.breakingdev.notesdefraisandroid.services.UserServices;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseReportDBManager;

import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Tibo Mathieu on 09/05/2016.
 */
public class BusinessExpensesApplication extends Application {

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public boolean isStandAlone() {
        String[] modes = getResources().getStringArray(R.array.application_modes_arrays);
        if(UserInfo.searchValueOf("mode").equals(modes[0])) // CLient-Serveur
            return false;
        return true;
    }

    public boolean isServerConfigOk(){
        if(isStandAlone()) return true;
        if(!UserInfo.searchValueOf("userToken").isEmpty()
            && !UserInfo.searchValueOf("serverURL").isEmpty())
            return true;
        return false;
    }

    public boolean isServerConnectionActive(){
        boolean isServerConnectionActive = false;

        if(!isServerConfigOk())
        {
            return false;
        }


        if(isOnline())
        {
//            String dataUrl = UserInfo.searchValueOf("serverURL");
//            URL url;
//            HttpURLConnection connection = null;
//            try
//            {
//                url = new URL(dataUrl);
//                connection = (HttpURLConnection) url.openConnection();
//
//                HttpServices.getInstance().setHttpURLConnection(connection, HttpServices.MethodRequest.GET, 0);
//
//            } catch (Exception e) {
//
//                e.printStackTrace();
//
//            } finally {
//
//                if (connection != null)
//                {
//                    connection.disconnect();
//                    isServerConnectionActive = true;
//                }
//            }
            UserServices userServices = new UserServices();
            if(!userServices.getUserId().isEmpty())
            {
                isServerConnectionActive = true;
            }
        }

        return isServerConnectionActive;
    }


    /* Format de date à respecter pour la base de données */
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dateFormatForGUI;
    private SimpleDateFormat dateFormatForServer;

    public SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(SimpleDateFormat dateFormat){
        this.dateFormat = dateFormat;
    }

    public static Date getDateFromDatePicker(DatePicker datePicker)
    {
        Calendar calendar = Calendar.getInstance();
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public SimpleDateFormat getDateFormatForGUI() {
        return dateFormatForGUI;
    }

    public void setDateFormatForGUI(SimpleDateFormat dateFormatForGUI) {
        this.dateFormatForGUI = dateFormatForGUI;
    }

    public SimpleDateFormat getDateFormatForServer() {
        return dateFormatForServer;
    }

    public void setDateFormatForServer(SimpleDateFormat dateFormatForServer) {
        this.dateFormatForServer = dateFormatForServer;
    }
}