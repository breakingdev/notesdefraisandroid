package com.esgi.breakingdev.notesdefraisandroid.application;

import android.content.Context;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.MileageAllowanceScalesAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;

/**
 * Created by Jeremy on 12/07/2016.
 */
public class StandaloneDataSet
{
    private Context context;

    public StandaloneDataSet(Context context)
    {
        this.context = context;
    }
    public void generateBusinessCategory()
    {
        try
        {
            BusinessCategory businessCategory = null;

            businessCategory = new BusinessCategory(0,"", "codeTest", "Hôtel", BusinessCategory.FormType.amount.toString());
            BusinessCategoryAdapter.getInstance(context).add(businessCategory,false,true);
            businessCategory = new BusinessCategory(0,"", "codeTest", "Location", BusinessCategory.FormType.amount.toString());
            BusinessCategoryAdapter.getInstance(context).add(businessCategory,false,true);
            businessCategory = new BusinessCategory(0,"", "codeTest", "Restaurant", BusinessCategory.FormType.amount.toString());
            BusinessCategoryAdapter.getInstance(context).add(businessCategory,false,true);
            businessCategory = new BusinessCategory(0,"", "codeTest", "Taxi", BusinessCategory.FormType.amount.toString());
            BusinessCategoryAdapter.getInstance(context).add(businessCategory,false,true);
            businessCategory = new BusinessCategory(0,"", "codeTest", "Déplacement professionnel", BusinessCategory.FormType.mileageExpense.toString());
            BusinessCategoryAdapter.getInstance(context).add(businessCategory,false,true);

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void generateMileageAllowanceScale()
    {
        try
        {
            MileageAllowanceScale.Vehicle voiture = MileageAllowanceScale.Vehicle.car;
            int annee = 2016;

            MileageAllowanceScale scale = null;

            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),3,0,5000, (float)0.410, (float)0.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),3,5001,20000, (float)0.245, (float)824.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),3,20000,100000, (float)0.286, (float)0.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),4,0,5000, (float)0.493, (float)0.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),4,5001,20000, (float)0.277, (float)1082.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),4,20000,100000, (float)0.332, (float)0.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),5,0,5000, (float)0.543, (float)0.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),5,5001,20000, (float)0.305, (float)1188.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),5,20000,100000, (float)0.364, (float)0.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
            scale = new MileageAllowanceScale(0,"",annee,voiture.toString(),6,0,5000, (float)0.680, (float)0.00);
            MileageAllowanceScalesAdapter.getInstance(context).add(scale,false,true);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}
