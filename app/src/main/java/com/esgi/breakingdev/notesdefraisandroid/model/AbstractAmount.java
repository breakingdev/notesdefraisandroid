package com.esgi.breakingdev.notesdefraisandroid.model;

import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

/**
 * Created by Tibo Mathieu  on 16/05/2016.
 */
abstract public class AbstractAmount {


    /* CLASS MEMBERS */
    @DBManagerable long amountID;  // Id du montant
    @DBManagerable long expenseLineID;  // Id de la ligne de frais
    @DBManagerable long expenseReportID;  // Id de la note de frais

    /* CONSTRUCTOR */
    protected AbstractAmount(long amountID, long expenseLineID, long expenseReportID) {
        this.amountID = amountID;
        this.expenseLineID = expenseLineID;
        this.expenseReportID = expenseReportID;
    }

    /* ABSTRACT METHODS */
    public abstract float getValue();

    /* GETTERS AND SETTERS */
    public long getAmountID() {
        return amountID;
    }

    public void setAmountID(long amountID) {
        this.amountID = amountID;
    }

    public long getExpenseLineID() {
        return expenseLineID;
    }

    public void setExpenseLineID(long expenseLineID) {
        this.expenseLineID = expenseLineID;
    }

    public long getExpenseReportID() {
        return expenseReportID;
    }

    public void setExpenseReportID(long expenseReportID) {
        this.expenseReportID = expenseReportID;
    }
}
