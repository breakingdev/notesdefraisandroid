package com.esgi.breakingdev.notesdefraisandroid.model;

import android.content.Context;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.services.ISerializableJson;
import com.esgi.breakingdev.notesdefraisandroid.sql.AbstractDBHelper;
import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Tibo Mathieu on 11/05/2016.
 *
 * Modèle d'une catégorie de dépense
 */
public class BusinessCategory implements Comparator<Object>,Comparable,Serializable,ISerializableJson {

    public enum FormType { amount, // Dépense normale
                           mileageExpense, // Frais kilométrique
                             flateRate }  // Dépense forfaitaire

    /* CLASS MEMBERS */
    @DBManagerable long businessCategoryId;                     // ID d'une catégorie de dépense
    @DBManagerable String stringId;                             // ID d'une catégorie de dépense (id serveur)
    @DBManagerable String code;                                 // Code de la catégorie
    @DBManagerable String description;                          // Description de la catégorie
    @DBManagerable FormType formType = FormType.amount;         // Type de la catégorie


    /* CONSTRUCTORS */
    public BusinessCategory(JSONObject jsonObject,Context appContext){ setObjectFromJson(jsonObject, appContext);}

    public BusinessCategory(long Id,String stringId, String code, String desc, String formType) {
        this.businessCategoryId = Id;
        setStringId(stringId);
        this.code = code;
        this.description = desc;
        setFormType(formType);
    }

    /*  IMPLEMENTED FROM ISerializableJson*/
    @Override
    public JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("businessCategoryId", stringId);
            jsonObject.put("code", code);
            jsonObject.put("description", description);
            jsonObject.put("description", description);
            jsonObject.put("formType", formType.toString());
            jsonObject.put("system", "");
        }catch(Exception e){

        }
        return jsonObject;
    }

    @Override
    public void setObjectFromJson(JSONObject jsonObject, Context appContext) {
        try {
            setStringId(jsonObject.getString("businessCategoryId"));
            setCode(jsonObject.getString("code"));
            setDescription(jsonObject.getString("description"));
            setFormType(jsonObject.getString("formType"));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /* GETTERS AND SETTERS */
    public long getBusinessCategoryId() {
        return businessCategoryId;
    }

    public void setBusinessCategoryId(long businessCategoryId) {
        this.businessCategoryId = businessCategoryId;
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId.replaceAll("\"", "");
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FormType getFormType() {
        return formType;
    }

    public static String getFormTypeString(FormType formType, Context context){
        String value = "";
        if(formType == FormType.amount) {
            value = context.getString(R.string.amountCategory);
        }
        else if(formType == FormType.flateRate) {
            value = context.getString(R.string.flateRateAmountCategory);
        }
        else if(formType == FormType.mileageExpense) {
            value = context.getString(R.string.mileageAmountCategory);
        }
        return value;
    }

    public void setFormType(String fType) {
        if(fType.equals(FormType.amount.toString())){
            formType = FormType.amount;

        }else if(fType.equals(FormType.mileageExpense.toString())){
            formType = FormType.mileageExpense;

        }else if(fType.equals(FormType.flateRate.toString())){
            formType = FormType.flateRate;

        }else throw new ClassCastException("Business category formType " + fType + " is not defined");
    }

    /* IMPLEMENTED METHODS */
    @Override
    public int compareTo(Object another) {
        if(another instanceof BusinessCategory){
            if(businessCategoryId == ((BusinessCategory) another).businessCategoryId){
                if(code.equals(((BusinessCategory) another).getCode())){
                    if(description.equals(((BusinessCategory) another).getDescription())){
                        if(formType.equals(((BusinessCategory) another).getFormType()))
                            return 0;
                    }
                }
            }
        }
        return -1;
    }

    @Override
    public int compare(Object lhs, Object rhs) {
        if(lhs instanceof BusinessCategory && rhs instanceof BusinessCategory){
            return ((BusinessCategory) lhs).compareTo(rhs);
        }
        return -1;
    }

    @Override
    public boolean equals(Object o) {
        if(compareTo(o) == 0)
            return true;
        return false;
    }
}
