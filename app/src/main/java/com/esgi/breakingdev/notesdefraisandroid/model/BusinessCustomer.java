package com.esgi.breakingdev.notesdefraisandroid.model;

import android.content.Context;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.services.ISerializableJson;
import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Jeremy on 31/05/2016.
 */
public class BusinessCustomer implements Comparator<Object>,Comparable,Serializable,ISerializableJson {
    /* CLASS MEMBERS */
    @DBManagerable long businessCustomerId;                     // ID d'un client
    @DBManagerable String stringId;                             // ID d'un client (serveur)
    @DBManagerable String code;                                 // Code du client
    @DBManagerable String description;                          // Description (ou nom) du client

    /* CONSTRUCTORS */
    public BusinessCustomer(JSONObject jsonObject,Context appContext)
    {
        setObjectFromJson(jsonObject, appContext);
    }
    public BusinessCustomer(long Id,String stringId, String code, String desc) {
        this.businessCustomerId = Id;
        setStringId(stringId);
        this.code = code;
        this.description = desc;
    }


    /* PUBLIC STATIC METHODS */
    public static String getCustomerDescription(long id )
    {
        String descriptionClient = "";

        List<Object> listClients = ExpenseLine.businessCustomerList;

        for (Object o : listClients) {
            BusinessCustomer tmpBC = (BusinessCustomer) o;

            if (id == tmpBC.getBusinessCustomerId()) {
                descriptionClient = tmpBC.getDescription();
            }
        }
        return descriptionClient;

    }

    public static String getCustomerDescription(String  stringId )
    {
        String descriptionClient = "";

        List<Object> listClients = ExpenseLine.businessCustomerList;

        for (Object o : listClients) {
            BusinessCustomer tmpBC = (BusinessCustomer) o;

            if (stringId.equals(tmpBC.getStringId())) {
                descriptionClient = tmpBC.getDescription();
            }
        }
        return descriptionClient;

    }

    /* IMPLEMENTED METHODS FROM ISerializableJson */

    @Override
    public JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customerId", stringId);
            jsonObject.put("code",code);
            jsonObject.put("name", description);
        }catch (JSONException e){
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void setObjectFromJson(JSONObject jsonObject, Context appContext) {
        try {
            setStringId(jsonObject.getString("customerId"));
            setCode(jsonObject.getString("code"));
            setDescription(jsonObject.getString("name"));
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    /* IMPLEMENTED METHODS FROM Comparable */
    @Override
    public int compareTo(Object another) {
        if(another instanceof BusinessCustomer){
            if(businessCustomerId == ((BusinessCustomer) another).businessCustomerId){
                if(code.equals(((BusinessCustomer) another).getCode())){
                    if(description.equals(((BusinessCustomer) another).getDescription())){
                        return 0;
                    }
                }
            }
        }
        return -1;
    }

    @Override
    public int compare(Object lhs, Object rhs) {
        if(lhs instanceof BusinessCustomer && rhs instanceof BusinessCustomer){
            return ((BusinessCustomer) lhs).compareTo(rhs);
        }
        return -1;
    }

    @Override
    public boolean equals(Object o) {
        if(compareTo(o) == 0)
            return true;
        return false;
    }


    /* GETTERS AND SETTERS */

    public long getBusinessCustomerId() {
        return businessCustomerId;
    }

    public void setBusinessCustomerId(long businessCustomerId) {
        this.businessCustomerId = businessCustomerId;
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId.replaceAll("\n", "").replaceAll("\"", "");
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



}
