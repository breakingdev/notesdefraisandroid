package com.esgi.breakingdev.notesdefraisandroid.model;

import android.content.Context;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCustomerAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.services.ISerializableJson;
import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Jeremy on 31/05/2016.
 */
public class BusinessProject implements Serializable,ISerializableJson {

    /* CLASS MEMBERS */
    @DBManagerable long businessProjectId;                     // ID d'une catégorie du projet
    @DBManagerable String stringId;                            // ID d'une catégorie du projet (serveur)
    @DBManagerable String code;                                 // Code du projet
    @DBManagerable String description;                          // Description du projet
    @DBManagerable boolean refacturation;                          // Refacturation du projet
    @DBManagerable long idClient;                          // Id Client du projet
    @DBManagerable String stringIdClient;                    // Id Client du projet (serveur)

    public boolean isInternal()
    {
        if(idClient == 0 && stringIdClient.isEmpty()) return true;
        return false;
    }

    /* CONSTRUCTOR */
    public BusinessProject(JSONObject jsonObject,Context appContext){setObjectFromJson(jsonObject, appContext);}
    public BusinessProject(long Id,String stringId, String code, String desc, boolean refacturation, long idClient,String stringIdClient) {
        this.businessProjectId = Id;
        setStringId(stringId);
        this.code = code;
        this.description = desc;
        this.refacturation = refacturation;
        this.idClient = idClient;
        this.stringIdClient = stringIdClient;
    }

    /* PUBLIC STATIC MEMBERS */
    public static List<Object> getProjectsOfCustomer(long customerId)
    {
        List<Object> list = new ArrayList<>();
        for(Object o : ExpenseLine.businessProjectList){
            if(o instanceof BusinessProject){
                if(((BusinessProject) o).getIdClient() == customerId)
                    list.add(o);
            }
        }
        return list;
    }

    /* IMPLEMENTED METHODS FROM ISerializableJson */

    @Override
    public JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("projectId",stringId);
            jsonObject.put("customerId",(stringIdClient.isEmpty() ? "null" : stringIdClient));
            jsonObject.put("code",code);
            jsonObject.put("name",description);
            jsonObject.put("rebilling",refacturation);
        }catch(JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void setObjectFromJson(JSONObject jsonObject, Context appContext) {
        try {
            setStringId(jsonObject.getString("projectId"));
            setStringIdClient(jsonObject.getString("customerId").equals("null") ? "" : jsonObject.getString("customerId"));
            long customerIdLong = 0;
            if(!stringIdClient.isEmpty()) {
                BusinessCustomer businessCustomer = (BusinessCustomer) BusinessCustomerAdapter.getInstance(appContext).getItemWithStringId(stringIdClient);
                customerIdLong = businessCustomer.getBusinessCustomerId();
                setIdClient(customerIdLong);
            }
            setCode(jsonObject.getString("code"));
            setDescription(jsonObject.getString("name"));
            setRefacturation(jsonObject.getBoolean("rebilling"));

        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    /* GETTERS AND SETTERS */
    public long getBusinessProjectId() {
        return businessProjectId;
    }

    public void setBusinessProjectId(long businessProjectId) {
        this.businessProjectId = businessProjectId;
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId.replaceAll("\n", "").replaceAll("\"", "");
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getRefacturation() {
        return refacturation;
    }

    public void setRefacturation(boolean refacturation) {
        this.refacturation = refacturation;
    }

    public long getIdClient() {
        return idClient;
    }

    public void setIdClient(long idClient) {
        this.idClient = idClient;
    }

    public String getStringIdClient() {
        return stringIdClient;
    }

    public void setStringIdClient(String stringIdClient) {
        this.stringIdClient = stringIdClient;
    }
}
