package com.esgi.breakingdev.notesdefraisandroid.model;

import android.content.Context;

import com.esgi.breakingdev.notesdefraisandroid.services.ISerializableJson;
import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Tibo Mathieu on 28/06/2016.
 */
public class BusinessService implements ISerializableJson {
    /* STATIC LIST */
    public static List<Object> businessServicesList;

    /* CLASS MEMBERS */
    @DBManagerable long serviceId;              // ID d'un service
    @DBManagerable String stringId;             // ID d'un service (serveur)
    @DBManagerable String description;          // Description du projet
    @DBManagerable String infos;                // SIRET/SIREN/TVA intra
    @DBManagerable String companyName;          // Nom de l'entreprise


    /* CONSTRUCTOR */
    public BusinessService(JSONObject jsonObject, Context appContext){ setObjectFromJson(jsonObject, appContext);}
    public BusinessService(long serviceId,String stringId, String description, String infos, String companyName) {
        this.serviceId = serviceId;
        setStringId(stringId);
        this.description = description;
        this.infos = infos;
        this.companyName = companyName;
    }


    /* IMPLEMENTED METHODS FROM ISerializableJson */

    @Override
    public JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonOrganization = new JSONObject();
        try {
            jsonOrganization.put("orgId",stringId);
            jsonOrganization.put("displayName",description);
            jsonOrganization.put("orgName",infos);
            jsonObject.put("organization", jsonOrganization);

        }catch(JSONException e){
            e.printStackTrace();
        }
        return jsonObject;
    }

    @Override
    public void setObjectFromJson(JSONObject jsonObject, Context appContext) {
        JSONObject jsonOrganization;
        try {
            jsonOrganization = jsonObject.getJSONObject("organization");
            setStringId(jsonOrganization.getString("orgId"));
            setDescription(jsonOrganization.getString("displayName"));
            setInfos(jsonOrganization.getString("orgName"));
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    /* GETTERS AND SETTERS */
    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId.replaceAll("\n","").replaceAll("\"", "");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInfos() {
        return infos;
    }

    public void setInfos(String infos) {
        this.infos = infos;
    }


    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /* STATIC TEST METHODS */
    public static BusinessService createServicesTest(String label,String infos)
    {
        long id = 1;
        String corpInfo = "Maarch";
        String stringId = "";
        BusinessService service = new BusinessService(id,stringId,label,infos,corpInfo);
        return service;
    }
}
