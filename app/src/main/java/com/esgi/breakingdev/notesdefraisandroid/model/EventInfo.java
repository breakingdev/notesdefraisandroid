package com.esgi.breakingdev.notesdefraisandroid.model;

import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import java.util.List;

/**
 * Created by Tibo Mathieu on 08/07/2016.
 */
public class EventInfo {

    public enum Action { ADD,EDIT};
    public enum ObjectType {ExpenseReport, ExpenseLine,Category, MileageAllowanceScale, Project,Customer,Service  }

    /* STATIC MEMBERS */
    public static List<Object> listEvent;

    /* CLASS MEMBERS */
    @DBManagerable private long id;                 // pas utilisé mais utile pour la connexion avec la bdd
    @DBManagerable private Action action;           // décrit l'action à synchroniser
    @DBManagerable private ObjectType objectType;   // décrit le type de l'objet concerné
    @DBManagerable private long objectId;           // Id de l'objet à synchroniser

    /* CONSTRUCTOR */
    public EventInfo(String actionStr, String objectTypeStr, long objectId) {
        setAction(actionStr);
        setObjectType(objectTypeStr);
        this.objectId = objectId;
    }

    /* GETTERS AND SETTERS */
    public Action getAction() {
        return action;
    }

    public void setAction(String actionTypeStr){
        if(actionTypeStr.equals(Action.ADD.toString()))
            action = Action.ADD;
        else if(actionTypeStr.equals(Action.EDIT.toString()))
            action = Action.EDIT;
    }
    public void setAction(Action action) {
        this.action = action;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectTypeStr){
        if(objectTypeStr.equals(ObjectType.ExpenseReport.toString()))
            objectType = ObjectType.ExpenseReport;
        else if(objectTypeStr.equals(ObjectType.ExpenseLine.toString()))
            objectType = ObjectType.ExpenseLine;
    }

    public void setObjectType(ObjectType objectType) {
        this.objectType = objectType;
    }

    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }
}
