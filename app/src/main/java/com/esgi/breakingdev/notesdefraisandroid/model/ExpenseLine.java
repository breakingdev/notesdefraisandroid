package com.esgi.breakingdev.notesdefraisandroid.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCustomerAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.services.ISerializableJson;
import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by Tibo Mathieu on 16/05/2016.
 */
public class ExpenseLine implements ISerializableJson {

    /* STATIC CLASS MEMBERS */
    public static List<Object> businessCategoryList; // Liste des catégories de dépense
    public static List<Object> simpleAmountList;    // Liste des montants normaux
    public static List<Object> mileageExpenseList;  // Liste des montants kilométriques
    public static List<Object> flateRateList;       // Liste des montants forfaitaires
    public static List<Object> businessProjectList;  // Liste des projets
    public static List<Object> businessCustomerList;       // Liste des clients


    /* CLASS MEMBERS */
    @DBManagerable   private long expenseLineId; // id ligne de frais
    @DBManagerable   private String stringId; // id ligne de frais
    @DBManagerable   private long expenseReportId; // id note de frais
    @DBManagerable   private String expenseReportStringId;  // id note de frais (serveur)
    @DBManagerable   private int lineNumber;  // numéro de la ligne
    @DBManagerable   private Date date;
    @DBManagerable   private long businessCategoryId; // id de la catégorie de dépense
    @DBManagerable   private String businessCategoryStringId; // id de la catégorie de dépense (serveur)
    @DBManagerable   private boolean rebilling;   // Indice de refacturation
    @DBManagerable   private String comment;      // commentaire de la ligne de frais
    @DBManagerable   private String tradingName; // Nom du facturateur
    @DBManagerable   private long customerId; // Id du client
    @DBManagerable   private String customerStringId; // Id du client (serveur)
    @DBManagerable   private long projectId; // Id du projet
    @DBManagerable   private String projectStringId; // Id du projet (serveur)
    @DBManagerable   private String proofReference = "";    // Référence justificatif
    @DBManagerable   private byte[] proofImage;        // Image justificatif

    private BusinessCategory.FormType category; // catégorie de dépense
    private AbstractAmount amount;      // Montant de la note de frais

    public boolean isInternal()
    {
        return customerId == 0;
    }
    /* CONSTRUCTOR */
    public ExpenseLine(JSONObject jsonObject, Context appContext){ setObjectFromJson(jsonObject, appContext);
    }
    public ExpenseLine(long expenseLineId,String stringId, long expenseReportId,String expenseReportStringId,int lineNumber, Date date, long businessCategoryId,String businessCategoryStringId,boolean rebilling, String comment, String tradingName,long customerId,String customerStringId, long projectId,String projectStringId,String proofReference,byte[] proofImage) {
        setVariables(expenseLineId,stringId,expenseReportId,expenseReportStringId,lineNumber,date,businessCategoryId,businessCategoryStringId,rebilling, comment,tradingName,customerId,customerStringId,projectId,projectStringId, proofReference,proofImage);
        setAmountFromList();
    }

    public ExpenseLine(long expenseLineId,String stringId, long expenseReportId,String expenseReportStringId,int lineNumber, Date date, long businessCategoryId,String businessCategoryStringId,boolean rebilling,String comment, String tradingName,long customerId,String customerStringId,long projectId,String projectStringId,AbstractAmount amount,String proofReference,byte[] proofImage)
    {
        setVariables(expenseLineId,stringId,expenseReportId,expenseReportStringId,lineNumber,date,businessCategoryId,businessCategoryStringId,rebilling, comment,tradingName,customerId,customerStringId, projectId,projectStringId,proofReference,proofImage);
        this.amount = amount;
        amount.setExpenseLineID(expenseLineId);
    }

    public ExpenseLine(ExpenseLine another)
    {
        setBusinessCategoryStringId(another.businessCategoryStringId);
        this.expenseReportStringId = another.expenseReportStringId;
        this.expenseLineId = another.expenseLineId;
        this.expenseReportId = another.expenseReportId;
        this.lineNumber = another.lineNumber;
        this.date = another.date;
        this.businessCategoryId = another.businessCategoryId;
        this.rebilling = another.rebilling;
        this.comment = another.comment;
        category = identifyBusinessCategory(this.businessCategoryId);
        this.tradingName = another.tradingName;
        this.projectId = another.projectId;
        setProjectStringId(another.projectStringId);
        this.customerId = another.customerId;
        setCustomerStringId(another.customerStringId);
        this.proofReference = another.proofReference;
        this.proofImage = another.proofImage;

        setExpenseReportStringId(another.expenseReportStringId);
        setStringId(another.stringId);
        this.amount = another.amount;
        amount.setExpenseLineID(another.expenseLineId);
    }

    private void setVariables(long expenseLineId,String stringId, long expenseReportId,String expenseReportStringId,int lineNumber, Date date, long businessCategoryId,String businessCategoryStringId,boolean rebilling, String comment,String tradingName,long customerId,String customerStringId,long projectId,String projectStringId, String proofReference,byte[] proofImage)
    {
        setBusinessCategoryStringId(businessCategoryStringId);
        this.expenseReportStringId = expenseReportStringId;
        this.expenseLineId = expenseLineId;
        this.expenseReportId = expenseReportId;
        this.lineNumber = lineNumber;
        this.date = date;
        this.businessCategoryId = businessCategoryId;
        this.rebilling = rebilling;
        this.comment = comment;
        category = identifyBusinessCategory(this.businessCategoryId);
        this.tradingName = tradingName;
        this.projectId = projectId;
        setProjectStringId(projectStringId);
        this.customerId = customerId;
        setCustomerStringId(customerStringId);
        this.proofReference = proofReference;
        this.proofImage = proofImage;

        setExpenseReportStringId(expenseReportStringId.replaceAll("\n", ""));
        setStringId(stringId);
    }


    /* IMPLEMENTED FROM ISerializableJson */
    @Override
    public JSONObject getJsonObject() {

        String fixExpenseLineId = stringId.replaceAll("\n", "");


        JSONObject jsonObjectExpenseReport= new JSONObject();
        JSONObject jsonObject= new JSONObject();
        try
        {

            jsonObject.put("businessCategoryId", businessCategoryStringId);
            jsonObject.put("customerId", customerStringId);
            jsonObject.put("date", date);
            jsonObject.put("expenseLineId", fixExpenseLineId);
            jsonObject.put("expenseReportId", expenseReportStringId);
            jsonObject.put("file", proofImage);
            jsonObject.put("lineNumber", lineNumber);
            jsonObject.put("nbDay", "");
            jsonObject.put("partyName", tradingName);

            jsonObject.put("projectId", projectStringId);
            jsonObject.put("proofReference", proofReference);
            jsonObject.put("reason",comment);
            jsonObject.put("rebilling", rebilling);
            jsonObject.put("flateRateAmount", "");
            jsonObject.put("amount", getAmountValue());

            // Justificatif
            String fileProof = "";
            if(proofImage != null)
            {
                fileProof = Base64.encodeToString(proofImage, 0);
            }
            jsonObject.put("file", fileProof);

            if(amount instanceof MileageExpenseAmount)
            {
                jsonObject.put("VatAmount", "");
                jsonObject.put("distance", ((MileageExpenseAmount) amount).getKilometers());
                jsonObject.put("taxHorsepower", ((MileageExpenseAmount) amount).getHorsePower());

            }
            else if(amount instanceof SimpleAmount)
            {
                jsonObject.put("VatAmount", ((SimpleAmount) amount).getAmountTVA());
                jsonObject.put("distance", "");
                jsonObject.put("taxHorsepower", "");
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObjectExpenseReport.put("expenseLine", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObjectExpenseReport;
    }

    @Override
    public void setObjectFromJson(JSONObject jsonObject,Context appContext) {
        BusinessExpensesApplication app = (BusinessExpensesApplication) appContext.getApplicationContext();
        SimpleDateFormat sdfServer = app.getDateFormatForServer();
        try {

            setStringId(jsonObject.getString("expenseLineId"));
            setExpenseReportStringId(jsonObject.getString("expenseReportId"));
            if(expenseReportStringId.isEmpty())
                setExpenseReportId(0);
            else {
                ExpenseReport reportRelated = (ExpenseReport) ExpenseReportAdapter.getInstance(appContext).getItemWithStringId(expenseReportStringId);
                setExpenseReportId(reportRelated.getExpenseReportId());
            }
            setLineNumber(jsonObject.getInt("lineNumber"));
            setDate(sdfServer.parse(jsonObject.getString("date")));
            setBusinessCategoryStringId(jsonObject.getString("businessCategoryId"));
            if(businessCategoryStringId.isEmpty())
                setBusinessCategoryId(0);
            else {
                BusinessCategory businessCategory = (BusinessCategory) BusinessCategoryAdapter.getInstance(appContext).getItemWithStringId(businessCategoryStringId);
                setBusinessCategoryId(businessCategory.getBusinessCategoryId());
                category = identifyBusinessCategory(this.businessCategoryId);
            }


            setRebilling(jsonObject.getBoolean("rebilling"));
            setComment(jsonObject.getString("reason"));
            setTradingName(jsonObject.getString("partyName"));

            setCustomerStringId(jsonObject.getString("customerId").equals("null") ? "" : jsonObject.getString("customerId"));
            if(customerStringId.isEmpty())
                setCustomerId(0);
            else {
                BusinessCustomer customerRelated = (BusinessCustomer) BusinessCustomerAdapter.getInstance(appContext).getItemWithStringId(customerStringId);
                setCustomerId(customerRelated.getBusinessCustomerId());
            }

            setProjectStringId(jsonObject.getString("projectId").equals("null") ? "" : jsonObject.getString("projectId"));
            if(projectStringId.isEmpty())
                setProjectId(0);
            else {
                BusinessProject businessProject = (BusinessProject) BusinessProjectAdapter.getInstance(appContext).getItemWithStringId(projectStringId);
                setProjectId(businessProject.getBusinessProjectId());
            }

//            jsonObject.put("file", proofImage);
            jsonObject.put("nbDay", "");
            setProofReference(jsonObject.getString("proofReference"));
            if(category.equals(BusinessCategory.FormType.amount)){
                float ttc = (float)jsonObject.getDouble("amount");
                float tva = jsonObject.getString("VatAmount").equals("null") ? 0.f : (float)jsonObject.getDouble("VatAmount");
                amount = new SimpleAmount(0,expenseLineId,expenseReportId, ttc,tva);

            }else if(category.equals(BusinessCategory.FormType.mileageExpense)){
                float distance = (float) jsonObject.getDouble("distance");
                int horsePower = jsonObject.getInt("taxHorsepower");
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                int year = cal.get(Calendar.YEAR);
                amount = new MileageExpenseAmount(0,expenseLineId,expenseReportId,year,distance, MileageAllowanceScale.Vehicle.car.toString(),horsePower);
            }

            jsonObject.put("flateRateAmount", "");
            jsonObject.put("file", "");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAmountFromList() {
        if(category == BusinessCategory.FormType.amount) {
            amount = getAmountInList(simpleAmountList);

        }else if(category == BusinessCategory.FormType.mileageExpense){
            amount = getAmountInList(mileageExpenseList);

        }else if(category == BusinessCategory.FormType.flateRate){
            amount =  getAmountInList(flateRateList);
        }
        else
            Log.d("ExpenseLine","setAmountFromList: amount not found !!");
    }

    /* METHODS */
    private BusinessCategory.FormType identifyBusinessCategory(long catId)
    {
        for(Object o : businessCategoryList){
            BusinessCategory cat = (BusinessCategory) o;
            if(cat.getBusinessCategoryId() == catId) {
                return cat.getFormType();
            }
        }
        return BusinessCategory.FormType.amount; // Montant simple Par défault
    }

    public float getAmountValue()
    {
        if(amount == null) return 0;
        return amount.getValue();
    }

    private AbstractAmount getAmountInList(List<Object> list)
    {
        for(Object o : list){
            AbstractAmount amount = (AbstractAmount) o;
            if(amount.getExpenseLineID() == expenseLineId) return amount;
        }
//        Log.d("ExpenseLine","amount not found for expenseLineId = " + expenseLineId);
        return null; // montant non trouvé
    }

    /* PUBLIC STATIC METHODS */
    public static void deleteAmountWithReportIdInList(long expenseReportId,List<Object> list)
    {
        int size = list.size();
        for(int i=0; i<size;i++)
        {
            Object o = list.get(i);
            boolean deleted =  true;
            if(o instanceof SimpleAmount){
                if(((SimpleAmount) o).getExpenseReportID() == expenseReportId)
                    list.remove(i);
            }else if(o instanceof MileageExpenseAmount){
                if(((MileageExpenseAmount) o).getExpenseReportID() == expenseReportId)
                    list.remove(i);
            }else if(o instanceof FlateRateAmount){
                if(((FlateRateAmount) o).getExpenseReportID() == expenseReportId)
                    list.remove(i);
            }else
                deleted = false;
            if(deleted){
                i--;
                size--;
            }
        }
    }

    public static void updateAmountList( AbstractAmount newAmount )
    {
        List<Object> list = null;
        if(newAmount instanceof SimpleAmount)
            list = simpleAmountList;
        else if (newAmount instanceof MileageExpenseAmount)
            list = mileageExpenseList;
        else if (newAmount instanceof FlateRateAmount)
            list = flateRateList;

        int i = 0;
        for(Object o : list ){
            AbstractAmount a = (AbstractAmount) o;
            if(a.getExpenseLineID() == newAmount.getExpenseLineID())
            {
                list.remove(i);
                Log.d("ExpenseLine","updateAmountList -> Remove object !");
                break;
            }
            ++i;
        }
        list.add(newAmount);
    }

    /* GETTERS AND SETTERS */
    public long getExpenseLineId() {
        return expenseLineId;
    }

    public void setExpenseLineId(long expenseLineId)
    {
        this.expenseLineId = expenseLineId;
        amount.setExpenseLineID(expenseLineId);
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId.replaceAll("\n", "");
        this.stringId = this.stringId .replaceAll("\"", "");
    }

    public long getExpenseReportId() {
        return expenseReportId;
    }

    public void setExpenseReportId(long expenseReportId) {
        this.expenseReportId = expenseReportId;
    }

    public String getExpenseReportStringId() {
        return expenseReportStringId;
    }

    public void setExpenseReportStringId(String expenseReportStringId) {
        this.expenseReportStringId = expenseReportStringId;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BusinessCategory.FormType getCategory() {
        return category;
    }

    private void setCategory(BusinessCategory.FormType category) {
        this.category = category;
    }

    public long getBusinessCategoryId() {
        return businessCategoryId;
    }

    public void setBusinessCategoryId(long businessCategoryId) {
        this.businessCategoryId = businessCategoryId;
    }

    public String getBusinessCategoryStringId() {
        return businessCategoryStringId;
    }

    public void setBusinessCategoryStringId(String businessCategoryStringId) {
        this.businessCategoryStringId = businessCategoryStringId.replaceAll("\n","").replaceAll("\"","");
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isRebilling() {
        return rebilling;
    }

    public void setRebilling(boolean rebilling) {
        this.rebilling = rebilling;
    }

    public AbstractAmount getAmount() {
        return amount;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getProjectStringId() {
        return projectStringId;
    }

    public void setProjectStringId(String projectStringId) {
        this.projectStringId = projectStringId.replaceAll("\n","").replaceAll("\"","");
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerStringId() {
        return customerStringId;
    }

    public void setCustomerStringId(String customerStringId) {
        this.customerStringId = customerStringId.replaceAll("\n","").replaceAll("\"","");
    }

    public String getProofReference() {
        return proofReference;
    }

    public void setProofReference(String proofReference) {
        this.proofReference = proofReference;
    }

    public byte[] getProofImage() {
        return proofImage;
    }

    public void setProofImage(byte[] proofImage) {
        this.proofImage = proofImage;
    }
}
