package com.esgi.breakingdev.notesdefraisandroid.model;

import android.content.Context;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.services.ISerializableJson;
import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Jeremy on 02/04/2016.
 */
public class ExpenseReport implements Comparable<ExpenseReport>, Comparator<ExpenseReport>,Serializable,ISerializableJson {

    public enum Status  {inEdition, inValidation, reject, inAccounting };

    /* CLASS MEMBERS */
    @DBManagerable    private long expenseReportId = 0;
    @DBManagerable    private String stringId = "";
    @DBManagerable    private long serviceId = 0;
    @DBManagerable    private String serviceStringId = "";
    @DBManagerable    private String validatorId = "0"; // non null
    @DBManagerable    private String personalId = ""; // non null
    @DBManagerable    private String description = ""; // non null
    @DBManagerable    Status status = Status.inEdition;
    @DBManagerable    private Date beginDate = new Date(); // non null
    @DBManagerable    private Date endDate = new Date(); // non null
    @DBManagerable    Date sendToValidatorDate = new Date();
    @DBManagerable    Date validationDate = new Date();
    @DBManagerable    String validatorComment = "";

    /* CONSTRUCTORS */
    public ExpenseReport(){   }
    public ExpenseReport(JSONObject jsonObject,Context appContext){  setObjectFromJson(jsonObject,appContext);  }

    public ExpenseReport(long expenseReportId,String strId, long serviceId,String serviceStringId,String validatorId,String personalId,String description,  String status, Date beginDate,  Date endDate, Date sendToValidatorDate, Date validationDate,String validatorComment) {
        this.expenseReportId = expenseReportId;
        setStringId(strId);
        this.serviceId = serviceId;
        this.serviceStringId = serviceStringId;
        this.validatorId = validatorId;
        this.personalId = personalId;
        this.description = description;
        setStatus(status);
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.sendToValidatorDate = sendToValidatorDate;
        this.validationDate = validationDate;
        this.validatorComment = validatorComment;

    }

    public ExpenseReport(String description, String status, Date beginDate,  Date endDate) {
        this.description = description;
        setStatus(status);
        setBeginDate(beginDate);
        setEndDate(endDate);
        this.serviceId = 0;
    }

    public ExpenseReport( String description, String stringId, String status, Date beginDate,  Date endDate, long serviceId, String serviceStringId) {
        this.description = description;
        this.stringId = stringId;
        setStatus(status);
        setBeginDate(beginDate);
        setEndDate(endDate);
        this.serviceId = serviceId;
        this.serviceStringId = serviceStringId;
    }

    public ExpenseReport(ExpenseReport another)
    {
        this.expenseReportId = another.expenseReportId;
        setStringId(another.stringId);
        this.serviceId = another.serviceId;
        this.serviceStringId = another.serviceStringId;
        this.validatorId = another.validatorId;
        this.personalId = another.personalId;
        this.description = another.description;
        setStatus(another.getStatus().toString());
        this.beginDate = another.beginDate;
        this.endDate = another.endDate;
        this.sendToValidatorDate = another.sendToValidatorDate;
        this.validationDate = another.validationDate;
        this.validatorComment = another.validatorComment;
    }


    /* PUBLIC STATIC METHODS */
    public static boolean isConflicted(Date dateDebut, Date dateFin,List<Object> expenseReportList, long idToExclude,long serviceId)
    {
        boolean conflict = false;
        for(Object o : expenseReportList){
            if(o instanceof ExpenseReport){
                if(idToExclude == ((ExpenseReport) o).getExpenseReportId()) continue;
                if(serviceId != ((ExpenseReport) o).getServiceId()) continue;
                if(dateDebut.equals(((ExpenseReport) o).getBeginDate())) conflict = true;
                if(dateDebut.equals(((ExpenseReport) o).getEndDate())) conflict = true;
                if(dateFin.equals(((ExpenseReport) o).getBeginDate())) conflict = true;
                if(dateFin.equals(((ExpenseReport) o).getEndDate())) conflict = true;
                if((dateDebut.compareTo(((ExpenseReport) o).getBeginDate()) > 0) &&
                        (dateDebut.compareTo(((ExpenseReport) o).getEndDate()) < 0)) conflict = true;
                if((dateFin.compareTo(((ExpenseReport) o).getBeginDate()) > 0) &&
                        (dateFin.compareTo(((ExpenseReport) o).getEndDate()) < 0)) conflict = true;
                if((dateDebut.compareTo(((ExpenseReport) o).getBeginDate()) < 0) &&
                        (dateFin.compareTo(((ExpenseReport) o).getEndDate()) > 0)) conflict = true;
            }

            if(conflict) break;
        }
        return conflict;
    }

    /* IMPLEMENTED FROM ISerializableJson */
    @Override
    public JSONObject getJsonObject() {
        JSONObject jsonObjectExpenseReport= new JSONObject();
        JSONObject jsonObject= new JSONObject();
        try
        {
            jsonObject.put("expenseReportId", stringId);
            jsonObject.put("serviceId", serviceStringId);
            jsonObject.put("validatorId", validatorId);
            jsonObject.put("personalId", personalId);
            jsonObject.put("status", status.toString());
            jsonObject.put("description",description);
            jsonObject.put("beginDate", beginDate);
            jsonObject.put("endDate", endDate);
            jsonObject.put("sendToValidationDate", sendToValidatorDate);
            jsonObject.put("validationDate", validationDate);
            jsonObject.put("accountantId", "");
            jsonObject.put("closingDate", "");
            jsonObject.put("validatorComment", validatorComment.isEmpty() ? "" : validatorComment);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            jsonObjectExpenseReport.put("expenseReport", jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObjectExpenseReport;
    }

    @Override
    public void setObjectFromJson(JSONObject jsonObject,Context appContext) {
        if(jsonObject == null)
            throw new RuntimeException("cannot set ExpenseReport object if jsonObject is null !");
        BusinessServiceAdapter serviceAdapter;
        BusinessExpensesApplication app = (BusinessExpensesApplication) appContext.getApplicationContext();
        SimpleDateFormat sdfServer = app.getDateFormatForServer();
        String value = "";
        try {
            serviceAdapter = BusinessServiceAdapter.getInstance( appContext);
            setStringId(jsonObject.getString("expenseReportId"));
            setServiceStringId(jsonObject.getString("serviceId"));
            setValidatorId(jsonObject.getString("validatorId"));
            setPersonalId(jsonObject.getString("personalId"));
            BusinessService serviceRelated = (BusinessService) serviceAdapter.getItemWithStringId(serviceStringId);
            setServiceId(serviceRelated.getServiceId());
            setStatus(jsonObject.getString("status"));
            setDescription(jsonObject.getString("description"));
            setBeginDate(sdfServer.parse(jsonObject.getString("beginDate")));
            setEndDate(sdfServer.parse(jsonObject.getString("endDate")));
            value =  jsonObject.getString("sendToValidationDate");
            if(!value.isEmpty() && !value.equals("null"))
                setSendToValidatorDate(sdfServer.parse(value));
            value = jsonObject.getString("validationDate");
            if(!value.isEmpty() && !value.equals("null"))
                setValidationDate(sdfServer.parse(value));
            setValidatorComment(jsonObject.getString("validatorComment").equals("null") ? "" : jsonObject.getString("validatorComment") );

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /* GETTERS AND SETTERS */
    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceStringId() {
        return serviceStringId;
    }

    public void setServiceStringId(String serviceStringId) {
        this.serviceStringId = serviceStringId;
    }

    public String getValidatorId() { return validatorId; }

    public void setValidatorId(String validatorId) { this.validatorId = validatorId; }

    public String getDescription() {
//        Log.i("ExpRport GET", "description=" + description);
        return description; }

    public void setDescription(String description) {
//        Log.i("ExpRport SET", "description=" + description);
        this.description = description;
    }

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(String stat) {
        if(stat.equals(Status.inEdition.toString())){
            this.status = Status.inEdition;
        }else if(stat.equals(Status.inValidation.toString()))
            this.status = Status.inValidation;
        else if(stat.equals(Status.reject.toString()))
            this.status = Status.reject;
        else if(stat.equals(Status.inAccounting.toString()))
            this.status = Status.inAccounting;
        else
            throw new ClassCastException("ExpenseReport.Status " + stat + " is not defined");

    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }


    public Date getEndDate() {
        return endDate;
    }


    public void setEndDate(Date endDate) {
        this.endDate = endDate;

    }

    public Date getSendToValidatorDate() {
        return sendToValidatorDate;
    }


    public void setSendToValidatorDate(Date sendToValidatorDate) {
        this.sendToValidatorDate = sendToValidatorDate;
    }


    public Date getValidationDate() {
        return validationDate;
    }


    public void setValidationDate(Date validationDate)
    {
        this.validationDate = validationDate;
    }


    public long getExpenseReportId() {
        return expenseReportId;
    }

    public void setExpenseReportId(long expenseReportId) {
        this.expenseReportId = expenseReportId;
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId.replaceAll("\"", "");
        this.stringId = stringId.replaceAll("\n", "");
    }

    public String getValidatorComment() {
        return validatorComment;
    }

    public void setValidatorComment(String validatorComment) {
        this.validatorComment = validatorComment.replaceAll("\n","").replaceAll("\"","");
    }

    /* IMPLEMENTED METHODS */

    @Override
    public int compareTo(ExpenseReport another) {
        if(getExpenseReportId() == another.getExpenseReportId()) {
            if(getDescription().equals(another.getDescription())){
                if(getBeginDate().equals(another.getBeginDate())){
                    return 0;
                }

            }
        }
        else if(getExpenseReportId() < another.getExpenseReportId()){
            return (int)(getExpenseReportId()-another.getExpenseReportId());
        }
        else
            return (int)(getExpenseReportId()-another.getExpenseReportId());
        return -1;
    }

    @Override
    public int compare(ExpenseReport lhs, ExpenseReport rhs) {
        return lhs.compareTo(rhs);
    }

    @Override
    public boolean equals(Object o) {
        ExpenseReport castedObject = (ExpenseReport) o;
        if(this.compareTo(castedObject) == 0) return true;
        return false;
    }





}
