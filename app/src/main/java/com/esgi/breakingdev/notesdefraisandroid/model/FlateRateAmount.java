package com.esgi.breakingdev.notesdefraisandroid.model;

import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import java.util.List;

/**
 * Created by Tibo Mathieu  on 16/05/2016.
 *
 * Montant forfaitaires
 */
public class FlateRateAmount extends AbstractAmount {

    /* CLASS MEMBERS */
    @DBManagerable private int days; // nombre de jours
    @DBManagerable private float rate; // taux de frais à appliquer par jour

    /* CONSTRUCTOR */
    public FlateRateAmount(long simpleAmountID, long expenseLineID,long expenseReportID, int days, float rate) {
        super(simpleAmountID, expenseLineID,expenseReportID);
        this.days = days;
        this.rate = rate;
    }

    /* METHODS */
    @Override
    public float getValue() {
        return days * rate;
    }

    /* GETTERS AND SETTERS */
    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }
}
