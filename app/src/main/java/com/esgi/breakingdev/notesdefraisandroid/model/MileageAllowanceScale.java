package com.esgi.breakingdev.notesdefraisandroid.model;

import android.content.Context;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.services.ISerializableJson;
import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Tibo Mathieu on 11/05/2016.
 *
 *  Modèle d'un barème kilométrique
 */
public class MileageAllowanceScale implements Comparator<Object>,Comparable,Serializable,ISerializableJson {

    public enum Vehicle { car, moto}

    public static List<Vehicle> listVehicles = new ArrayList<Vehicle>(){{
        add(Vehicle.car);
        add(Vehicle.moto);
    }};

    /* CLASS MEMBERS */
    @DBManagerable private long mileageAllowanceScaleId;  // ID du barème
    @DBManagerable private String stringId;               // ID du barème (serveur)
    @DBManagerable private int mileageAllowanceYear;      // Année du barème
    @DBManagerable private Vehicle vehicle = Vehicle.car; // type de véhicule (voiture par défault)
    @DBManagerable private int horsePower;                 // Nombre de Chevaux Fiscaux
    @DBManagerable private int minDistance;                // Min de l'intervalle de distance
    @DBManagerable private int maxDistance;                // Max de l'intervalle de distance
    @DBManagerable private float rate;                     // Coeeficient à appliquer au calcul du frais kilométrique
    @DBManagerable private float offset;                     // Offset à appliquer au calcul du frais kilométrique


    /* CONSTRUCTORS */
    public MileageAllowanceScale(JSONObject jsonObject,Context appContext){ setObjectFromJson(jsonObject, appContext);}
    public MileageAllowanceScale(long mileageAllowanceScaleId,String stringId, int mileageAllowanceYear, String vehicle, int horsePower, int minDistance, int maxDistance, float rate, float offset) {

        this.mileageAllowanceScaleId = mileageAllowanceScaleId;
        setStringId(stringId);
        this.mileageAllowanceYear = mileageAllowanceYear;
        setVehicle(vehicle);
        this.horsePower = horsePower;
        this.minDistance = minDistance;
        this.maxDistance = maxDistance;
        this.rate = rate;
        this.offset = offset;

        Log.i("MileageAllowanceScale", "(construct) rate = " + this.rate + " & (construct) offset = " +  this.offset);
    }

    /* METHODS */
    public float calculateMileageAmount(float kilometers)
    {
        return rate * kilometers + offset;
    }

    public boolean canCalculateAmountFor(MileageExpenseAmount amount)
    {
        if(amount.getVehicle().equals(vehicle)) {
            if (amount.getYear() == mileageAllowanceYear) {
                if (amount.getHorsePower() == horsePower){
                    if(amount.getKilometers() >= getMinDistance() && amount.getKilometers() <= getMaxDistance()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* PUBLIC STATIC METHODS */
    public static String getVehicleString(Vehicle vehicle, Context context) {
        String value = "";
        switch(vehicle)
        {
            case car: value = context.getString(R.string.car); break;
            case moto: value = context.getString(R.string.moto); break;
            default: value = "null";
        }
        return value;
    }

    public static List<String> getListVehicles(Context context)
    {
        List<String> list = new ArrayList<>();
        for(Vehicle v : listVehicles)
        {
            list.add(getVehicleString(v,context));
        }
        return list;
    }

    /* IMPLEMENTED METHODS FROM ISerializableJson */

    @Override
    public JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mileageAllowanceScaleId",stringId);
            jsonObject.put("category", vehicle.toString());
            jsonObject.put("year", mileageAllowanceYear);
            jsonObject.put("taxHorsepower", horsePower);
            jsonObject.put("rangeBegin", minDistance);
            jsonObject.put("rangeEnd", maxDistance);
            jsonObject.put("rate", rate);
            jsonObject.put("regularization", offset < 0.000001 ? "null" : offset);

        }catch(JSONException e){
            e.printStackTrace();
        }
        return jsonObject;

    }

    @Override
    public void setObjectFromJson(JSONObject jsonObject, Context appContext) {
        try {
            setStringId(jsonObject.getString("mileageAllowanceScaleId"));
            setVehicle(jsonObject.getString("category"));
            setMileageAllowanceYear(jsonObject.getInt("year"));
            setHorsePower(jsonObject.getInt("taxHorsepower"));
            setMinDistance(jsonObject.getInt("rangeBegin"));
            setMaxDistance(jsonObject.getInt("rangeEnd"));
            setRate((float) jsonObject.getDouble("rate"));
            float regularization = 0;
            if (!jsonObject.getString("regularization").equals("null"))
                regularization = (float) jsonObject.getDouble("regularization");
            setOffset(regularization);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* GETTERS AND SETTERS */
    public long getMileageAllowanceScaleId() {
        return mileageAllowanceScaleId;
    }

    public void setMileageAllowanceScaleId(long mileageAllowanceScaleId) {
        this.mileageAllowanceScaleId = mileageAllowanceScaleId;
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId.replaceAll("\n","").replaceAll("\"", "");
    }

    public int getMileageAllowanceYear() {
        return mileageAllowanceYear;
    }

    public void setMileageAllowanceYear(int mileageAllowanceYear) {
        this.mileageAllowanceYear = mileageAllowanceYear;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        if(vehicle.equals(Vehicle.car.toString())){
            this.vehicle = Vehicle.car;
        }else if(vehicle.equals(Vehicle.moto.toString())){
            this.vehicle = Vehicle.moto;
        }else throw new ClassCastException("Vehicle " + vehicle + " is not defined");
    }

    public int getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(int minDistance) {
        this.minDistance = minDistance;
    }

    public float getOffset() {
        return offset;
    }

    public void setOffset(float offset) {
        this.offset = offset;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    /* IMPLEMENTED METHODS */
    @Override
    public int compareTo(Object another) {
        if (another instanceof MileageAllowanceScale) {
            if (mileageAllowanceYear == ((MileageAllowanceScale) another).getMileageAllowanceYear()) {
                if (vehicle.equals(((MileageAllowanceScale) another).getVehicle())) {
                    if (horsePower == ((MileageAllowanceScale) another).getHorsePower()) {
                        // tester si another.minDistance n'est pas compris entre min et max
                        int otherMinDistance =  ((MileageAllowanceScale) another).getMinDistance();
                        if(otherMinDistance >= minDistance && otherMinDistance <= maxDistance)
                            return 0;
                        // tester si another.maxDistance n'est pas compris entre min et max
                        int otherMaxDistance = ((MileageAllowanceScale) another).getMaxDistance();
                        if (otherMaxDistance >= minDistance && otherMaxDistance <= maxDistance)
                           return 0;

                        if(otherMinDistance < minDistance && otherMaxDistance > maxDistance)
                            return 0;
                    }
                }
            }
        }
        return -1;
    }

    @Override
    public int compare(Object lhs, Object rhs) {
        if(lhs instanceof MileageAllowanceScale && rhs instanceof MileageAllowanceScale){
            return ((MileageAllowanceScale) lhs).compareTo(rhs);
        }
        return -1;
    }

    @Override
    public boolean equals(Object o) {
        if(compareTo(o) == 0)
            return true;
        return false;
    }

}

