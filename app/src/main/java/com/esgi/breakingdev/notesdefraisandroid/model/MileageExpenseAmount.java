package com.esgi.breakingdev.notesdefraisandroid.model;

import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import java.util.List;

/**
 * Created by Tibo Mathieu  on 16/05/2016.
 *
 * Représente les montants kilométriques
 */
public class MileageExpenseAmount extends AbstractAmount {

    /* STATIC CLASS MEMBERS */
    public static List<Object> mileageAllowanceScaleList;   // Liste des barèmes kilométriques

    /* CLASS MEMBERS */
    @DBManagerable private int year;
    @DBManagerable private float kilometers;
    @DBManagerable private MileageAllowanceScale.Vehicle vehicle;
    @DBManagerable private int horsePower;


    /* CONSTRUCTOR */
    public MileageExpenseAmount(long simpleAmountID, long expenseLineID,long expenseReportID, int year, float kilometers, String vehicle, int horsePower) {
        super(simpleAmountID, expenseLineID,expenseReportID);
        this.year = year;
        this.kilometers = kilometers;
        setVehicle(vehicle);
        this.horsePower = horsePower;
    }

    /* METHODS */
    @Override
    public float getValue() {
        if(mileageAllowanceScaleList == null)
            Log.d("MileageExpenseAmount","list MileageAllance Scale List is null !");
        for(Object o : mileageAllowanceScaleList)
        {
            MileageAllowanceScale bareme = (MileageAllowanceScale) o;
            if(bareme.getMileageAllowanceYear() == year){
                if(bareme.getVehicle().equals(vehicle)){
                    if(bareme.getHorsePower() == horsePower){
                        if(kilometers >= bareme.getMinDistance() && kilometers <= bareme.getMaxDistance())
                            return bareme.calculateMileageAmount(kilometers);
                    }
                }

            }
        }
        return -1; // aucun bareme compatible trouvé
    }

    /* PUBLIC STATIC METHODS */
    public static boolean doesScaleExistFor(MileageExpenseAmount amount)
    {
        for(Object o : mileageAllowanceScaleList){
            MileageAllowanceScale item = (MileageAllowanceScale) o;
            if(item.canCalculateAmountFor(amount))
                return true;
        }
        return false;
    }



    /* GETTERS AND SETTERS */
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getKilometers() {
        return kilometers;
    }

    public void setKilometers(float kilometers) {
        this.kilometers = kilometers;
    }

    public MileageAllowanceScale.Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(String veh) {
        if(veh.equals(MileageAllowanceScale.Vehicle.car.toString())){
            vehicle = MileageAllowanceScale.Vehicle.car;

        }else if(veh.equals(MileageAllowanceScale.Vehicle.moto.toString())){
            vehicle = MileageAllowanceScale.Vehicle.moto;

        }else throw new ClassCastException("MileageExpenseAmount vehicle  " + veh + " is not defined");
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

}
