package com.esgi.breakingdev.notesdefraisandroid.model;

import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

/**
 * Created by Tibo Mathieu  on 16/05/2016.
 *
 * Représente les "Montant simples" (catégorie frais normaux)
 */
public class SimpleAmount extends AbstractAmount {

    /* CLASS MEMBERS */
    @DBManagerable float amountTTC;     // montant TTC
    @DBManagerable float amountTVA;     // montant TVA
//    @DBManagerable String devise;     // Devise ?

    @Override
    public float getValue() {
        return amountTTC;
    }

    /* CONSTRUCTOR */
    public SimpleAmount(long simpleAmountID, long expenseLineID,long expenseReportID, float amountTTC, float amountTVA) {
        super(simpleAmountID, expenseLineID,expenseReportID);
        this.amountTTC = amountTTC;
        this.amountTVA = amountTVA;
    }

    /* GETTERS AND SETTERS */
    public float getAmountTTC() {
        return amountTTC;
    }

    public void setAmountTTC(float amountTTC) {
        this.amountTTC = amountTTC;
    }

    public float getAmountTVA() {
        return amountTVA;
    }

    public void setAmountTVA(float amountTVA) {
        this.amountTVA = amountTVA;
    }

}
