package com.esgi.breakingdev.notesdefraisandroid.model;

import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.sql.DBManagerable;

import java.util.List;
import java.util.Map;

/**
 * Created by Tibo Mathieu on 01/07/2016.
 */
public class UserInfo {
    /* STATIC MEMBERS */
    public static List<Object> listData;

    /* CLASS MEMBERS */
    @DBManagerable private long id;             // pas utilisé mais utile pour la connexion avec la bdd
    @DBManagerable private String Key;
    @DBManagerable private String Value;

    /* CONSTRUCTOR */
    public UserInfo(String key, String value) {
        Key = key;
        Value = value;
    }

    /* STATIC PUBLIC METHODS */
    public static String searchValueOf(String searchKey)
    {
        String resultValue = "";

        for(Object infosEnCours : listData)
        {
            UserInfo userInfos = (UserInfo) infosEnCours;
            if(userInfos.getKey().equals(searchKey))
            {
                resultValue = userInfos.getValue();
            }
        }
        Log.d("UserInfo", "searchValue of " + searchKey + " = " + resultValue);
        return resultValue;
    }




    /* GETTERS AND SETTERS */
    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }


}
