package com.esgi.breakingdev.notesdefraisandroid.services;

import android.util.Log;

/**
 * Created by Tibo Mathieu on 07/07/2016.
 */
public abstract class AbstractHttpServices {
    protected HttpServices httpServices;

    public String getUrlServer() {
        return httpServices.getUrlServer();
    }

    protected static String ROOT_NAME = "/businessExpenses";
    protected static String LIST_ROUTE = "/index";
    protected static String RESOURCE_ROUTE = "/resource";
    protected String getRootUrlServer(){return "http://" + getUrlServer() + ROOT_NAME; }
    protected String getFullObjectUrl(){return getRootUrlServer() + getObjectRoute(); }
    protected String getFullListObjectUrl(){return getRootUrlServer() + getObjectRoute() + LIST_ROUTE; }
//    protected String getSpecialListObjectUrl(){return getSpecialObjectUrl(); }
    protected String getResourceObjectUrl(){ return getFullObjectUrl() + RESOURCE_ROUTE; }

    protected AbstractHttpServices(){
        httpServices = HttpServices.getInstance();
    }

    protected abstract String getObjectRoute();
//    protected abstract String getSpecialObjectUrl();

    public String editObject(String dataJSON) {
        String dataUrl = getFullObjectUrl();

        Log.i("editObject", "dataUrl (JPA) = " + dataUrl);
        Log.i("editObject", "urlServer (JPA) = " + getUrlServer());

        return httpServices.putJSONService(dataUrl, dataJSON);
    }

    public String editObject(String urlRoute, String idObject) {
        String dataUrl = getFullObjectUrl() +  urlRoute + idObject;

        Log.i("editObject", "dataUrl (JPA) = " + dataUrl);
        Log.i("editObject", "urlServer (JPA) = " + getUrlServer());

        return httpServices.putObject(dataUrl);
    }

    public String deleteObject(String id) {
        String url = getFullObjectUrl() + "/" + id;
        Log.i("delete " + getObjectRoute(), "url = " + url);
        return httpServices.deleteJSONService(url);
    }

    public String postObject(String dataJSON) {
        String dataUrl = getFullObjectUrl();
        return httpServices.postJSONService(dataUrl, dataJSON);
    }

    public String getListObject(String id){
        String dataURL = getFullListObjectUrl() + "/" + id;
        return httpServices.getJSONService(dataURL);
    }

    public String getListObject(){
        String dataUrl = getFullListObjectUrl();

        Log.i("getListObject", "dataUrl = " + dataUrl);

        return httpServices.getJSONService(dataUrl);
    }


    public String getObject(String id)
    {
        return "";
    }

    public String getResourceOfObject(String id){
        String dataUrl = getResourceObjectUrl() + "/" + id;
//        return httpServices.postJSONService(dataUrl, dataJSON);
        return "";
    }




}
