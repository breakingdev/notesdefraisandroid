package com.esgi.breakingdev.notesdefraisandroid.services;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * Created by Jeremy on 09/07/2016.
 */
public class BusinessServiceServices {

    /* CLASS MEMBERS */
    private HttpServices httpServices = HttpServices.getInstance();
    private String urlServer = httpServices.getUrlServer();

    /* SPECIFIC METHODS */
    public String getListServices()
    {
//        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        String dataUrl = "http://" + urlServer + "/organization/userPosition";

        String donneesJSON = httpServices.getJSONService(dataUrl);
        return donneesJSON;
    }

    public HttpServices getHttpServices() {
        return httpServices;
    }
    public void setHttpServices(HttpServices httpServices) {
        this.httpServices = httpServices;
    }
    public String getUrlServer() {
        return urlServer;
    }
    public void setUrlServer(String urlServer) {
        this.urlServer = urlServer;
    }
}
