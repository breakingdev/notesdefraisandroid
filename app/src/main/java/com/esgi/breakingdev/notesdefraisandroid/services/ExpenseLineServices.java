package com.esgi.breakingdev.notesdefraisandroid.services;

import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;

/**
 * Created by Jeremy on 06/07/2016.
 */
public class ExpenseLineServices extends AbstractHttpServices {

    /* IMPLEMENTED METHODS FROM AbstractHttpServices */

    @Override
    protected String getObjectRoute() {
        return "/expenseLine";
    }

//    @Override
//    protected String getSpecialObjectUrl() {
//        return null;
//    }

    /* SPECIFIC METHODS */
    public String getExpenseLineProof(String id)
    {
        return getResourceOfObject(id);
    }
    public String getExpenseLinesOf(String expenseReportStringId){
        String dataURL = getFullListObjectUrl() + "/" + expenseReportStringId;
        return httpServices.getJSONService(dataURL);
    }

}
