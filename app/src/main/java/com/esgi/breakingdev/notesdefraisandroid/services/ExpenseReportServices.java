package com.esgi.breakingdev.notesdefraisandroid.services;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * Created by Jeremy on 06/07/2016.
 */
public class ExpenseReportServices extends AbstractHttpServices {

    /* IMPLEMENTED METHODS FROM AbstractHttpServices */
    @Override
    protected String getObjectRoute() {
        return "/expenseReport";
    }

//    @Override
//    protected String getSpecialObjectUrl() {
//        return null;
//    }

}
