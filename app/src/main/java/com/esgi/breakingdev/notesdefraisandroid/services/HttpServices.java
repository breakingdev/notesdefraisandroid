package com.esgi.breakingdev.notesdefraisandroid.services;

import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.model.UserInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by Jeremy on 01/07/2016.
 */
public class HttpServices {
    public enum MethodRequest{ GET,POST,PUT,DELETE};

    private static HttpServices instance = HttpServices.getInstance();

    /* CLASS MEMBERS */
    private String token = "" ;
    private String urlServer = "";

    /* CONSTRUCTORS */
    private HttpServices(){ notifyUserInfos();  }

    /* SINGLETON */
    public static HttpServices getInstance()
    {
        if(instance == null) {
            instance = new HttpServices();
        }
        return instance;
    }

    /* Méthode de mise à jour des infos */
    public static void notifyUserInfos(){
        HttpServices instanceHttpService = instance;
        if(instanceHttpService == null) return;
        instanceHttpService.setUrlServer(UserInfo.searchValueOf("serverURL"));
        Log.d("HttpServices", "notifyUserInfos() -> urlServer = " + instanceHttpService.getUrlServer());
        instanceHttpService.setToken(UserInfo.searchValueOf("userToken"));
        Log.d("HttpServices","notifyUserInfos() -> userToken = " + instanceHttpService.getToken());
    }

    /* GETTERS AND SETTERS */
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUrlServer() {
        return urlServer;
    }

    public void setUrlServer(String urlServer) {
        this.urlServer = urlServer;
    }

    /* PRIVATE METHODS (Config URLConnection)*/
    public void setHttpURLConnection(HttpURLConnection connection,MethodRequest methodRequest,int timeoutMili) throws ProtocolException {
        if(timeoutMili > 0)
            connection.setConnectTimeout(timeoutMili);
        setHttpURLConnectionWithAuthInfo(connection);
        setHttpURLConnectionByMethodRequest(connection, methodRequest);
    }

    public void setHttpURLConnectionWithAuthInfo(HttpURLConnection connection)
    {
        if(connection == null) return;
        connection.addRequestProperty("Cookie", "LAABS-AUTH=" + token);
        connection.addRequestProperty("Content-Type", "application/json");
    }

    public void setHttpURLConnectionByMethodRequest(HttpURLConnection connection,MethodRequest methodRequest) throws ProtocolException {
        if(connection == null) return;

        switch(methodRequest)
        {
            case GET:
                break;
            case POST:
                connection.setRequestMethod("POST");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
                break;
            case PUT:
                Log.i("methodRequest","***** JPA PUT OK *****");
                connection.setRequestMethod("PUT");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
                break;
            case DELETE:
                Log.i("methodRequest","***** JPA DELETE OK *****");
                connection.setRequestMethod("DELETE");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);
                break;
        }
    }

    /* PRIVATE METHODS (Read and Send Data) */
    private boolean sendData(HttpURLConnection connection,String data)
    {
        boolean succeed = true;
        OutputStreamWriter wr = null;
        try {
            wr = new OutputStreamWriter(connection.getOutputStream());
            wr.write(data);
        } catch (IOException e) {
            e.printStackTrace();
            succeed = false;
        }

        // FLUSH
        try {
            wr.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return succeed;
    }

    private String readData(HttpURLConnection connection, int timeoutMili) throws IOException {
        if(timeoutMili > 0)
            connection.setReadTimeout(timeoutMili);  // Timeout = 2 sec

        InputStream is;
        try {
            is = connection.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuffer response = new StringBuffer();
        while ((line = rd.readLine()) != null) {
            response.append(line);
            response.append('\n');
        }
        rd.close();
        return response.toString();
    }

    /* PUBLIC METHODS */
    public String getJSONService(String dataUrl)
    {
        String jsonResult = "";

        URL url;
        HttpURLConnection connection = null;
        try {
// Create connection
            url = new URL(dataUrl);
            connection = (HttpURLConnection) url.openConnection();

            setHttpURLConnection(connection, MethodRequest.GET, 2*1000);
            jsonResult = readData(connection,2*1000);
            Log.d("Server response: ", jsonResult);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }

        return jsonResult;
    }

    public String postJSONService(String dataUrl, String dataJSON)
    {
        URL url;
        HttpURLConnection connection = null;
        String responseStr = "";

        try {
// Create connection
            url = new URL(dataUrl);

            Log.i("postJSONService", "dataUrl = " + dataUrl);
            Log.i("postJSONService", "dataJSON = " + dataJSON);
            Log.i("postJSONService", "url = " + url);

            connection = (HttpURLConnection) url.openConnection();
            setHttpURLConnection(connection, MethodRequest.POST, 2 * 1000);
            sendData(connection, dataJSON);

// Get Response
            responseStr = readData(connection, 2 * 1000);

            responseStr = responseStr.replaceAll("\"", "");

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (connection != null)
                connection.disconnect();
        }
        return responseStr;
    }

    public String putJSONService(String dataUrl, String dataJSON)
    {
        URL url;
        HttpURLConnection connection = null;
        String responseStr = "";

        try {
// Create connection
            url = new URL(dataUrl);

            Log.i("putJSONService", "dataUrl = " + dataUrl);
            Log.i("putJSONService", "dataJSON = " + dataJSON);
            Log.i("putJSONService", "url = " + url);

            connection = (HttpURLConnection) url.openConnection();
            setHttpURLConnection(connection, MethodRequest.PUT, 2 * 1000);
            sendData(connection, dataJSON);
            responseStr = readData(connection, 2 * 1000);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return responseStr;
    }

    public String deleteJSONService(String dataUrl)
    {
        URL url;
        HttpURLConnection connection = null;
        String responseStr = "";

        try {
// Create connection
            url = new URL(dataUrl);

            connection = (HttpURLConnection) url.openConnection();
            setHttpURLConnection(connection, MethodRequest.DELETE, 2 * 1000);


// Get Response
            responseStr = readData(connection, 2 * 1000);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return responseStr;
    }


    public String putObject(String dataUrl)
    {
        URL url;
        HttpURLConnection connection = null;
        String responseStr = "";

        try {
// Create connection
            url = new URL(dataUrl);

            Log.i("putJSONService", "dataUrl = " + dataUrl);
            Log.i("putJSONService", "url = " + url);

            connection = (HttpURLConnection) url.openConnection();
            setHttpURLConnection(connection,MethodRequest.PUT,2*1000);
            responseStr = readData(connection, 2 * 1000);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
        return responseStr;
    }

}
