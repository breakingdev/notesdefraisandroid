package com.esgi.breakingdev.notesdefraisandroid.services;

import android.content.Context;

import org.json.JSONObject;

/**
 * Created by Tibo Mathieu on 07/07/2016.
 */
public interface ISerializableJson {
    JSONObject getJsonObject();
    void setObjectFromJson(JSONObject jsonObject,Context appContext);
}
