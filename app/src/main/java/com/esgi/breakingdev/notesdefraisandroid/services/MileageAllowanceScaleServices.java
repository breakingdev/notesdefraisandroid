package com.esgi.breakingdev.notesdefraisandroid.services;

/**
 * Created by Jeremy on 06/07/2016.
 */
public class MileageAllowanceScaleServices extends AbstractHttpServices{

    /* IMPLEMENTED METHODS FROM AbstractHttpServices */
    @Override
    protected String getObjectRoute() {
        return "/mileageallowancescale";
    }

//    @Override
//    protected String getSpecialObjectUrl() {
//        return null;
//    }
}
