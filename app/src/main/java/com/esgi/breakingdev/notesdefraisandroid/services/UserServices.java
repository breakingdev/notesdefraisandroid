package com.esgi.breakingdev.notesdefraisandroid.services;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;

/**
 * Created by Jeremy on 07/07/2016.
 */
public class UserServices {

    /* CLASS MEMBERS */
    private HttpServices httpServices = HttpServices.getInstance();
    private String urlServer = httpServices.getUrlServer();

    /* SPECIFIC METHODS */
    public String getUserPosition()
    {
//        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        String dataUrl = "http://" + urlServer + "/organization/userPosition";

        String userPosition = "";

        try
        {
            String donneesJSON = httpServices.getJSONService(dataUrl);

            JSONArray mJsonAray = new JSONArray(donneesJSON);
            JSONObject mJsonObject = new JSONObject();

            for(int i=0; i<mJsonAray.length(); i++)
            {
                mJsonObject = mJsonAray.getJSONObject(i);

                if(userPosition.equals(""))
                {
                    userPosition = mJsonObject.getJSONObject("organization").getString("displayName");
                }
                else
                {
                    userPosition = userPosition + ", " + mJsonObject.getJSONObject("organization").getString("displayName");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userPosition;
    }

    public String getUserId()
    {
//        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        String dataUrl = "http://" + urlServer + "/organization/userPosition";

        String userId = "";

        try
        {
            String donneesJSON = httpServices.getJSONService(dataUrl);

            JSONArray mJsonAray = new JSONArray(donneesJSON);
            JSONObject mJsonObject = new JSONObject();

            for(int i=0; i<mJsonAray.length(); i++)
            {
                mJsonObject = mJsonAray.getJSONObject(i);
                userId = mJsonObject.getString("userAccountId");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userId;
    }


    public String getUserDisplayName(String id)
    {
//        CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
        String dataUrl = "http://" + urlServer + "/auth/userAccount/" + id;

        String displayName = "";

        try
        {
            String donneesJSON = httpServices.getJSONService(dataUrl);

            JSONObject jsonObj = new JSONObject(donneesJSON);
            displayName = jsonObj.getString("displayName");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return displayName;
    }

    public HttpServices getHttpServices() {
        return httpServices;
    }

    public void setHttpServices(HttpServices httpServices) {
        this.httpServices = httpServices;
    }

    public String getUrlServer() {
        return urlServer;
    }

    public void setUrlServer(String urlServer) {
        this.urlServer = urlServer;
    }
}
