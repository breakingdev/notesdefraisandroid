package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Tibo Mathieu on 10/05/2016.
 *
 * Cette classe permet de gérer la création et la mise à jour d'une table SQLite quelconque
 * en lui attribuant automatiquement une colonne ID
 */


public abstract class AbstractDBHelper extends SQLiteOpenHelper {
    public enum ColumnType { INT_PRIMARY,INT,INT_NOT_NULL,INT_UNIQUE,INT_UNIQUE_NOT_NULL,
                                REAL,REAL_NOT_NULL,REAL_UNIQUE,REAL_UNIQUE_NOT_NULL,
                                    TEXT,TEXT_NOT_NULL,TEXT_UNIQUE,TEXT_UNIQUE_NOT_NULL,
                                        BLOB}

    private String TABLE_NAME = ""; // nom de la table
    private static final int DATABASE_VERSION = 32; // Version de la base de données
    private Map<String,ColumnType> tableColumns = new LinkedHashMap<String,ColumnType>() {{ // Liste des colonnes
            put("ID", ColumnType.INT_PRIMARY);
    }};

    public String getTableName() {return this.TABLE_NAME; }

    /* CONSTRUCTEUR PRINCIPAL */
    protected AbstractDBHelper(Context context, String tablename){
        super(context, tablename, null, DATABASE_VERSION);
        TABLE_NAME = tablename;
    }

    /* Ajout d'une colonne */
    public void addColumn(String column_name, ColumnType ctype){
        tableColumns.put(column_name, ctype);
    }

    /* Génération du schéma de création de la table */
    public String getCreateSchema()
    {
        String schema ="CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" ;
        Iterator entries = tableColumns.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            String column_name = (String) entry.getKey();
            ColumnType column_type = (ColumnType) entry.getValue();

            schema += column_name + " ";

            switch(column_type)
            {
                case INT_PRIMARY: schema +=  "INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL"; break;
                case INT: schema +=  "INTEGER"; break;
                case INT_NOT_NULL: schema +=  "INTEGER NOT NULL"; break;
                case INT_UNIQUE: schema +=  "INTEGER UNIQUE"; break;
                case INT_UNIQUE_NOT_NULL: schema +=  "INTEGER UNIQUE NOT NULL"; break;
                case REAL: schema +=  "REAL"; break;
                case REAL_NOT_NULL: schema +=  "REAL NOT NULL"; break;
                case REAL_UNIQUE: schema +=  "REAL UNIQUE"; break;
                case REAL_UNIQUE_NOT_NULL: schema +=  "REAL UNIQUE NOT NULL"; break;
                case TEXT: schema +=  "TEXT"; break;
                case TEXT_NOT_NULL: schema +=  "TEXT NOT NULL"; break;
                case TEXT_UNIQUE: schema +=  "TEXT UNIQUE"; break;
                case TEXT_UNIQUE_NOT_NULL: schema +=  "TEXT UNIQUE NOT NULL"; break;
                case BLOB: schema +=  "BLOB"; break;
                default: break;
            }
            if(entries.hasNext())    schema += ",";
            else schema += ");";
        }
        return schema;
    }

    /* Récupération des colonnes de la table */
    public String[] getAllColumns()
    {
        int size = tableColumns.size();
        String[] columns = new String[size];
        Iterator entries = tableColumns.entrySet().iterator();
        int i = 0;
        while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            columns[i] = new String((String) entry.getKey());
            ++i;
        }
        return columns;
    }

    /* Récupération d'une colonne particulière de la table */
    public String getColumnName(int column) {
        String[] columns = getAllColumns();
        return columns[column];
    }


    /* Création de la table */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(getCreateSchema());
    }

    /* Mise à jour de la table */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(AbstractDBHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }


}
