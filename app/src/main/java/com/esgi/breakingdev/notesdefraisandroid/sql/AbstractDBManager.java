package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Tibo Mathieu on 10/05/2016.
 *
 * Cette classe permet de gérer l'ajout d'éléments ou la récupération d'éléments
 * depuis une table existante
 */
public abstract class AbstractDBManager {

    protected SQLiteDatabase db; // Instance de la base de données
    protected AbstractDBHelper dbHelper; // Instance du manager de création de la base de données
    protected Context context; // Contexte de la base (activity associée)

    protected AbstractDBManager(Context context,AbstractDBHelper dbhelper) throws Exception {
        this.context = context;
        this.dbHelper = dbhelper;
        checkDatabaseIntegrity();
    }


    protected void checkDatabaseIntegrity() throws Exception {
        checkHelperConnection();
    }


    /* Obtention du nombre de colonnes à stocker dans la base de données */
    public int getFieldsToStoreInDatabase(Object object) {

        Class actualClass = object.getClass();
        int count = 0;

        // Pour toutes les classes d'objets héritées en comptant celle de l'instance actuelle
        while(!actualClass.equals(Object.class))
        {
//            Log.d("getFieldsToStoreInDb","Class " + actualClass.getName() + ",count = " + count);

            Field[] fields = actualClass.getDeclaredFields();
            for (Field f : fields) {
                Annotation[] annotations = f.getAnnotations();
//                            Log.d("getFieldsToStoreInDb","field " + f.getName() + " has " + annotations.length + " annotations");
                //            Log.d("getFieldsToStoreInDb","DBManagerable.class.getName() = " + DBManagerable.class.getName());
                for (Annotation a : annotations) {
                    //                Log.d("getFieldsToStoreInDb","annotation " + i + ": " + a.toString().substring(1,a.toString().length()-2));
                    if (a.toString().substring(1, a.toString().length() - 2).equals(DBManagerable.class.getName())) {
                        ++count;
                        break;
                    }
                }
            }
            actualClass = actualClass.getSuperclass();
        }
        return count;
    }

    /* Vérification de l'intégrité de la connection du manager avec le modèle de l'objet à associer */
    protected void checkObjectModelConnection(Object object) throws Exception {
        int nbFields = getFieldsToStoreInDatabase(object);
        if(dbHelper.getAllColumns().length != nbFields) {
            Log.d("checkObjectModelCon","class: " + this.getClass().getName() + ", nbFields = " + nbFields + " instead of " + dbHelper.getAllColumns().length );
            throw new Exception("class: " + this.getClass().getName() + " does not match with object model " + object.getClass().getName());
        }
    }

    /* Vérification de l'intégrité de la connection du manager avec la base de donnée */
    private void checkHelperConnection() throws Exception {
        this.open();
        Cursor cursor = db.query(dbHelper.getTableName(), dbHelper.getAllColumns(), null, null, null, null, null);
        if(!doesNameColumnsMatch(cursor))
            throw new Exception("class: " + this.getClass().getName() + " does not match with helper " + dbHelper.getClass().getName());
        cursor.close();
        this.close();
    }





    /* Ouverture de la table */
    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    /* Fermeture de la table */
    public void close() {
        db.close();
    }

    /* Récupération des données de l'objet pour la base de données */
    abstract protected ContentValues getContentValues(Object object, boolean with_id) throws Exception;

    /* Création d'un objet de la table */
    public Object createItem(Object object) throws Exception {
        return createItem(object,false);
    }
    abstract public Object createItem(Object object, boolean with_id) throws Exception;

    /* Mise à jour d'un objet de la table */
    abstract public void updateItem(Object object) throws Exception;

    /* Mise à jour d'un objet de la table dont l'id a changé */
    public void updateItemWithId(long oldId, Object object) throws Exception {
        deleteItem(oldId);
        createItem(object,true);
    }

    /* Effacement d'une ligne de la table */
    public void deleteItem(long uid)
    {
        this.open();
        db.delete(dbHelper.getTableName(), dbHelper.getColumnName(0) + " = " + uid, null);
        this.close();
    }

    /* Effacement de toute la table */
    public void deleteTable()
    {
        this.open();
        db.delete(dbHelper.getTableName(), null, null);
        this.close();
    }

    /* Récupération d'un objet de la table  */
    abstract protected Object cursorToObject(Cursor cursor) throws ParseException;

    /* Vérification que les colonnes de la base de données sont correctes */
    protected boolean doesNameColumnsMatch(Cursor cursor)
    {
        if(cursor.getColumnCount() != dbHelper.getAllColumns().length)
            return false;
        for(int i = 0; i< cursor.getColumnCount(); ++i)
        {
            if(!dbHelper.getColumnName(i).equals(cursor.getColumnName(i)))
                return false;
        }
        return true;
    }

    /* Récupération de tous les objets de la table */
    public ArrayList<Object> getDBItems()
    {
        this.open();

        ArrayList<Object> objectList = new ArrayList<Object>();
        Cursor cursor;
        String orderBy = null;
//        Log.d("getDBItems","Class " + this.getClass().getSimpleName() + " tableName " + dbHelper.getTableName() + " columns: "+ dbHelper.getAllColumns()[1]);
        cursor = db.query(dbHelper.getTableName(), dbHelper.getAllColumns(), null, null, null, null, orderBy);

        cursor.moveToFirst();
        Object object = null;
        while (!cursor.isAfterLast()) {
            try {
                object = cursorToObject(cursor);
            } catch (ParseException e) {
                e.printStackTrace();
                break;
            }
            objectList.add(object);
            cursor.moveToNext();
        }
        cursor.close();
        this.close();

        return objectList;
    }

}
