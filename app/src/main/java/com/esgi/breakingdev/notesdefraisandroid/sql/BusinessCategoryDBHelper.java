package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 11/05/2016.
 *  Classe de Management de création de la  table "catégories de dépense"

 */
public class BusinessCategoryDBHelper extends AbstractDBHelper {


    protected BusinessCategoryDBHelper(Context context) {
        super(context, "businessCategory");
        addColumn("stringId",ColumnType.TEXT);
        addColumn("code",ColumnType.TEXT_NOT_NULL);
        addColumn("description",ColumnType.TEXT);
        addColumn("formType",ColumnType.TEXT_NOT_NULL);
    }
}
