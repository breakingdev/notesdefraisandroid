package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

import java.util.ArrayList;


/**
 * Created by Tibo Mathieu on 11/05/2016.
 *  * Classe permettant de gérer les données de la table de catégories de dépense

 */
public class BusinessCategoryDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static BusinessCategoryDBManager instance = null;

    /* CONSTRUCTOR */
    protected BusinessCategoryDBManager(Context context)  throws Exception {
        super(context,new BusinessCategoryDBHelper(context));
    }

    /* SINGLETON */
    public static BusinessCategoryDBManager getInstance(Context context)  throws Exception
    {
        if(instance == null) {
            instance = new BusinessCategoryDBManager(context.getApplicationContext());
        }
        return instance;
    }

    /* IMPLEMENTED METHODS */
    @Override
    protected Object cursorToObject(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        String stringId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        String code = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        String desc = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        String formType = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(4)));
        return new BusinessCategory(id,stringId,code,desc,formType);
    }

    @Override
    /* Récupération des valeurs du modèle de catégorie de dépense pour la base de données */
    protected ContentValues getContentValues(Object object, boolean with_id)  throws Exception {
        BusinessCategory businessCat = (BusinessCategory) object;
        checkObjectModelConnection(businessCat);

        ContentValues values = new ContentValues();
        if(with_id) values.put(dbHelper.getColumnName(0) , businessCat.getBusinessCategoryId());
        values.put(dbHelper.getColumnName(1) , businessCat.getStringId());
        values.put(dbHelper.getColumnName(2) , businessCat.getCode());
        values.put(dbHelper.getColumnName(3), businessCat.getDescription());
        values.put(dbHelper.getColumnName(4), businessCat.getFormType().toString());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;
    }



    /* Création d'une  catégorie de dépense dans la table  */
    @Override
    public Object createItem(Object object, boolean with_id)  throws Exception{
        BusinessCategory businessCat = (BusinessCategory) object;
        this.open();
        ContentValues values = getContentValues(businessCat,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) businessCat.setBusinessCategoryId(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();
        ExpenseLine.businessCategoryList.add(object); // Mise à jour de la liste des catégories de dépense dans les notes de frais

        Log.i("BusinessCategoryDB", "createItem OK");
        return businessCat;
    }

    @Override
    /* Mise à jour d'une catégorie de dépense */
    public void updateItem(Object object)  throws Exception{
        BusinessCategory businessCat = (BusinessCategory) object;
        this.open();
        ContentValues values = getContentValues(businessCat,false);
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(app.isStandAlone() || businessCat.getStringId().isEmpty())
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(0) + " = " + businessCat.getBusinessCategoryId(), null);

        else {
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(1) + " = '" + businessCat.getStringId() + "'", null);
            BusinessCategory oldObject = (BusinessCategory) BusinessCategoryAdapter.getInstance(context).getItemWithStringId(businessCat.getStringId());
            businessCat.setBusinessCategoryId(oldObject.getBusinessCategoryId());
        }

        this.close();
    //        ExpenseLine.businessCategoryList.remove()
        BusinessCategoryAdapter.getInstance(context).remove(businessCat,false);
        BusinessCategoryAdapter.getInstance(context).add(businessCat,false, false);
        Log.i("BusinessCategoryDB", "updateItem OK");
    }

    @Override
    public void deleteItem(long uid) {
        super.deleteItem(uid);
        try {
            ListDeleter.removeBusinessCategoryOfList(uid, ExpenseLine.businessCategoryList);
            Log.i("BusinessCategoryDB", "deleteItem OK");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
        ExpenseLine.businessCategoryList.clear();
    }

    public void getInfoBusinessCategory() {
        this.open();
        String selectQuery = "SELECT  * FROM " + dbHelper.getTableName();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Log.i("TableBusCat", "cursor0 = " + cursor.getString(0));
                Log.i("TableBusCat", "cursor1 = " + cursor.getString(1));
                Log.i("TableBusCat", "cursor2 = " + cursor.getString(2));
                Log.i("TableBusCat", "cursor3 = " + cursor.getString(3));
            } while (cursor.moveToNext());
        }
        this.close();
    }

    public ArrayList<Object> getBusinessCategorySearchByDescription(String nomCategorieDepense)
    {
        this.open();

        ArrayList<Object> listExpenseReports = new ArrayList<>();
        Cursor cursor;
        String orderBy = null;

        cursor = db.query(dbHelper.getTableName(), dbHelper.getAllColumns(), "" + dbHelper.getColumnName(2) + " LIKE '%" + nomCategorieDepense + "%'",null, null, null, orderBy);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            BusinessCategory businessCategory = (BusinessCategory) cursorToObject(cursor);
            listExpenseReports.add(businessCategory);
            cursor.moveToNext();
        }
        cursor.close();
        this.close();

        return listExpenseReports;
    }
}
