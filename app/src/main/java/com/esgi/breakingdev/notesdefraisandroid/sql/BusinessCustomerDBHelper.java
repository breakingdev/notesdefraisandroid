package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Jeremy on 31/05/2016.
 */
public class BusinessCustomerDBHelper extends AbstractDBHelper {


    protected BusinessCustomerDBHelper(Context context) {
        super(context, "businessCustomer");
        addColumn("stringId",ColumnType.TEXT);
        addColumn("code",ColumnType.TEXT_NOT_NULL);
        addColumn("description",ColumnType.TEXT);
    }
}