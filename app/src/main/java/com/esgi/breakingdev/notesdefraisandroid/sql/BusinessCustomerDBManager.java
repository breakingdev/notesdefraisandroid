package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCustomerAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

import java.text.ParseException;

/**
 * Created by Jeremy on 31/05/2016.
 */
public class BusinessCustomerDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static BusinessCustomerDBManager instance = null;

    /* CONSTRUCTOR */
    protected BusinessCustomerDBManager(Context context)  throws Exception {
        super(context,new BusinessCustomerDBHelper(context));
    }

    /* SINGLETON */
    public static BusinessCustomerDBManager getInstance(Context context)  throws Exception
    {
        if(instance == null) {
            instance = new BusinessCustomerDBManager(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        BusinessCustomer businessCust = (BusinessCustomer) object;
        checkObjectModelConnection(businessCust);

        ContentValues values = new ContentValues();
        if(with_id) values.put(dbHelper.getColumnName(0) , businessCust.getBusinessCustomerId());
        values.put(dbHelper.getColumnName(1) , businessCust.getStringId());
        values.put(dbHelper.getColumnName(2) , businessCust.getCode());
        values.put(dbHelper.getColumnName(3), businessCust.getDescription());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;
    }

    @Override
    public Object createItem(Object object, boolean with_id) throws Exception {
        BusinessCustomer businessCust = (BusinessCustomer) object;
        this.open();
        ContentValues values = getContentValues(businessCust,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) businessCust.setBusinessCustomerId(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();
        ExpenseLine.businessCustomerList.add(object); // Mise à jour de la liste des clients dans les notes de frais

        Log.i("BusinessCustomerDB", "createItem OK");
        return businessCust;
    }

    @Override
    public void updateItem(Object object) throws Exception {
        BusinessCustomer businessCust = (BusinessCustomer) object;
        this.open();
        ContentValues values = getContentValues(businessCust,false);
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(app.isStandAlone() || businessCust.getStringId().isEmpty()) {
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(0) + " = " + businessCust.getBusinessCustomerId(), null);
        }
        else {
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(1) + " = '" + businessCust.getStringId() + "'", null);
            BusinessCustomer oldObject = (BusinessCustomer) BusinessCustomerAdapter.getInstance(context).getItemWithStringId(businessCust.getStringId());
            businessCust.setBusinessCustomerId(oldObject.getBusinessCustomerId());
        }
        this.close();
        BusinessCustomerAdapter.getInstance(context).remove(businessCust);
        BusinessCustomerAdapter.getInstance(context).add(businessCust,false);
    }

    @Override
    protected Object cursorToObject(Cursor cursor) throws ParseException {
        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        String stringId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        String code = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        String desc = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        return new BusinessCustomer(id,stringId,code,desc);
    }

    @Override
    public void deleteItem(long uid) {
        super.deleteItem(uid);
        try {
            ListDeleter.removeBusinessCustomerOfList(uid, ExpenseLine.businessCustomerList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
        ExpenseLine.businessCustomerList.clear();
    }
}
