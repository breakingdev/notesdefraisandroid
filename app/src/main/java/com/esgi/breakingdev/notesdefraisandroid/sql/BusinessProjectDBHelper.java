package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Jeremy on 31/05/2016.
 */
public class BusinessProjectDBHelper extends AbstractDBHelper {

    protected BusinessProjectDBHelper(Context context) {
        super(context, "businessProject");
        addColumn("stringId", ColumnType.TEXT);
        addColumn("code", ColumnType.TEXT_NOT_NULL);
        addColumn("description",ColumnType.TEXT);
        addColumn("refacturation",ColumnType.INT_NOT_NULL);
        addColumn("idClient",ColumnType.INT_NOT_NULL);
        addColumn("stringIdClient",ColumnType.TEXT);
    }
}
