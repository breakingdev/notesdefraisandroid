package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;

import java.text.ParseException;

/**
 * Created by Jeremy on 31/05/2016.
 */
public class BusinessProjectDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static BusinessProjectDBManager instance = null;

    /* CONSTRUCTOR */
    protected BusinessProjectDBManager(Context context)  throws Exception {
        super(context,new BusinessProjectDBHelper(context));
    }

    /* SINGLETON */
    public static BusinessProjectDBManager getInstance(Context context)  throws Exception
    {
        if(instance == null) {
            instance = new BusinessProjectDBManager(context.getApplicationContext());
        }
        return instance;
    }

    @Override
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        BusinessProject businessProj = (BusinessProject) object;
        checkObjectModelConnection(businessProj);

        ContentValues values = new ContentValues();
        if(with_id) values.put(dbHelper.getColumnName(0) , businessProj.getBusinessProjectId());
        values.put(dbHelper.getColumnName(1) , businessProj.getStringId());
        values.put(dbHelper.getColumnName(2) , businessProj.getCode());
        values.put(dbHelper.getColumnName(3), businessProj.getDescription());
        values.put(dbHelper.getColumnName(4), businessProj.getRefacturation());
        values.put(dbHelper.getColumnName(5), businessProj.getIdClient());
        values.put(dbHelper.getColumnName(6), businessProj.getStringIdClient());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;
    }

    @Override
    public Object createItem(Object object, boolean with_id) throws Exception {
        BusinessProject businessProj = (BusinessProject) object;
        this.open();
        ContentValues values = getContentValues(businessProj,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) businessProj.setBusinessProjectId(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();
        ExpenseLine.businessProjectList.add(object); // Mise à jour de la liste des projets dans les notes de frais

        Log.i("BusinessProjectDB", "createItem OK");
        return businessProj;
    }

    @Override
    public void updateItem(Object object) throws Exception {
        BusinessProject businessProj = (BusinessProject) object;
        this.open();
        ContentValues values = getContentValues(businessProj,false);
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(app.isStandAlone() || businessProj.getStringId().isEmpty())
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(0) + " = " + businessProj.getBusinessProjectId(), null);
        else{
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(1) + " = '" + businessProj.getStringId() + "'", null);
            BusinessProject oldObject = (BusinessProject) BusinessProjectAdapter.getInstance(context).getItemWithStringId(businessProj.getStringId());
            businessProj.setBusinessProjectId(oldObject.getBusinessProjectId());
        }
        this.close();
        ExpenseLine.businessProjectList.remove(businessProj);
        ExpenseLine.businessProjectList.add(businessProj);

        BusinessProjectAdapter.getInstance(context).remove(businessProj, false);
        BusinessProjectAdapter.getInstance(context).add(businessProj, true, false);
    }

    @Override
    protected Object cursorToObject(Cursor cursor) throws ParseException {
        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        String stringId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        String code = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        String desc = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        boolean refacturation = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(4))) <= 0 ? false : true;
        long idClient = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(5)));
        String strIdClient = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(6)));
        return new BusinessProject(id,stringId,code,desc,refacturation,idClient,strIdClient);
    }

    @Override
    public void deleteItem(long uid) {

        super.deleteItem(uid);

        for(int i = 0; i<ExpenseLine.businessProjectList.size();++i){
            Object o = ExpenseLine.businessProjectList.get(i);
            if(o instanceof BusinessProject){
                if(((BusinessProject) o).getBusinessProjectId() == uid) {
                    ExpenseLine.businessProjectList.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
        ExpenseLine.businessProjectList.clear();
    }
}
