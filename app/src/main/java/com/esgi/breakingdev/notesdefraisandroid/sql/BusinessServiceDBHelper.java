package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 28/06/2016.
 */
public class BusinessServiceDBHelper extends AbstractDBHelper{

    public BusinessServiceDBHelper(Context context) {
        super(context, "businessService");
        addColumn("stringId", ColumnType.TEXT);
        addColumn("description", ColumnType.TEXT_NOT_NULL);
        addColumn("infos",ColumnType.TEXT);
        addColumn("companyName", ColumnType.TEXT);   }
}
