package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;

import java.text.ParseException;

/**
 * Created by Tibo Mathieu on 28/06/2016.
 */
public class BusinessServiceDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static BusinessServiceDBManager instance = null;

    /* CONSTRUCTOR */
    protected BusinessServiceDBManager(Context context)  throws Exception {
        super(context,new BusinessServiceDBHelper(context));
    }

    /* SINGLETON */
    public static BusinessServiceDBManager getInstance(Context context)  throws Exception
    {
        if(instance == null) {
            instance = new BusinessServiceDBManager(context.getApplicationContext());
        }
        return instance;
    }

    /* IMPLEMENTED METHODS */
    @Override
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        BusinessService businessService = (BusinessService) object;
        checkObjectModelConnection(businessService);

        ContentValues values = new ContentValues();
        if(with_id) values.put(dbHelper.getColumnName(0), businessService.getServiceId());
        values.put(dbHelper.getColumnName(1), businessService.getStringId());
        values.put(dbHelper.getColumnName(2), businessService.getDescription());
        values.put(dbHelper.getColumnName(3), businessService.getInfos());
        values.put(dbHelper.getColumnName(4), businessService.getCompanyName());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;
    }

    @Override
    public Object createItem(Object object, boolean with_id) throws Exception {
        BusinessService businessService = (BusinessService) object;
        this.open();
        ContentValues values = getContentValues(businessService,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) businessService.setServiceId(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();
        BusinessService.businessServicesList.add(object); // Mise à jour de la liste des services

        Log.i("BusinessServiceDB", "createItem OK");
        return businessService;
    }

    @Override
    public void updateItem(Object object) throws Exception {
        BusinessService businessService = (BusinessService) object;
        this.open();
        ContentValues values = getContentValues(businessService,false);
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(app.isStandAlone() || businessService.getStringId().isEmpty())
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(0) + " = " + businessService.getServiceId(), null);
        else {
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(1) + " = '" + businessService.getStringId() + "'", null);
            BusinessService oldObject = (BusinessService) BusinessServiceAdapter.getInstance(context).getItemWithStringId(businessService.getStringId());
            businessService.setServiceId(oldObject.getServiceId());
        }

        BusinessServiceAdapter.getInstance(context).remove(businessService, false);
        BusinessServiceAdapter.getInstance(context).add(businessService, true, false);
    }

    @Override
    protected Object cursorToObject(Cursor cursor) throws ParseException {
        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        String stringId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        String label = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        String infos = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        String corpInfo = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(4)));
        return new BusinessService(id,stringId,label,infos,corpInfo);
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
        BusinessService.businessServicesList.clear();
    }
}
