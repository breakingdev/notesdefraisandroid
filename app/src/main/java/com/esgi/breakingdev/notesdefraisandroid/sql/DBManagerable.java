package com.esgi.breakingdev.notesdefraisandroid.sql;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Tibo Mathieu on 13/05/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DBManagerable {

}
