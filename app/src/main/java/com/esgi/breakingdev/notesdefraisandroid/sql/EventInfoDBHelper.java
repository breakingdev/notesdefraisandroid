package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 08/07/2016.
 */
public class EventInfoDBHelper extends AbstractDBHelper {
    public EventInfoDBHelper(Context context) {
        super(context, "eventInfo");
        addColumn("action",ColumnType.TEXT_NOT_NULL);
        addColumn("objectType",ColumnType.TEXT_NOT_NULL);
        addColumn("objectId",ColumnType.INT_NOT_NULL);
    }
}
