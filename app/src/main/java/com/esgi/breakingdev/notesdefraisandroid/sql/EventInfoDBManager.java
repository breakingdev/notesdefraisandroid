package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.model.EventInfo;

import java.text.ParseException;

/**
 * Created by Tibo Mathieu on 08/07/2016.
 */
public class EventInfoDBManager extends AbstractDBManager{
    /* CLASS MEMBERS */
    private static EventInfoDBManager instance = null;

    /* CONSTRUCTOR */
    protected EventInfoDBManager(Context context)  throws Exception {
        super(context,new EventInfoDBHelper(context));
    }

    /* SINGLETON */
    public static EventInfoDBManager getInstance(Context context)  throws Exception
    {
        if(instance == null) {
            instance = new EventInfoDBManager(context.getApplicationContext());
        }
        return instance;
    }

    /* IMPLEMENTED METHODS FROM AbstractDBManager */

    @Override
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        EventInfo eventInfo = (EventInfo) object;
        checkObjectModelConnection(eventInfo);

        ContentValues values = new ContentValues();
//        if(with_id) values.put(dbHelper.getColumnName(0), userInfo.getId());
        values.put(dbHelper.getColumnName(1), eventInfo.getAction().toString());
        values.put(dbHelper.getColumnName(2), eventInfo.getObjectType().toString());
        values.put(dbHelper.getColumnName(3), eventInfo.getObjectId());

        int sizeExpected = getFieldsToStoreInDatabase(object);
//        if(!with_id) sizeExpected--;
        if(values.size() != (--sizeExpected)) { // champ id non utilisé
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;      }

    @Override
    public Object createItem(Object object, boolean with_id) throws Exception {
        EventInfo eventInfo = (EventInfo) object;
        this.open();
        ContentValues values = getContentValues(eventInfo,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
//        if(!with_id) userInfo.setServiceId(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();
        EventInfo.listEvent.add(object); // Mise à jour de la liste des infos utilisateur

        Log.i("EventInfoDBManager", "createItem OK");
        return eventInfo;
    }

    @Override
    public void updateItem(Object object) throws Exception {
        return; // Update d'évènements non autorisé
    }

    @Override
    protected Object cursorToObject(Cursor cursor) throws ParseException {
//        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        String action = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        String objectType = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        long objId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        return new EventInfo(action,objectType,objId);
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
        EventInfo.listEvent.clear();
    }
}
