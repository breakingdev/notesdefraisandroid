package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 18/05/2016.
 */
public class ExpenseLineDBHelper extends AbstractDBHelper {
    public ExpenseLineDBHelper(Context context) {
        super(context, "expenseLine");
//        addColumn("expenseLineID",ColumnType.INT_PRIMARY);
        addColumn("stringId",ColumnType.TEXT);
        addColumn("expenseReportId",ColumnType.INT_NOT_NULL);
        addColumn("expenseReportStringId",ColumnType.TEXT);
        addColumn("lineNumber",ColumnType.INT);
        addColumn("date",ColumnType.TEXT_NOT_NULL);
        addColumn("businessCategoryId",ColumnType.INT_NOT_NULL);
        addColumn("businessCategoryStringId",ColumnType.TEXT);
        addColumn("rebilling",ColumnType.INT_NOT_NULL);
        addColumn("comment",ColumnType.TEXT);
        addColumn("tradingName",ColumnType.TEXT);
        addColumn("customerId",ColumnType.INT);
        addColumn("customerStringId",ColumnType.TEXT);
        addColumn("projectId",ColumnType.INT);
        addColumn("projectStringId",ColumnType.TEXT);
        addColumn("projectId",ColumnType.INT);
        addColumn("proofReference", ColumnType.TEXT);
        addColumn("proofImage", ColumnType.BLOB);
    }
}
