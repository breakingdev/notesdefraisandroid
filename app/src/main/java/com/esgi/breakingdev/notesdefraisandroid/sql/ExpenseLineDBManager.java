package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.AbstractAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Tibo Mathieu on 18/05/2016.
 */
public class ExpenseLineDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static ExpenseLineDBManager instance = null;

    /* CONSTRUCTOR */
    protected ExpenseLineDBManager(Context context) throws Exception {
        super(context,new ExpenseLineDBHelper(context));
    }

    /* SINGLETON */
    public static ExpenseLineDBManager getInstance(Context context) throws Exception {
        if(instance == null)
            instance = new ExpenseLineDBManager(context.getApplicationContext());
        return instance;
    }

    /* IMPLEMENTED METHODS */

    @Override
    /* Récupération des valeurs du modèle pour la base de données */
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        ExpenseLine line = (ExpenseLine)object;
        checkObjectModelConnection(line);

        ContentValues values = new ContentValues();

        // Récupération de l'utilitaire de formatage de date pour la base de données
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        SimpleDateFormat sdf = app.getDateFormat();

        //values.put(MySQLiteHelper.COLUMN_EXPENSE_REPORT_VALIDATOR, note.getValidatorId());
        if(with_id) values.put(dbHelper.getColumnName(0) , line.getExpenseLineId());
        values.put(dbHelper.getColumnName(1) , line.getStringId());
        values.put(dbHelper.getColumnName(2) , line.getExpenseReportId());
        values.put(dbHelper.getColumnName(3) , line.getExpenseReportStringId());
        values.put(dbHelper.getColumnName(4) , line.getLineNumber());
        values.put(dbHelper.getColumnName(5) , sdf.format(line.getDate()));
        values.put(dbHelper.getColumnName(6), line.getBusinessCategoryId());
        values.put(dbHelper.getColumnName(7), line.getBusinessCategoryStringId());
        values.put(dbHelper.getColumnName(8), line.isRebilling());
        values.put(dbHelper.getColumnName(9), line.getComment());
        values.put(dbHelper.getColumnName(10), line.getTradingName());
        values.put(dbHelper.getColumnName(11), line.getCustomerId());
        values.put(dbHelper.getColumnName(12), line.getCustomerStringId());
        values.put(dbHelper.getColumnName(13), line.getProjectId());
        values.put(dbHelper.getColumnName(14), line.getProjectStringId());
        values.put(dbHelper.getColumnName(15), line.getProofReference());
        values.put(dbHelper.getColumnName(16), line.getProofImage());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;    }

    @Override
    public Object createItem(Object object, boolean with_id) throws Exception {
        ExpenseLine line = (ExpenseLine)object;
        this.open();
        ContentValues values = getContentValues(line,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) line.setExpenseLineId(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();

        Log.i("ExpenseLineDBManager", "createItem OK");
        return line;
    }

    @Override
    public void updateItem(Object object) throws Exception {
        ExpenseLine line = (ExpenseLine)object;
        boolean noStringId = line.getStringId().isEmpty();
        Log.d("ExpenseLineDB","updateItem ->  noStringId = " + noStringId);
        Log.d("ExpenseLineDB","updateItem ->  note.getExpenseLineId = " + line.getExpenseLineId());
        if(line.getExpenseLineId() > 0) {
            ExpenseLine lineInDB = (ExpenseLine) ExpenseLineAdapter.getInstance(context,line.getExpenseReportId()).getItemWithId(line.getExpenseLineId());
            noStringId = lineInDB.getStringId().isEmpty();
            Log.d("ExpenseLineDB","updateItem -> lineInDB.getStringId() = " + lineInDB.getStringId());
            Log.d("ExpenseLineDB", "updateItem -> noStringId = " + noStringId);
        }
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(!app.isStandAlone() && !noStringId) {
            Log.d("ExpenseLineDB","updateItem -> !app.isStandAlone() && !noStringId  " );
            ExpenseLineAdapter lineAdapter = ExpenseLineAdapter.getInstance(context, line.getExpenseReportId());
            lineAdapter.refresh();
            ExpenseLine lineinDB =  (ExpenseLine) lineAdapter.getItemWithStringId(line.getStringId());
            long amountId = lineinDB.getAmount().getAmountID();
            line.getAmount().setAmountID(amountId);
        }


        this.open();
        ContentValues values = getContentValues(line, false);
        if(app.isStandAlone() || noStringId)
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(0) + " = " + line.getExpenseLineId(), null);
        else{
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(1) + " = '" + line.getStringId() + "'", null);
            ExpenseLine oldObject = (ExpenseLine) ExpenseLineAdapter.getInstance(context,line.getExpenseReportId()).getItemWithStringId(line.getStringId());
            line.setExpenseLineId(oldObject.getExpenseLineId());
        }
        this.close();
        /* Mise à jour de la liste en mémoire */
        ExpenseLineAdapter.getInstance(context,line.getExpenseReportId()).remove(line,false);
        ExpenseLineAdapter.getInstance(context,line.getExpenseReportId()).add(line,false,false);

        AbstractDBManager dbmanager;

        switch(line.getCategory())
        {
            case amount:
//                updateAmountList(ExpenseLine.simpleAmountList,line.getExpenseLineId(),line.getAmount());
                // mettre à jour la table
                SimpleAmountDBManager.getInstance(context).updateItem(line.getAmount());

                break;
            case mileageExpense:
//                updateAmountList(ExpenseLine.mileageExpenseList,line);
                // mettre à jour la table
                MileageExpenseAmountDBManager.getInstance(context).updateItem(line.getAmount());
                break;
            case flateRate:
//                updateAmountList(ExpenseLine.flateRateList,line);
                // mettre à jour la table
               FlateRateAmountDBManager.getInstance(context).updateItem(line.getAmount());
               break;
        }
    }

    @Override
    protected Object cursorToObject(Cursor cursor) throws ParseException {

        // Récupération de l'utilitaire de formatage de date pour la base de données
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        SimpleDateFormat sdf = app.getDateFormat();


        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        String strId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        long reportId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        String reportStrId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        int lNumber = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(4)));
        String dateStr = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(5)));
        Date date = sdf.parse(dateStr);
        long catId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(6)));
        String catStrId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(7)));
        boolean rebilling = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(8))) != 0 ? true : false;
        String comment = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(9)));
        String tradName = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(10)));
        long cusId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(11)));
        String cusStrId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(12)));
        long projId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(13)));
        String projStrId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(14)));
        String proofRef = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(15)));
        byte[] img = cursor.getBlob(cursor.getColumnIndex(dbHelper.getColumnName(16)));
        return new ExpenseLine(id,strId,reportId,reportStrId,lNumber,date,catId,catStrId,rebilling,comment,tradName,cusId,cusStrId,projId,projStrId,proofRef,img);
    }


    /* OVERRIDED METHODS*/
    @Override
    public void deleteItem(long uid) {
        super.deleteItem(uid);
        // Effacer les montants associés
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
    }

    /* SPECIFIC METHODS */
    public void deleteLinesRelatedToExpenseReport(long expenseReportId)
    {
        this.open();
        db.delete(dbHelper.getTableName(), dbHelper.getColumnName(1) + " = " + expenseReportId, null);
        this.close();
        try {
            SimpleAmountDBManager.getInstance(context).deleteSimpleAmountsRelatedTo(expenseReportId);
            MileageExpenseAmountDBManager.getInstance(context).deleteMileageAmountsRelatedTo(expenseReportId);
            FlateRateAmountDBManager.getInstance(context).deleteFlateRateAmountsRelatedTo(expenseReportId);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
