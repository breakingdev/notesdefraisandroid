package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 10/05/2016.
 *
 * Classe de Management de création de la  table Notes de frais
 */
public class ExpenseReportDBHelper extends AbstractDBHelper {

    public ExpenseReportDBHelper(Context context) {
        super(context, "ExpenseReport");
        //addColumn("expenseReportId", ColumnType.TEXT);
        addColumn("stringId", ColumnType.TEXT);
        addColumn("serviceId", ColumnType.INT);
        addColumn("serviceStringId", ColumnType.TEXT);
        addColumn("validatorId", ColumnType.TEXT_NOT_NULL);
        addColumn("personalId", ColumnType.TEXT_NOT_NULL);
        addColumn("description", ColumnType.TEXT_NOT_NULL);
        addColumn("status", ColumnType.TEXT_NOT_NULL);
        addColumn("beginDate", ColumnType.TEXT_NOT_NULL);
        addColumn("endDate", ColumnType.TEXT_NOT_NULL);
        addColumn("sendToValidationDate", ColumnType.TEXT);
        addColumn("validationDate", ColumnType.TEXT);
        addColumn("validatorComment", ColumnType.TEXT);
    }


}


