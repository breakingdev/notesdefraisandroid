package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Tibo Mathieu on 10/05/2016.
 *
 * Classe permettant de gérer les données de la table de notes de frais
 */
public class ExpenseReportDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static ExpenseReportDBManager instance = null;

    /* CONSTRUCTOR */
    protected ExpenseReportDBManager(Context context) throws Exception {
        super(context,new ExpenseReportDBHelper(context));
    }

    /* SINGLETON */
    public static ExpenseReportDBManager getInstance(Context context) throws Exception {
        if(instance == null)
            instance = new ExpenseReportDBManager(context.getApplicationContext());
        return instance;
    }



    /* IMPLEMENTED METHODS */
    @Override
    /* Récupération des valeurs du modèle pour la base de données */
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        ExpenseReport note = (ExpenseReport)object;
        checkObjectModelConnection(note);

        ContentValues values = new ContentValues();

        // Récupération de l'utilitaire de formatage de date pour la base de données
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        SimpleDateFormat sdf = app.getDateFormat();

        //values.put(MySQLiteHelper.COLUMN_EXPENSE_REPORT_VALIDATOR, note.getValidatorId());
        if(with_id) values.put(dbHelper.getColumnName(0) , note.getExpenseReportId());
        values.put(dbHelper.getColumnName(1) , note.getStringId());
        values.put(dbHelper.getColumnName(2) , note.getServiceId());
        values.put(dbHelper.getColumnName(3) , note.getServiceStringId());
        values.put(dbHelper.getColumnName(4) , note.getValidatorId());
        values.put(dbHelper.getColumnName(5), note.getPersonalId());
        values.put(dbHelper.getColumnName(6) , note.getDescription());
        values.put(dbHelper.getColumnName(7), note.getStatus().toString());
        values.put(dbHelper.getColumnName(8), sdf.format(note.getBeginDate()));
        values.put(dbHelper.getColumnName(9), sdf.format(note.getEndDate()));
        values.put(dbHelper.getColumnName(10), sdf.format(note.getSendToValidatorDate()));
        values.put(dbHelper.getColumnName(11), sdf.format(note.getValidationDate()));
        values.put(dbHelper.getColumnName(12), note.getValidatorComment());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;
    }


    /* Création d'une notes de frais dans la table  */
    @Override
    public Object createItem(Object object, boolean with_id)  throws Exception   {
        ExpenseReport note = (ExpenseReport)object;
        this.open();
        ContentValues values = getContentValues(note,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) note.setExpenseReportId(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();

        Log.i("createExpenseReport", "createExpenseReport OK");
        return note;
    }


    /* Mise à jour d'une note de frais */
    @Override
    public void updateItem(Object object) throws Exception {
        ExpenseReport note = (ExpenseReport)object;
        boolean noStringId = note.getStringId().isEmpty();
        Log.d("ExpenseReportDB","updateItem ->  noStringId = " + noStringId);
        Log.d("ExpenseReportDB","updateItem ->  note.getExpenseReportId = " + note.getExpenseReportId());
        if(note.getExpenseReportId() > 0) {
            ExpenseReport reportInDB = (ExpenseReport) ExpenseReportAdapter.getInstance(context).getItemWithId(note.getExpenseReportId());
            noStringId = reportInDB.getStringId().isEmpty();
            Log.d("ExpenseReportDB","updateItem -> reportInDB.getStringId() = " + reportInDB.getStringId());
            Log.d("ExpenseReportDB","updateItem -> noStringId = " + noStringId);
        }
        this.open();
        ContentValues values = getContentValues(note, false);
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(app.isStandAlone() || noStringId)
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(0) + " = " + note.getExpenseReportId(), null);
        else {

            int res = db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(1) + " = '" + note.getStringId() + "'", null);
            Log.d("ExpenseReportDB","updateItem ->  res = " + res);
            ExpenseReport oldObject = (ExpenseReport) ExpenseReportAdapter.getInstance(context).getItemWithStringId(note.getStringId());
            note.setExpenseReportId(oldObject.getExpenseReportId());
        }

        this.close();
        ExpenseReportAdapter.getInstance(context).remove(note, false);
        ExpenseReportAdapter.getInstance(context).add(note, false, false);

    }


    @Override
    /* Récupération d'une note de frais depuis une ligne de la table */
    protected Object cursorToObject(Cursor cursor) throws ParseException {

        // Récupération de l'utilitaire de formatage de date pour la base de données
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        SimpleDateFormat sdf = app.getDateFormat();


        long id             =   cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        String strId         =   cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        long servId         =   cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        String servStrId         =   cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        String valId        =   cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(4)));
        String persId       =   cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(5)));
        String desc         =   cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(6)));
        String status       =   cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(7)));
        String strDateBegin =   cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(8)));
        Date dateBegin      =   sdf.parse(strDateBegin);
        String strDateEnd   =   cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(9)));
        Date dateEnd        =   sdf.parse(strDateEnd);

        String strDateSendValidation = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(10)));
        Date dateSendVal= sdf.parse(strDateSendValidation);

        String strDateValidation = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(11)));
        Date dateVal= sdf.parse(strDateValidation);
        String valComment = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(12)));


        return new ExpenseReport(id,strId,servId,servStrId,valId,persId,desc,status,dateBegin,dateEnd,dateSendVal,dateVal,valComment);
    }


    /* SPECIFIC METHODS */
    public void getInfoExpenseReports() {
        this.open();
        String selectQuery = "SELECT  * FROM " + dbHelper.getTableName();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Log.i("TableExpenseRpt", "cursor0 = " + cursor.getString(0));
                Log.i("TableExpenseRpt", "cursor1 = " + cursor.getString(1));
                Log.i("TableExpenseRpt", "cursor2 = " + cursor.getString(2));
                Log.i("TableExpenseRpt", "cursor3 = " + cursor.getString(3));
            } while (cursor.moveToNext());
        }
        this.close();
    }

    public ArrayList<Object> getExpenseReportsSearchByDescription(String nomNoteDeFrais) throws Exception
    {
        this.open();

        ArrayList<Object> listExpenseReports = new ArrayList<>();
        Cursor cursor;
        String orderBy = null;

        cursor = db.query(dbHelper.getTableName(), dbHelper.getAllColumns(), "" + dbHelper.getColumnName(2) + " LIKE '%" + nomNoteDeFrais + "%'",null, null, null, orderBy);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ExpenseReport expenseReport = (ExpenseReport) cursorToObject(cursor);
            listExpenseReports.add(expenseReport);
            cursor.moveToNext();
        }
        cursor.close();
        this.close();

        return listExpenseReports;
    }

    @Override
    public void deleteItem(long uid) {
        super.deleteItem(uid);
        // Effacement des lignes de frais associées
        try{
            ExpenseLineDBManager.getInstance(context).deleteLinesRelatedToExpenseReport(uid);
        }catch(Exception e){
            Log.d("ExpenseReportDBManager","deleteItem : failed to delete related expenseLines of report id " + uid);
            e.printStackTrace();
        }
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
    }
}
