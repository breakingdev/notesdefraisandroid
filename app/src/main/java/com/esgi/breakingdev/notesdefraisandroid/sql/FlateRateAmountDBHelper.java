package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 17/05/2016.
 */
public class FlateRateAmountDBHelper extends AbstractDBHelper {
    public FlateRateAmountDBHelper(Context context) {
        super(context, "flateRateAmount");
//        addColumn("simpleAmountID",ColumnType.INT_PRIMARY);
        addColumn("expenseReportID",ColumnType.INT_NOT_NULL);
        addColumn("expenseLineID",ColumnType.INT_NOT_NULL);
        addColumn("days",ColumnType.INT_NOT_NULL);
        addColumn("rate",ColumnType.REAL_NOT_NULL);
    }
}
