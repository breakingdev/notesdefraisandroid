package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.FlateRateAmount;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

import java.text.ParseException;

/**
 * Created by Tibo Mathieu on 17/05/2016.
 */
public class FlateRateAmountDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static FlateRateAmountDBManager instance = null;

    /* CONSTRUCTOR */
    protected FlateRateAmountDBManager(Context context) throws Exception {
        super(context,new FlateRateAmountDBHelper(context));
    }

    /* SINGLETON */
    public static FlateRateAmountDBManager getInstance(Context context) throws Exception {
        if(instance == null) {
            instance = new FlateRateAmountDBManager(context.getApplicationContext());
            ExpenseLine.flateRateList = instance.getDBItems();
        }
        return instance;
    }

    /* IMPLEMENTED METHODS */

    @Override
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        FlateRateAmount amount = (FlateRateAmount)object;
        checkObjectModelConnection(amount);

        ContentValues values = new ContentValues();

        //values.put(MySQLiteHelper.COLUMN_EXPENSE_REPORT_VALIDATOR, note.getValidatorId());
        if(with_id) values.put(dbHelper.getColumnName(0) , amount.getAmountID());
        values.put(dbHelper.getColumnName(1) , amount.getExpenseLineID());
        values.put(dbHelper.getColumnName(2) , amount.getExpenseReportID());
        values.put(dbHelper.getColumnName(3), amount.getDays());
        values.put(dbHelper.getColumnName(4), amount.getRate());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;    }

    @Override
    public Object createItem(Object object, boolean with_id) throws Exception {
        FlateRateAmount amount = (FlateRateAmount)object;
        this.open();
        ContentValues values = getContentValues(amount,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) amount.setAmountID(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();
        ExpenseLine.flateRateList.add(object);
        Log.i("FlateRateAmountDB", "createItem OK");
        return amount;
    }

    @Override
    public void updateItem(Object object) throws Exception {
        FlateRateAmount amount = (FlateRateAmount)object;
        this.open();
        ContentValues values = getContentValues(amount, false);
        db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(0) + " = " + amount.getAmountID(), null);
        this.close();
        ExpenseLine.updateAmountList(amount);
        Log.i("FlateRateAmountDB", "updateItem OK");
    }

    @Override
    protected Object cursorToObject(Cursor cursor) throws ParseException {

        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        long lineId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        long reportId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        int days = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        float rate = cursor.getFloat(cursor.getColumnIndex(dbHelper.getColumnName(4)));
        return new FlateRateAmount(id,lineId,reportId,days,rate);
    }

    /* OVERRIDED METHODS */
    @Override
    public void deleteItem(long uid) {
        super.deleteItem(uid);
        try {
            ListDeleter.removeAmountOfList(uid, ExpenseLine.flateRateList);
        }catch(Exception e){
            e.printStackTrace();
        }

//        for(int i = 0; i<ExpenseLine.flateRateList.size();++i){
//            Object o = ExpenseLine.flateRateList.get(i);
//            if(o instanceof FlateRateAmount){
//                if(((FlateRateAmount) o).getAmountID() == uid) {
//                    ExpenseLine.flateRateList.remove(i);
//                    break;
//                }
//            }
//        }
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
        ExpenseLine.flateRateList.clear();
    }

    public void deleteFlateRateAmountsRelatedTo(long expenseReportId)
    {
        this.open();
        db.delete(dbHelper.getTableName(), dbHelper.getColumnName(2) + " = " + expenseReportId, null);
        this.close();
        ExpenseLine.deleteAmountWithReportIdInList(expenseReportId, ExpenseLine.flateRateList);

    }
}
