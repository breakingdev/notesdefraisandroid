package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 11/05/2016.
 *
 *   Classe de Management de création de la  table de Barèmes Kilométriques
 */
public class MileageAllowanceScaleDBHelper extends AbstractDBHelper {
    public MileageAllowanceScaleDBHelper(Context context) {
        super(context, "mileageAllowanceScale");
        addColumn("stringId", ColumnType.TEXT);
        addColumn("mileageAllowanceYear", ColumnType.INT_NOT_NULL);
        addColumn("vehicle", ColumnType.TEXT_NOT_NULL);
        addColumn("horsePower",ColumnType.INT_NOT_NULL);
        addColumn("minDistance",ColumnType.INT);
        addColumn("maxDistance",ColumnType.INT);
        addColumn("rate",ColumnType.REAL_NOT_NULL);
        addColumn("offset",ColumnType.REAL);
    }
}
