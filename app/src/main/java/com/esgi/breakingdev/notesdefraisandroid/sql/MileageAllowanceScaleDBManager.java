package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.MileageAllowanceScalesAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

/**
 * Created by Tibo Mathieu on 11/05/2016.
 *
 * Classe permettant de gérer les données de la table des barèmes kilométriques
 */
public class MileageAllowanceScaleDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static MileageAllowanceScaleDBManager instance = null;

    /* CONSTRUCTOR */
    protected MileageAllowanceScaleDBManager(Context context)  throws Exception {
        super(context,new MileageAllowanceScaleDBHelper(context));
    }

    /* SINGLETON */
    public static MileageAllowanceScaleDBManager getInstance(Context context)  throws Exception
    {
        if(instance == null)
            instance = new MileageAllowanceScaleDBManager(context.getApplicationContext());
        return instance;
    }


    /*  IMPLEMENTED METHODS */
    @Override
    /* Récupération des valeurs du modèle de barème kilométrique pour la base de données */
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        MileageAllowanceScale bareme = (MileageAllowanceScale) object;
        checkObjectModelConnection(bareme);

        ContentValues values = new ContentValues();
        if(with_id) values.put(dbHelper.getColumnName(0) , bareme.getMileageAllowanceScaleId());
        values.put(dbHelper.getColumnName(1) , bareme.getStringId());
        values.put(dbHelper.getColumnName(2) , bareme.getMileageAllowanceYear());
        values.put(dbHelper.getColumnName(3), bareme.getVehicle().toString());
        values.put(dbHelper.getColumnName(4), bareme.getHorsePower());
        values.put(dbHelper.getColumnName(5), bareme.getMinDistance());
        values.put(dbHelper.getColumnName(6), bareme.getMaxDistance());
        values.put(dbHelper.getColumnName(7), bareme.getRate());
        values.put(dbHelper.getColumnName(8), bareme.getOffset());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;
    }

    /* Création d'un barème kilométrique dans la table  */
    @Override
    public Object createItem(Object object, boolean with_id)  throws Exception {
        MileageAllowanceScale bareme = (MileageAllowanceScale) object;
        this.open();
        ContentValues values = getContentValues(bareme,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) bareme.setMileageAllowanceScaleId(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();

        Log.i("MileageAllowanceScaleDB", "createItem OK");
        return bareme;
    }

    /* Mise à jour d'un barème kilométrique dans la table  */
    @Override
    public void updateItem(Object object) throws Exception {
        MileageAllowanceScale bareme = (MileageAllowanceScale) object;
        this.open();
        ContentValues values = getContentValues(bareme,false);
        BusinessExpensesApplication app = (BusinessExpensesApplication) context.getApplicationContext();
        if(app.isStandAlone() || bareme.getStringId().isEmpty())
            db.update(dbHelper.getTableName(),values, dbHelper.getColumnName(0) + " = " + bareme.getMileageAllowanceScaleId(), null);
        else {
            db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(1) + " = '" + bareme.getStringId() + "'", null);
            MileageAllowanceScale oldObject = (MileageAllowanceScale) MileageAllowanceScalesAdapter.getInstance(context).getItemWithStringId(bareme.getStringId());
            bareme.setMileageAllowanceScaleId(oldObject.getMileageAllowanceScaleId());
        }

        MileageAllowanceScalesAdapter.getInstance(context).remove(bareme, false);
        MileageAllowanceScalesAdapter.getInstance(context).add(bareme, false, false);

        this.close();
    }

    @Override
    protected Object cursorToObject(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        String stringId = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        int year = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        String vehicle = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        int hp = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(4)));
        int minDist = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(5)));
        int maxDist = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(6)));
        float rate = cursor.getFloat(cursor.getColumnIndex(dbHelper.getColumnName(7)));
        float offset = cursor.getFloat(cursor.getColumnIndex(dbHelper.getColumnName(8)));
        return new MileageAllowanceScale(id,stringId,year,vehicle,hp,minDist,maxDist,rate,offset);
    }


}
