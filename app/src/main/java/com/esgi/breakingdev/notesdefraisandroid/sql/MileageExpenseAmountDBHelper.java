package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 17/05/2016.
 */
public class MileageExpenseAmountDBHelper extends AbstractDBHelper {

    /* CONSTRUCTOR */
    public MileageExpenseAmountDBHelper(Context context) {
        super(context, "mileageExpenseAmount");
//        addColumn("simpleAmountID",ColumnType.INT_PRIMARY);
        addColumn("expenseLineID",ColumnType.INT_NOT_NULL);
        addColumn("expenseReportID",ColumnType.INT_NOT_NULL);
        addColumn("year",ColumnType.INT_NOT_NULL);
        addColumn("kilometers",ColumnType.REAL_NOT_NULL);
        addColumn("vehicle",ColumnType.TEXT_NOT_NULL);
        addColumn("horsePower",ColumnType.INT_NOT_NULL);
    }
}
