package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.model.AbstractAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageExpenseAmount;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

import java.text.ParseException;

/**
 * Created by Tibo Mathieu on 17/05/2016.
 */
public class MileageExpenseAmountDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static MileageExpenseAmountDBManager instance = null;

    /* CONSTRUCTOR */
    protected MileageExpenseAmountDBManager(Context context) throws Exception {
        super(context,new MileageExpenseAmountDBHelper(context));
    }

    /* SINGLETON */
    public static MileageExpenseAmountDBManager getInstance(Context context) throws Exception {
        if(instance == null) {
            instance = new MileageExpenseAmountDBManager(context.getApplicationContext());
            ExpenseLine.mileageExpenseList = instance.getDBItems();
        }
        return instance;
    }

    /* IMPLEMENTED METHODS */
    @Override
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        MileageExpenseAmount amount = (MileageExpenseAmount)object;
        checkObjectModelConnection(amount);

        ContentValues values = new ContentValues();

         //values.put(MySQLiteHelper.COLUMN_EXPENSE_REPORT_VALIDATOR, note.getValidatorId());
        if(with_id) values.put(dbHelper.getColumnName(0) , amount.getAmountID());
        values.put(dbHelper.getColumnName(1) , amount.getExpenseLineID());
        values.put(dbHelper.getColumnName(2) , amount.getExpenseReportID());
        values.put(dbHelper.getColumnName(3), amount.getYear());
        values.put(dbHelper.getColumnName(4), amount.getKilometers());
        values.put(dbHelper.getColumnName(5), amount.getVehicle().toString());
        values.put(dbHelper.getColumnName(6), amount.getHorsePower());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;
    }

    @Override
    public Object createItem(Object object, boolean with_id) throws Exception {
        MileageExpenseAmount amount = (MileageExpenseAmount)object;
        this.open();
        ContentValues values = getContentValues(amount,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) amount.setAmountID(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();
        ExpenseLine.mileageExpenseList.add(amount);
        Log.i("MileageExpenseAmountDB", "createItem OK");
        return amount;
    }

    @Override
    public void updateItem(Object object) throws Exception {
        MileageExpenseAmount amount = (MileageExpenseAmount)object;
        this.open();
        ContentValues values = getContentValues(amount, false);
        db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(0) + " = " + amount.getAmountID(), null);
        this.close();

        updateAmountList(amount);
        Log.i("MileageExpenseAmountDB", "updateItem OK");
    }

    private void updateAmountList(MileageExpenseAmount amount)
    {
        Log.d("MileageExpenseAmount","mileageExpenseList size = " + ExpenseLine.mileageExpenseList.size());
        int i = 0;
        for(Object o : ExpenseLine.mileageExpenseList ){
            AbstractAmount a = (AbstractAmount) o;
            if(a.getExpenseLineID() == amount.getExpenseLineID())
            {
                ExpenseLine.mileageExpenseList.remove(i);
                Log.d("MileageExpenseAmount", "mileageExpenseList size after removing = " + ExpenseLine.mileageExpenseList.size());
                break;
            }
            ++i;
        }
        ExpenseLine.mileageExpenseList.add(amount);
        Log.d("MileageExpenseAmount", "mileageExpenseList size after adding = " + ExpenseLine.mileageExpenseList.size());

    }

    @Override
    protected Object cursorToObject(Cursor cursor) throws ParseException {

        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        long lineId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        long reportId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        int year = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        float km = cursor.getFloat(cursor.getColumnIndex(dbHelper.getColumnName(4)));
        String vehicle = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(5)));
        int hp = cursor.getInt(cursor.getColumnIndex(dbHelper.getColumnName(6)));
        return new MileageExpenseAmount(id,lineId,reportId,year,km,vehicle,hp);
    }

    /* OVERRIDED METHODS */
    @Override
    public void deleteItem(long uid) {
        super.deleteItem(uid);
        try {
            ListDeleter.removeAmountOfList(uid, ExpenseLine.mileageExpenseList);
        }catch(Exception e){
            e.printStackTrace();
        }
        //        for(int i = 0; i<ExpenseLine.mileageExpenseList.size();++i){
//            Object o = ExpenseLine.mileageExpenseList.get(i);
//            if(o instanceof MileageExpenseAmount){
//                if(((MileageExpenseAmount) o).getAmountID() == uid) {
//                    ExpenseLine.mileageExpenseList.remove(i);
//                    break;
//                }
//            }
//        }
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
        ExpenseLine.mileageExpenseList.clear();
    }

    public void deleteMileageAmountsRelatedTo(long expenseReportId)
    {
        this.open();
        db.delete(dbHelper.getTableName(), dbHelper.getColumnName(2) + " = " + expenseReportId, null);
        this.close();
        ExpenseLine.deleteAmountWithReportIdInList(expenseReportId, ExpenseLine.mileageExpenseList);

    }
}
