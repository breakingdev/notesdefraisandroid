package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 16/05/2016.
 */
public class SimpleAmountDBHelper extends AbstractDBHelper {
    public SimpleAmountDBHelper(Context context) {
        super(context, "simpleAmount");
//        addColumn("simpleAmountID",ColumnType.INT_PRIMARY);
        addColumn("expenseLineID",ColumnType.INT_NOT_NULL);
        addColumn("expenseReportID",ColumnType.INT_NOT_NULL);
        addColumn("amountTTC", ColumnType.REAL_NOT_NULL);
        addColumn("amountTVA", ColumnType.REAL);
    }


}
