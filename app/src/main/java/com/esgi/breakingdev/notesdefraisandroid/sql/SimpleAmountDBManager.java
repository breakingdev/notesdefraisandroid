package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.SimpleAmount;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

import java.text.ParseException;

/**
 * Created by Tibo Mathieu on 16/05/2016.
 */
public class SimpleAmountDBManager extends AbstractDBManager {

    /* CLASS MEMBERS */
    private static SimpleAmountDBManager instance = null;

    /* CONSTRUCTOR */
    protected SimpleAmountDBManager(Context context)  throws Exception {
        super(context,new SimpleAmountDBHelper(context));
    }

    /* SINGLETON */
    public static SimpleAmountDBManager getInstance(Context context)  throws Exception
    {
        if(instance == null) {
            instance = new SimpleAmountDBManager(context.getApplicationContext());
            ExpenseLine.simpleAmountList = instance.getDBItems();
        }
        return instance;
    }

    /* IMPLEMENTED METHODS */

    @Override
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        SimpleAmount amount = (SimpleAmount) object;
        checkObjectModelConnection(amount);

        ContentValues values = new ContentValues();
        if(with_id) values.put(dbHelper.getColumnName(0) , amount.getAmountID());
        values.put(dbHelper.getColumnName(1), amount.getExpenseLineID());
        values.put(dbHelper.getColumnName(2), amount.getExpenseReportID());
        values.put(dbHelper.getColumnName(3), amount.getAmountTTC());
        values.put(dbHelper.getColumnName(4), amount.getAmountTVA());

        int sizeExpected = getFieldsToStoreInDatabase(object);
        if(!with_id) sizeExpected--;
        if(values.size() != sizeExpected) {
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;
    }

    @Override
    public Object createItem(Object object, boolean with_id) throws Exception {
        SimpleAmount amount = (SimpleAmount) object;
        this.open();
        ContentValues values = getContentValues(amount,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
        if(!with_id) amount.setAmountID(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();
        ExpenseLine.simpleAmountList.add(object); // Mise à jour de la liste des catégories de dépense dans les notes de frais

        Log.i("SimpleAmountDB", "createItem OK");
        return amount;
    }

    @Override
    public void updateItem(Object object) throws Exception {
        SimpleAmount amount = (SimpleAmount) object;
        this.open();
        ContentValues values = getContentValues(amount,false);
        db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(0) + " = " + amount.getAmountID(), null);
        this.close();
        ExpenseLine.updateAmountList( amount);
        Log.i("SimpleAmountDB", "updateItem OK");

    }

    @Override
    protected Object cursorToObject(Cursor cursor) throws ParseException {
        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        long lineId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        long reportId = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        float ttc = cursor.getFloat(cursor.getColumnIndex(dbHelper.getColumnName(3)));
        float tva = cursor.getFloat(cursor.getColumnIndex(dbHelper.getColumnName(4)));
        return new SimpleAmount(id, lineId,reportId,ttc,tva);
    }

    @Override
    public void deleteItem(long uid) {
        super.deleteItem(uid);
        try {
            ListDeleter.removeAmountOfList(uid, ExpenseLine.simpleAmountList);
        }catch(Exception e){
            e.printStackTrace();
        }

//        for(int i = 0; i<ExpenseLine.simpleAmountList.size();++i){
//            Object o = ExpenseLine.simpleAmountList.get(i);
//            if(o instanceof SimpleAmount){
//                if(((SimpleAmount) o).getAmountID() == uid) {
//                    ExpenseLine.simpleAmountList.remove(i);
//                    break;
//                }
//            }
//        }
    }

    @Override
    public void deleteTable() {
        super.deleteTable();
        ExpenseLine.simpleAmountList.clear();
    }

    public void deleteSimpleAmountsRelatedTo(long expenseReportId)
    {
        this.open();
        db.delete(dbHelper.getTableName(), dbHelper.getColumnName(2) + " = " + expenseReportId, null);
        this.close();
        ExpenseLine.deleteAmountWithReportIdInList(expenseReportId,ExpenseLine.simpleAmountList);
    }
}
