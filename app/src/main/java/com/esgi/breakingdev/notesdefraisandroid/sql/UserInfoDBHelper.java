package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.Context;

/**
 * Created by Tibo Mathieu on 01/07/2016.
 */
public class UserInfoDBHelper extends AbstractDBHelper {
    public UserInfoDBHelper(Context context) {
        super(context, "userInfos");
        addColumn("Key",ColumnType.TEXT_UNIQUE_NOT_NULL);
        addColumn("Value",ColumnType.TEXT_NOT_NULL);
    }
}
