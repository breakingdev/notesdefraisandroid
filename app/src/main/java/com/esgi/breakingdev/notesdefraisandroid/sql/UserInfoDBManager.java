package com.esgi.breakingdev.notesdefraisandroid.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.model.UserInfo;
import com.esgi.breakingdev.notesdefraisandroid.tools.ListDeleter;

import java.text.ParseException;

/**
 * Created by Tibo Mathieu on 01/07/2016.
 */
public class UserInfoDBManager extends AbstractDBManager {
    /* CLASS MEMBERS */
    private static UserInfoDBManager instance = null;

    /* CONSTRUCTOR */
    protected UserInfoDBManager(Context context)  throws Exception {
        super(context,new UserInfoDBHelper(context));
    }

    /* SINGLETON */
    public static UserInfoDBManager getInstance(Context context)  throws Exception
    {
        if(instance == null) {
            instance = new UserInfoDBManager(context.getApplicationContext());
        }
        return instance;
    }

    /* IMPLEMENTED METHODS */

    @Override
    protected ContentValues getContentValues(Object object, boolean with_id) throws Exception {
        UserInfo userInfo = (UserInfo) object;
        checkObjectModelConnection(userInfo);

        ContentValues values = new ContentValues();
//        if(with_id) values.put(dbHelper.getColumnName(0), userInfo.getId());
        values.put(dbHelper.getColumnName(1), userInfo.getKey());
        values.put(dbHelper.getColumnName(2), userInfo.getValue());

        int sizeExpected = getFieldsToStoreInDatabase(object);
//        if(!with_id) sizeExpected--;
        if(values.size() != (--sizeExpected)) { // champ id non utilisé
            throw new Exception("getContentValues: All dbmanagerable fields has not been gathered for Object " + object.getClass().getName());
        }
        return values;    }

    @Override
    public Object createItem(Object object, boolean with_id) throws Exception {
        UserInfo userInfo = (UserInfo) object;
        this.open();
        ContentValues values = getContentValues(userInfo,with_id);
        long uid = db.insert(dbHelper.getTableName(), null, values);
//        if(!with_id) userInfo.setServiceId(uid);   // Mise à jour de l'ID s'il s'agit d'un ajout simple et non d'une synchro
        this.close();
        UserInfo.listData.add(object); // Mise à jour de la liste des infos utilisateur

        Log.i("UserInfoDBManager", "createItem OK");
        return userInfo;
    }

    @Override
    public void updateItem(Object object) throws Exception {
        UserInfo userInfo = (UserInfo) object;
        this.open();
        ContentValues values = getContentValues(userInfo, true);
        db.update(dbHelper.getTableName(), values, dbHelper.getColumnName(1)+ " = '" + userInfo.getKey() + "'", null);
        this.close();
        ListDeleter.removeUserInfoEntryWithKey(userInfo.getKey(), UserInfo.listData);
        UserInfo.listData.add(userInfo);
    }

    @Override
    protected Object cursorToObject(Cursor cursor) throws ParseException {
//        long id = cursor.getLong(cursor.getColumnIndex(dbHelper.getColumnName(0)));
        String key = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(1)));
        String value = cursor.getString(cursor.getColumnIndex(dbHelper.getColumnName(2)));
        return new UserInfo(key,value);    }

    /* Effacement d'un ancien paramètre */
    public void deleteItem(String key) {
        this.open();
        db.delete(dbHelper.getTableName(), dbHelper.getColumnName(1) + " = '" + key + "'", null);
        this.close();
        ListDeleter.removeUserInfoEntryWithKey(key, UserInfo.listData);
    }
}
