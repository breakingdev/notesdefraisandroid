package com.esgi.breakingdev.notesdefraisandroid.synchro;

import android.content.Context;

import java.util.List;

/**
 * Created by Tibo Mathieu on 09/07/2016.
 */
public abstract class AbstractSynchroObject {
    protected Context appContext;

    protected AbstractSynchroObject(Context context){
        appContext = context;
    }

    public abstract void addNewElements(List<Object> list);
    public abstract void removeDeletedElements(List<Object> list);
    public abstract List<Object> getListFromJson(String json);
}
