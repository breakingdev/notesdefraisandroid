package com.esgi.breakingdev.notesdefraisandroid.synchro;

import android.content.Context;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCategoryDBManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 09/07/2016.
 */
public class BusinessCategorySynchro extends AbstractSynchroObject {
    public BusinessCategorySynchro(Context context) {
        super(context);
    }

    @Override
    public List<Object> getListFromJson(String jsonStr) {

        List<Object> list = new ArrayList<>();
        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
            return list;
        }
        for (int i = 0; i < jsonarray.length(); i++)
        {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonarray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
                continue;
            }
            list.add(new BusinessCategory(jsonObject,appContext));
        }
        return list;    }

    @Override
    public void addNewElements(List<Object> list) {
        if(list == null)
            throw new RuntimeException("BusinessCategorySynchro addNewElements() -> list is null !");
        BusinessCategoryAdapter adapter = null;
        try {
            adapter = BusinessCategoryAdapter.getInstance(appContext);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        for(Object o : list)
        {
            try {

                if(o instanceof BusinessCategory) {
                    if (adapter.getItemWithStringId(((BusinessCategory) o).getStringId()) == null) {

                        adapter.add(o, false, true);

                    } else  // update
                        BusinessCategoryDBManager.getInstance(appContext).updateItem(o);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void removeDeletedElements(List<Object> list) {
        if(list == null)
            throw new RuntimeException("BusinessCategorySynchro addNewElements() -> list is null !");
        BusinessCategoryAdapter adapter = null;
        try {
            adapter = BusinessCategoryAdapter.getInstance(appContext);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        for(Object o : new ArrayList<>(adapter.getDbListObjects()))
        {
            try {

                if(o instanceof BusinessCategory) {
                    boolean foundOnServer = false;
                    if(((BusinessCategory) o).getStringId().isEmpty()) continue;
                    for(Object objServer : list){
                        if(objServer instanceof BusinessCategory) {
                            if (((BusinessCategory) o).getStringId().equals(((BusinessCategory) objServer).getStringId())) {
                                foundOnServer = true;
                                break;
                            }
                        }
                    }
                    if(!foundOnServer) // note supprimée sur le serveur
                        adapter.remove(o, true);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
