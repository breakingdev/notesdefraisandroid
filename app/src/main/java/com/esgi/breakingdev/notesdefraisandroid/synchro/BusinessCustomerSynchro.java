package com.esgi.breakingdev.notesdefraisandroid.synchro;

import android.content.Context;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCustomerAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessCustomerDBManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 09/07/2016.
 */
public class BusinessCustomerSynchro extends AbstractSynchroObject {
    public BusinessCustomerSynchro(Context context) {
        super(context);
    }

    @Override
    public List<Object> getListFromJson(String jsonStr) {
        List<Object> list = new ArrayList<>();
        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
            return list;
        }
        for (int i = 0; i < jsonarray.length(); i++)
        {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonarray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
                continue;
            }
            list.add(new BusinessCustomer(jsonObject,appContext));
        }
        return list;
    }
    @Override
    public void addNewElements(List<Object> list) {
        if(list == null)
            throw new RuntimeException("BusinessCustomerSynchro addNewElements() -> list is null !");
        BusinessCustomerAdapter adapter = null;
        try {
            adapter = BusinessCustomerAdapter.getInstance(appContext);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        for(Object o : list)
        {
            try {

                if(o instanceof BusinessCustomer) {
                    if (adapter.getItemWithStringId(((BusinessCustomer) o).getStringId()) == null) {

                        adapter.add(o, false, true);

                    } else  // update
                        BusinessCustomerDBManager.getInstance(appContext).updateItem(o);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void removeDeletedElements(List<Object> list) {
        if(list == null)
            throw new RuntimeException("BusinessCustomerSynchro addNewElements() -> list is null !");
        BusinessCustomerAdapter adapter = null;
        try {
            adapter = BusinessCustomerAdapter.getInstance(appContext);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        for(Object o : new ArrayList<>(adapter.getDbListObjects()))
        {
            try {

                if(o instanceof BusinessCustomer) {
                    boolean foundOnServer = false;
                    if(((BusinessCustomer) o).getStringId().isEmpty()) continue;
                    for(Object objServer : list){
                        if(objServer instanceof BusinessCustomer) {
                            if (((BusinessCustomer) o).getStringId().equals(((BusinessCustomer) objServer).getStringId())) {
                                foundOnServer = true;
                                break;
                            }
                        }
                    }
                    if(!foundOnServer) // note supprimée sur le serveur
                        adapter.remove(o, true);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
