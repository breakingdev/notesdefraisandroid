package com.esgi.breakingdev.notesdefraisandroid.synchro;

import android.content.Context;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessProjectDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.BusinessServiceDBManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 09/07/2016.
 */
public class BusinessServiceSynchro extends AbstractSynchroObject {
    public BusinessServiceSynchro(Context context) {
        super(context);
    }

    @Override
    public List<Object> getListFromJson(String jsonStr) {
        List<Object> list = new ArrayList<>();
        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
            return list;
        }
        for (int i = 0; i < jsonarray.length(); i++)
        {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonarray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
                continue;
            }
            list.add(new BusinessService(jsonObject,appContext));
        }
        return list;
    }

    @Override
    public void addNewElements(List<Object> list) {
        if(list == null)
           throw new RuntimeException("BusinessServiceSynchro addNewElements() -> list is null !");
        BusinessServiceAdapter adapter = null;
        try {
            adapter = BusinessServiceAdapter.getInstance(appContext);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        for(Object o : list)
        {
            try {

                if(o instanceof BusinessService) {
                    if (adapter.getItemWithStringId(((BusinessService) o).getStringId()) == null) {

                        adapter.add(o, false, true);

                    } else  // update
                        BusinessServiceDBManager.getInstance(appContext).updateItem(o);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void removeDeletedElements(List<Object> list) {
        if(list == null)
            throw new RuntimeException("BusinessServiceSynchro removeDeletedElements() -> list is null !");
        BusinessServiceAdapter adapter = null;
        try {
            adapter = BusinessServiceAdapter.getInstance(appContext);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        for(Object o : new ArrayList<>(adapter.getDbListObjects()))
        {
            try {

                if(o instanceof BusinessService) {
                    boolean foundOnServer = false;
                    if(((BusinessService) o).getStringId().isEmpty()) continue;
                    for(Object objServer : list){
                        if(objServer instanceof BusinessService) {
                            if (((BusinessService) o).getStringId().equals(((BusinessService) objServer).getStringId())) {
                                foundOnServer = true;
                                break;
                            }
                        }
                    }
                    if(!foundOnServer) // note supprimée sur le serveur
                        adapter.remove(o, true);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
