package com.esgi.breakingdev.notesdefraisandroid.synchro;

import android.content.Context;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseLineDBManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 09/07/2016.
 */
public class ExpenseLineSynchro extends AbstractSynchroObject {
    public ExpenseLineSynchro(Context context) {
        super(context);
    }

    @Override
    public void addNewElements(List<Object> list) {
        if(list == null)
            throw new RuntimeException("ExpenseLineSynchro addNewElements() -> list is null !");
        ExpenseLineAdapter adapter = null;
        try {
            adapter = ExpenseLineAdapter.getInstance(appContext,0);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        long expenseReportId = 0;
        for(Object o : list)
        {
            try {

                if(o instanceof ExpenseLine) {
//                    Log.d("ExpenseLineSynchro", "addNewElements -> searching for lineId = " + ((ExpenseLine) o).getStringId());
                    if(expenseReportId != ((ExpenseLine) o).getExpenseReportId()){
                        expenseReportId = ((ExpenseLine) o).getExpenseReportId();
                        adapter = ExpenseLineAdapter.getInstance(appContext, expenseReportId);
                        adapter.refresh();
                    }
                    if (adapter.getItemWithStringId(((ExpenseLine) o).getStringId()) == null) {
//                        Log.d("ExpenseLineSynchro", "addNewElements -> adding lineId = " + ((ExpenseLine) o).getStringId());
                        adapter.add(o, false, true);

                    } else {  // update
//                        Log.d("ExpenseLineSynchro", "addNewElements -> updating lineId = " + ((ExpenseLine) o).getStringId());
                        ExpenseLineDBManager.getInstance(appContext).updateItem(o);

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Object> getListFromJson(String jsonStr) {
        List<Object> list = new ArrayList<>();
        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
            return list;
        }
        for (int i = 0; i < jsonarray.length(); i++)
        {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonarray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
                continue;
            }
            list.add(new ExpenseLine(jsonObject,appContext));
        }
        return list;
    }

    @Override
    public void removeDeletedElements(List<Object> list) {
    }
    public void removeDeletedElements(List<Object> list,String expenseReportStringId) {

        if(list == null)
            throw new RuntimeException("ExpenseLineSynchro addNewElements() -> list is null !");
        ExpenseLineAdapter adapter = null;
        try {
            adapter = ExpenseLineAdapter.getInstance(appContext,0);
            ExpenseReport expenseReport = (ExpenseReport) ExpenseReportAdapter.getInstance(appContext).getItemWithStringId(expenseReportStringId);
            adapter.setExpenseReportId(expenseReport.getExpenseReportId());
            adapter.refresh();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        for(Object o : new ArrayList<>(adapter.getDbListObjects()))
        {
            try {

                if(o instanceof ExpenseLine) {
                    boolean foundOnServer = false;
                    if(((ExpenseLine) o).getStringId().isEmpty()) continue;
                    if(!((ExpenseLine) o).getExpenseReportStringId().equals(expenseReportStringId)) continue;
//                    Log.d("ExpenseLineSynchro", "removeDeletedElements -> searching for lineId = " + ((ExpenseLine) o).getStringId());


                    for(Object objServer : list){
                        if(objServer instanceof ExpenseLine) {
//                            Log.d("ExpenseLineSynchro", "appli line ID = " + ((ExpenseLine) o).getStringId());
//                            Log.d("ExpenseLineSynchro", "server line ID = " + ((ExpenseLine) objServer).getStringId());

                            if (((ExpenseLine) o).getStringId().equals(((ExpenseLine) objServer).getStringId())) {
//                                Log.d("ExpenseLineSynchro", "found appli Line Id in server : " + ((ExpenseLine) o).getStringId());
                                foundOnServer = true;
                                break;
                            }
                        }
                    }
                    if(!foundOnServer) // note supprimée sur le serveur
                    {
//                        Log.d("ExpenseLineSynchro", "remove appli Line Id  : " + ((ExpenseLine) o).getStringId());
                        adapter.remove(o, true);
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
