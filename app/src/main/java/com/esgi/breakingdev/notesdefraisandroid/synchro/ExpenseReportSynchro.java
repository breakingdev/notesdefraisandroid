package com.esgi.breakingdev.notesdefraisandroid.synchro;

import android.content.Context;

import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseReportDBManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 09/07/2016.
 */
public class ExpenseReportSynchro extends AbstractSynchroObject {
    public ExpenseReportSynchro(Context context) {
        super(context);
    }

    @Override
    public List<Object> getListFromJson(String jsonStr) {
        List<Object> list = new ArrayList<>();
        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
            return list;
        }
        for (int i = 0; i < jsonarray.length(); i++)
        {
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonarray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
                continue;
            }
            list.add(new ExpenseReport(jsonObject,appContext));
        }
        return list;
    }

    @Override
    public void addNewElements(List<Object> list) {
        if(list == null)
            throw new RuntimeException("ExpenseReportSynchro addNewElements() -> list is null !");
        ExpenseReportAdapter adapter = null;
        try {
            adapter = ExpenseReportAdapter.getInstance(appContext);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        for(Object o : list)
        {
            try {

                if(o instanceof ExpenseReport) {
                    if (adapter.getItemWithStringId(((ExpenseReport) o).getStringId()) == null) {

                        adapter.add(o, false, true);

                    } else  // update
                        ExpenseReportDBManager.getInstance(appContext).updateItem(o);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void removeDeletedElements(List<Object> list) {
        if(list == null)
            throw new RuntimeException("ExpenseReportSynchro addNewElements() -> list is null !");
        ExpenseReportAdapter adapter = null;
        try {
            adapter = ExpenseReportAdapter.getInstance(appContext);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        for(Object o : new ArrayList<>(adapter.getDbListObjects()))
        {
            try {

                if(o instanceof ExpenseReport) {
                    boolean foundOnServer = false;
                    if(((ExpenseReport) o).getStringId().isEmpty()) continue;
                    for(Object objServer : list){
                        if(objServer instanceof ExpenseReport) {
                            if (((ExpenseReport) o).getStringId().equals(((ExpenseReport) objServer).getStringId())) {
                                foundOnServer = true;
                                break;
                            }
                        }
                    }
                    if(!foundOnServer) // note supprimée sur le serveur
                        adapter.remove(o, true);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
