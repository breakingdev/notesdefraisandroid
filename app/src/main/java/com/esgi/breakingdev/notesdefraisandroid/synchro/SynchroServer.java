package com.esgi.breakingdev.notesdefraisandroid.synchro;

import android.content.Context;
import android.util.Log;

import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCategoryAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessCustomerAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessProjectAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.BusinessServiceAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseLineAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.ExpenseReportAdapter;
import com.esgi.breakingdev.notesdefraisandroid.adapter.MileageAllowanceScalesAdapter;
import com.esgi.breakingdev.notesdefraisandroid.application.BusinessExpensesApplication;
import com.esgi.breakingdev.notesdefraisandroid.model.EventInfo;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseReport;
import com.esgi.breakingdev.notesdefraisandroid.services.BusinessCategoryServices;
import com.esgi.breakingdev.notesdefraisandroid.services.BusinessCustomerServices;
import com.esgi.breakingdev.notesdefraisandroid.services.BusinessProjectServices;
import com.esgi.breakingdev.notesdefraisandroid.services.BusinessServiceServices;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseLineServices;
import com.esgi.breakingdev.notesdefraisandroid.services.ExpenseReportServices;
import com.esgi.breakingdev.notesdefraisandroid.services.MileageAllowanceScaleServices;
import com.esgi.breakingdev.notesdefraisandroid.sql.EventInfoDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseLineDBManager;
import com.esgi.breakingdev.notesdefraisandroid.sql.ExpenseReportDBManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 08/07/2016.
 * Classe permettant de synchroniser les actions de l'utilisateur avec le serveur
 * après certain un temps de déconnection
 */
public class SynchroServer {

    private static SynchroServer instance = null;
    private Context appContext;

    public Context getAppContext() {
        return appContext;
    }

    /* CLASS MEMBERS */
    private SynchroThread synchroThread;

    /* CONSTRUCTORS */
    private SynchroServer(Context appContext){
        synchroThread = new SynchroThread(this);
        this.appContext = appContext;
    }

    public void startThread(){
        synchroThread.start();
    }

    /* SINGLETON */
    public static SynchroServer getInstance(Context context)
    {
        if(instance == null) {
            instance = new SynchroServer(context);
        }
        return instance;
    }

    public boolean hasNewEvents()
    {
        return !EventInfo.listEvent.isEmpty();
    }

    public boolean isConnectedToServer(){
        BusinessExpensesApplication app  = (BusinessExpensesApplication) appContext.getApplicationContext();
        return app.isServerConnectionActive();
    }

    public void clearEvents()
    {
        try {
            EventInfoDBManager.getInstance(appContext).deleteTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean sendNonTransmittedEventsToServer()
    {
        if(!isConnectedToServer()) return false;
        for(Object o : EventInfo.listEvent)
        {
            if(o instanceof EventInfo)
            {
                if(((EventInfo) o).getAction().equals(EventInfo.Action.ADD)){
                    postObjectToServer(((EventInfo) o).getObjectType(), ((EventInfo) o).getObjectId());
                }
                else if(((EventInfo) o).getAction().equals(EventInfo.Action.EDIT)){
                    updateObjectOnServer(((EventInfo) o).getObjectType(), ((EventInfo) o).getObjectId());
                }
            }
        }
        clearEvents();
        return true;
    }

    public void postObjectToServer(EventInfo.ObjectType objectType, long objectId)
    {
        ExpenseReportAdapter expenseReportAdapter;
        ExpenseLineAdapter expenseLineAdapter;

        try {
            expenseReportAdapter = ExpenseReportAdapter.getInstance(appContext);
            expenseLineAdapter = ExpenseLineAdapter.getInstance(appContext, 0);
        } catch (Exception e) {            e.printStackTrace();
            return;
        }

        ExpenseReportServices expenseReportServices = new ExpenseReportServices();
        ExpenseLineServices expenseLineServices = new ExpenseLineServices();


        switch(objectType) {
            case ExpenseReport:
                ExpenseReport expenseReport;
                try {
                    expenseReport = (ExpenseReport) expenseReportAdapter.getItemWithId(objectId);
                    Log.d("SynchroServeur", "post Expense Report + " + expenseReport.getDescription());
                    String idNoteDeFrais = expenseReportServices.postObject(expenseReport.getJsonObject().toString());
                    Log.d("SynchroServeur", "id note de frais + " + idNoteDeFrais);
                    ExpenseReport newExpenseReport = new ExpenseReport(expenseReport); // Duplication de l'objet pour ne pas corrompre celui qui est dans l'adapter
                    newExpenseReport.setStringId(idNoteDeFrais);
                    ExpenseReportDBManager.getInstance(appContext).updateItem(newExpenseReport);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case ExpenseLine:
                ExpenseLine expenseLine;
                try {
                    expenseLine = (ExpenseLine) expenseLineAdapter.getItemWithId(objectId);
                    if(expenseLine == null)
                        throw new RuntimeException("expenseline is null !");
                    String expenseLineStringId = expenseLineServices.postObject(expenseLine.getJsonObject().toString());
                    ExpenseLine newExpenseLine = new ExpenseLine(expenseLine);
                    newExpenseLine.setStringId(expenseLineStringId);
                    ExpenseLineDBManager.getInstance(appContext).updateItem(newExpenseLine);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void updateObjectOnServer(EventInfo.ObjectType objectType, long objectId) {
        ExpenseReportAdapter expenseReportAdapter;
        ExpenseLineAdapter expenseLineAdapter;


        try {
            expenseReportAdapter = ExpenseReportAdapter.getInstance(appContext);
            expenseLineAdapter = ExpenseLineAdapter.getInstance(appContext, 0);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        ExpenseReportServices expenseReportServices = new ExpenseReportServices();
        ExpenseLineServices expenseLineServices = new ExpenseLineServices();

        switch (objectType) {
            case ExpenseReport:
                ExpenseReport expenseReport;
                expenseReport = (ExpenseReport) expenseReportAdapter.getItemWithId(objectId);
                expenseReportServices.editObject(expenseReport.getJsonObject().toString());
                break;
            case ExpenseLine:
                ExpenseLine expenseLine;
                try {
                    expenseLineAdapter.refresh();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                expenseLine = (ExpenseLine) expenseLineAdapter.getItemWithId(objectId);
                expenseLineServices.editObject(expenseLine.getJsonObject().toString());
                break;
        }
    }

    public void checkNewServerObjects() {
        BusinessExpensesApplication app = (BusinessExpensesApplication) appContext.getApplicationContext();
//        if(!app.isServerConfigOk() || !app.isServerConnectionActive())
        if(!app.isServerConfigOk())
            return;
        // L'ordre est important !
        checkAndAddNewServerElements(EventInfo.ObjectType.Service);
        checkAndAddNewServerElements(EventInfo.ObjectType.Customer);
        checkAndAddNewServerElements(EventInfo.ObjectType.Project);
        checkAndAddNewServerElements(EventInfo.ObjectType.Category);
        checkAndAddNewServerElements(EventInfo.ObjectType.MileageAllowanceScale);
        try {
            ExpenseReportAdapter.getInstance(appContext).refresh();
        } catch (Exception e) {
            e.printStackTrace();
        }
        checkAndAddNewServerElements(EventInfo.ObjectType.ExpenseReport);
        checkAndAddNewServerElements(EventInfo.ObjectType.ExpenseLine);
    }

    private void checkAndAddNewServerElements(EventInfo.ObjectType objectType) {
        ExpenseReportAdapter expenseReportAdapter;

        try {
            expenseReportAdapter = ExpenseReportAdapter.getInstance(appContext);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        List<Object> serverObjects = new ArrayList<>();
        String jsonString = "";


        switch (objectType){

            case ExpenseReport:
                ExpenseReportServices expenseReportServices = new ExpenseReportServices();
                ExpenseReportSynchro expenseReportSynchro = new ExpenseReportSynchro(appContext);

                jsonString = expenseReportServices.getListObject();
                serverObjects = expenseReportSynchro.getListFromJson(jsonString);
                expenseReportSynchro.addNewElements(serverObjects);
                expenseReportSynchro.removeDeletedElements(serverObjects);
                break;

            case ExpenseLine:
                ExpenseLineServices expenseLineServices = new ExpenseLineServices();
                ExpenseLineSynchro expenseLineSynchro = new ExpenseLineSynchro(appContext);
                List<Object> expenseReports = expenseReportAdapter.getDbListObjects();
                for(Object o : expenseReports)
                {
                    if(o instanceof ExpenseReport) {
                        jsonString = expenseLineServices.getExpenseLinesOf(((ExpenseReport) o).getStringId());
                        serverObjects  = expenseLineSynchro.getListFromJson(jsonString);
                        expenseLineSynchro.addNewElements(serverObjects);
                        expenseLineSynchro.removeDeletedElements(serverObjects, ((ExpenseReport) o).getStringId());
                    }
                }
                break;

            case Category:
                BusinessCategoryServices businessCategoryServices = new BusinessCategoryServices();
                BusinessCategorySynchro businessCategorySynchro = new BusinessCategorySynchro(appContext);

                jsonString = businessCategoryServices.getListObject();
                serverObjects = businessCategorySynchro.getListFromJson(jsonString);
                businessCategorySynchro.addNewElements(serverObjects);
                businessCategorySynchro.removeDeletedElements(serverObjects);
                break;

            case MileageAllowanceScale:
                MileageAllowanceScaleServices mileageAllowanceScaleServices = new MileageAllowanceScaleServices();
                MileageAllowanceScaleSynchro mileageAllowanceScaleSynchro = new MileageAllowanceScaleSynchro(appContext);

                jsonString = mileageAllowanceScaleServices.getListObject();
                serverObjects = mileageAllowanceScaleSynchro.getListFromJson(jsonString);
                mileageAllowanceScaleSynchro.addNewElements(serverObjects);
                mileageAllowanceScaleSynchro.removeDeletedElements(serverObjects);
                break;

            case Project:
                BusinessProjectServices businessProjectServices = new BusinessProjectServices();
                BusinessProjectSynchro businessProjectSynchro = new BusinessProjectSynchro(appContext);

                jsonString = businessProjectServices.getListObject();
                serverObjects = businessProjectSynchro.getListFromJson(jsonString);
                businessProjectSynchro.addNewElements(serverObjects);
                businessProjectSynchro.removeDeletedElements(serverObjects);
                break;

            case Customer:
                BusinessCustomerServices businessCustomerServices = new BusinessCustomerServices();
                BusinessCustomerSynchro businessCustomerSynchro = new BusinessCustomerSynchro(appContext);

                jsonString = businessCustomerServices.getListObject();
                serverObjects = businessCustomerSynchro.getListFromJson(jsonString);
                businessCustomerSynchro.addNewElements(serverObjects);
                businessCustomerSynchro.removeDeletedElements(serverObjects);
                break;

            case Service:
                BusinessServiceServices businessService = new BusinessServiceServices();
                BusinessServiceSynchro businessServiceSynchro = new BusinessServiceSynchro(appContext);

                jsonString = businessService.getListServices();
                serverObjects = businessServiceSynchro.getListFromJson(jsonString);
                businessServiceSynchro.addNewElements( businessServiceSynchro.getListFromJson(jsonString));
                businessServiceSynchro.removeDeletedElements(serverObjects);
                break;
        }
    }
}
