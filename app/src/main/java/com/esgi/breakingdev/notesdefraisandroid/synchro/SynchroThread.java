package com.esgi.breakingdev.notesdefraisandroid.synchro;

import com.esgi.breakingdev.notesdefraisandroid.model.EventInfo;

/**
 * Created by Tibo Mathieu on 08/07/2016.
 */
public class SynchroThread extends Thread{
    private SynchroServer synchroServer;

    public SynchroThread(SynchroServer synchroServer) {
        super();
        this.synchroServer = synchroServer;
    }

    @Override
    public void run() {
        synchroServer.checkNewServerObjects();
        while(true){
            if(!synchroServer.hasNewEvents() || !synchroServer.isConnectedToServer()) try {
                sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            else { // Effectuer la synchronisation ici
                synchroServer.sendNonTransmittedEventsToServer();
                synchroServer.checkNewServerObjects();
            }

        }
    }



}
