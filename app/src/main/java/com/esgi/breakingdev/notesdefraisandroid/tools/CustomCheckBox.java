package com.esgi.breakingdev.notesdefraisandroid.tools;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.esgi.breakingdev.notesdefraisandroid.activity.special.IActivityWithCustomerSelection;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 14/06/2016.
 */
public class CustomCheckBox {
    public static void setCheckBoxWithCustomerSpinner(final Activity activity, final IActivityWithCustomerSelection cus_select,int spinnerId, int checkboxId,int[] itemsToHide)
    {
        setCheckBoxWithCustomerSpinner(activity,cus_select,spinnerId,checkboxId,itemsToHide,0);
    }

    public static void setCheckBoxWithCustomerSpinner(final Activity activity, final IActivityWithCustomerSelection cus_select,int spinnerId, final int checkboxId,int[] itemsToHide,long customerId)
    {
        Spinner lstDropDownClients = (Spinner) activity.findViewById(spinnerId);
        List<String> list = new ArrayList<>();

        final List<Object> listClients = ExpenseLine.businessCustomerList;
        int selection_client = 0;
        int cpt = 0;
        for (Object o : listClients) {
            BusinessCustomer tmpBC = (BusinessCustomer) o;
            list.add(tmpBC.getDescription());
            if (customerId > 0 && customerId == tmpBC.getBusinessCustomerId()) selection_client = cpt;
            ++cpt;
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lstDropDownClients.setAdapter(dataAdapter);

        final View[] viewsToHide = new View[itemsToHide.length];
        int i=0;
        for(int id : itemsToHide){
            viewsToHide[i] = activity.findViewById(id);
            ++i;
        }
        final Spinner spinnerCustomer = (Spinner) activity.findViewById(spinnerId);
        spinnerCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cus_select.setSelectedCustomer((BusinessCustomer)ExpenseLine.businessCustomerList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        final CheckBox checkBoxInternal = (CheckBox)activity.findViewById(checkboxId);
        checkBoxInternal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked) {
                    cus_select.setInternalItem(true);
                    spinnerCustomer.setVisibility(View.INVISIBLE);
                    for(View v : viewsToHide)
                        v.setVisibility(View.INVISIBLE);
                } else {

                    if(ExpenseLine.businessCustomerList.isEmpty()){
//                        Log.d("CustomCheckBox", "setCheckBoxWithCustomerSpinner -> You must register a customer first !");
                        Toast.makeText(activity.getApplicationContext(), "Il faut d'abord enregistrer un client !", Toast.LENGTH_LONG).show();


                        checkBoxInternal.setChecked(true);
                        cus_select.setInternalItem(true);
                        spinnerCustomer.setVisibility(View.INVISIBLE);
                        for(View v : viewsToHide)
                            v.setVisibility(View.INVISIBLE);
                        return;
                    }
                    spinnerCustomer.setVisibility(View.VISIBLE);
                    for(View v : viewsToHide)
                        v.setVisibility(View.VISIBLE);
                    cus_select.setInternalItem(false);
                    Log.d("CustomCheckBox", "setCheckBoxWithCustomerSpinner -> onCheckedChanged -> cus_select.setSelectedCustomer !");
                    cus_select.setSelectedCustomer((BusinessCustomer)ExpenseLine.businessCustomerList.get(0));
                }

            }
        });
        checkBoxInternal.setChecked(false); // pour forcer le déclenchement de OnCheckedChangeListener
        checkBoxInternal.setChecked(true);

        if(customerId > 0) { // S'il s'agit d'une édition d'objet avec un client choisi (id non nul)
            checkBoxInternal.setChecked(false);
            cus_select.setSelectedCustomer((BusinessCustomer) ExpenseLine.businessCustomerList.get(selection_client));
            lstDropDownClients.setSelection(selection_client);
            cus_select.setInternalItem(false);
            spinnerCustomer.setVisibility(View.VISIBLE);
            for(View v : viewsToHide)
                v.setVisibility(View.VISIBLE);
        }
        else
            checkBoxInternal.setChecked(true);

    }
}
