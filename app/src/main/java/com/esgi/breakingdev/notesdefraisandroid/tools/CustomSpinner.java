package com.esgi.breakingdev.notesdefraisandroid.tools;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.esgi.breakingdev.notesdefraisandroid.R;
import com.esgi.breakingdev.notesdefraisandroid.activity.ViewExpenseLineActivity;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.ActivityWithCategorySelection;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.ActivityWithServiceSelection;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.IActivityWithCustomerSelection;
import com.esgi.breakingdev.notesdefraisandroid.activity.special.IActivityWithProjectSelection;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessProject;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessService;
import com.esgi.breakingdev.notesdefraisandroid.model.ExpenseLine;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tibo Mathieu on 01/06/2016.
 */
public class CustomSpinner {
    public static void setSpinnerForBusinessCategories(int spinnerUid,final ActivityWithCategorySelection activity,
                                                    final int[] gridTextCellId, final int[] gridEditCellId )
    {
        // LISTE DES CATEGORIES DE DEPENSES
        Spinner lstCategoriesDepenses = (Spinner) activity.findViewById(spinnerUid);
        List<String> list = new ArrayList<>();

        final List<Object> listCategories = ExpenseLine.businessCategoryList;

        for (Object o : listCategories) {
            BusinessCategory tmpBC = (BusinessCategory) o;
            list.add(tmpBC.getDescription());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lstCategoriesDepenses.setAdapter(dataAdapter);
        lstCategoriesDepenses.setSelection(0);

        final Context context = (Context) activity;


        lstCategoriesDepenses.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                activity.setCategorySelected((BusinessCategory) ExpenseLine.businessCategoryList.get(position));
                BusinessCategory bcEnCours = (BusinessCategory) listCategories.get(position);

                changeAmountFieldsAndBusinessProof(activity, bcEnCours.getFormType(), gridTextCellId, gridEditCellId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
                activity.setCategorySelected(null);
            }

        });
    }

    public static void changeAmountFieldsAndBusinessProof(Activity activity, BusinessCategory.FormType formType, int[] gridTextCellId, int[] gridEditCellId)
    {
        //Champs dynamiques en fonction de la categorie selectionnée
        //1
        TextView gridTextViewElement1 = (TextView) activity.findViewById(gridTextCellId[0]);
        EditText gridEditTextElement1 = (EditText) activity.findViewById(gridEditCellId[0]);
        //2
        TextView gridTextViewElement2 = (TextView) activity.findViewById(gridTextCellId[1]);
        EditText gridEditTextElement2 = (EditText) activity.findViewById(gridEditCellId[1]);


        LinearLayout linearLayoutBusinessProof = null;
        linearLayoutBusinessProof = (LinearLayout) activity.findViewById(R.id.linearLayoutBusinessProof);

        if ((formType == BusinessCategory.FormType.amount) || (formType == BusinessCategory.FormType.flateRate)) {

            gridTextViewElement1.setText("Montant TVA : ");
            gridEditTextElement1.setHint("Montant TVA");
            gridTextViewElement2.setText(BusinessCategory.getFormTypeString(formType, activity) + " : ");
            gridEditTextElement2.setHint("Montant TTC");

            linearLayoutBusinessProof.setVisibility(View.VISIBLE);

        } else if (formType == BusinessCategory.FormType.mileageExpense) {

            gridTextViewElement1.setText("Distance (km) : ");
            gridEditTextElement1.setHint("Distance");
            gridTextViewElement2.setText("Chevaux fiscaux : ");
            gridEditTextElement2.setHint("Chevaux fiscaux");

            linearLayoutBusinessProof.setVisibility(View.GONE);
        }

        if (formType == BusinessCategory.FormType.flateRate) {
            linearLayoutBusinessProof.setVisibility(View.GONE);
        }
    }


    private static void fillSpinnerProjects(Activity activity,final IActivityWithProjectSelection proj_select, long customerId,int projectSpinId)
    {
        final Spinner lstProjects = (Spinner) activity.findViewById(projectSpinId);

        List<String> list = new ArrayList<>();
        list.add(activity.getString(R.string.select_no_projet)); // 1ère sélection: Pas de projet (Par défault)

        List<Object> listProjectsOfCustomer = BusinessProject.getProjectsOfCustomer(customerId);
        proj_select.setProjectsOfCustomer(listProjectsOfCustomer);
        for (Object o : listProjectsOfCustomer) {
            BusinessProject project = (BusinessProject) o;
            list.add(project.getDescription());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lstProjects.setAdapter(dataAdapter);
//        lstProjects.setSelection(0);
    }

    public static void setProjectSpinnerAccordingToCustomerSelected(Activity activity,final IActivityWithProjectSelection proj_select,final IActivityWithCustomerSelection cus_select,
                                                                    int customerSpinId, int projectSpinId)
    {
        setProjectSpinnerAccordingToCustomerSelected(activity, proj_select, cus_select, customerSpinId, projectSpinId, 0, 0);
    }

    public static void setProjectSpinnerAccordingToCustomerSelected(final Activity activity,final IActivityWithProjectSelection proj_select,final IActivityWithCustomerSelection cus_select,
                                                                    int customerSpinId,final int projectSpinId,long customerId,long projectId)
    {
        final Context context = activity;

        final Spinner lstProjects = (Spinner) activity.findViewById(projectSpinId);
        final Spinner lstCustomers = (Spinner) activity.findViewById(customerSpinId);
        final AdapterView.OnItemSelectedListener fillProjectsListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BusinessCustomer customerSelected =  (BusinessCustomer) ExpenseLine.businessCustomerList.get(position);
                cus_select.setSelectedCustomer(customerSelected);
                BusinessCustomer actualCustomer = (BusinessCustomer) ExpenseLine.businessCustomerList.get(position);
                fillSpinnerProjects(activity, proj_select, actualCustomer.getBusinessCustomerId(), projectSpinId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                cus_select.setSelectedCustomer(null);

            }
        };
        if(projectId > 0)
            lstCustomers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    lstCustomers.setOnItemSelectedListener(fillProjectsListener);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        else
            lstCustomers.setOnItemSelectedListener(fillProjectsListener);


        if(customerId > 0 && projectId > 0){
            List<Object> listCustomerProjects = BusinessProject.getProjectsOfCustomer(customerId);
            int selected_project = 0;
            int cpt = 0;
            for (Object o : listCustomerProjects) {
                BusinessProject project = (BusinessProject) o;
                if(project.getBusinessProjectId() == projectId)
                    selected_project = cpt;
                ++cpt;
            }
            fillSpinnerProjects(activity,proj_select,customerId,projectSpinId);
            lstProjects.setSelection(selected_project+1);
        }
    }

    public static void setRebillingSwitchAccorgindToProjectSelected(final Activity activity, final IActivityWithProjectSelection proj_select, int projectSpinId, final int rebillingSwitchId)
    {
        final AdapterView.OnItemSelectedListener switchRebillingListener =  new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Switch switchRebilling = (Switch) activity.findViewById(rebillingSwitchId);
                if(position == 0){
                    switchRebilling.setEnabled(false);
                    switchRebilling.setClickable(false);
                    switchRebilling.setChecked(false);
                    return; // Par défault sur aucun projet
                }
                List<Object> listCustomerProjects = proj_select.getCustomerProjects();
                BusinessProject project = (BusinessProject) listCustomerProjects.get(position-1);
                switchRebilling.setEnabled(true);
                switchRebilling.setClickable(true);
                switchRebilling.setChecked(project.getRefacturation());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        final Spinner lstProjects =(Spinner) activity.findViewById(projectSpinId);
        lstProjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                lstProjects.setOnItemSelectedListener(switchRebillingListener);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public static void setSpinnerForServices(final ActivityWithServiceSelection activity, int spinnerUid)
    {
        setSpinnerForServices(activity,spinnerUid,0);
    }
    public static void setSpinnerForServices(final ActivityWithServiceSelection activity, int spinnerUid,long serviceId)
    {
        Spinner servicesSpinner = (Spinner) activity.findViewById(spinnerUid);
        List<String> list = new ArrayList<>();

        final List<Object> listServices = BusinessService.businessServicesList;

        int cpt = 0;
        int selected_service = 0;
        for (Object o : listServices) {
            BusinessService tmp = (BusinessService) o;
            list.add(tmp.getDescription());
            if(serviceId > 0 && tmp.getServiceId() == serviceId)
                selected_service = cpt;
            ++cpt;
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        servicesSpinner.setAdapter(dataAdapter);
        servicesSpinner.setSelection(selected_service);

//        final Context context = (Context) activity;


        servicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                activity.setServiceSelected((BusinessService) BusinessService.businessServicesList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
}
