package com.esgi.breakingdev.notesdefraisandroid.tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Tibo Mathieu on 07/07/2016.
 */
public class JsonTool {

    public static boolean isJSONValid(String test) {
        if(test == null || test.isEmpty()) return false;
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}
