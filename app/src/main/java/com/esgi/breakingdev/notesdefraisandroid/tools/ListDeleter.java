package com.esgi.breakingdev.notesdefraisandroid.tools;

import com.esgi.breakingdev.notesdefraisandroid.model.AbstractAmount;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCategory;
import com.esgi.breakingdev.notesdefraisandroid.model.BusinessCustomer;
import com.esgi.breakingdev.notesdefraisandroid.model.MileageAllowanceScale;
import com.esgi.breakingdev.notesdefraisandroid.model.UserInfo;

import android.util.Log;

import java.util.List;

/**
 * Created by Tibo Mathieu on 31/05/2016.
 */
public class ListDeleter {



    public static boolean removeBusinessCategoryOfList(long businessCategoryId, List<Object> list) throws Exception {
        boolean removed = false;
        int size = list.size();
        for(int i = 0; i<size;++i) {
            Object o = list.get(i);
            if (o instanceof BusinessCategory) {
                if (((BusinessCategory) o).getBusinessCategoryId() == businessCategoryId) {
                    list.remove(i);
                    i--; size--;
                    removed = true;
                }

            } else
                throw new Exception("Removing object BusinessCategory of list with not the same type ! ");
        }
        return removed;
    }
    public static boolean removeMileageAllowanceScaleOfList(long mileageAllowanceScaleId, List<Object> list) throws Exception {
        boolean removed = false;
        int size = list.size();
        for(int i = 0; i<size;++i) {
            Object o = list.get(i);
            if (o instanceof MileageAllowanceScale) {
                if (((MileageAllowanceScale) o).getMileageAllowanceScaleId() == mileageAllowanceScaleId) {
                    list.remove(i);
                    i--; size--;
                    removed = true;
                }

            } else
                throw new Exception("Removing object MileageAllowanceScale of list with not the same type ! ");
        }
        return removed;
    }

    public static boolean removeBusinessCustomerOfList(long businessCustomerId, List<Object> list) throws Exception {
        boolean removed = false;
        int size = list.size();
        for(int i = 0; i<size;++i) {
            Object o = list.get(i);
            if (o instanceof BusinessCustomer) {
                if (((BusinessCustomer) o).getBusinessCustomerId() == businessCustomerId) {
                    list.remove(i);
                    i--; size--;
                    removed = true;
                }

            } else
                throw new Exception("Removing object businessCustomerId of list with not the same type ! ");
        }
        return removed;
    }

    public static boolean removeAmountOfList(long amountId, List<Object> list) throws Exception {
        boolean removed = false;
        int size = list.size();
        for(int i = 0; i<size;++i) {
            Object o = list.get(i);
            if (o instanceof AbstractAmount) {
               if(((AbstractAmount) o).getAmountID() == amountId){
                    list.remove(i);
                    i--; size--;
                    removed = true;
                   Log.d("ListDeleter","removeAmountOfList -> object removed !");
                }

            } else
                throw new Exception("Removing object businessCustomerId of list with not the same type ! ");
        }
        if(!removed)
            Log.d("ListDeleter","removeAmountOfList -> object NOT removed !");
        return removed;
    }


    public static void removeUserInfoEntryWithKey(String key, List<Object> listData)
    {
        int size = listData.size();
        for(int i = 0; i<size;++i) {
            UserInfo userInfos = (UserInfo) listData.get(i);
            if(userInfos.getKey().equals(key))
            {
                listData.remove(i);
                i--;
                size--;
            }
        }
    }

}
