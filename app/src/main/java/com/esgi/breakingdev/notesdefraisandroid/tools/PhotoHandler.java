package com.esgi.breakingdev.notesdefraisandroid.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Tibo Mathieu on 06/07/2016.
 */
public class PhotoHandler {

    public static Uri writeBitmapToFile(Context context,Bitmap bitmap,String filename)
    {
        Uri bitmapUri = null;
        //create a file to write bitmap data
        File f = new File(context.getCacheDir(), filename);
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Convert bitmap to byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return bitmapUri;
        }
        try {
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            return bitmapUri;
        }
        return Uri.fromFile(f);
    }

    public static Bitmap getBitmapFromUri(Uri photoUri)
    {
        Bitmap bitmap = null;
        File file = new File(photoUri.getPath());
        if(file.exists())
            Log.d("onActivityResult", "File length = " + file.length());
        else{
            Log.e("onActivityResult", "file Not found at " + file.getPath());
            return null;
        }
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }

    public static Bitmap compressBitmap(Bitmap bitmap,int quality){
        int factor = 1;
        if(quality > 100)
            factor = 100;
        else if(quality > 0)
            factor = quality;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream);
        bitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.toByteArray().length);
        return bitmap;
    }

    public static void deleteFileFromUri(Uri photoUri)
    {
        File file = new File(photoUri.getPath());
        if(file != null && !file.delete())
            Log.d("PhotoHandler", "deleteFileFromUri -> Failed to delete file " + file.getPath());
    }

}
