package com.esgi.breakingdev.notesdefraisandroid.tools;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Tibo Mathieu on 09/07/2016.
 */
public class QRCodeFileReader {
    public static String FILE_NAME = "qrcode_infos.txt";
    private static QRCodeFileReader instance = null;
    private Context appContext;
    private String urlServer = "";
    private String token = "";

    private QRCodeFileReader(Context appContext){ this.appContext = appContext;}
    public static QRCodeFileReader getInstance(Context appContext){
        if(instance == null) {
            instance = new QRCodeFileReader(appContext);
        }
        return instance;
    }

    public boolean readInfo(){
        try {
            InputStream is = null;
            try {
                is = appContext.getAssets().open(FILE_NAME);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            String str = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            if (is != null) {
                int lineNumber = 0;
                while ((str = reader.readLine()) != null && lineNumber < 2) {
                    if(lineNumber == 0)
                        urlServer = str;
                    else
                        token = str;
                    ++lineNumber;
                }
                if(lineNumber != 2) return false;
            }
            is.close();

        } catch (IOException e) {
            // Should never happen!
            throw new RuntimeException(e);
        }
        return true;
    }

    /*  GETTERS AND SETTERS */
    public String getUrlServer() {
        return urlServer;
    }

    public String getToken() {
        return token;
    }
}
